---
title: Matrix e Telegram
description: 
published: true
date: 2025-03-01T17:56:04.439Z
tags: 
editor: markdown
dateCreated: 2025-02-26T19:16:26.526Z
---

# Grupos brasileiros no Matrix e no Telegram

Ajuda e suporte sobre questões técnicas
- [debianbrasil](https://t.me/debianbrasil)

Colaborar na tradução do Debian
- [debl10nptBR](https://t.me/debl10nptBR)
- [Sala no Matrix com ponte para o telegram](https://matrix.to/#/#debian-l10n-br:matrix.debian.social)

Colaborar com design
- [debiandesign](https://t.me/debiandesign)

Organizadores do Debian Day em cidades brasileiras
- [debian_day_br](https://t.me/debian_day_br)

Notícias sobre o Debian:
- [DebianBrasilNoticias](https://t.me/DebianBrasilNoticias)

Comunidade de Curitiba
- [debiancwb](https://t.me/debiancwb)

Comunidade de São Paulo
- [DebianSaoPaulo](https://t.me/DebianSaoPaulo)

Comunidade de Brasília
- [debianbrasilia](https://t.me/debianbrasilia)

Comunidade do Rio Grande do Sul
- [debianrs](https://t.me/debianrs)

Comunidade de Minas Gerais
- [debianmg](https://t.me/debianmg)
- [Sala no Matrix com ponte para o telegram](https://matrix.to/#/#debian-mg:matrix.debian.social)

Comunidade do Nordeste
- [debian_nordeste](https://t.me/debian_nordeste)
- [Sala no Matrix com ponte para o telegram](https://matrix.to/#/#debian-nordeste:matrix.debian.social)

Comunidade do Rio de Janeiro
- [debianrj](https://t.me/debianrj)