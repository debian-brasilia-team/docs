---
title: Sites Úteis
description: 
published: true
date: 2025-03-01T17:56:13.563Z
tags: 
editor: markdown
dateCreated: 2024-04-14T17:21:47.267Z
---

## Code Search

- https://codesearch.debian.net

O site CodeSearch é uma ferramenta valiosa para desenvolvedores, mantenedores de pacotes e entusiastas do sistema operacional Debian. Ele oferece uma interface de busca que permite aos usuários explorar o vasto repositório de código-fonte dos pacotes Debian.

Com essa ferramenta, os desenvolvedores podem buscar por qualquer termo específico dentro do código-fonte de pacotes Debian, facilitando a localização de funções, variáveis, e outras referências relevantes.

Isso é particularmente útil para entender como determinadas funcionalidades são implementadas, depurar problemas, ou até mesmo contribuir para o desenvolvimento de pacotes.

Em resumo, o CodeSearch Debian serve como um recurso indispensável para a comunidade de desenvolvedores que trabalham com o Debian, tornando mais eficiente a exploração e compreensão do código-fonte dos pacotes disponíveis.

<br/>

## Tracker

- https://tracker.debian.org


O site Tracker é uma ferramenta essencial para acompanhar o estado e o desenvolvimento dos pacotes Debian. Ele oferece uma interface amigável que permite aos usuários monitorar informações detalhadas sobre cada pacote, incluindo metadados, histórico de versões, bugs relatados e corrigidos, além de outras informações relevantes.

<br/>

## WNPP

- https://wnpp.debian.net

O site **WNPP**, significa `Work-Needing and Prospective Packages`, pode ser traduzido para o português como `Pacotes que Precisam de Trabalho e Pacotes Prospectivos`. Ele oferece uma visão abrangente dos pacotes que precisam de trabalho (work-needing) e dos pacotes que estão em fase prospectiva (prospective) para inclusão no repositório Debian.

Os pacotes listados como "work-needing" geralmente requerem atenção adicional, como correções de bugs, atualizações para novas versões ou até mesmo novos mantenedores para assumir sua manutenção. Isso é crucial para manter a qualidade e a estabilidade do ecossistema de pacotes Debian.

Por outro lado, os pacotes "prospective" representam propostas de novos pacotes que estão sendo considerados para inclusão no repositório Debian. Esses pacotes podem ser sugeridos por membros da comunidade ou desenvolvedores interessados em contribuir com novos softwares para o Debian.

Em resumo, o WNPP Debian é uma ferramenta valiosa para coordenar esforços de desenvolvimento, priorizar tarefas e garantir que o repositório Debian continue a crescer e se manter relevante no cenário de software livre e de código aberto.

<br/>

## UDD


O site **Ultimate Debian Database (UDD)** integra e armazena informações de diversas fontes, como repositórios de pacotes, bugs, dados de QA (Quality Assurance), e atividade dos desenvolvedores, permitindo consultas e agregando dados para a manutenção e melhoria contínua do projeto Debian.

- https://udd.debian.net

<br/>

## ftp-master

[ftp-master.debian.org](https://ftp-master.debian.org) é o serviço central e site responsável pela manutenção e gerenciamento do archive de software do Debian.

1. **Gerenciamento do Arquivo Debian**: O site está envolvido no gerenciamento dos repositórios oficiais de software do Debian. Isso inclui o processamento de novos pacotes enviados pelos desenvolvedores do Debian, garantindo que eles atendam às diretrizes de qualidade e políticas do Debian, e então os disponibilizando para os usuários.

2. **Uploads e Processamento de Software**: Os desenvolvedores enviam seus pacotes para o ftp-master, onde eles são automaticamente verificados quanto a várias violações de políticas, correção e problemas de segurança. Esta é uma etapa crucial antes que os pacotes sejam incluídos nos repositórios oficiais do Debian.

3. **Distribuição de Pacotes**: Após o processamento e aceitação dos pacotes, eles são distribuídos para os vários espelhos (mirrors) ao redor do mundo, que os usuários finais podem acessar por meio das ferramentas de gerenciamento de pacotes do Debian.

4. **Segurança e Integridade**: O serviço também desempenha um papel fundamental na manutenção da segurança e integridade do arquivo do Debian, verificando assinaturas, lidando com chaves criptográficas (GPG) e garantindo que os pacotes não tenham sido adulterados.

A página https://ftp-master.debian.org/dm.txt pode ser utilizada para verificar quais pacotes Debian Maintainers (DM) tem permissão de upload.

<br/>

## Guia para novos mantenedores

- https://www.debian.org/doc/manuals/maint-guide

<br/>

## Wiki oficial do projeto Debian

- https://wiki.debian.org

<br/>

## Wiki de alguns times

- Python: <https://wiki.debian.org/Python/GitPackaging>
- Ruby: <https://wiki.debian.org/Teams/Ruby/Packaging>
- Golang: <https://go-team.pages.debian.net>