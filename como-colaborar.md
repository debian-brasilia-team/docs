---
title: Como colaborar
description: 
published: true
date: 2025-03-01T17:55:41.325Z
tags: 
editor: markdown
dateCreated: 2025-02-27T13:58:57.596Z
---

# Como Colaborar com o Projeto Debian

Você quer começar a colaborar com o Debian? Veja abaixo onde seu perfil mais se encaixa. Se você já é um(a) colaborador(a) e seu método ou grupo de colaboração não está listado nessa página, por favor compartilhe conosco!

Usar, testar, e enviar bugs para o [Sistema de Rastreamento de Bugs (BTS - Bug
Tracking System)](http://bugs.debian.org).

Enviar patches com correções para bugs.

Ajudar outros(as) usuários(as) em:

- [Listas de discussão](/listas)
- [Canais de IRC](/irc)
- [Grupos no Matrix e Telegram](matrix-telegram)

Traduzir. Veja como começar [aqui](https://wiki.debian.org/Brasil/Traduzir):

- Páginas web do site oficial
- Descrições de pacotes
- Manpages
- Documentos

Divulgar o Debian nesses locais abaixo e em vários outros. Veja alguns materiais gráfico [aqui](https://debian.pages.debian.net/debian-flyers)

- Sites
- Escolas
- Universidades/Faculdades
- Sindicatos
- Eventos como congressos, fóruns, encontros, etc.

Escrever tutoriais básicos ou avançados, para usuários(as) ou desenvolvedores(as). A [wiki do Debian](https://wiki.debian.org/Brasil/Documentos) é um bom lugar para publicá-los.

Fazer testes de instalação e [enviar relatórios](https://d-i.debian.org/manual/en.i386/ch05s04.html#submit-bug).

Manter pacotes de software no Debian. Leia este [FAQ](https://eriberto.pro.br/wiki/index.php?title=FAQ_empacotamento_Debian) e o [Empacotamento 101](/empacotamento-101). Veja alguns pacotes que você pode contribuir:

- [Pedidos de pacotes novos](https://www.debian.org/devel/wnpp/requested)
- [Pacotes órfãos](https://www.debian.org/devel/wnpp/orphaned.pt.html)
- [Pacotes mantidos por equipes](https://wiki.debian.org/Teams#Packaging_teams)

Desenvolver atividades de [publicidade e imprensa](https://wiki.debian.org/Teams/Publicity), e [escrever notícias](https://micronews.debian.org/pages/contribute.html).

Organizar [eventos e encontros](https://wiki.debianbrasil.org.br/eventos) focados em Debian na sua cidade.