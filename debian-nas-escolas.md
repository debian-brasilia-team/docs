---
title: Debian nas escolas
description: 
published: true
date: 2025-03-10T23:32:16.077Z
tags: 
editor: markdown
dateCreated: 2025-03-10T23:30:41.589Z
---

# Projeto Debian nas escolas

[pt_BR] Bem-vindo/a ao projeto da comunidade brasileira de divulgação do Debian GNU/Linux nas escolas de Ensino Médio.

[es] Bienvenido/a al proyecto de la comunidad brasileña para promover Debian GNU/Linux en las escuelas secundarias.

[en] Welcome to the Brazilian community project to promote Debian GNU/Linux in high schools.

Esta é a primeira formulação do projeto, estamos em um momento de organização. O canal para comunicações é o [grupo DebianEduBr no Telegram](https://t.me/DebianEduBR).

## Objetivo principal

Divulgar o projeto Debian GNU/Linux nas escolas de Ensino Médio através de apresentações curtas e de cunho introdutório.

## Objetivos secundários

- Apresentar aos/às estudantes o movimento Software Livre e possibilidades de participação.
- Promover a atuação de contribuidores/as Debian em colaboração com desenvolvedores/as Debian.
- Criar um espaço e mecanismos para interação e troca de informações entre participantes.

## Público-alvo

Estudantes matriculados no Ensino Médio, regular ou profissional, com idades entre 14 e 16 anos.

## Justificativa

Acompanhando as diversas iniciativas de divulgação do projeto Debian GNU/Linux pela comunidade brasileira, notou-se que ainda **poucas voltavam-se especificamente para a Educação Básica**. Uma ação pontual nesse
sentido foi a [apresentação da professora Virgínia Fernandes](https://www.youtube.com/watch?v=D-S5WuMW8Fc), da Rede Pública de Ensino Fundamental do Distrito Federal, na MiniDebConf Brasil Online 2020.

Outra intervenção foi a [live DebianEduBR - Software Livre e Educação](https://www.youtube.com/watch?v=cSMrbOQjjdY), com os professores Ramon Mulin e Fred Guimarães em 2021. Atualmente, o canal mais atuante de divulgação com foco em Educação Básica, e que parte da própria comunidade Debian, é o [o blog do professor de História Mulin](https://mulin.codeberg.page), que também realiza encontros presenciais nas escolas em que trabalha.

Não há dúvidas de que os sistemas escolares são campos importantes para divulgação do projeto Debian. Reforça essa constatação o fato de que em março de 2022 entra em vigor **a nova legislação do Novo Ensino Médio**, apoiada pela reformulação da [Base Nacional Comum Curricular](http://basenacionalcomum.mec.gov.br). Em resumo, isto implica que as escolas terão que orientar seus currículos, entre outras coisas, para estudos obrigatórios sobre cultura digital, pensamento computacional e reflexões críticas sobre tecnologias.


## Como a comunidade Debian entra nisso?

A importância do projeto Debian GNU/Linux na história do movimento Software Livre já é uma forte razão para que a comunidade aproveite esta oportunidade para ingressar com mais fôlego com divulgação nas escolas.
Além disso, já temos professores/as na própria comunidade realizando este trabalho. Esta iniciativa, portanto, busca **convergir esforços e oportunidades** e assim potencializar o alcance e a qualidade da disseminação da comunidade Debian nas escolas de Ensino Médio.


## Alguns esclarecimentos

A construção deste grupo não significa criar um centro de difusão, com regras ou conteúdos pré-determinados. Pelo contrário, as ações continuarão a ser **individuais**, como ocorre no trabalho distribuído em projetos de software livre. Trata-se, portanto, de estruturar um espaço para que as atuações individuais possam dialogar, buscar e fornecer ideias, disponibilizar materiais para utilização e remixagem, e mesmo organizar ações conjuntas ou eventos de maior porte.

A organização também ajudará na **legitimação da iniciativa no contato com as escolas**. Página na Internet, portfólios, identificação de participantes, *feedbacks* de estudantes e diretores/as, são formas de validar para a comunidade escolar que esta iniciativa têm critérios sólidos e origem conhecida.

Importante indicar, principalmente para quem não tem proximidade com o contexto escolar, que as escolas são um ambiente complexo, com regras e culturas próprias. Isto significa que as apresentações devem se **articular com a vida dos estudantes, com as demandas de professores, com a burocracia curricular**, e aí por diante. Nesse sentido, os/as educadores/as do grupo poderão auxiliar ao esclarecer dúvidas e direcionar as iniciativas com foco na realidade escolar. Em outro sentido, participantes mais próximas da área de tecnologias poderão esclarecer dúvidas e direcionar as iniciativas com foco numa compreensão qualificada das tecnologias.

Por fim, a escolha por apresentações curtas e pontuais nas escolas é apenas um **primeiro e viável passo** para as ações do grupo, que podem ser postas em prática rapidamente. Contudo, não é necessário ficar preso às apresentações, outras possibilidades podem e devem ser pensadas.

## O que precisamos fazer daqui por diante?

Mais imediatamente:

- Aprovar coletivamente, com modificações se preciso, o traçado do projeto.
- Escolher nome e logotipo do projeto.
- Elaborar proposta para escolas.
- Definir uma plataforma on-line e estabelecer referências essenciais (textos, vídeos).

Em curto e médio prazo:

- Propor articulações com outras organizações (software livre, DebianEdu, [[LocalGroups|grupos locais]], grupos de pesquisa).

## Registro de encontros e comunicações

- 12-03-2022:: Chamada no Telegram para interessados/as.
- 15-03-2022:: Videochamada para apresentação do esboço do projeto. Participantes: Thiago (SP), Everlon (MG) e Guilherme (BA).

## Repositório colaborativo

Os recursos estão sendo armazenados [neste repositório no Salsa](https://salsa.debian.org/tico/debian-nas-escolas), o qual possibilita o controle de alterações. Caso não sinta-se confortável em usar o git, entre em contato para que possamos intermediar o envio das contribuições.
