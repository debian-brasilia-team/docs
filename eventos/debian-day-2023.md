---
title: Debian Day 2023
description: 
published: true
date: 2025-03-01T17:43:03.158Z
tags: eventos, debian-day
editor: markdown
dateCreated: 2023-06-05T01:45:36.981Z
---

# Debian Day 2023

No dia 16 de agosto de 2023, o Projeto Debian estará completando **30 anos** de idade! O Debian Day é um dia dedicado a comemoração do aniversário do maior projeto de software livre do mundo!

Neste ano, comemoraremos o aniversário do Projeto Debian na **Faculdade do Gama (FGA) da Universidade de Brasília**. Levando em conta o calendário acadêmico da universidade, o segundo semestre de 2023 se iniciará dia 25 de agosto, então comemoraremos na primeira semana do semestre, entre os dias 28 de agosto e 1 de setembro.

## Formato do evento

Teremos um dia inteiro de atividades relacionadas ao Projeto Debian. Durante a manhã, teremos palestras apresentando conceitos de software livre e o Projeto Debian. Usaremos o período da tarde para atividades `hands-on` de contribuição para o projeto (preferencialmente relacionado a empacotamento de software). Após um dia inteiro de compartilhamento de conhecimento, iremos ter um `Happy Hour` para socialização dos participantes :).

## Voluntários

Para realizarmos o Debian Day 2023 com sucesso precisaremos de voluntários para as seguintes tarefas:

* **Reserva de espaço junto com funcionários da FGA/UnB**. Provavelmente, o nosso ponto de contato será a Prof. Carla Rocha ou o Prof. Bruno Ribas.
* **Elaboração de material gráfico do evento para divulgação**. Criação de folhetos, banners para redes sociais, etc, com o intuito de divulgar o evento para interessados.
* **Divulgação do evento**. Compartilhar o material gráfico elaborado em redes sociais (grupos acadêmicos, de software livre, de profissionais de TI), e pregar folhetos em murais da universidade.

## Resumo

* Local: **Faculdade do Gama - Universidade de Brasília (FGA/UnB)**
* Data: **Entre 28 de agosto e 1 de setembro**
* Formato: **Palestras + atividades de contribuição**
* Contato: **Canal do Debian Brasília**
  * IRC: `#debian-bsb` no servidor `OFTC`
  * Matrix: https://matrix.to/#/#debian-bsb:matrix.debian.social
  * Telegram: t.me/debianbrasilia