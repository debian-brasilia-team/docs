---
title: Processo de contribuição
description: Processo utilizado pelo Debian Brasilia para contribuicao com pacotes Debian
published: true
date: 2025-03-01T17:56:08.937Z
tags: pacotes, contribuicao
editor: markdown
dateCreated: 2023-06-03T23:52:46.891Z
---

Está interessado em contribuir com pacotes Debian? Encontrar um sponsor para revisar o seu trabalho? Siga os passos a seguir para finalmente fazer o seu pacote Debian entrar para o repositório oficial.

<br/>

## Selecione um pacote

Existem algumas formas para escolher um pacote para trabalhar:

* Pacotes de algum time: https://tracker.debian.org/teams/
* Pacotes órfãos: https://www.debian.org/devel/wnpp/orphaned.pt.html
* Pacotes de interesse pessoal (software ou linguagem especifica)
* Usando `how-can-i-help` para encontrar pacotes instalados na sua maquina onde mantenedores procuram ajuda. Mais informações: https://wiki.debian.org/how-can-i-help

<br/>

## Permissão para manipulação de issue no salsa

Como iremos trabalhar com issues no nosso repo `doc` no salsa, você precisa ter permissão para manipular alguns campos como `Label` e `Assignee`. Para isso, você precisa ser convidado a ser membro do repo `doc` como `developer`, caso você não tenha essa permissão, peça para que alguém faça isso no canal do Debian Brasil.

<br/>

## Crie uma issue no salsa

Assim que selecionar o pacote em que irá trabalhar, crie uma issue no nosso repositório `docs`:

https://salsa.debian.org/debian-brasil-team/docs/-/issues/new

Se possível, tente usar a língua inglesa; dessa forma praticamos a língua para interações com o restante da comunidade. Preencha os campos da seguinte forma:

* **Título**: `Package <nome_do_pacote>`
* **Tipo**: `Issue`
* **Descrição**: Tente adicionar o máximo de informações para contextualizar a revisão que será feita. Por exemplo:
	* Link para a página do pacote no [Tracker](https://tracker.debian.org).
  * Link para o repositório no salsa, se existir (caso não exista, mencione isso).
  * Link para a Merge Request no repositório Salsa do pacote se houver.
  * Link para bugs relacionados ao trabalho que será feito (bugs contra o WNPP, contra o próprio pacote que será solucionado). Caso não planeje fechar nenhum bug com o seu upload, não é necessário adicionar nenhum link.
  * Link para a página do pacote em lintian.debian.org (https://lintian.debian.org/sources/<nome_do_pacote>).

Com a issue criada, envie uma mensagem no nosso canal do Debian Brasília com o link da mesma para considerações iniciais. Assim que encontrar um revisor, o mesmo deve verificar a viabilidade do trabalho a ser executado no pacote. Caso seja considerado viável, adicione seu usuário no campo `Assignee` e marque a issue com a label `Todo`.

A label de cada issue facilitará o monitoramento do trabalho feito pelo nosso grupo nesse board:

https://board.debianbrasil.org.br

<br/>

## Trabalhando no salsa

A maioria dos revisores do Debian Brasil preferem trabalhar no salsa, usando `git-buildpackage`. Caso o pacote que esteja trabalhando não possua um repositório no salsa, fale com o seu revisor para providenciar a criação do mesmo. Faça um fork do repositório e trabalhe nele.

Assim que o contribuidor iniciar o trabalho de empacotamento, o mesmo deve mudar a label para `Doing`. Durante o trabalho, sinta-se à vontade para tirar dúvidas com o seu revisor; se o mesmo não estiver disponível, o canal do Debian Brasil sempre está aberto para perguntas :) 

Quando o trabalho estiver finalizado e pronto para revisão, submeta um Merge Request (MR) para o repositório oficial do pacote, adicione um link para o MR na issue no repositório `docs`, e mude a label da issue para `Review`.

<br/>

## Revisão do pacote

Com sua issue no estado (label) `Review`, envie uma notificação pro canal do Debian Brasil para encontrar um sponsor, usando o link para a issue. Ao encontrar um sponsor, o mesmo deve se adicionar como `Assignee`.

A revisão será feita no próprio MR, com comentário inline quando possível. Fique atento à sua caixa de entrada de email para notificações. Pode haver uma ou mais iterações de revisão até que seu sponsor esteja satisfeito com as modificações, aprove seu MR e faça o merge das suas modificações.

<br/>

## Upload do pacote

O sponsor irá construir o pacote e fazer o upload do mesmo para o repositório oficial do Debian. Assim que o email de confirmação do upload for recebido, a issue deverá ser fechada pelo sponsor or contribuidor.