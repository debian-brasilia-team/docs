---
title: Traduzir
description: 
published: true
date: 2025-03-11T21:27:12.743Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:08:42.796Z
---

# Equipe de localização para português do Brasil


Bem-vindos(as) ao portal da equipe de tradução do Debian para o português do Brasil (pt_BR)!

Os esforços de localização do Debian para português do Brasil começaram em [1999](https://lists.debian.org/debian-l10n-portuguese/1999/07/msg00000.html).  Atualmente, a maior parte das ações de tradução e de revisão é coordenada através da lista de discussão, seguindo um processo de garantia de qualidade com base em [pseudo-urls](/traduzir/pseudo-urls).

> Link para divulgação: <https://debianbrasil.org.br/traduzir> <br />
> Lista de discussão: [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese)<br />
> Canal no IRC: #debian-l10n-br [via webchat](https://webchat.oftc.net/?channels=debian-l10n-br)<br />
> Grupo no Telegram: [@debl10nptBR](https://t.me/debl10nptBR)<br />
> Sala no Matrix: [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social)<br />
> Exise uma ponte ligando o IRC, o Telegram e o Matrix.<br />
> Repositório no Salsa: <https://salsa.debian.org/l10n-br-team><br />
{.is-info}

## Frentes de tradução

![icon-warning-32x32.png](/assets/icon-warning-32x32.png) [Lista de prioridades para tradução](/traduzir/lista-de-prioridades)
O time de tradução definiu uma lista de prioridades para tradução baseado nas pendências que temos.

![official-doc.png](/assets/official-doc.png) [Páginas web](/traduzir/webwml)
Nesta iniciativa, a equipe traduz o site oficial do projeto Debian. A maior parte do trabalho não envolve programação, por isso é indicada para '''iniciantes'''. A coordenação é realizada pela lista de discussão e esta página wiki explica como preparar o ambiente de trabalho e como realizar a tradução, a revisão e o envio do arquivo para o repositório. Veja este [fluxograma](DebianWiki:Brasil/Traduzir/WebWML#Fluxograma_de_tradu.2BAOcA4w-o_de_p.2BAOE-ginas_web) que apresenta uma panorâmica dos procedimentos.

![icon-process-32x32.png](/assets/icon-process-32x32.png)[Modelos debconf](/traduzir/debconf)
O projeto consiste na tradução de arquivos [modelos POT](traduzir/templates-pot) utilizados pelo [debconf](https://wiki.debian.org/debconf), o sistema de configuração de pacotes da distribuição Debian que roda durante a instalação. A coordenação dos trabalhos é feita pela lista de discussão. Esta página wiki apresenta todo o processo e explica sobre a utilização de scripts.

![icon-debian-32x32.png](/assets/icon-debian-32x32.png) [Descrições de pacotes](/traduzir/ddtp)
São consideradas um dos principais pontos de interação com usuários(as). O Projeto de Tradução das Descrições de Pacotes Debian (DDTP - Debian Description Translation Project) foi implementado para apoiar todo o processo de tradução e revisão, realizado via inteface web e com textos normalmente pequenos. A equipe brasileira foi o segunda a aderir ao DDTP e se mantém entre as mais ativas, graças ao trabalho de muitos(as) voluntários(as). Nesta seção apresenta-se o mecanismo, regras, dicas e dúvidas comuns.

![d-i.png](/assets/d-i.png =50x30) [Instalador do Debian](/traduzir/debian-installer)
O instalador do Debian (Debian Installer ou D-I) é composto por dois componentes principais, o próprio instalador e o guia de instalação, o que requer um trabalho continuado e o alinhamento das traduções entre os materiais. O trabalho é coordenado pela equipe de desenvolvimento do *d-i*. Na página wiki é explicado como contatar e ingressar no projeto.

![icon-development-32x32.png](/assets/icon-development-32x32.png) [Páginas man](/traduzir/manpages)
Este projeto visa a tradução de páginas de manual (manpages) de pacotes específicos do Debian. O trabalho é coordenado através da lista de discussão. A seção explica como preparar o ambiente de trabalho, orientações especiais para os arquivos e como proceder com o acompanhamento do processo.
   
![wikijs-logo](/assets/wikijs-logo.png =32x32) [Páginas do wiki](/traduzir/wiki)
O wiki é a documentação criada pela comunidade Debian que complementa a [documentação oficial](https://www.debian.org/doc) do projeto. É um ambiente de fácil edição, via hipertextos colaborativos, não havendo a necessidade de coordenação dos trabalhos. Nesta página são apresentadas algumas orientações para edição e tradução, bem como sugestões para que se crie uma página pessoal.

![icon-community-32x32.png](/assets/icon-community-32x32.png) [Publicidade e notícias](/traduzir/publicidade)
As notícias sobre o projeto Debian e sua comunidade são divulgadas pelo [time de publicidade](https://wiki.debian.org/Teams/Publicity) através do blog oficial [Bits do Debian](https://bits.debian.org), das [Micronotícias do Debian](https://micronews.debian.org) e dos [comunicados de imprensa](https://wiki.debian.org/Teams/Publicity#Translate_announcements_and_press_releases). A equipe de tradução contribui em paralelo à produção dos textos, tentando divulgar rapidamente os anúncios mais importantes em português.

![icon-presentation-32x32.png](/assets/icon-presentation-32x32.png) [Manual do(a) Administrador(a) Debian](https://debian-handbook.info/contribute/)
O projeto iniciou-se com a tradução para o inglês do bem-sucedido livro em francês, e agora disponibilizado para tradução colaborativa em outros idiomas. O livro apresenta o fundamental para uma administração independente e efetiva do Debian GNU/Linux. O processo de tradução é gerenciado pela plataforma [Weblate](https://debian.weblate.org/projects/debian-handbook). É necessário inscrever-se na lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), onde você pode conseguir mais informações.

![icon-graphic-32x32.png](/assets/icon-graphic-32x32.png) [DebianEdu](/traduzir/debian-edu)
Um [Debian Pure Blend](https://wiki.debian.org/DebianPureBlends) voltado para ambientes educacionais com diversos serviços pré-configurados e facilidade de implementação para públicos não especializados. A frente de tradução inclui o site web, wiki, documentos e pacotes. A descrição de pacotes é feita pelo [DDTP](/traduzir/ddtp) e já está praticamente completa. O [site web do projeto](https://blends.debian.org/edu) é localizado através de arquivos PO que estão em um [repositório no Salsa](https://salsa.debian.org/blends-team/website/tree/master/www/edu). Há uma [iniciativa anterior de tradução](/traduzir/skolelinux) pela comunidade pt_BR, mas parece estar inativa.

![icon-emulator-32x32.png](/assets/icon-emulator-32x32.png) [Serviços](/traduzir/servicos)
O Debian utiliza muitos [subsistemas e infraestrutura](https://wiki.debian.org/Services) para dar conta da complexidade de suas operações. Grande parte desses mecanismos não estão localizados para o português do Brasil. E muitos ainda não se encontram nem mesmo internacionalizados. Nesta página são listados alguns desses serviços e a situação de localização em que se encontram.

## Eu quero ajudar a traduzir!

A equipe de tradução está sempre aberta a contribuições. Com apenas **1 hora** por semana já é possível revisar, traduzir, sugerir melhorias ou indicar erros nas documentações em português. Você está convidado(a) a participar!

Algumas orientações para não se perder no meio de tanta informação:

- ⚡ Inscreva-se na lista de e-mails, é o canal principal. Apresente-se e tire dúvidas.
- ⚡ Participe do canal no IRC, da sala no Matrix ou do grupo no Telegram.
- ⚡ Não inicie imediatamente a traduzir. Conheça o grupo, seus hábitos e processos.
- ⚡ Peça indicações. Temos arquivos pequenos para iniciantes.
- ⚡ Tirar um tempo para aprender procedimentos e sistemas nunca é um tempo perdido.
- ⚡ Revisar é mais fácil que traduzir, e tão importante quanto.
- :cinema: Assista os vídeos de apresentaões e tutoriais sobre [tradução do Debian para português do Brasil](https://www.youtube.com/playlist?list=PLU90bw3OpxLqt1S5K-z1ASi1Ula8UWzOB).

> E não, você **não** precisa ser desenvolvedor(a)! A tradução é uma de inúmeras maneiras de contribuir com o projeto Debian. Veja como [aqui](/como-colaborar) e [aqui](https://wiki.debian.org/pt_BR/DebianForNonCoderContributors).
{.is-warning}

## Ferramentas de referência para a equipe

[Estado das traduções](https://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html) 
Coordenação dos arquivos enviados pela lista de discussão.

[Estatísticas das traduções](https://www.debian.org/international/l10n/index.pt.html)
Pacotes prontos para serem traduzidos e já traduzidos.

🍥 [Pacotes nativos do Debian](/traduzir/pacotes-nativos-debian)
Pacotes nativos do Debian com arquivos .po que a tradução acontece apenas no Debian.

:speech_balloon: [Boas práticas](/traduzir/boas-praticas) *atualizar*

:speech_balloon: [Vocabulário padrão](/traduzir/vocabulario)
Uniformidade dos termos adotados pela equipe.

:speech_balloon: [Manual de estilo](/traduzir/manual-de-estilo)
Diretrizes para redação, padronizações e diagramação, orientações para escrita não sexista e para acessibilidade.

:speech_balloon: [Referências externas](/traduzir/manual-de-estilo)
Dicionários, gramáticas e vocabulários com campo de pesquisa.

## Outras informações

 * [Debian Brasil](https://debianbrasil.org.br) página inicial da comunidade Debian Brasil.

 * [Código de Conduta](https://www.debian.org/code_of_conduct.pt.html) do projeto Debian.
 * [Campanhas (sprints)](/traduzir/campanhas) realizadas pela equipe.
 * [Lista de páginas do projeto de tradução](/traduzir/lista-de-paginas)
 
## Projetos inativos

 * [Documentos traduzidos do Projeto de Documentação Debian](https://wiki.debian.org/Brasil/Documentos/Traduzidos)
Tradução dos manuais criados pelo Projeto de Documentação Debian (Debian Documentation Project - DDP), tais como Guia de referência, Guia dos novos(as) mantenedores(as), Manual de segurança, entre outros. Há versões em pdf, html e texto puro.

 * [Projeto Debian-Edu-BR](/traduzir/skolelinux)
Baseado no projeto DebianEdu (SkoleLinux), busca "cuidar não só da tradução do projeto Debian-Edu, mas também de uma localização do projeto para a nossa realidade, produzindo documentação e relatos locais e contribuindo para a discussão do projeto como um todo". Último registro no wiki de 2017.

 * [Vocabulário Padrão](/traduzir/vocabulario)
Visa uniformizar os termos adotados pela equipe de tradução. Não há, no momento, uma ação coletiva e organizada para continuar sua construção. Recentemente foi migrado para o [Salsa](https://salsa.debian.org/l10n-br-team/vocabulario) e está sendo exportado para uso no [DDTP/DDTSS](/traduzir/ddtp). Foi reunido um [histórico](/traduzir/vocabulario/historico) com iniciativas anteriores para que um registro dos esforços da equipe fosse mantido.