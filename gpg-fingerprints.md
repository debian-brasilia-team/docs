---
title: GPG Fingerprint
description: 
published: true
date: 2025-03-01T17:55:50.790Z
tags: 
editor: markdown
dateCreated: 2025-02-27T20:42:49.406Z
---

Segue a lista das fingerprints de algumas pessoas da comunidade Debian Brasil.

Caso ainda não tenha criado sua chave GPG siga a página [gerando-uma-nova-chave](/pt-br/howto/gpg/gerando-uma-nova-chave).

---

- Arthur Diniz
```bash
gpg --receive-keys 08EB78A7FA4A89E05D5C869ECBCEB26D96B98CBB

```
- Aquila Macedo Costa
```
gpg --receive-keys 15D407B7843776118BAEDB8E3C7F6AF85A6CD440
```
- Thaís Rebouças de Araujo
```
gpg --receive-keys 52C53483156E6A3D62F9A8C5278C73062B9C551E
```
- Joenio Marques da Costa 
```
gpg --receive-keys 9B2D64BF843D6DC1D8C1752C2E31D3FDE6D08FF4
```
- Sergio Durigan Junior

```
gpg --receive-keys 237A54B1028728BF00EF31F4D0EB762865FC5E36
```
- Sergio de Almeida Cipriano Junior
```
gpg --receive-keys 4573E9D2AD27DB896BBF2F9EF15DCE5316051F27
```
- Lucas Kanashiro
```
gpg --receive-keys 8ED6C3F8BAC9DB7FC130A870F823A2729883C97C
```
- Carlos Henrique Lima Melara
```
gpg --receive-keys 61C81DF22F4C386D0CCABCA75291703BFA09034E
```
- Marcos Talau
```
gpg --receive-keys B522F39159DA07DD39DC0B11F4BAAA80DB28BA4C
```
- Daniel Lenharo de Souza
```
gpg --receive-keys 31D80509460EFB31DF4B9629FB0E132DDB0AA5B1
```

- Paulo Henrique de Lima Santana
```
gpg --receive-keys 4324A92B2E7B85DA7F9C9C40C66D06B40443C450
```

- Athos Ribeiro
```
gpg --receive-keys 439884E6862A429C290DF63B033C4CA276024834
```

- Valessio Soares de Brito
```
gpg --receive-keys 211C4AFBDE7C4E6956F742979022243B57D7E283
```
