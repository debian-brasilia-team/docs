---
title: Links
description: 
published: true
date: 2025-03-01T17:55:59.521Z
tags: 
editor: markdown
dateCreated: 2025-02-27T19:55:59.429Z
---

## Sites Oficiais

[Projeto Debian](https://www.debian.org/): site oficial do Projeto.

[Projeto de documentação Debian](https://www.debian.org/doc/ddp): projeto que cuida da documentação do Debian.

[Desenvolvimento Debian](https://www.debian.org/devel): página de interesse para desenvolvedores e interessados em desenvolvimento Debian. Com manuais e políticas de desenvolvimento.

[Novos(as) mantenedores(as) Debian](https://nm.debian.org): página onde se cadastram candidatos a mantenedores Debian. Pode-se também fazer consultas sobre a situação dos inscritos.

[Banco de dados de desenvolvedores(as) Debian](https://db.debian.org): nesse endereço pode-se conseguir informações sobre as máquinas disponíveis para o Debian e sobre os desenvolvedores.

[Sistema de rastreamento bugs](https://bugs.debian.org): aqui você pode acessar os relatórios de erros enviados sobre pacotes Debian e aprender como enviar novos.

[Pacotes Debian](https://packages.debian.org): aqui você encontra informações sobre pacotes Debian e pode baixar pacotes e pacotes fontes facilmente.

[Sistema de construção automática de pacotes](https://buildd.debian.org): página dos famosos buildds, que constróem os pacotes para todas as arquiteturas automaticamente. Pode-se conseguir logs e ver gráficos para saber como as diversas arquiteturas estão conseguindo se manter na luta para chegar a uma
situação razoável para o lançamento.

[Mais links](https://people.debian.org/~eriberto/#links): lista de links úteis mantida pelo João Eriberto Mota Filho.

## Sites de Notícias ou ajuda Debian

[Micronews](https://micronews.debian.org): notícias oficiais do Projeto Debian.

[Bits](https://bits.debian.org): links para várias notícias sobre Debian.

[Planet internacional do Debian](https://planet.debian.org): agregador de postagens de terceiros sobre Debian.

[DebianHelp](https://www.debianhelp.org): site de ajuda para usuários(as) de Debian.
