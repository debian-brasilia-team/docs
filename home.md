---
title: Home
description: 
published: true
date: 2025-03-07T01:03:36.618Z
tags: 
editor: markdown
dateCreated: 2023-05-30T21:22:33.434Z
---

![logo-debian-brasil.svg](/logo/logo-debian-brasil.svg =200x)

Bem-vindo(a) à comunidade Debian Brasil.

Aqui podem ser encontrados conteúdos variados sobre o desenvolvimento do Debian.

Para notícias sobre a comunidade brasileira de usuários(as) e desenvolvedores(as) do Debian, acesse nosso [blog](/blog).

<br/>

## Comunicação

A forma de comunicação mais utilizada dentro do projeto Debian é o [IRC](https://en.wikipedia.org/wiki/IRC), e as [listas de e-mail](/listas) principalmente para mensagens mais extensas e detalhadas. Contudo, para tornar a comunicação mais amigável para as novas gerações, também utilizamos o Matrix e o Telegram.

<br/>

#### IRC

O nome de host `irc.debian.org` é um atalho (alias) para `irc.oftc.net`. A maioria dos canais de IRC do Debian estão na rede [OFTC](https://www.oftc.net/).

- Canais: `#debian-devel-br` (para empacotamento) e `debian-l10n-br` (para tradução)
- Veja mais informações sobre os canais brasileiros no IRC [nesta página](/irc)
- https://chat.debianbrasil.org.br

> Para configuração utilizando o cliente **Element**, veja a página [utilizando irc com element](/howto/utilizando-irc-com-element). 
{.is-info}

<br/>

#### Matrix e Telegram

Nós também temos algumas salas no servidor matrix mantido pelo grupo debian.social. Para acessá-las, basta abrir os seguintes links:

- [debian-devel-br para empacotamento](https://matrix.to/#/#debian-devel-br:matrix.debian.social).
- [debian-l10n-br para tradução](https://matrix.to/#/#debian-l10n-br:matrix.debian.social)

Veja outras salas de grupos brasileiros no Matrix e no Telegram [nesta página](/matrix-telegram).

<!--
> Temos uma ponte (bridge) entre o canal `#debian-devel-br` do IRC com o grupo do `Telegram`.
{.is-info}

<br/>
-->

## Plataforma Colaborativa

- https://cloud.debianbrasil.org.br

> Para ter acesso a essa plataforma é necessario se registrar na instância do `GitLab` do Debian que se chama `Salsa`
{.is-info}

<br/>

## Repositório de Código

O Debian mantém uma instância do GitLab própria que chamamos de Salsa e pode ser acessada em: 

- https://salsa.debian.org

Nós utilizamos o Salsa para a maioria das tarefas do grupo, portanto sugerimos que crie uma conta na plataforma.

> A criação de conta requer intervenção manual para diminuir os spams e bots, então pode demorar alguns poucos dias. Se sua conta não for ativada em três dias, avise-nos! 
{.is-warning}


Para fazermos rastreamento e revisão de seus pacotes, crie uma **issue** no board abaixo e tente mantê-la atualizada.

Ao mover para **review** também não se esqueça de mandar mensagem para algum **Debian Developer** ou **mentor** para ver seu pacote. 

- Lista de mentores pode ser acessada [aqui](https://cloud.debianbrasil.org.br/f/17164).
- Lista de pacotes para novatos pode ser acessada [aqui](https://cloud.debianbrasil.org.br/f/17172).

> Veja também como funciona nosso [processo de contribuicao com pacotes](/processo-de-contribuicao-com-pacotes).

**Board**: https://board.debianbrasil.org.br

<br/>

## FAQ
- Estamos enfrentando problemas técnicos no momento?
	- Mantemos um dashboard que exibe o status de cada serviço que oferecemos.
	- https://uptime.debianbrasil.org.br
