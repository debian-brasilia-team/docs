---
title: Debian Brasil Community Report - January 2025
description: This report provides a comprehensive overview of the Debian Brasil community's activities in January 2025.
published: true
date: 2025-03-01T17:25:19.084Z
tags: contribuicao, report, january, 2025, janeiro
editor: markdown
dateCreated: 2025-02-20T21:22:52.786Z
---

# Debian Brasil Community Report - January 2025


## Overview

In January 2025, the local Debian Brazil community actively contributed to packaging, bug fixes, and discussions surrounding important project decisions. Alongside the direct contributions recorded in the Debian Ultimate Database (UDD) and Salsa issue tracker, the IRC channel `#debian-devel-br` served as a key platform for discussions, coordination, and troubleshooting.

<br>

## 1. Top Contributors and Notable Package Uploads

<br>

### Top Contributors:

- **Lucas Kanashiro**: 
  - **ruby-defaults** (1:3.1+support3.3, 1:3.3~3.1)
  - **rubygems-integration** (1.19)
  - **valkey** (8.0.2+dfsg1-1)
  - **ruby3.3** (3.3.7-1)
  - **ruby-memory-profiler** (1.1.0-1, 1.1.0-2)
  - **racc** (1.8.1-1)
  - **thin** (1.8.2-3)

- **Arthur Diniz**:
  - **golang-mvdan-editorconfig** (0.2.0+git20240816.eab549b-1)
  - **gum** (0.14.4-1)
  - **kind** (0.26.0-1)
  - **golang-k8s-sigs-kustomize-kyaml** (0.18.1+ds-1, 0.18.1+ds-2)
  - **golang-k8s-component-base** (0.31.4-1, 0.31.4-2)
  - **golang-k8s-cli-runtime** (0.31.4-1, 0.31.4-2)

- **Samuel Henrique**:
  - **rsync** (3.3.0+ds1-3, 3.3.0+ds1-4)
  - **powerline-go** (1.24-1, 1.25-1)

- **Sergio Durigan Junior**:
  - **gdb** (15.2-2~exp1, 16.0.90.20250111-1~exp1, 16.0.90.20250111-1~exp2, 16.1-1, 16.1-2)
  - **snac2** (2.68-1, 2.69-1)
  - **intel-processor-trace** (2.1.2-1)

- **Carlos Henrique Lima Melara**:
  - **neomutt** (20241212+dfsg-2, 20250109+dfsg-1, 20250113+dfsg-1)

<br>

### Notable Package Uploads:
- **rsync**: Critical vulnerability fixes (CVE-2024-12084) were addressed in versions 3.3.0+ds1-3 and 3.3.0+ds1-4.
- **gdb**: Multiple experimental and unstable versions were uploaded, indicating active development and testing.
- **ruby3.3**: New version uploaded, reflecting ongoing support for Ruby in Debian.
- **powerline-go**: New package introduced, providing an alternative to powerline for Go developers.

<br>

---

<br>

## 2. Issues Resolved from Salsa

<br>

### Key Issues Resolved:
- **Package arjun** (Issue #386): Closed on 2025-01-29. Uploaded by Arlisson Jaques, with contributions from the community.
- **Package powerline** (Issue #399): Closed on 2025-01-23. Uploaded by Lucas Gabriel, reflecting community interest in Go-related tools.
- **Package golang-mvdan-editorconfig** (Issue #390): Closed on 2025-01-09. Uploaded by Arthur Diniz, addressing bug #1090232.
- **Package rsync** (Issue #715): Investigated and resolved by Samuel Henrique, leading to the upload of fixed versions.

<br>

### Matching Issues to Uploads:
- **rsync**: The issue related to CVE-2024-12084 was resolved with the upload of versions 3.3.0+ds1-3 and 3.3.0+ds1-4.
- **golang-mvdan-editorconfig**: Issue #390 was resolved with the upload of version 0.2.0+git20240816.eab549b-1.
- **powerline-go**: Issue #399 was resolved with the upload of versions 1.24-1 and 1.25-1.

<br>

---

<br>

## 3. Insights from IRC Discussions

<br>

### Key Technical Topics:

- **BTS (Bug Tracking System) Improvements**: Discussions around improving the BTS, including the possibility of a modern web interface. The community debated the feasibility of maintaining the current email-based system versus developing a new frontend.
- **ETIAS for FOSDEM**: The community discussed the new ETIAS requirement for traveling to Europe, with members sharing information on how to apply and whether it was mandatory.
- **rsync Vulnerability**: A critical vulnerability in rsync (CVE-2024-12084) was discussed, with Samuel Henrique leading the effort to fix and upload the patched versions.
- **Debian Mirror Setup**: Charles Melara announced that mirrors.ic.unicamp.br became an official Debian mirror, with contributions from Sergio Durigan Junior and students from Unicamp.

<br>

### Community Decisions:
- **FOSDEM Dinner Reservation**: The community coordinated a dinner reservation at Chez Leon in Brussels for FOSDEM attendees, with Lucas Kanashiro handling the reservation.
- **Debian Mirror**: The decision to set up a new Debian mirror at Unicamp was finalized, with contributions from the local community.

<br>

### Critical Information Learned:
- **ETIAS Status**: The community learned that ETIAS is not yet mandatory but will be required soon. Members shared resources on how to apply.
- **rsync Vulnerability**: The community was informed about the critical vulnerability in rsync and the steps taken to address it.

<br>

---

<br>

## 4. Activity Metrics for IRC

<br>

- **Total Messages**: 429 messages were exchanged in the #debian-devel-br channel during January 2025.

<br>

The #debian-devel-br channel saw 429 messages exchanged in January 2025.

- Most Active Day: January 16, 2025
- Most Active Week: Week 3 of January
- Most Active Weekday: Thursday
- Daily Average Messages: 17.16 messages/day
- Weekly Average Messages: 85.8 messages/week


<br>

Message Distribution by Weekday

- Thursday: 128 messages (Most Active)
- Tuesday: 71 messages
- Monday: 66 messages
- Wednesday: 60 messages
- Sunday: 38 messages
- Friday: 37 messages
- Saturday: 29 messages (Least Active)

---

<br>

## Conclusion

January 2025 was a productive month for the Debian Brasil community, with significant contributions in package maintenance, issue resolution, and community coordination. The community successfully addressed critical vulnerabilities, set up a new Debian mirror, and prepared for FOSDEM. The IRC channel remained highly active, with discussions on technical topics, community decisions, and travel logistics. The community's efforts reflect a strong commitment to maintaining and improving Debian, both locally and globally.

<br>
