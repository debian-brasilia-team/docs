---
title: Desenvolvimento
description: 
published: true
date: 2025-03-01T17:24:58.399Z
tags: 
editor: markdown
dateCreated: 2024-04-14T17:11:56.113Z
---

# Desenvolvimento

- [Como você pode ajudar o Debian programando em Python - Antonio Terceiro [PyBR2020]](https://www.youtube.com/watch?v=WFHun1rs4QE)
- [Sou programador{a,}, como posso ajudar o Debian?](https://www.youtube.com/watch?v=kanDL540lVc)