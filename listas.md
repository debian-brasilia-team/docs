---
title: Listas de Discussão
description: Listas de discussão
published: true
date: 2025-03-01T17:56:01.954Z
tags: listas
editor: markdown
dateCreated: 2025-02-26T16:11:17.868Z
---

# Listas de Discussão

Existem algumas listas de discussão usadas para troca de informações, solução de dúvidas, tradução e interação entre entusiastas de Debian. 

## Como se inscrever

Para se inscrever (assinar) em uma delas, mande uma mensagem para o endereço da lista acrescido de **-request** com o assunto **subscribe**. Por exemplo:
Para assinar a <debian-user-portuguese@lists.debian.org>
Mande uma mensagem para [debian-user-portuguese-request@lists.debian.org](mailto:debian-user-portuguese-request@lists.debian.org?subject=subscribe)
Colocando no campo do assunto a palavra: **subscribe**
Pouco tempo depois você receberá uma mensagem de confirmação.
Basta responder essa mensagem sem alterar o assunto (o **Re:** é aceito sem problemas) para ter sua inscrição confirmada.

Você pode também visitar o [site de inscrições nas listas](http://www.debian.org/MailingLists/subscribe) do Debian. 

As únicas exceções são a lista **debian-br-geral** e as listas dos grupos regionais do Brasil (veja a seção !Regionais: elas são hospedadas no [servidor de listas do Debian no alioth](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo), onde podem ser assinadas. Para as listas regionais do Brasil, procure por *debian-br-gud-\**.

## Principais listas brasileiras

Antes de enviar uma dúvida, verifique se a lista é apropriada a ela.

Por exemplo: uma dúvida sobre configuração não deve ser enviada para a lista [debian-br-geral](https://lists.alioth.debian.org/mailman/listinfo/debian-br-geral) e sim para a lista [Debian User Portuguese](http://lists.debian.org/debian-user-portuguese).

Você também pode procurar saber se ela já não foi respondida no nosso [FAQ](http://www.debianbrasil.org/docs/traduzidos/sgml/debian-faq/debian-faq.html/index.html) ou ainda faça uma [procura nos arquivos das listas](http://lists.debian.org/search.html).

Lembramos que as listas que usamos fazem parte do Projeto Debian Brasil e os princípios do projeto devem ser respeitados nelas. Aconselhamos fortemente a todos(as) que sigam as recomendações do [Código de Conduta](http://www.debian.org/MailingLists/#codeofconduct) das listas do Debian, além de nossas próprias [regras de conduta](http://debianbrasil.org.br/recomendacoes-para-utilizacao-das-listas) visando um convívio adequado com os(as) demais participantes.

### debian-br-geral
 * Objetivo: principal lista da Comunidade Debian no Brasil, reúne desenvolvedores(as), usuários(as) e entusiastas para coordenação do Projeto Debian-BR
 * Observação: esta **não é** a lista para tirar dúvidas técnicas como instalação, configuração, etc.
 * Página da lista: [Debian-BR-geral](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-geral)
 * E-mail: <debian-br-geral@alioth-lists.debian.net>
 * IRC: [#debian-br](irc://irc.debian.org/debian-br)

### debian-devel-portuguese
 * Objetivo: discussões sobre desenvolvimento em Debian, seria a versão em português da debian-devel. É usada pela equpe de empacotamento Debian, serve como uma versão brasileira da debian-mentors, tanto para dúvidas como para encontrar padrinhos(as) que possam enviar pacotes para o repositório. 
 * Página da lista: [Debian Devel Portuguese](http://lists.debian.org/debian-devel-portuguese) 
 * E-mail: <debian-devel-portuguese@lists.debian.org>
 * IRC: [#debian-devel-br](irc://irc.debian.org/debian-devel-br)

### debian-l10n-portuguese
 * Objetivo: coordenação dos esforços de localização e tradução para o português do Brasil.
 * Página da lista: [Debian Localization Portuguese](http://lists.debian.org/debian-l10n-portuguese)
 * E-mail: <debian-l10n-portuguese@lists.debian.org>
 * IRC: [#debian-l10n-br](irc://irc.debian.org/debian-l10n-br)

### debian-news-portuguese
 * Objetivo: divulgação de novidades sobre o Debian, os e-mails são em português, a lista costuma receber a tradução do Debian Project News e dos Alertas de Segurança Debian (DSA):
 * Página da lista: [Debian News Portuguese](http://lists.debian.org/debian-news-portuguese) 
 * E-mail: <debian-news-portuguese@lists.debian.org>

### debian-user-portuguese (também conhecida como d-u-p)
 * Objetivo: dúvidas de usuários, suporte técnico, questões gerais sobre o uso de sistemas Debian
 * Observação: **esta é** a lista para tirar dúvidas técnicas como instalação, configuração, etc.
 * Página da lista: [Debian User Portuguese](http://lists.debian.org/debian-user-portuguese)
 * E-mail: <debian-user-portuguese@lists.debian.org>

### debian-br-eventos
 * Objetivo: questões gerais sobre eventos com a Comunidade Debian Brasil.
 * Página da lista: [Debian-br-eventos](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos)
 * E-mail: <debian-br-eventos@alioth-lists.debian.net>
 * IRC: [#debian-br-eventos](irc://irc.debian.org/debian-br-eventos)

