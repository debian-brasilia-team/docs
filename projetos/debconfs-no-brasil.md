---
title: DebConfs no Brasil
description: 
published: true
date: 2025-03-01T17:43:35.141Z
tags: 
editor: markdown
dateCreated: 2025-02-27T20:50:53.019Z
---

# DebConfs no Brasil

A [DebConf](https://debconf.org) é uma conferencia realizada anualmente e
intinerante para voluntários(as) e desenvolvedores(as) do projeto Debian.
Além de uma programação ampla com palestras técnicas, sociais e políticas, a
DebConf fornece uma oportunidade para que desenvolvedores(as), colaboradores(as)
e outras pessoas interessadas se encontrem pessoalmente e trabalhem juntas.
A conferência tem sido realizada anualmente desde 2000 em locais variados como
Canadá, Finlândia, México e Brasil.

Conferências Debian anteriores contaram com palestrantes de todo o mundo. Elas
também foram extremamente benéficas para o desenvolvimento de componentes chaves
do Debian, incluindo o novo instalador e melhorar a internacionalização do
Debian.

Site: <https://debconf.org>

## DebConf4 em Porto Alegre

A primeira edição da DebConf no Brasíl (e quinta edição geral do evento) foi a
[DebConf4](http://debconf4.debconf.org) que aconteceu de 26 de maio a 2 de junho
de 2004 na cidade de Porto Alegre - RS, na sede do SESC. Com a participação de
desenvolvedores(as), voluntários(as), organizadores(aa). Confira um relatório
completo [aqui](https://debconf4.debconf.org/final-report.html).

A DebConf4 ocorreu próximo à 5ª edição do
[FISL - Fórum Internacional Software Livre](http://fisl.org.br), o que
possibilitou trabalhar junto em prol de convidar mais pessoas de fora do Brasil
para o evento. Assim como a instituição qual captou e geriu os recursos
administrativo e financeiro, foram em parte o mesmo corpo do FISL.

Site: <http://debconf4.debconf.org>

![Debianday 2020 banner simples](/blog/imagens/DebConf4-horizontal.png)

## BID de Belo Horozionte para a DebConf12

Em 2010/2011 a comunidade brasileira escreveu um BID e candidatou a cidade de
Belo Horizonte para sediar a [DebConf12](http://debconf12.debconf.org/) em 2012.

BID: <https://wiki.debconf.org/wiki/DebConf12/Bids/Brazil/BeloHorizonte>

Belo Horizonte perdeu para a cidade de Manágua na Nicarágua.

## BID de Curitiba para a DebConf18

Em 2016/2017 a comunidade Debian de Curitiba escreveu um BID e candidatou a
cidade para sediar a [DebConf18](https://debconf18.debconf.org/) em 2018.

BID: <https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba>

Curitiba perdeu para a cidade de Hsinchu em Taiwan.

## DebConf19 em Curitiba

Em 2017/2018 a comunidade Debian de Curitiba atualizou o BID anterior e
candidatou novamente a cidade para sediar a
[DebConf19](https://debconf19.debconf.org) em 2018.

BID: <https://wiki.debconf.org/wiki/DebConf19/Bids/Curitiba/>

A [DebConf19](https://debconf19.debconf.org) foi a 20ª edição da Conferência e
aconteceu de 21 a 28 de julho de 2019, no Campus Central da
[UTFPR - Universidade Tecnológica Federal do Paraná](http://portal.utfpr.edu.br/),
em Curitiba - Paraná.

Na semana anterior à DebConf19, aconteceu a
[DebCamp](https://wiki.debian.org/DebConf/19/DebCamp) de 14 a 19 de julho, que
foi um período em que os(as)participantes do Projeto se reuniram para
trabalharem nas melhorias do sistema operacional. E no dia 20 de julho aconteceu
o [Open Day](https://debconf19.debconf.org/schedule/openday/), um dia totalmente
dedicado ao público local com atividades que vão além do Debian, contendo temas
relacionados a Software Livre e Código Aberto em geral.

Site: <https://debconf19.debconf.org>

![Debianday 2020 banner simples](/blog/imagens/DebConf19-horizontal.png =400x)