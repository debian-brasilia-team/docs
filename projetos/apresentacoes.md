---
title: Apresentações
description: 
published: true
date: 2025-03-01T17:43:30.509Z
tags: 
editor: markdown
dateCreated: 2025-02-27T20:50:46.359Z
---

# Apresentações básicas

### Título: O Projeto Debian quer você!

- Autor: Paulo Henrique de Lima Santana
- Data: 04 de maio de 2019
- [Arquivo pdf](https://debianbrasil.org.br/arquivos-de-apresentacoes/o-projeto-debian-quer-voce.pdf)

### Título: Debian - O sistema universal

- Autor: Daniel Lenharo
- Data: 06 de Maio de 2017
- [Arquivo pdf](https://debianbrasil.org.br/arquivos-de-apresentacoes/debian-o-sistema-universal-daniel-lenharo.pdf)
- [Arquivo odp](https://debianbrasil.org.br/arquivos-de-apresentacoes/debian-o-sistema-universal-daniel-lenharo.odp)

### Título: Bastidores Debian - Entenda como a distribuição funciona

- Autor: João Eriberto Mota Filho
- Data: 08 de abril de 2017
- [Arquivo pdf](http://www.eriberto.pro.br/palestras/bastidores_debian.pdf)

### Título: Debian - um universo em construção

- Autor: Giovani Ferreira
- Data: 17 de março de 2017
- [Arquivo pdf](https://debianbrasil.org.br/arquivos-de-apresentacoes/o-projeto-debian-quer-voce-paulo-santana.pdf)

### Título: Debian 101 - Introdução ao Debian

- Autor: Samuel Henrique
- Data: Outubro de 2018
- [Arquivo pdf](https://debianbrasil.org.br/arquivos-de-apresentacoes/introducao-ao-debian-samuel-henrique.pdf)

### Título: Debian - o que é, como funciona, e como participar

- Autor: Antonio Terceiro
- Data: 13 de agosto de 2016
- [Arquivo pdf](https://debianbrasil.org.br/arquivos-de-apresentacoes/debian-o-que-eh-e-como-funciona-antonio-terceiro.pdf)
