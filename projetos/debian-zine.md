---
title: Debian Zine
description: 
published: true
date: 2025-03-01T17:43:37.430Z
tags: 
editor: markdown
dateCreated: 2025-02-27T20:50:56.338Z
---

# Debian Zine

Fanzine Eletrônico sobre o Debian em português. Foram publicadas 4 edições.

### Número 0 - novembro de 2004 - Conisli

[PDF versão colorida.](/zine/debianzine_0.pdf)

### Número 1 - março de 2005

[PDF versão colorida.](/zine/debianzine_2005_001_co.pdf)

### Número 2 - junho de 2005 - FISL

[PDF versão colorida.](/zine/debianzine_2005_002_fisl_co_full.pdf)

### Número 3 - outubro de 2005

[PDF versão colorida.](/zine/debianzine_2005_003_on.pdf)
