---
title: Legendas para vídeos
description: 
published: true
date: 2025-03-01T17:43:39.687Z
tags: 
editor: markdown
dateCreated: 2025-02-27T20:50:59.630Z
---

# Legendas para vídeos

Projeto para criação de legendas em português de vídeos relativos ao
[Debian](https://www.debian.org).
A princípio, vídeos da [DebConf](https://www.debconf.org/), mas não restrito a
estes. Vale salientar que vídeos em português devem ser legendados também, para que a comunidade surdo-muda possa se beneficiar.

## Informações do time de vídeo do Debian

<https://wiki.debian.org/Teams/DebConf/Video/Subtitles>

Para trabalhar com legendas online, você pode participar do time de vídeo no
Amara:

<https://amara.org/en/teams/debconf/>

## DebConf5

- What is free software - Gunnar Wolf
- Evaluating Open Source Software for Enterprise Use - Martin Langhoff
