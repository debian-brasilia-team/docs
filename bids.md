---
title: Bids
description: 
published: true
date: 2025-03-07T19:14:33.334Z
tags: 
editor: markdown
dateCreated: 2025-03-07T18:57:54.803Z
---

# Bids para MiniDebConf

## MiniDebConf 2025

   * [Modelo e campos](/bids/modelo)
   * Campinas
   * [Maceió - AL](/bids/2025/maceio)
   * [Natal - RN](/bids/2025/natal)

## O que uma MiniDebConf precisa ou pode ter

A ideia é listar itens que podem ou não ser obrigatórios para organizar uma MiniDebConf.

   * Local
   * Definir coordenadores
   * Data
   * Infra
      * Auditório(s) para palestras
      * Sala(s) para oficina(s)
      * Espaço para hacklab/hackspace
   * Pessoas para ajudar localmente
   * Pessoas para ajudar remotamente
   * Patrocínio:
      * Escrever projeto
      * Enviar para empresas
   * Apoio financeiro do Debian
   * Site
   * Sistema de inscrição
   * Design
   * Alimentação
   * Hospedagem
   * Divulgação
   * Day trip/passeio
   * Programação
      * Chamada de propostas
      * Definir atividades
   * Gravação/transmissão
   * Kit do participante - não são obrigatórios e depende de quanto dinheiro o evento tem pra gastar.
      * Camiseta
      * Copo
      * Cordão de crachá
      * Outros
   * Material gráfico
      * Crachá
      * Cartaz para divulgação local
      * Banners de lona para sinalização/identificação
