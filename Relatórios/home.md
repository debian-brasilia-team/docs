---
title: Relatórios
description: 
published: true
date: 2025-03-01T17:56:17.833Z
tags: report, relatório, relatórios
editor: markdown
dateCreated: 2025-02-20T21:42:00.793Z
---

Segue abaixo a lista de relatórios mensais da comunidade Debian Brasil.

## 2025
- [Janeiro](/Relatórios/janeiro-2025)