---
title: Relatório da Comunidade Debian Brasil - Janeiro 2025
description: 
published: true
date: 2025-03-01T17:56:20.191Z
tags: report, january, janeiro, pt-br
editor: markdown
dateCreated: 2025-02-20T21:36:48.232Z
---

## Visão Geral

Em janeiro de 2025, a comunidade local do Debian Brasil contribuiu ativamente com empacotamento, correção de bugs e discussões sobre decisões importantes do projeto. Além das contribuições diretas registradas no Debian Ultimate Database (UDD) e no rastreador de issues do Salsa, o canal IRC `#debian-devel-br` serviu como uma plataforma chave para discussões, coordenação e solução de problemas.

<br>

## 1. Principais Contribuidores e Uploads Notáveis de Pacotes

<br>

### Principais Contribuidores:

- **Lucas Kanashiro**:
  - **ruby-defaults** (1:3.1+support3.3, 1:3.3~3.1)
  - **rubygems-integration** (1.19)
  - **valkey** (8.0.2+dfsg1-1)
  - **ruby3.3** (3.3.7-1)
  - **ruby-memory-profiler** (1.1.0-1, 1.1.0-2)
  - **racc** (1.8.1-1)
  - **thin** (1.8.2-3)

- **Arthur Diniz**:
  - **golang-mvdan-editorconfig** (0.2.0+git20240816.eab549b-1)
  - **gum** (0.14.4-1)
  - **kind** (0.26.0-1)
  - **golang-k8s-sigs-kustomize-kyaml** (0.18.1+ds-1, 0.18.1+ds-2)
  - **golang-k8s-component-base** (0.31.4-1, 0.31.4-2)
  - **golang-k8s-cli-runtime** (0.31.4-1, 0.31.4-2)

- **Samuel Henrique**:
  - **rsync** (3.3.0+ds1-3, 3.3.0+ds1-4)
  - **powerline-go** (1.24-1, 1.25-1)

- **Sergio Durigan Junior**:
  - **gdb** (15.2-2~exp1, 16.0.90.20250111-1~exp1, 16.0.90.20250111-1~exp2, 16.1-1, 16.1-2)
  - **snac2** (2.68-1, 2.69-1)
  - **intel-processor-trace** (2.1.2-1)

- **Carlos Henrique Lima Melara**:
  - **neomutt** (20241212+dfsg-2, 20250109+dfsg-1, 20250113+dfsg-1)

<br>

### Uploads Notáveis de Pacotes:
- **rsync**: Correção de vulnerabilidades críticas (CVE-2024-12084) nas versões 3.3.0+ds1-3 e 3.3.0+ds1-4.
- **gdb**: Uploads múltiplos de versões experimentais e instáveis, indicando desenvolvimento e testes ativos.
- **ruby3.3**: Nova versão carregada, refletindo o suporte contínuo ao Ruby no Debian.
- **powerline-go**: Novo pacote introduzido, fornecendo uma alternativa ao powerline para desenvolvedores Go.

<br>

---

<br>

## 2. Issues Resolvidos no Salsa

<br>

### Issues Principais Resolvidos:
- **Pacote arjun** (Issue #386): Fechado em 29-01-2025. Upload realizado por Arlisson Jaques, com contribuições da comunidade.
- **Pacote powerline** (Issue #399): Fechado em 23-01-2025. Upload realizado por Lucas Gabriel, refletindo o interesse da comunidade em ferramentas relacionadas ao Go.
- **Pacote golang-mvdan-editorconfig** (Issue #390): Fechado em 09-01-2025. Upload realizado por Arthur Diniz, corrigindo o bug #1090232.
- **Pacote rsync** (Issue #715): Investigado e resolvido por Samuel Henrique, levando ao upload das versões corrigidas.

<br>

### Correspondência de Issues com Uploads:
- **rsync**: A issue relacionada à CVE-2024-12084 foi resolvida com o upload das versões 3.3.0+ds1-3 e 3.3.0+ds1-4.
- **golang-mvdan-editorconfig**: A issue #390 foi resolvida com o upload da versão 0.2.0+git20240816.eab549b-1.
- **powerline-go**: A issue #399 foi resolvida com o upload das versões 1.24-1 e 1.25-1.

<br>

---

<br>

## 3. Insights das Discussões no IRC

<br>

### Tópicos Técnicos Principais:

- **Melhorias no BTS (Bug Tracking System)**: Discussões sobre melhorias no BTS, incluindo a possibilidade de uma interface web moderna.
- **ETIAS para FOSDEM**: Debate sobre a obrigatoriedade do ETIAS para viagens à Europa e como aplicar.
- **Vulnerabilidade no rsync**: Discussão sobre a vulnerabilidade crítica no rsync (CVE-2024-12084), com Samuel Henrique liderando a correção.
- **Configuração de espelho do Debian**: Anúncio da oficialização do mirrors.ic.unicamp.br como espelho Debian.

<br>

### Decisões da Comunidade:
- **Reserva de Jantar para o FOSDEM**: Coordenação da reserva no restaurante Chez Leon em Bruxelas, organizada por Lucas Kanashiro.
- **Espelho Debian na Unicamp**: Decisão de configurar um novo espelho Debian na Unicamp, com contribuições da comunidade local.

<br>

---

<br>

## 4. Métricas de Atividade no IRC

<br>

- **Total de Mensagens**: 429 mensagens foram trocadas no canal #debian-devel-br durante janeiro de 2025.

- **Dia Mais Ativo**: 16 de janeiro de 2025
- **Semana Mais Ativa**: 3ª semana de janeiro
- **Dia da Semana Mais Ativo**: Quinta-feira
- **Média Diária de Mensagens**: 17.16 mensagens/dia
- **Média Semanal de Mensagens**: 85.8 mensagens/semana

<br>

Distribuição de Mensagens por Dia da Semana:

- Quinta-feira: 128 mensagens (Mais Ativo)
- Terça-feira: 71 mensagens
- Segunda-feira: 66 mensagens
- Quarta-feira: 60 mensagens
- Domingo: 38 mensagens
- Sexta-feira: 37 mensagens
- Sábado: 29 mensagens (Menos Ativo)

---

## Conclusão

Janeiro de 2025 foi um mês produtivo para a comunidade Debian Brasil, com contribuições significativas em manutenção de pacotes, resolução de issues e coordenação da comunidade.

