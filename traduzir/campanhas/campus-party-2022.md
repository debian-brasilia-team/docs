---
title: Campus Party 2022
description: 
published: true
date: 2025-03-11T18:15:33.637Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:35:34.014Z
---

# Encontro na Campus Party 2022

Anotações sobre o encontro que ocorrerá na [Campus Party 2022](https://brasil.campus-party.org) em São Paulo, de 11 a 15 de novembro de 2022.

Temas sugeridos:

- quem é e o que faz a equipe de tradução Debian pt_BR;
- práticas de tradução voltadas para inclusão e diversidade;
- quais as possibilidades para facilitar o nosso trabalho;
- como aliar práticas de tradução voltadas para inclusão/diversidade e ferramentas de acessibilidade;
- como manter novos(as) contribuidores(as) ativos(as) na equipe;
