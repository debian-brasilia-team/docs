---
title: Campanha novos colaboradores
description: 
published: true
date: 2025-03-11T18:35:26.947Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:33:29.581Z
---

# Campanha para atrair novos(as) colaboradores(as)

> Esta campanha já terminou! Veja novas informações na [página inicial da equipe de tradução](/traduzir).
{.is-info}

De tempos em tempos recebemos mensagens na lista de tradutores brasileiros com pedidos de informações sobre como colaborar com as traduções. A resposta padrão era indicar a documentação do time de tradução, na intenção de que a pessoa em seguida começasse a traduzir. Mas, dado o número de potenciais colaboradores que por um motivo ou outro não seguiam trabalhando com a equipe, concluímos que essa resposta era ineficaz.

Em 2018 tentaremos uma nova estratégia, com o intuito de incentivar e trabalhar em solidariedade com qualquer pessoa que tenha interesse em colaborar, independente do seu conhecimento/habilidades, técnicas e/ou sociais.

## Ações

### No dia a dia

#### 1. Comunicação

Falar de "1 para N" pode ser intimidante, principalmente para novatos(as). Tentaremos criar espaços de interação "1 para 1", seja por e-mail, IRC, telegram, signal, sinal de fumaça... certamente acharemos um meio que funcione! O importante é saber que tem alguém do outro lado da linha disposto a saber das suas dificuldades e te acompanhar na descoberta do seu caminho de contribuição.

Basta mostrar interesse e indicar o seu contato: nós retornaremos!

Obs: não estamos propondo que o processo de tradução seja migrado para telegram ou signal. Queremos abrir a possibilidade das pessoas interessadas em ajudar poderem conversar com tradutores nessas ferramentas, para que o tradutor mostre quais são os canais oficiais usados pelo time de tradução.

#### 2. Aprendizado

Leitura de longas páginas de documentação não é pra todo mundo, ainda mais sozinho(a). Pensando nisso, vamos tentar acompanhar melhor os(as) novatos(as) no seu processo de iniciação, até que eles se sintam autônomos pra seguirem sozinhos(as) (e apostamos que não demora muito!). Neste processo pretendemos revisar nossos tutoriais e quem sabe criar novos materiais didáticos.

Mostrar que quem ajuda na tradução pode criar uma conta no [contributors](http://contributors.debian.org) e ter a sua ajuda registrada na página [i18n.debian.org do contributors](https://contributors.debian.org/source/i18n.debian.org).

#### 3. Comunidade

Pra que o espírito de equipe esteja sempre ativo, tentaremos estar sempre presentes no IRC e promover encontros de tradução remotos, ou presenciais, sempre que possível. Muitos de nós funciona melhor em grupo (e isso não vale só pra iniciantes!), então vamos aproveitar do potencial do time.

Nos dias 11 e 12 de abril acontecerá um sprint de tradução na [MiniDebConf Curitiba 2018](https://br2018.mini.debconf.org/2018/schedule/).

### Retroativamente

Estamos contactando pessoalmente quem enviou mensagem de interesse à lista no passado. Tentaremos re-ativar seu interesse e oferecer-lhes o suporte necessário para que consigam de fato contribuir para o projeto.

Se este é o seu caso, e ainda não te contactamos, não precisa esperar! Com uma nova mensagem de interesse, você entra no topo da fila ;-)

## Dúvidas, comentários, outras idéias?

Não hesite em contactar a equipe brasileira na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR)

E mãos à obra!

ToDo: a campanha está inativa, é preciso atualizar a página.

FixMe: sugestão para inclusão na campanha:

**Procedimento para iniciantes (ou: não deixe de contribuir por causa do GIT)**

O fluxo completo de trabalho de tradução inclui não somente a tradução em si, mas também a instalação e a configuração de sistemas de apoio, como o [GIT](https://git-scm.com). Esses fatores podem inibir contribuidores(as) que não são muito próximos(as) dessas tecnologias e procedimentos.

Algumas dessas etapas que geram um pouco de confusão são a criação do ambiente de trabalho, a utilização de marcadores de status nos e-mails, a escolha por arquivos mais adequados a iniciantes, as ferramentas de patches, e outras.

Pensando nisso, o time de tradução pode ajudar os(as) tradutores(as) principiantes através de um processo sumário, pelo qual inicia-se rapidamente a traduzir enquanto acostuma-se com o fluxo completo:

 * Manifeste-se na lista para verificar se há algum(a) participante disponível para ajudar na tradução sem que você tenha que passar (por enquanto) pelas etapas mais complicadas;
 * Se alguém estiver disponível, você receberá um pequeno arquivo texto para traduzir, com algumas poucas instruções de edição e formatação;
 * Faça a tradução deste arquivo e envie-o de volta à pessoa;

Em resumo, alguém do time de tradução fica responsável por obter um arquivo simples, sinalizar a lista de discussão sobre os status, fazer a revisão e os testes.

O(A) tradutor(a) iniciante pode acompanhar como tudo acontece através da lista de discussão. E rapidamente já está contribuindo para o Debian. Contudo, lembre-se que este processo é **temporário** e deve ser acompanhado em paralelo pelo aprendizado do fluxo completo, a seguir.