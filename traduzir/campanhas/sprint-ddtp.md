---
title: Sprint DDTP
description: 
published: true
date: 2025-03-11T18:27:22.882Z
tags: 
editor: markdown
dateCreated: 2025-03-08T15:07:30.780Z
---

# Sprint DDTP

Esta página reúne informações sobre a organização do *sprint* de tradução utilizando o sistema [DDTP](https://www.debian.org/international/l10n/ddtp)/[DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_B) com data prevista para **março de 2021**.

Esta página é direcionada para **os(as) organizadores(as)**. Caso queira participar, entre em contato! Nossos canais de comunicação podem ser encontrados [aqui](/traduzir).

## Aprendizado das últimas campanhas

- Sprints funcionam e geram resultados;
- É importante acompanhar novos(as) participantes em interações "1 para 1";
- Divulgação via Telegram e não só IRC/e-mail;
- Indicação de materiais para tradução, para que as pessoas recém-chegadas não se percam;
- Uma grande quantidade de material de apoio (apresentação da equipe, fluxograma, orientações por frente de tradução) foi criada em 2018 e revisada em 2020;

## Objetivo desta iniciativa

**Realizar um *sprint* de uma semana de tradução de descrição de pacotes pelo sistema [DDTP](https://www.debian.org/international/l10n/ddtp)/[DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_B), enfatizando o ingresso de novas pessoas na equipe.**

- Fazer uma videochamada  (ou mais) dia 20 de março (sábado), às 20h, para recepção de iniciantes e orientações.
- Os(As) atuais contribuidores(as) se disponibilizam durante a semana para ajudar.

## Orientações preliminares

- Focar no [DDTP](https://www.debian.org/international/l10n/ddtp)/[DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_B), que tem processos mais simples;
- Divulgar nas redes sociais;
- Reunir por videochamada para acolhida e instruções iniciais;
- Oferecer ajuda "real time" aos(às) iniciantes;
- Divulgar os resultados posteriormente em uma das lives;

## Organização

Os itens marcados com :warning: precisam ser verificados:

- Previsão de datas e horários de disponibilidade; ✅

Quem gostaria de participar; ✅
1. Thiago (tico)
1. Fred Maranhão
1. Berlim
1. Charles Melara
1. Daniel Lenharo
1. Paulo Santana (phls)

Proposta de banner para chamar o evento pelas redes; ✅

![banner.proposta.png](/assets/banner.proposta.png =500x500)

- Definição da estrutura da recepção; ✅

1. Boas vindas
1. A equipe l10n-pt-BR
   1. Portal e canais de comunicação
1. Referências de tradução
   1. Manual de estilo, vocabulário, ajuda
1. Apresentação DDTP/DDTSS
1. Processo de tradução via DDTSS
   1. Conta
   1. Listas de descrições
   1. Responsável e abandono
   1. Descrições curtas e longas
   1. Edição e comentários
1. (Total da apresentação: 30min no máximo)

Data e hora das videochamadas via jitsi para recepção e orientações: ✅

- 21 de março (domingo), 20h;
- 23 de /março (terça-feira), 20h;
- 24 de /março (quarta-feira), 20h;

## Fechamento

Vídeochamada via jitsi de encerramento, análise do processo e dos resultados: ✅

- 1 de abril (quinta-feira), 20h;

Pontos levantados pelo Fred para nortear a discussão: ✅

- O que acertamos e o que erramos? Na opinião de quem começou esta semana e na opinião de quem já participava.
- Quando fazer o próximo sprint do DDTP?
- Deveríamos ter uma meta no próximo sprint?
- Estratégia para apresentar os(as) novos(as) voluntários(as) do DDTP aos outros projetos do Debian.
- O DDTP deve ser usado como porta de entrada para novos(as) voluntários(as)?
- Apresentação de resultados.

Pontos discutidos no fechamento :warning:

- O canal [debl10nptBR](https://t.me/debl10nptBR) do telegram foi o principal canal de comunicação pelo qual ficaram sabendo do sprint;

- As chamadas em vídeo com acompanhamento prático foram muito elogiadas;
- A interface [DDTP](https://www.debian.org/international/l10n/ddtp)/[DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_B) foi bem avaliada como porta de entrada para a equipe de tradução;
- Trabalhar no DDTSS foi mais produtivo que traduzir páginas web devido aos procedimentos mais simples;
- Podemos definir metas, nem que sejam tentativas;
- Outra porta de entrada sugerida foi arquivos [PO+Salsa](/traduzir/debconf); podemos convidar alguém da equipe para explicar;

- Descrições demoram a aparecer na versão estável, isso pode desmotivar contribuidores(as);
- Poderíamos selecionar descrições que sejam iguais na estável/teste/sid para que apareçam logo;

- Sprints desse tipo podem ser possíveis a cada 6 meses;
- Próximo sprint poderia ocorrer na [DebConf](https://debconf21.debconf.org) (agosto), !MiniDebConf Brasil (2o semestre) e na [lançamento](https://wiki.debian.org/ReleasePartyBullseye) do Bullseye;

- Selecionar pacotes com pouco texto para facilitar a participação dos iniciantes;
- Precisamos incentivar a diversidade de gênero entre participantes;
- Faltou atentar para a ausências de estatísticas pelo sistema (conseguimos com o administrador posteriormente);


## Resultados

 . **Traduções**: 100+ descrições de pacotes; ✅
 . **Revisões** : 650+ descrições de pacotes; ✅

Obs: números estão subcontabilizados, pois esquecemos de delimitar um ponto de corte para quem já tinha histórico de trabalho. O DDTSS não permite saber quantas contribuições uma pessoa fez durante um período de tempo.

## Agradecimentos

Agradecemos as pessoas abaixo pelas contribuições realizadas ao longo desses dias:

- Alexandro Souza
- Carlos Henrique Lima Melara 
- Daniel Lenharo de Souza 
- Felipe Viggiano 
- Fred Maranhão
- Gabriel Thiago 
- Luis Paulo Linares 
- Michel Recondo 
- Paulo Henrique de Lina Santana 
- Ricardo Berlim Fonseca 
- Thiago Hauck 
- Thiago Pezzo 
- Tiago Zaniquelli 
- Victor Moreira 
- Wellington Almeida 

## Encaminhamentos

- Proposta de próximo sprint : [Debian Handbook](https://hosted.weblate.org/languages/pt_BR/debian-handbook), atualização de [páginas wiki](/traduzir/wiki/status);
- Focar em notícias do Debian 11 (bullseye) já que o [lançamento](https://salsa.debian.org/ddp-team/release-notes/-/tree/master/pt-br) se aproxima;
- ~~(Criar um texto para publicizar no site [debianbrasil.org.br](http://debianbrasil.org.br) a iniciativa e resultados entre a comunidade brasileira e internacional.)~~ [Texto publicado](https://debianbrasil.org.br/blog/resultado-do-sprint-ddtp-ddtss-da-equipe-l10n-pt-br)
- Fazer um vídeo para acompanhar o procedimento que está no wiki;
