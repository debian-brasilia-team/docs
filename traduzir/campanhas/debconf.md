---
title: Campanha DebConf
description: 
published: true
date: 2025-03-11T18:51:28.423Z
tags: 
editor: markdown
dateCreated: 2025-03-11T18:39:35.286Z
---

# Campanhas de tradução dos arquivos po-debconf

Página estava contrução. Esta página é apenas uma proposta que está sendo escrita. Não se baseie nesta página para nenhum trabalho real.

## Estado das traduções

Este gráfico representa o estado atual das traduções:

http://www.arg.eti.br/tmp/podebconf-pt_BR.png

Fonte dos dados: <http://i18n.debian.org/l10n-rrd/unstable/podebconf>

Estas são as metas finais a serem atingidas:

| Cor | estado | descrição | ação | meta | pacotes |
| -- |  -- | -- | -- | -- | -- |
| 🟢 | traduzidas | mensagens que estão traduzidas | nenhuma ação é necessária | 100% | [pacotes](https://www.debian.org/international/l10n/po-debconf/pt_BR#done) |
| 🔵 | fuzzy | mensagens que estão traduzidas de forma aproximada | atualizar a tradução | 0% | [pacotes](https://www.debian.org/international/l10n/po-debconf/pt_BR#todo) |
| 🔴 | não traduzidas | mensagens que não estão traduzidas, contidas em arquivos que já foram parcialmente traduzidos | traduzir | 0% | [pacotes](https://www.debian.org/international/l10n/po-debconf/pt_BR#todo) |
| 🟡 | sem arquivo po | mensagens que não estão traduzidas, contidas em arquivos que nunca foram traduzidos | traduzir | 0% | [pacotes](https://www.debian.org/international/l10n/po-debconf/pt_BR#i18n) |

Ou seja, queremos atingir 100% de mensagens traduzidas nos arquivos po-debconf.

## Campanhas

Estas são as campanhas propostas para atingirmos 100% de mensagens traduzidas nos arquivos po-debconf:

### Campanhas em andamento

| nome da campanha | o quê | início | término | quem | como | indicador | meta | atingido | observações |
| --- | --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- | 

### Campanhas futuras

| nome da campanha | o quê | início | término | quem | como | indicador | meta | atingido | observações |
| --- | --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- | 

### Campanhas encerradas

| nome da campanha  | o quê  | início  | término  | quem  | como  | indicador  | meta  | atingido  | observações  |
| --- |  --- | --- | --- | --- | --- | --- | --- | --- | --- |
 | stretch-documentacao-1  | melhorar a documentação para tradução de po-debconf  | 01/11/2015  | 16/11/2015  | adrianorg  | editando o wiki.debianbrasil.org, editando a documentação do dpo-tools e gravando screencasts  | listagem das URLs com documentação  | conter URLs que documentem: a) o fluxo de trabalho; b) como participar da equipe  | listagem de URLs  |  |
 | stretch-voluntarios-1  | convidar voluntários para retomar a tradução dos po-debconf  | 16/11/2015  | 29/11/2015  | adrianorg  | enviando mensagem de convite na lista l10n  | quantidade de voluntários que responderem ao convite  | no mínimo 2  | 4  | voluntários: adrianorg, Júnior Santos, José de Figueiredo, Leonardo Rocha  |
 | stretch-traducao-1  | traduzir 10 mensagens  | 30/11/2015  | 13/12/2015  | adrianorg  | seguindo o procedimento padrão  | quantidade de mensagens traduzidas  | no mínimo 10  | 24  | ntlmaps phpgacl  |

## Como interpretar as informações das campanhas

Estas são as informações contidas nas campanhas:

- nome da campanha: um identificador único para a campanha
- o quê: descrição curta e objetiva da ação que será feita na campanha; é o objetivo da campanha; deve conter um verbo no infinitivo
- início: data de início da campanha
- término: data de término da campanha
- quem: nome da pessoa responsável pelo andamento da campanha
- como: procedimentos que serão executados para atender o objetivo da campanha; deve conter um ou mais verbos no gerúndio
- indicador: o que será testado para verificar o sucesso ou a falha da campanha; deve ser algo verificável
- meta: o valor do indicador que deve ser alcançado no término da campanha
- atingido: o valor do indicador que foi atingido no término da campanha
- observações: qualquer observação adicional sobre a campanha

Por exemplo:

- A campanha "nome da campanha" atuará para "o quê", iniciando em "início" e terminando em "término". A pessoa responsável pelo andamento da campanha é "quem". A campanha será executada "como". Em "término", será testado se o valor "atingido" de "indicador" satisfez a meta de "meta".
- A campanha "stretch-voluntarios-1" atuará para "convidar voluntários para retomar a tradução dos po-debconf", iniciando em "16/11/2015" e terminando em "29/11/2015". A pessoa responsável pelo andamento da campanha é "adrianorg". A campanha será executada "enviando mensagem de convite na lista l10n". Em "29/11/2015", será testado se o valor "1" de "quantidade de voluntários que responderem ao convite" satisfez a meta de "no mínimo 2".
- A campanha "stretch-traducao-1" atuará para "traduzir 10 mensagens", iniciando em "30/11/2015" e terminando em "13/12/2015". A pessoa responsável pelo andamento da campanha é "adrianorg". A campanha será executada "seguindo o procedimento padrão". Em "13/12/2015", será testado se o valor "atingido" de "quantidade de mensagens traduzidas" satisfez a meta de "no mínimo 10".