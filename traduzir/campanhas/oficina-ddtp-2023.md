---
title: Oficina DDTP 2023
description: 
published: true
date: 2025-03-11T18:13:20.075Z
tags: 
editor: markdown
dateCreated: 2025-03-08T15:06:57.320Z
---

# Oficina DDTP 2023

Esta página reúne informações sobre a organização da oficina de tradução utilizando o sistema [DDTP](https://www.debian.org/international/l10n/ddtp)/[DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_BR) com data prevista para **fevereiro de 2023**.

Esta página é direcionada para **os(as) organizadores(as)**. Caso queira participar, entre em contato! Nossos canais de comunicação podem ser encontrados [aqui](/traduzir).

## Objetivo

Realizar uma oficina de dois (ou mais) dias de tradução de descrição de pacotes pelo sistema [DDTP](https://www.debian.org/international/l10n/ddtp)/[DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_BR), enfatizando o ingresso de novas pessoas na equipe.

- Utilizaremos videochamada Jitsi.
- Aproveitaremos a comemoração do [I love free software day](https://fsfe.org/activities/ilovefs/index.html) como mote.
- Poderemos tentar traduzir os pacotes PopCon500 - faltam aproximadamente 100 descrições.


## Divulgação

- Email enviado para as listas:
  - [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese/2023/02/msg00014.html)
  - [debian-news-portuguese](https://lists.debian.org/debian-news-portuguese/2023/msg00001.html)
  - [debian-user-portuguese](https://lists.debian.org/debian-user-portuguese/2023/02/msg00000.html)
- [Twitter Debian Brasil](https://twitter.com/debianbrasil/status/1625239667665772544)
- Grupos no Telegram, Linkedin (divulgado pela comunidade)

## Organização

Itens marcados com :warning precisam ser verificados. Itens marcados com ✅ foram concluídos:

Quem gostaria de participar; ✅
1. Thiago (tico)
1. Charles Melara
1. Paulo Santana (phls)

Proposta de banner para chamar o evento pelas redes; ✅

![oficina-ddtss-banner.png](/assets/oficina-ddtss-banner.png)

Definição da estrutura da recepção; ✅
1. Boas vindas
1. A equipe l10n-pt-BR
    1. Portal e canais de comunicação
    1. Visão panorâmica das frentes de tradução
1. Breve introdução às referências de tradução
    1. Manual de estilo, vocabulário, ajuda
1. Apresentação DDTP/DDTSS
1. Processo de tradução via DDTSS
    1. Conta
    1. Listas de descrições
    1. Responsável e abandono
    1. Descrições curtas e longas
    1. Edição e comentários
1. (Total da apresentação: 30min no máximo)

Data e hora das videochamadas via Jitsi para recepção e orientações: ✅
- 14 de fevereiro (terça-feira), 20h-22h;
- 15 de fevereiro (quarta-feira), 20h-22h;

## Fechamento e resultados

Documento para registro dos participantes, das traduções e das revisões: [OficinaDDTP2023 pad](https://pad.riseup.net/p/OficinaDDTP2023-keep)

Lista dos 500 pacotes mais populares (com tamanho da descrição): [Lista top 500](https://pad.riseup.net/p/popcon_top500_wc-keep)

Videochamada via Jitsi de encerramento, análise do processo e dos resultados: ✅

Participantes: ✅
- Inscritos(as): 29
- Novos(as) contribuidores(as): 22
- Traduções: 175
- Revisões: 261

## Encaminhamentos

- E-mail de fechamento para a lista de discussão  ✅
- Notícia nos canais da comunidade :warning:
