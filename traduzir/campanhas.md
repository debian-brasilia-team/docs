---
title: Campanhas
description: 
published: true
date: 2025-03-11T18:39:46.616Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:32:58.076Z
---

# Campanhas para tradução;

Esta página reúne as campanhas realizadas pela equipe de tradução [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese).

## 2023

- Fevereiro de 2023: [Oficina DDTP/DDTSS](/traduzir/campanhas/oficina-ddtp-2023) para iniciantes.

## 2022

- Novembro de 2022: [Apresentação](/traduzir/campanhas/campus-party-2022) na Campus Party sobre nosso trabalho de tradução.

## 2021

- Março de 2021: [Sprint](/traduzir/campanhas/sprint-ddtp) de tradução das descrições de pacotes e incentivo para entrada de novos(as) colaboradores(as).
- Abril de 2021: Sprint de tradução do livro [DebianHandbook](https://debian-handbook.info) e incentivo para entrada de novos(as) colaboradores(as).

## 2020

- Maio de 2020: [Sprint](https://wiki.debian.org/Sprints/2020/l10n-portuguese) de tradução das páginas web que ocorreu durante os dias da MiniDebCamp on-line.

## 2018

- Fevereiro de 2018: [Campanha](/traduzir/campanhas/campanha-novos-colaboradores) para atrair novos(as) colaboradores(as).

### Sugestão para criação de páginas

Sugerimos que a criação de páginas para novos eventos mantenham (mas não se limitem a) uma estrutura mínima de tópicos:

 1. Aprendizado das últimas campanhas (revisão dos resultados dos últimos eventos);
 1. Objetivos (definição clara e bem delimitada do que se pretende);
 1. Orientações preliminares (ideias para nortear a organização);
 1. Organização (detalhamento das etapas e procedimentos);
 1. Fechamento (procedimentos finais, feedback e reunião de informações);
 1. Resultados (apresentação do resultado dos objetivos);
 1. Encaminhamentos (possibilidades a partir do fechamento e resultados);

## Campanha antiga

[Tradução DebConf](/traduzir/campanhas/debconf)

