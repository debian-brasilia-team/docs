---
title: Manpages
description: 
published: true
date: 2025-03-09T22:49:21.476Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:22:24.278Z
---

# Tradução das manpages

> As orientações nessa página precisam ser melhoradas.
{.is-warning}

## Descrição

Este projeto visa a tradução de páginas de manual de [pacotes específicos do Debian](traduzir/pacotes-nativos-debian), tais como debconf, fakeroot, dpkg e kernel-package. Pois já existe na [ldp-br (translation team for brazilian portuguese)](https://translationproject.org/team/pt_BR.html) um projeto de tradução de páginas de manual mais comuns.

Veja o status dos arquivos nessa página: <https://www.debian.org/international/l10n/po4a/pt_BR>

## Ajudando

O process de tradução é parecido com os arquivos [debconf](/traduzir/debconf)

Lembre-se alguns termos nas páginas de manual são padronizados e devem ser traduzidos da seguinte forma:

| Inglês | Português |
| --- | --- |
| NAME | NOME |
| SYNOPSIS | RESUMO |
| DESCRIPTION | DESCRIÇÃO |
| OPTIONS | OPÇÕES |
| ENVIRONMENT | AMBIENTE |
| AUTHOR | AUTOR |
| CAVEATS | PROBLEMAS |
| FILES | ARQUIVOS |

Qualquer dúvida entre em contato na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR)
