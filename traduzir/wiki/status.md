---
title: Status
description: 
published: true
date: 2025-03-09T23:41:53.847Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:39:01.926Z
---

# Status da tradução das páginas wiki =

## Páginas traduzidas

Se você traduziu alguma página wiki, insira ela abaixo para que possamos revisar.

Você também deve avisar na lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese)

| Página | Traduzida | Tradutor(a) | Revisado | Revisor(a) |
| ---- | ---- | ---- | ---- | ---- |
| <https://wiki.debian.org/pt_BR/Accessibility/Orca> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/AdvancedBuildingTips> | Sim | skizzhg | | |
| <https://wiki.debian.org/pt_BR/Alioth> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/AndroidTethering> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Apache> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Apt> | Sim | fioddor | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/BIOS> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/BSP/BeginnersHOWTO> | Sim | FernandoIke | | |
| <https://wiki.debian.org/pt_BR/BTS/BugTag> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/BTS/HowTo> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/BTS> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Bash> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/BootLoader> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/BridgeNetworkConnections> | Sim | nodiscc | | |
| <https://wiki.debian.org/pt_BR/BugReport> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/BuildingALinuxDomain> | Sim | nodiscc | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/BuildingTutorial> | Sim | skizzhg | | |
| <https://wiki.debian.org/pt_BR/Bumblebee> | Sim | Diego Alonso | | |
| <https://wiki.debian.org/pt_BR/CDDVD> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/ChangeLanguage> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Chromium> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/CommandLineInterface> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/Community> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/Console> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/ConvertNetToStatic> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/CornerCase> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DDPO> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DHCP_Client> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DHCP_Server> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/DataBase> | Sim | LiaTakiguchi | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DatabaseServers> | Sim | LiaTakiguchi | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DateTime> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DebConf> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/Debhelper> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianAcerOne> | Sim | PaulWise | | |
| <https://wiki.debian.org/pt_BR/DebianAlternatives> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianArt/Mascot> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianArt/StructureThemePackage> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianArt/Themes> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianArt> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianBo> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianBookworm> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianBullseye> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianBuster> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianBuzz> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianDay> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianDesktop/Artwork/MakeSysImage> | Sim |  ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianDesktop/Artwork/Requirements> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianDeveloper/JoinTheProject/NewMemberCheckList> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianDeveloper/JoinTheProject/NewMember> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianDeveloper/JoinTheProject> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianDeveloper> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianDevelopment> | Sim | fioddor | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DebianDocumentation> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianEdu> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianEtch> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianEvents> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianExperimental> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianFAQ> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianForNonCoderContributors> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianForky> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianFreeSoftwareGuidelines> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianGNUstep> | Sim | slackjeff | | |
| <https://wiki.debian.org/pt_BR/DebianGis> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianHamm> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianHistory> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianInstaller/Accessibility> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianInstaller/SataRaid> | Sim | fsbano | | |
| <https://wiki.debian.org/pt_BR/DebianInstaller/Today> | Sim | PaulWise | | |
| <https://wiki.debian.org/pt_BR/DebianInstaller> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/DebianIntroduction> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianJessie> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianKDE> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianLenny> | Sim | AndreasRönnquist | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DebianLogo> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianMailingLists/SearchHowto> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianMailingLists> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianMaintainer> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianNPDatabases> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianNetworkInstall> | Sim | AndreaNeroni | | |
| <https://wiki.debian.org/pt_BR/DebianOldStable> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianPolicy> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianPotato> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianReference/Network> | Sim | nodiscc | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DebianReleases/PointReleases> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianReleases> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianRepository> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianResources> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianRex> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianSarge> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianSlink> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianSpamAssassin> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianSqueeze> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianStability> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianStable> | Sim | fioddor | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DebianStretch> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianTesting> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianTrixie> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianUnstable> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianUpgrade> | Sim | nodiscc | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DebianWheezy> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWiki/Contact> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWiki/Content> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWiki/EditorGuide> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWiki/EditorQuickStart> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWiki/FAQ> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWiki/FeedBack> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWiki> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWomen/BuildItEvent> | Sim | MonicaRamirezArceda | | |
| <https://wiki.debian.org/pt_BR/DebianWomen/History> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWomen/IRC20November2004> | Sim | skizzhg | | |
| <https://wiki.debian.org/pt_BR/DebianWomen/Index> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWomen/Profiles> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWomen/Projects> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWomen/PythonTutorial> | Sim | Diego Alonso | | |
| <https://wiki.debian.org/pt_BR/DebianWomen> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DebianWoody> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Debian> | Sim | fioddor | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Debootstrap> | Sim | vauss | | |
| <https://wiki.debian.org/pt_BR/DesertIslandTest> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DesktopDefaultSettings> | Sim | slackjeff | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DesktopEnvironment> | Sim | slackjeff | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DialupConf> | Sim | nodiscc | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/DisplayManager> | Sim | ThiagoPezzo| | |
| <https://wiki.debian.org/pt_BR/DissidentTest> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/DontBreakDebian> | Sim | ThiagoPezzo| | |
| <https://wiki.debian.org/pt_BR/Emulator> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/EnvironmentVariables> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Exim> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/FAQ> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/FTP> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/FileSystem> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/FilesystemHierarchyStandard> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Firefox> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Firmware> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Floppy> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/FluxBox> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Fonts> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/FreeDesktop> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/FreeSoftwareFoundation> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/FreeSoftware> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/FromWindowsToDebian> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/FrontPage> | Sim | AlbanVidal | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/GNU> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/GPL> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Game> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Gamepad> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Games/GameDataPackager> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Games/Links> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Games/PlayIt> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Games/Sponsors> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Games/Suggested> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Games> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Gnome> | Sim | leela52452 | | |
| <https://wiki.debian.org/pt_BR/GraphicsCard> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Grub> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Handheld> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/HardDisk> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/HardwareDesignedForFreedom> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Hardware> | Sim | gordao300 | Sim | ThiagoPezzo | | | |
| <https://wiki.debian.org/pt_BR/HelpDebian/FAQ> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/HelpDebian/WithDocumentation> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/HelpDebian/WithPackages> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/HelpDebian> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Help> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/HowToGetABacktrace> | Sim | RafaelFontenelle | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/HowToIdentifyADevice> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/HowToPackageForDebian> | Sim | BeatriceTorracca | | |
| <https://wiki.debian.org/pt_BR/HowtoProxyThroughSSH> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/HowtoRecordVNC> | Sim | leela52452 | | |
| <https://wiki.debian.org/pt_BR/HowtoSendWithEximThroughSSH> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/IRC> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/ImapProxy> | Sim |  RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/InstallFAQ> | Sim | JPierreGiraud | | |
| <https://wiki.debian.org/pt_BR/InstallingDebianOn/Acer/AspireOne> | Sim | PaulWise | | |
| <https://wiki.debian.org/pt_BR/InstallingDebianOn/PageFragments/AdditionalSuggestions> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/InstallingDebianOn/PageFragments/Philosophy> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/InstallingDebianOn> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/InstantMessaging> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/InterWiki> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/KDE> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/KernelModuleBlacklisting> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Kernel> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Keyboard/ABNT2> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Keyboard/AK-N801P> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Keyboard/Chaintech_9CJS> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Keyboard/MultimediaKeys> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Keysigning/Coordination> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Keysigning> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/L10n/Portuguese> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/L10n> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/LDAPFormatAliases> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/LDAPFormatFstab> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/LDAPFormatInternalVertices> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/LDAPFormatNisMap> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/LDAP> | Sim | nodiscc | | |
| <https://wiki.debian.org/pt_BR/LILO> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/LTS/Extended> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/LTS/FAQ> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/LTSP/Howto> | Sim | anônimo | | |
| <https://wiki.debian.org/pt_BR/LTSP> | Sim | nodiscc | | |
| <https://wiki.debian.org/pt_BR/LTS> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/LaMp> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Linux> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Locale> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Lynx> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/MIME> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Memory> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Merchandise> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Modem> | Sim | nodiscc | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Motherboard> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Multiarch/HOWTO> | Sim | MarceloSantana | | |
| <https://wiki.debian.org/pt_BR/Multiarch> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/NFSServerSetup> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/NFSTroubleshooting> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/NFS> | Sim | leela52452| | |
| <https://wiki.debian.org/pt_BR/NTP> | Sim | leela52452 | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Ncurses> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/NetworkBooting> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/NetworkConfiguration> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/NetworkConfiguration> | Sim | nodiscc | | |
| <https://wiki.debian.org/pt_BR/NetworkManager> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/NetworkManager> | Sim | leela52452 | | |
| <https://wiki.debian.org/pt_BR/Network> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/News> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Nginx> | Sim | skizzhg | | |
| <https://wiki.debian.org/pt_BR/OpenSourceHardware> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/OpenSource> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/OracleDB> | Sim | nodiscc | | |
| <https://wiki.debian.org/pt_BR/POSIX> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/PPP> | Sim | nodiscc | | |
| <https://wiki.debian.org/pt_BR/PackageManagement/Searching> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/PackageManagement> | Sim | nodiscc | | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Packaging/BinaryPackage> | Sim | ThiagoPezzo |  |  |
| <https://wiki.debian.org/pt_BR/Packaging/Intro> | Sim | ThiagoPezzo |  |  |
| <https://wiki.debian.org/pt_BR/Packaging/Learn> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Packaging/Pre-Requisites> | Sim | ThiagoPezzo |  |  |
| <https://wiki.debian.org/pt_BR/Packaging/SourcePackage> | Sim | ThiagoPezzo |  |  |
| <https://wiki.debian.org/pt_BR/PackagingWithGit> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Packaging> | Sim | EmersonQueiroz | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Part-UUID> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Partition> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/PkgExim4> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Portal/IDB> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/PostfixAndSASL> | Sim | nodiscc | | |
| <https://wiki.debian.org/pt_BR/Postfix> | Sim | odiscc | | |
| <https://wiki.debian.org/pt_BR/Printing> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/ProgrammingApplication> | Sim | fioddor | | |
| <https://wiki.debian.org/pt_BR/ProgrammingLanguage> | Sim | fioddor | | |
| <https://wiki.debian.org/pt_BR/ProjectNews/HowToContribute> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/ProjectNews> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Promote/ButtonsAndBanners> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Promote/PrintAdvertising> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Promote/SocialNetworking> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Promote> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/ProxyAutodetectConf> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/QuickInstall> | Sim | brunobuys | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Rdesktop> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/RemoteDesktop> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Root> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/SSH> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/SVN> | Sim | leela52452 | | |
| <https://wiki.debian.org/pt_BR/Salsa/Doc> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Salsa/SSO> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Salsa/support> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Salsa> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Samba/ClientSetup> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Samba/DcWithLdapBackend> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Samba/ServerSetup> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/SecuringNFS> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/ShellCommands> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/ShellConfiguration> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/ShellScript> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Shell> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/SimpleBackportCreation> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/SocialContract> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Software> | Sim | AndreaNeroni | | |
| <https://wiki.debian.org/pt_BR/SoundCard> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/SourcesList> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Statistics> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Steam> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Subkeys> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Synaptic> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/SystemAdministration> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/SystemBootProcess> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Teams/Welcome/Translators> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Teams> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/TerminalEmulator> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/TextEditor> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/TheHurd> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/TheUnixWay> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/The_Linux_Foundation> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/TimeZoneChanges> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/TorBrowser> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/TroubleShooting> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/USB> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/UsingQuilt> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/VNCviewer> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/WNPP> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/WakeOnLan> | Sim | nodiscc | | |
| <https://wiki.debian.org/pt_BR/WebBrowsers> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/WebServers> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Webcam> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Welcome/Contributors> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/WhyDebian> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/WiFi> | Sim | nodiscc | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/WikiTag> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Wireless> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/Xen> | Sim | hudsonfas| | |
| <https://wiki.debian.org/pt_BR/Xfce> | Sim | fioddor | | |
| <https://wiki.debian.org/pt_BR/Xorg> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/Xsession> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Year> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/Zope> | Sim | RicardoCosta | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/accessibility-maint> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/accessibility> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/acessibilidade> | Sim | ThiagoPezzo | Sim | PauloSantana |
| <https://wiki.debian.org/pt_BR/apcupsd> | Sim | Daniel Roque | | |
| <https://wiki.debian.org/pt_BR/debconf> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/debian/changelog> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/debian/patches> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/debian/upstream/edam> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/debian/upstream> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/debian/watch> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/fstab> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/i3> | Sim | slackjeff | Sim | ThiagoPezzo |
| <https://wiki.debian.org/pt_BR/iwconfig> | Sim | brunobuys | | |
| <https://wiki.debian.org/pt_BR/netconf> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/plymouth> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/pppd> | Sim |  ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/reportbug> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/sSMTP> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/sbuild> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/systemd> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/udev> | Sim | ThiagoPezzo | | |
| <https://wiki.debian.org/pt_BR/vsftpd> | Sim | RicardoCosta | | |
| <https://wiki.debian.org/pt_BR/w3m> | Sim | ThiagoPezzo | | |
| https://wiki.debian.org/pt_BR | Sim | AlbanVidal | Sim | ThiagoPezzo |

## Páginas com prioridade para traduzir

- <https://wiki.debian.org/Packaging/ComplexWebApps>
- <https://wiki.debian.org/PackagingFAQ>
- <https://wiki.debian.org/Packaging/HackingDependencies>
- <https://wiki.debian.org/PackagingLessCommonBinutilsTargets>
- <https://wiki.debian.org/Packaging/ruby-team-meta-build>
- <https://wiki.debian.org/Packaging/WikiCleanUp>
- <https://wiki.debian.org/PackagingWithDarcs>
- <https://wiki.debian.org/PackagingWithDarcsAndTailor>
- <https://wiki.debian.org/PackagingWithDocker>
- <https://wiki.debian.org/PackagingWithGit/Build>
- <https://wiki.debian.org/PackagingWithGit/GitDpm>
- <https://wiki.debian.org/PackagingWithGit/GitDpm/ImportDsc>
- <https://wiki.debian.org/PackagingWithGit/GitDpm/Initialize>
- <https://wiki.debian.org/PackagingWithGit/GitDpm/SimilarEnough>
- <https://wiki.debian.org/PackagingWithGit/Svn-buildpackageConversion>