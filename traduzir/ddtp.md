---
title: DDTP
description: 
published: true
date: 2025-03-09T22:34:21.046Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:20:24.069Z
---

# Tradução das descrições de pacotes

O **Projeto de Tradução das Descrições de Pacotes Debian** (**DDTP** - Debian Description Translation Project) é um projeto internacional cujo principal objetivo é traduzir as descrições dos pacotes do Debian. Isso auxilia as pessoas que não leem inglês a encontrarem pacotes que sejam úteis a suas necessidades. O DDTP foi implementado pelo alemão *Michael Bramer* e fornece a infraestrutura dos processos de tradução e de revisão. Desde a DebConf8 o DDTP foi oficialmente integrado aos repositórios do projeto Debian. . 

Durante anos, a principal interface com o DDTP foi o e-mail, até que *Martijn van Oosterhout* desenvolveu o  Satélite do Servidor de Traduções Distribuídas Debian ([DDTSS](https://ddtp.debian.org/ddtss/index.cgi/xx) - Debian Distributed Translation Server Satellite). O DDTSS é uma interface web que interage com o DDTP e seu banco de dados de forma simples e direta, e é administrado pelo [Thomas Vincent](thomas@vinc-net.fr)

A **equipe brasileira** foi a segunda a aderir ao DDTP e se mantém entre as mais ativas, graças ao trabalho de muitos(as) voluntários(as). É muito fácil colaborar com o DDTP, já que o processo de tradução e revisão é feito pelo navegador web e os textos são normalmente pequenos. Um curto espaço de tempo por dia pode ajudar muito a equipe.

> Acesse a [página da equipe brasileira](https://ddtp.debian.org/ddtss/index.cgi/pt_BR) no DDTSS.
{.is-info}

Também considere ajudar a traduzir [outras partes](/traduzir) do Debian. Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

## Documentação oficial

- [Manual de políticas Debian](https://www.debian.org/doc/debian-policy), mais especificamente o item [3.4 A descrição de um pacote](https://www.debian.org/doc/debian-policy/ch-binary.html#s-descriptions)

- [Referência para desenvolvedores(as) Debian](https://www.debian.org/doc/developers-reference), mais especificamente o item [6.2 Melhores práticas para debian/control](https://www.debian.org/doc/developers-reference/best-pkging-practices.html#bpp-desc-basics), subitens 6.2.1, 6.2.2 e 6.2.3.


## Mecanismo de funcionamento

O **DDTSS** é um sistema muito fácil de usar. Basta fazer o login, escolher um pacote e fazer a tradução ou a revisão nos campos apropriados. Clique em aceitar as alteração e pronto. Muito simples!

Após a tradução inicial, são necessárias **três revisões sem alterações** para que uma tradução seja enviada para o banco de dados e, posteriormente, incluída nos repositórios oficiais. Quando se propõe mudanças de revisão, a contagem de revisão é zerada, e novamente três revisões sem novas alterações são necessárias.

### Papel de tradutor(a) e revisor(a)

- **Tradutor(a)**: é a pessoa que realiza a tradução inicial de um pacote, ficando responsável (owner) por ela.
- **Revisor(a)**: é a pessoa que revisa a tradução. Quando um(a) revisor(a) propõe uma alteração, assume o papel de responsável (owner) por aquele pacote.

Um(a) tradutor(a) também atua como revisor(a), bastando que revise os pacotes traduzidos por outras pessoas. Ou seja, os papeis do(a) usuário(a) vão se alternando para cada pacote na medida em que se trabalha nas descrições.

### Interface do sistema

O sistema apresenta as seguintes listas de pacotes:

- Há pacotes **Pending translation** (aguardando tradução). Ao lado de cada um consta uma *prioridade*. Pacotes com prioridade maior representam pacotes que são mais vistos pelos(as) usuários(as).
- Há pacotes **Pending review** (aguardando revisão). Foram traduzidos por outras pessoas e aguardam na fila.
- Há pacotes **Reviewed by you** (já revisados por você). Ao passar pela sua revisão, esta não contará novamente para as três revisões necessárias. Mas você pode modificá-los novamente, bastando acessar a descrição.
- Há pacotes **Recently translated** (recentemente traduzidos). Se ao lado de algum nome constar a expressão *check*, verifique o que aconteceu.

A lista de pacotes para traduzir é incrementada automaticamente quando a quantidade de descrições está baixa. As descrições finalizadas só são enviadas ao repositório quando uma certa quantidade de revisões está pronta.

## Metas permanentes

:star: Sugestão da próxima meta: classificar pacotes por [popcon/uso regular](https://popcon.debian.org/stable/by_vote).

A meta de manter 100% das traduções para os pacotes de alta prioridade: **Required**, **Important** e **Standard**, e para os pacotes do **POPCON500**, foi alcançada! Consulte a seção "Outras referências" abaixo para estatísticas por prioridade e por idioma de cada versão do Debian.


## Processo de tradução usando DDTSS

:speech_balloon: Antes de iniciar as traduções, pedimos que você inscreva-se na lista de e-mails [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese). Este é nosso canal de comunicação principal e um requisito essencial para a boa coordenação dos trabalhos. Mais informações e outras formas de contato estão disponíveis na [página da equipe de tradução](/traduzir).

### Criação da conta

O primeiro passo é criar uma conta. A conta permite que você acompanhe seu trabalho mesmo em diferentes computadores, independente do navegador ou do computador que você use. O sistema também mantém estatísticas das traduções e revisões que você realizou.

- Para criar sua conta acesse o site <https://ddtp.debian.org/ddtss/index.cgi/createlogin>.

:warning: O certificado é autoassinado, por isso é provável que o navegador reclame dele.

- Informe um endereço de e-mail válido, para que você receba as informações para ativar sua conta. Seu *alias*  (apelido) será seu login. Seu nome verdadeiro é usado para referência dentro do banco de dados. E, claro, entre com sua senha.

- Uma vez que sua conta foi criada e você já tenha feito a ativação, faça login no sistema em <https://ddtp.debian.org/ddtss/index.cgi/login>.

> Se você **não** receber um email pedindo a confirmação para a criação da conta, envie um para o [Thomas Vincent](thomas@vinc-net.fr) pedindo que ele libere o seu login. Isso tem acontecido principalmente com emails do gmail. *There's an issue with accounts created using a gmail address. DDTSS sends a confirmation e-mail upon account creation that is deemed as spam by Google.* 
{.is-warning}

### Descrição curta e descrição longa

A descrição de todo pacote é composta por duas partes, uma descrição curta e uma descrição longa.

A **descrição curta** deve seguir as seguintes diretrizes:

- uma única linha, deve ser breve e sumária;
- conter até 80 caracteres e sem ponto final;
- não incluir nome de pacote, mas pode incluir nome de aplicativo;
- deve começar com letra minúscula, exceto quando nome próprio ou sigla;
- evite o uso de artigos, definidos ou indefinidos - seja direto;

A **descrição longa** deve seguir as seguintes diretrizes:

- descreve o pacote, suas dependências e conflitos;
- pode conter de duas a cinco linhas, ou vários parágrafos;
- deve apresentar sentenças curtas e completas;
- evite tabulações no texto;
- quebras de linha são entendidas como espaços entre palavras;
- parágrafos são separados por uma linha que contém somente um ponto (símbolo: {{{.}}});
- mantenha a diagramação dos textos e das listas;

### Tradução e revisão

Aqui começa o trabalho braçal :-)

Sugerimos que você comece **revisando**. É uma maneira de se acostumar com os padrões do sistema e com os hábitos da equipe. Mas os processos de tradução e de revisão seguem um mesmo padrão:

 1. **Escolher** um pacote;
 1. **Digitar** nos campos apropriados;
 1. **Salvar** ou **Abandonar** a tradução.

Quando se trata de uma nova descrição, o campo de tradução contém a expressão ```<trans>```, indicando que o parágrafo deve ser traduzido. É preciso então substituir essa expressão pela tradução do texto fornecido mais acima na página.

Quando um pacote tem um parágrafo da descrição idêntico a outro pacote já traduzido, o DDTSS **recupera** a tradução como sugestão, inserindo-a no campo de texto. O sistema somente deixará a expressão ```<trans>``` nos parágrafos que precisam de uma nova tradução.

Quando uma revisão acontece, o DDTSS marca as alterações usando cores. **Verde** para o que foi adicionado e **vermelho** para o que foi retirado. É uma maneira de auxiliar o trabalho dos(as) tradutores(as).

Pacotes que já foram traduzidos podem retornar para uma nova tradução. Por exemplo, se o(a) mantenedor(a) de um pacote muda *Gnome* para *GNOME* na descrição, o pacote aparece novamente na lista. Felizmente o DDTP tem um **histórico** e gera um ```diff``` da versão anterior. Isso permite que você possa identificar se as mudanças foram significativas ou se foi algo simples, como o exemplo acima. Este bloco de informações aparece após a área de tradução, quase no final da página.

### Vocabulário (wordlist)

O DDTSS permite que um arquivo com uma [lista de palavras (wordlist)](https://ddtp.debian.org/ddtss/index.cgi/pt_BR/wordlist) seja carregada pela interface. Ao se passar o ponteiro do mouse sobre a palavra, uma nota surge com as sugestões de tradução e observações. Com isto, podemos uniformizar a tradução de termos recorrentes ou complicados.

Este arquivo é derivado do **vocabulário padrão** da equipe de tradução. Caso queira consultá-lo ou deseje incluir ou alterar termos, veja a página do [vocabulário padrão](/traduzir/vocabulario).

:speech_balloon: Contribua com o vocabulário, anotando os termos e suas traduções enquanto trabalha.

### O campo de comentários

Nós usamos essa área para identificar quem trabalhou no pacote e para deixar observações, recomendações e dicas para outros(as) tradutores(as). **Sempre assine** seu trabalho no campo de comentários, que fica logo abaixo do campo de tradução.

Utilize o formato a seguir para padronizar as comunicações:
```
AAAAMMDD: NOME: AÇÃO. COMENTÁRIO.
```

onde ```AAAA``` é o **ano**, ```MM``` é o **mês** e ```DD``` é o **dia**. O nome pode ser preenchido com o **nome** ou o **apelido** de quem está realizando a ação. A ação deve indicar se foi realizada uma **tradução**, **revisão** ou trata-se de uma sugestão ou observação.

Um exemplo típico de comunicação utilizando-se o campo de comentários:

```
   20050607: MachadoAssis: tradução.
   20050608: MonteiroLobato: revisão. Sugiro usarmos "extensão" para a tradução de "plugin".
   20050609: CeciliaMeireles: revisão. Sugestão aceita.
   20050609: MachadoAssis: revisão.
```

### Enviar ou abandonar

Ao fim do trabalho de tradução, clique no botão **Submit** (enviar) para atualização do sistema. **Nunca** clique no botão de voltar do navegador quando não quiser continuar com o trabalho de tradução. Para isso, utilize o botão **Abandon** (abandonar), de forma que o sistema retire a responsabilidade (owner) do(a) seu(sua) usuário(a).

Ao fim do trabalho de revisão, clique no botão **Accept as is** (Aceitar como está) para confirmar o texto traduzido sem modificações. Clique em **Accept with changes** (Aceitar com alterações) para confirmar o texto revisado com suas modificações. Clique no botão **Change comment only** (Altere o comentário somente) caso tenha alterado somente este campo.

## Dicas

- ⚡ Instale um **corretor ortográfico** no seu navegador e use-o nos campos de texto. Isso ajuda demais no trabalho de tradução e revisão, e melhorará a qualidade da sua produção textual final.
- ⚡ Em descrições muito longas ou muito complicadas, não é preciso terminar tudo no mesmo dia. Traduza uma parte e **anote** no campo de comentários que o processo está em andamento. Salve e, na próxima oportunidade, busque a descrição na lista de revisões à direita da tela (Reviewed by you). Só não esqueça de retomar a tradução, pois a responsabilidade (owner) sobre o pacote ainda permanecerá com seu(sua) usuário(a).
- ⚡ Aproveite para trabalhar em pacotes **similares**. É grande a chance de possuírem as mesmas descrições iniciais, cujos parágrafos repetidos serão fornecidos já traduzidos pelo DDTSS. É uma forma de agilizar suas atividades.
- ⚡ Lembre-se de manter-se alinhado(a) com a **equipe de tradução**. Estabelecemos procedimentos e metas para facilitar o trabalho de todos(as).

## Checklist

Uma lista de verificação rápida para acompanhar o trabalho de tradução:

**Geral**

- [ ] ligue o corretor ortográfico do navegador
- [ ] use o botão ABANDONAR na seção de tradução, não clique em voltar no navegador
- [ ] anote o comentário

**Descrições curtas**

- [ ] inicia com letra minúscula ou Nome/Sigla
- [ ] quantidade de caracteres <= 80
- [ ] não inserir nome do pacote ou ponto final
- [ ] evitar artigos, definidos ou indefinidos

**Descrições longas**

- [ ] manter a diagramação do original
- [ ] evitar artigos, definidos ou indefinidos
- [ ] sentenças completas, usar ponto final
- [ ] não usar TAB, não colocar linha em branco
- [ ] linha em branco entre parágrafos: um ponto final

## Dúvidas comuns

Algumas dúvidas e respostas.

### Como corrigir uma tradução que já foi realizada? Ao tentar puxar a tradução, o DDTSS informa "All descriptions for package <nome-do-pacote-> already translated"?
  
O DDTSS evita que pacotes que já foram traduzidos voltem para a fila de tradução, mas às vezes identificamos um erro de digitação ou alguma melhoria que pode ser feita em pacotes que já foram traduzidos. Neste caso, acesse o DDTSS, informe o nome do pacote no campo **Fetch specific description** (Busque uma descrição específica) e marque a opção **Force fetching even if not untranslated** (Force a recuperação mesmo que já traduzida).

### O que fazer ao encontrar um erro na descrição em inglês?

Se for um erro de digitação e a forma correta está clara, faça a tradução como se não houvesse erro.

Se o erro deixar o texto muito confuso, faça o possível para fornecer a melhor tradução possível, procure mais sobre o pacote para poder melhorar a qualidade da tradução.

Relate o erro. Se não souber como fazê-lo, veja mais informações sobre [como relatar bugs](https://www.debian.org/Bugs/Reporting). Se preferir, informe na lista de discussão DebianList:debian-l10n-portuguese, alguém se encarregará de encaminhar o bug.
- Lembre-se de marcar a tag `l10n` e a prioridade como `minor`.
- Se puder envie um patch, as descrições ficam no `debian/control`.

### Como debater sobre diferentes possibilidades de tradução para uma determinada tradução/revisão/descrição?
  
O DDTSS faz com que alterações na descrição do pacote forcem um novo ciclo de revisão. Com isso, tradutores(as) e revisores(as) têm que concordar 100% no que está escrito para que a tradução seja aprovada. Diferentes abordagens podem ser adotadas, a ordem recomendada seria:
- O campo de comentários é o principal local para *debates*. Contudo, ele só é lido no momento da tradução/revisão, ou seja, o(a) tradutor(a) original pode acabar não vendo os comentários se não houver alteração. Por isso, às vezes é preciso realizar uma modificação no texto e clicar em *Accept with changes* (Aceitar com alterações) para forçar que o ciclo de revisão seja reiniciado. Por exemplo, insira a expressão `FIXME`.
- Use o campo de comentários para justificar suas decisões. O(A) tradutor(a) original pode discordar e reverter a mudança, também se justificando. Se não houver consenso, use a marcação `FIXME(termo-em-inglês)`e peça ajuda na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese).

### Estou tentando traduzir o pacote <nome-do-pacote>, mas o DDTSS diz que não há um pacote com esse nome... o que estou fazendo errado?

O DDTP só trabalha com pacotes que são considerados software livre. Sendo assim, somente pacotes que estão na área *main* ou *contrib* dos repositórios oficiais do Debian estão disponíveis para tradução/revisão.

Um dos planos do DDTP é adicionar a capacidade de saber se pacotes na área *non-free* podem ou não ser traduzidos (se possuem uma licença que permite isso). Outro plano é a adição de pacotes em repositórios não oficiais.

Use o site <https://packages.debian.org> para verificar se o pacote é parte do repositório oficial e a qual seção ele pertence.

## Outras referências

- [Informações sobre os idiomas no DDTP](https://ddtp.debian.org)

Gráficos para cada equipe e acesso às descrições e traduções dos pacotes, e respectivas assinaturas. A parte logo abaixo do primeiro gráfico lista as traduções ativas para cada idioma.

- [Estatísticas de tradução do DDTP](https://ddtp.debian.org/stats)

Estatísticas para as diferentes versões do Debian divididas pelas prioridades dos pacotes e com uma categoria especial denominada *POPCON500*, que são os 500 pacotes mais populares dentro do Debian de acordo com o [POPCON](https://popcon.debian.org).

- [Estatísticas das equipes que usam o DDTSS](https://ddtp.debian.org/ddtss/index.cgi/xx)

Informações de quantos pacotes estão pendentes para tradução ou revisão para cada uma das equipes que usam o DDTSS.

- [Todos os pacotes traduzidos](https://ddtp.debian.org/ddt.cgi?alltranslatedpackages=$IDIOMA)

Substitua no link `$IDIOMA` por `pt_BR` (português do Brasil) ou outro código válido para ver todos os pacotes já traduzidos.

- [Todos os pacotes não traduzidos](https://ddtp.debian.org/ddt.cgi?alluntranslatedpackages=$IDIOMA)

Substitua no link `$IDIOMA` por `pt_BR` (português do Brasil) ou outro código válido para ver todos os pacotes que ainda não foram traduzidos.

# Veja também

- Mais informações sobre o [DDTP](https://www.debian.org/international/l10n/ddtp).
- [Lista de palavras (wordlist)](https://ddtp.debian.org/ddtss/index.cgi/pt_BR/wordlist) carregada pelo DDTSS.
- [Manual de estilo](/traduzir/manual-de-estilo) direcionado à equipe de tradução em português.
- [Vocabulário padrão](/traduzir/manual-de-estilo) e outras referências para suporte à tradução.
- É possível executar [uma instância local do DDTSS](https://salsa.debian.org/l10n-team/ddtp/-/blob/master/documentation/run-locally.adoc) para testes
- [Controle de desenvolvimento do DDTSS no Salsa](https://salsa.debian.org/l10n-team/ddtp/-/issues)

:star: ToDo: rever/atualizar as metas da equipe quanto ao DDTP.


