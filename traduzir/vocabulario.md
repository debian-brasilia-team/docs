---
title: Vocabulario
description: 
published: true
date: 2025-03-10T23:24:26.323Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:36:06.249Z
---

# Vocabulário padrão do Debian para português do Brasil

O **vocabulário padrão** visa a uniformidade dos termos adotados pela [equipe de tradução do português do Brasil](/traduzir) em todas as frentes de tradução do projeto Debian.

Atualmente o vocabulário é mantido neste [repositório do Salsa](https://salsa.debian.org/l10n-br-team/vocabulario). A edição é liberada e colaborativa, bastando abrir uma conta nesse sistema. 

Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

- Na página [README](https://salsa.debian.org/l10n-br-team/vocabulario) do repositório há instruções mais detalhadas.
- O arquivo está em formato markdown e pode ser automaticamente baixado e atualizado pelo git/Salsa, mantendo-se uma cópia no computador local. Pode-se assim consultá-lo facilmente por qualquer meio que o(a) tradutor(a) achar conveniente.
- Há um script para conversão automática para o formato do [DDTSS wordlist](https://ddtp.debian.org/ddtss/index.cgi/pt_BR/wordlist). A equipe tem atualizado ambos os sistemas com frequência.

## Histórico

Muitos esforços já foram feitos para se elaborar um vocabulário específico ao trabalho de tradução do Debian. Em resumo, tentavam estabelecer procedimentos padronizados e formas automáticas de acesso à lista de termos. Infelizmente o arquivo ainda não foi encontrado.

Um [histórico dessas iniciativa](/traduzir/vocabulario/historico) foi reunido, junto a uma pequena contextualização, apresentando algumas discussões que aconteceram principalmente na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese).