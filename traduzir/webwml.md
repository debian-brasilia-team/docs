---
title: Web wml
description: 
published: true
date: 2025-03-09T22:32:15.809Z
tags: 
editor: markdown
dateCreated: 2025-03-08T15:00:55.363Z
---

# Tradução das páginas web do Debian

As informações e as iniciativas do **projeto Debian** podem ser encontradas através do site [www.debian.org](https://www.debian.org) Essas páginas estão disponíveis no idioma português do Brasil devido às atividades da equipe de tradução.

No ano de 2020 foi realizado um esforço coletivo de atualização das páginas principais. Conseguimos traduzir **todas** aquelas classificadas em [desatualizadas e não traduzidas](https://www.debian.org/devel/website/stats/pt.en). Ficaram de fora somente as páginas de consultoria, em idioma que não o inglês e os arquivos de notícias antigas (sobre notícias, anúncios e comunicados de imprensa, veja [a frente de tradução de publicidade](/traduzir/publicidade)).

Também considere ajudar a traduzir [outras partes](/traduzir) do Debian. Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

## Visão geral do processo

O ciclo de tradução de uma página web pode ser assim sintetizado:

1. **Escolhe-se** um arquivo no repositório;
1. Faz-se a **tradução**;
1. Uma ou mais pessoas fazem a **revisão** da tradução;
1. Aprovado o texto, aguarda-se últimos **comentários** e possíveis modificações;
1. **Envia-se** o arquivo atualizado de volta para o repositório.

Para que o processo ocorra de forma coordenada, alguns sistemas e procedimentos foram estabelecidos:

- Os arquivos-fonte do site web ficam armazenados em um [repositório](https://salsa.debian.org/webmaster-team/webwml.git) do sistema de controle de versão **Salsa/git**, cujo acesso é público;
- Os arquivos estão no formato **wml**, que automática e periodicamente são reconstruídos para o formato *html*;
- Todo o trabalho é **coordenado** através de e-mails pela lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese):
  - Cada etapa do trabalho é informada à equipe, evitando retrabalho;
  - Um aplicativo da própria lista de discussão automaticamente atualiza o [sistema de monitoramento](https://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html);
- Ao término da tradução, o arquivo é enviado para o repositório **git** por algumas pessoas da equipe que têm acesso de escrita ao repositório;

:star: Dê uma olhada no fluxograma abaixo criado pela equipe. Ele pode ajudar na visualização do processo.

## Visão geral da coordenação pela lista de e-mails

Todo o processo de tradução e revisão se dá através da lista de e-mails  [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese). Por ela alinhamos nossos esforços e discutimos casos particulares. Mas é também pela lista que o trabalho é coordenado, utilizando **marcadores** ou [pseudo-urls](/traduzir/pseudo-urls) que são capturadas por sistemas de monitoramento. Cada e-mail enviado contém uma pseudo-url no campo de assunto, seguido do arquivo ao qual a ação se aplica, na forma: ```Assunto: [<estado>] <tipo>://<pacote>/<arquivo>```

:speech_balloon: O processo ficará muito mais claro se você acompanhar a troca de e-mails por algum tempo.

:speech_balloon: Não crie e-mails novos, sempre responda em cima do último. Isto cria um encadeamento (thread) que auxilia na coordenação dos trabalhos.

Vamos utilizar o arquivo *webwml/portuguese/legal/index.wml* como exemplo. O ciclo de trabalho da tradução ficará assim:

- Informe sua **Intenção de Traduzir (ITT)** o arquivo, para iniciar o monitoramento do processo e evitar que duas pessoas traduzam ao mesmo tempo o documento:

```[ITT] wml://www.debian.org/legal/index.wml```

- Você também pode requisitar vários arquivos de uma vez utilizando este formato:

```[ITT] wml://www.debian.org/legal/{index.wml,anssi.wml,patent.wml```}

- Terminada a tradução ou atualização do arquivo, envie sua **Requisição para Revisão (RFR)**. Responde-se à mensagem anterior, trocando [ITT] por [RFR] no assunto e removendo partículas como "Res:" ou "Re:":

```[RFR] wml://www.debian.org/legal/index.wml```

- O(A) revisor(a) responde à requisição, podendo ou não sugerir alterações. Como ainda se está na fase de revisão, a pseudo-url **não** é alterada. O campo de assunto do e-mail pode ficar deste jeito:

```Re: [RFR] wml://www.debian.org/legal/index.wml```

- Se muitas alterações foram sugeridas ou rejeitadas, o processo de revisão pode se estender. Por exemplo, peça uma segunda ou mais revisões da tradução, sempre respondendo à mensagem anterior:

```[RFR2] wml://www.debian.org/legal/index.wml```

```[RFR3] wml://www.debian.org/legal/index.wml```

- Quando o texto é longo ou complicado, o(a) revisor(a) deve informar a lista de que vai demorar um pouco. Reserve o documento através de uma **Intenção de Revisar (ITR)**:

```[ITR] wml://www.debian.org/legal/index.wml```

- Quando chega-se a um consenso, informe a lista sobre a **Última Chance Para Comentários (LCFC)**, respondendo à mensagem anterior e anexando a versão final  do arquivo:

```[LCFC] wml://www.debian.org/legal/index.wml```

- A pessoa com permissão de escrita (commiter) no repositório atualiza o arquivo no Salsa/git do Debian e também responde à mensagem anterior **Finalizando (DONE)** o ciclo:

```[DONE] wml://www.debian.org/legal/index.wml```

## Inscrição na lista de discussão

Inscreva-se na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) e apresente-se. A equipe está sempre a procura de mais participantes! Recomendamos que durante algum tempo, você simplesmente acompanhe as mensagens para conhecer o modo como trabalhamos e quais os temas atuais em discussão.

## Inscrição no irc, matrix e/ou telegram

Você também pode se inscrever no canal #debian-l10n-br  no IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) e/ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR) para uma conversa em tempo real com a equipe.

## Leituras iniciais

> **Documentação oficial:**  [ajudando com as páginas web do Debian](http://www.debian.org/devel/website) e [sugestões úteis para tradução](http://www.debian.org/devel/website/translation_hints).
{.is-info}

Nas documentações oficiais encontra-se orientações do projeto para tradução, a organização dos arquivos e dos sistemas, dicas para tradução.

Não se assuste com a quantidade de informações! O importante no início é se habituar ao ambiente colaborativo e conhecer somente alguns detalhes técnicos. Não tenha receio de nos perguntar em caso de dúvidas, mesmo antes de começar a traduzir.

Você pode acompanhar o trabalho de tradução visitando a [página de monitoramento automatizado](https://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html). Na [página de estatísticas de tradução](https://www.debian.org/devel/website/stats/pt.en) você encontra uma lista das páginas traduzidas que estão desatualizadas, as páginas que ainda não estão traduzidas e outras estatísticas.

## Criando seu ambiente de trabalho

A seguir, faremos um resumo dos principais pontos, considerando um acesso apenas de leitura ao **git**.

Instale os pacotes *git*, *gettext*, *wml* (para geração de páginas HTML a partir de arquivos WML) e *tidy* (para testar o código HTML gerado):

```
# apt install git gettext wml tidy
```

Faça uma cópia dos arquivos **wml** do repositório remoto para sua máquina local. O código abaixo faz o download apenas das páginas originais (em inglês) e das traduções em português, o que o **recomendado** por nossa equipe:

```
$ git clone --no-checkout https://salsa.debian.org/webmaster-team/webwml.git
$ cd webwml
$ git config core.sparseCheckout true

Dentro do diretório webwml: Crie o arquivo
.git/info/sparse-checkout
com o conteúdo:
/*
!/[a-z]*/
/english/
/portuguese/

Em seguida faça:
$ git checkout --
```

Alternativamente, você pode baixar os arquivos de todos os idiomas:

```
$ git clone https://salsa.debian.org/webmaster-team/webwml.git
```

O ambiente está pronto para se começar a trabalhar!

:warning: Atenção: sempre atualize os arquivos **antes** de iniciar. Execute o comando abaixo dentro do diretório *webwml*:

```
$ cd webwml
$ git pull
```

## Como atualizar e revisar uma tradução

### Para quem vai atualizar uma tradução

:star: Veja este [tutorial em vídeo para revisao de traduções do site debian.org](https://www.youtube.com/watch?v=qH6XxMdiZ3A) para ajudar a entender o processo de atualização.

#### Coordenação pela lista de e-mails

- Antes de começar a traduzir qualquer arquivo que esteja desatualizado, por favor **envie uma mensagem** para a lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese).
- Inicie o campo de **assunto** com a pseudo-url ```[ITT]```, conforme explicado na seção anterior. Após a pseudo-url, digite o caminho e o nome do arquivo que você pretende trabalhar.
- O envio dos e-mails é importante para evitar **retrabalhos**, com pessoas traduzindo o mesmo arquivo.

#### Seleção do arquivo

:star: Antes de iniciar, sempre atualize seus arquivos locais com o git!

Para escolher um arquivo para atualizar, você deve acessar as duas páginas abaixo:

1. Acesse <https://www.debian.org/devel/website/stats/pt.en#outdated> e veja a lista de arquivos no tópico "Traduções desatualizadas". Escolha um desses arquivos.
1. Depois acesse a página <https://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html> e pesquise se o arquivo que você escolheu na outra página está listado em algum dos tópicos. Se o arquivo estiver lá, significa que alguém já está trabalhando nele. Então você deverá escolher outro arquivo na página anterior.

#### Edição do arquivo

Entre no diretório do arquivo em questão e comece atualizando o **cabeçalho** do arquivo. Altere o **identificador do commit** do original em inglês, de acordo com o git. O identificador vai vincular a última atualização do arquivo em inglês com a tradução que está sendo feita.

Supondo que você vá atualizar o arquivo *webwml/portuguese/legal/index.wml*:

- O original do arquivo estará em *webwml/english/legal/index.wml*;
- No terminal, encontre o último identificador do commit através do comando:

```
$ git log webwml/english/legal/index.wml
```

- Um exemplo (fictício) de identificador de commit seria *257c21bc3adb2b3c7174439a605ea9010623492d*;
- Edite o arquivo *webwml/portuguese/legal/index.wml* e insira no cabeçalho do arquivo em português o código encontrado anteriormente:

```
#use wml::debian::translation-check translation="257c21bc3adb2b3c7174439a605ea9010623492d"
```
- Continue com a tradução do conteúdo.

#### Teste do arquivo

Após concluir a atualização do arquivo, faça um teste para verificar se o arquivo *HTML* está sendo gerado corretamente. Esse teste é **muito importante**, pois se um arquivo *WML* estiver com alguma falha (tag faltando, por exemplo), o processo de reconstrução do site do Debian poderá falhar.

- No terminal, entre no diretório do arquivo *WML* e execute:

```
$ make index.pt.html
$ tidy -e index.pt.html
Info: Doctype given is "-//W3C//DTD HTML 4.01//EN"
Info: Document content looks like HTML 4.01 Strict
No warnings or errors were found.
```

#### Geração do patch

Após a atualização da tradução e o devido teste de geração do *HTML*, é necessário gerar **outro** arquivo com as **alterações** que foram feitas em relação ao arquivo original. Isto facilita examinar somente o que foi modificado, pois às vezes os arquivos são grandes.

- No terminal, ainda no diretório do arquivo sendo traduzido, execute:

```
$ git diff -u index.wml > index.wml.patch
```

#### Informe a lista de e-mails

Agora você deve enviar uma mensagem para a lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) informando a continuidade do trabalho:

- Responda o seu **primeiro e-mail**, mudando no assunto a pseudo-url de ```[ITT]``` para ```[RFR]``` (requisição de revisão), conforme explicado na seção anterior.
- Anexe ao e-mail o arquivo **.patch** com as alterações para que outra pessoa possa realizar a revisão
- O assunto da mensagem sempre deve começar com ```[RFR]```, por isso **remova** qualquer *Re:* no início.

Alguém irá revisar a sua atualização, e se ela tiver alguma correção a ser feita, enviará um novo arquivo .patch. Após ver quais são as correções que o(a) revisor(a) enviou, você pode aceitar essas alterações ou debater na lista se elas são mesmo necessárias ou não.

### Para quem vai revisar uma tradução atualizada

:star: Antes de iniciar, sempre atualize seus arquivos locais com o git!

Esta seção é a continuidade da seção anterior. Uma tradução foi atualizada e enviada para a lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) com o estado ```RFR``` (requisição para revisão). O campo de assunto do e-mail do nosso exemplo é:

```
[RFR] wml://www.debian.org/legal/index.wml
```

O processo é um pouco mais simples que o anterior. Você deve aplicar as alterações de tradução em seu repositório local, pois seu arquivo ainda é o antigo. Após aplicar a atualização da tradução, você revisa o texto e, em caso de sugestões de revisão, responde para a lista.

#### Baixando e aplicando a atualização da tradução

- Baixe o arquivo **.patch** anexado na mensagem para o diretório correspondente, dentro de sua cópia local do repositório **git**;
- Lembre-se que o diretório raiz será **portuguese** e não *english*, pois estamos revisando o texto já em português;
- No exemplo que estamos usando, o arquivo *index.wml.patch* será copiado para o diretório *webwml/portuguese/legal/*;
- Aplicaremos as alterações da seguinte forma:

```
$ cd webwml/portuguese/legal
$ git apply index.wml.patch
$ # (você pode usar "git apply --check" para testar antes se o patch será aplicado corretamente)
```

#### Revisando o arquivo

- Após aplicado o patch, o arquivo em português foi alterado com as modificações do(a) tradutor(a). Você pode começar a revisão!
- Lembre-se que o texto original encontra-se em *webwml/**english**/legal/index.wml*. Consulte-o se precisar confirmar certos excertos;

É importante que você faça a revisão em **todo** o arquivo. Acontece com frequência que o(a) tradutor(a) esqueça de atualizar alguma parte do texto.

#### Geração de patch

Caso tenha feito alguma correção, será necessário gerar um novo arquivo **.patch** com suas alterações:

```
$ git diff -u index.wml > index.seu-nome.wml.patch
```

#### Informe a lista de e-mails

Responda ao e-mail de requisição de revisão, anexando seu arquivo **.patch**. Dessa vez, não é preciso de se preocupar com as pseudo-urls, pois não estamos mudando o estado da tradução, ainda estamos trabalhando na revisão. O campo **assunto** deve ficar assim:

```
Re: [RFR] wml://www.debian.org/legal/index.wml
```

Se você não tem sugestões de revisão, informe a lista com o mesmo campo de assunto.

:speech_balloon: Dica: se o texto é grande, avise a equipe que você está trabalhando no arquivo. Use o campo de assunto acima.

## Como criar e revisar uma nova tradução

### Para quem vai criar uma nova tradução

:star: Veja este [tutorial em vídeo sobre como criar uma nova traducao site debian](https://www.youtube.com/watch?v=RmXGnIAZShU) para ajudar a entender o processo de tradução.

#### Coordenação pela lista de e-mails

- Antes de começar a traduzir qualquer arquivo, por favor **envie uma mensagem** para a lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese)
- Inicie o campo de **assunto** com a pseudo-url ```[ITT]```, conforme explicado a seção anterior Após a pseudo-url, digite o caminho e o nome do arquivo que você pretende trabalhar.
- O envio dos e-mails é importante para evitar **retrabalhos**, com pessoas traduzindo o mesmo arquivo.

#### Seleção do arquivo

:star: Antes de iniciar, sempre atualize seus arquivos locais com o git!

Para escolher um arquivo para atualizar, você deve acessar as duas páginas abaixo:

1. Acesse <https://www.debian.org/devel/website/stats/pt.en#untranslated> e veja a lista de arquivos no tópico "Páginas gerais não traduzidas". Escolha um desses arquivos.
1. Depois acesse a página <https://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html> e pesquise se o arquivo que você escolheu na outra página está listado em algum dos tópícos. Se o arquivo estiver lá, significa que alguém já está trabalhando nele. Então você deverá escolher outro arquivo na página anterior.

#### Criação do arquivo em português

Sendo uma nova tradução, não há no repositório um arquivo correspondente em português. Para criar novas traduções você deve utilizar o script ```copypage.pl``` que está no diretório principal *webwml*. Vamos utilizar como **exemplo** o arquivo de notícias *webwml/english/News/2018/20180601.wml*

- Defina a variável de ambiente **DWWW_LANG** e execute o script no diretório principal:

```
$ cd webwml
$ export DWWW_LANG=portuguese
$ ./copypage.pl english/News/2018/20180601.wml
```

- O script criará o arquivo em português, com o conteúdo em inglês para ser traduzido. No exemplo que estamos utilizando, o novo arquivo ficará abaixo do diretório **portuguese**:

```
webwml/portuguese/News/2018/20180601.wml
```

#### Edição do arquivo

Confirme se o script fez a vinculação correta entre o arquivo original em inglês e o novo arquivo em português. Para saber qual o último identificador do commit do arquivo em inglês, execute o comando:

```
$ git log webwml/english/News/2018/20180601.wml
```

- Um exemplo (fictício) de identificador de commit seria *257c21bc3adb2b3c7174439a605ea9010623492d*;
- Abra o arquivo recém-criado *webwml/portuguese/News/2018/20180601.wml* e compare com o código encontrado no cabeçalho:

```
#use wml::debian::translation-check translation="257c21bc3adb2b3c7174439a605ea9010623492d"
```

Continue com a tradução do conteúdo se os códigos forem o mesmo. Em caso contrário, verifique se rodou o script para o arquivo certo.

#### Teste do arquivo

Após concluir a atualização do arquivo, faça um teste para verificar se o arquivo *HTML* está sendo gerado corretamente. Esse teste é **muito importante**, pois se um arquivo *WML* estiver com alguma falha (tag faltando, por exemplo), o processo de reconstrução do site do Debian poderá falhar.

- No terminal, entre no diretório do arquivo *WML* e execute:

```
$ cd webwml/portuguese/News/2018/
$ make 20180601.pt.html
$ tidy -e 20180601.pt.html
Info: Doctype given is "-//W3C//DTD HTML 4.01//EN"
Info: Document content looks like HTML 4.01 Strict
No warnings or errors were found.
```

#### Informe a lista de e-mails

Após a tradução e o devido teste de geração do HTML, você deve enviar uma mensagem para a lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) informando a continuidade do trabalho:

- Responda o seu **primeiro e-mail**, mudando no assunto a pseudo-url de ```[ITT]``` para ```[RFR]``` (requisição de revisão), conforme explicado na seção anterior.
- Anexe ao e-mail o arquivo **.wml** para que outra pessoa possa realizar a revisão
- O assunto da mensagem sempre deve começar com ```[RFR]```, por isso **remova** qualquer *Re:* no início.

Alguém irá revisar a sua tradução, e se ela tiver alguma correção a ser feita, enviará um arquivo **.patch**. Após ver quais são as correções que o(a) revisor(a) enviou, você pode aceitar essas alterações ou debater na lista se elas são mesmo necessárias ou não.

#### Aplicando a revisão ao arquivo

- Para aplicar o patch enviado pelo(a) revisor(a), execute no terminal:

```
$ cd webwml/portuguese/News/2018/
$ patch -p0 < arquivo-enviado-pelo(a)-revisor(a).patch
patching file index.wml
$ # (você pode usar "patch -p0 --dry-run" para testar antes se o patch será aplicado corretamente)
```

### Para quem vai revisar uma nova tradução

:star: Veja este [tutorial em vídeo revisando uma nova traducao site debian](https://www.youtube.com/watch?v=ZNC4hPEWidw) para ajudar a entender o processo de revisão de uma nova tradução.

#### Baixando e editando o arquivo

- Como trata-se de revisão de um arquivo em português recém-criado, só temos o arquivo original em inglês para nos basear;
- Recomendamos que sejam feitas duas cópias do arquivo enviado por e-mail para facilitar o trabalho. Pelo nosso exemplo:

```
webwml/portuguese/News/2018/20180601.wml
webwml/portuguese/News/2018/20180601_rev.wml
```

#### Geração do patch

- Caso alguma revisão precise ser feita, editamos o arquivo auxiliar, que no nosso exemplo é o *20180601_rev.wml*;
- Ao final da revisão, criamos um arquivo **.patch** com a diferença entre o arquivo do(a) tradutor(a) e a sua cópia modificada:

```
$ diff -uN 20180601.wml 20180601_rev.wml > 20180601.wml.patch
```

#### Informe a lista de e-mails

Responda ao e-mail de requisição de revisão, anexando seu arquivo **.patch**. Dessa vez, não é preciso de se preocupar com as pseudo-urls, pois não estamos mudando o estado da tradução, ainda estamos trabalhando na revisão. O campo **assunto** deve ficar assim:

```
Re: [RFR] wml://webwml/portuguese/News/2018/20180601.wml
```

Se você não tem sugestões de revisão, informe a lista com o mesmo campo de assunto.

:speech_balloon: Dica: se o texto é grande, avise a equipe que você está trabalhando no arquivo. Use o campo de assunto acima.

# Fluxograma de tradução de páginas web

No intuito de ajudar as pessoas que não estão familiarizadas com o processo de tradução, resolvemos criar um fluxograma com as informações acima. Ajude-nos a melhorá-lo e envie um e-mail com sugestões para a nossa lista.

![fluxograma.svg](/assets/fluxograma.svg)

Outros formatos do fluxograma estão disponíveis em <https://l10n-br-team.pages.debian.net/fluxograma>. O repositório usado na organização do fluxograma está em <https://salsa.debian.org/l10n-br-team/fluxograma>.

# Resumo das especificações do site web

Segue um sumário sobre a infraestrutura do site:

- Páginas escritas em Metalinguagem de site web (Website META Language - WML);
- Scripts escritos em Perl ou Python;
- Etiquetas (tags) reutilizáveis nos modelos (templates);
- Construção via Makefiles;
- Diagramação e aparência mantidas somente via arquivos CSS, sem uso de !JavaScript;
- Conversão automática para HTML;

A criação de conteúdo e parte da infraestrutura do site ficam a cargo do [equipe de publicidade](https://wiki.debian.org/Teams/Publicity). Além da tradução do site, a equipe de tradução auxilia pontualmente em [notícias e anúncios](/traduzir/publicidade) de divulgação do Debian.

Caso tenha interesse em atuar diretamente na infraestrutura do site do Debian, entre em contato (em inglês) com o [equipe webmaster](https://wiki.debian.org/Teams/Webmaster).

# Veja também

- [Manual de estilo](/traduzir/manual-de-estilo) direcionado à equipe de tradução em português.
- [Vocabulário padrão](/traduzir/vocabulario) e outras referências para suporte à tradução.