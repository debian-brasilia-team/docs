---
title: Boas práticas
description: 
published: true
date: 2025-03-10T23:03:14.033Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:16:19.604Z
---

# Guia de boas práticas da equipe de tradução do Brasil

## Apresentação

Este guia visa orientar a colaboração entre os interessados no processo de tradução realizado pela comunidade Debian no Brasil. Além disso auxiliará na comunicação entre os(as) tradutores(as) e demais pessoas interessadas.

Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

## Código de Conduta do Projeto Debian

O [Código de Conduta](https://www.debian.org/code_of_conduct.pt.html) descreve preceitos e valores definidos pela comunidade como sendo o correto a seguir, e devem ser estudados para orientar o comportamento dos membros da comunidade. Seus principais pontos são:

1. Seja respeitoso(a)
1. Suponha boa fé
1. Seja colaborativo(a)
1. Tente ser conciso(a)
1. Esteja aberto(a)
1. Em caso de problemas, use o Código de Conduta como referência

## Receptividade a novos(as) colaboradores(as)

Não assuma nenhum conhecimento prévio de um(a) novo(a) colaborador(a). Pergunte sobre seu entendimento, sem julgamento de valor, apenas para ajudar da melhor forma. Facilite o processo de aprendizado. Ajude a outra pessoa a avançar no seu percurso de contribuição.

Novos(as) colaboradores(as) não devem ser vistos como um peso, mas sim como um investimento na nova geração da equipe.

Se você não está disponível para ajudar, recomende o(a) novato(as) para outros membros da equipe.

## Avanço das revisões

O processo de revisão não avança automaticamente de um estado para outro apenas por tempo. Você deve sempre esperar que seu arquivo seja revisado pelo grupo, e só então avançar para o próximo passo. Isso aumenta a qualidade das traduções.

## Arquivos po-debconf

Para tradução de arquivos po, leia também sobre práticas específicas em [nesta página](/traduzir/debconf), na seção "etiqueta".

## Arquivos po de programas

Eventualmente traduzimos arquivos po de programas como dpkg ou apt (localização dos programas) e é necessário alguns cuidados extras. Isto ocorre porque estamos traduzindo strings que estão diretamente colocadas no código-fonte, e há a presença dos objetos colocáveis (placeholders). Por exemplo, observe o código C abaixo:

```
char my_string[] "Teste";
printf("Isto é um %s.\n", my_string);
```

Neste caso, `%s` será substituído pelo conteúdo de `my_string` e a mensagem exibida será `Isto é um Teste.`. Contudo, é possível usar mais de objeto `%s` e a indicação a qual variável ele se refere é pela posição do argumento após o fim da string. Então, é preciso tomar cuidado quando formos traduzir uma sentença que possui mais de objeto colocável  (placeholder). Há dois casos gerais: quando há a necessidade de trocar a ordem da sentença e quando não há. Neste último, não há complicações e a tradução pode ser feita normalmente.

Contudo, quando há a necessidade de trocar a ordem da sentença, a ordem dos objetos colocáveis (placeholders) pode ser alterada também. Neste caso, é imprescindível indicarmos na sentença traduzida essa alteração. A indicação é feita pela sequência de caracteres `X$`, na qual `X` deve ser trocada pela posição original do objeto. Portanto, a tradução da setença `The %s group %s` ficaria `O %2$s, do grupo %1$s` - isto é só um ~~(péssimo)~~ exemplo.

## Padrão de comunicação na lista de discussão

Os itens abaixo são recomendações para que a comunicação seja fluida.

Desta forma será possível evitar ruídos nas trocas de mensagens e evitar prejuízos nos trabalhos.

### Não é recomendado envio de e-mail em formato HTML

Além do fato do tamanho da mensagem ser relativamente maior do que e-mails escritos em formato de texto puro, dois motivos para se evitar o uso de e-mails no formato HTML são:

1. Evitar que as mensagens sejam pegas por anti-spam; e
1. Muitos clientes de e-mail não são compatíveis com o formato HTML.

### Mantenha a thread

Todos os e-mails relativos a um mesmo pacote ou página web devem ser respostas a um e-mail anterior sobre o pacote em questão (exceto o primeiro e-mail).

### Limite linhas a 80 caracteres

Quebre as linhas dos e-mails (e arquivos de tradução) no máximo na coluna 80, para caber em um terminal ou emulador de terminal.

## Notificações sobre comportamentos

Novos(as) colaboradores(as), eventualmente, poderão ser orientados por membros mais experientes sobre sua conduta no trabalho com a equipe. Este procedimento de correção de conduta é algo que deve ser encarado de maneira madura, assumindo boa fé, em prol da melhoria do trabalho em equipe.

Em geral, sugerimos que visitantes ou colaboradores(as) esporádicos devem ser poupados de notificações de comportamento publicamente, para que isto não interfira negativamente na sua motivação para contribuir. A recomendação é de que primeiramente a pessoa que se enquadre nas características citadas anteriormente seja abordada e orientada em particular.

Somos um time e como tal consideramos boas todas as formas de colaboração entre os tradutores.