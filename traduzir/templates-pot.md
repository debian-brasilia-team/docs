---
title: Templates pot
description: 
published: true
date: 2025-03-10T23:15:17.736Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:32:26.024Z
---

# Exemplo de um arquivo templates.pot

> Esta página precisa ser melhorada porque não cita os script dpo para facilitar a criação de nvoas traduções
{.is-warning}

Nesta seção, mostraremos um exemplo verdadeiro de um **templates.pot** baixado [nesta página](http://www.debian.org/international/l10n/po-debconf/pt_BR) pronto para ser traduzido.

O pacote escolhido foi o **adjtimex**. Que por sua vez encontravasse [nesta página](http://www.debian.org/international/l10n/po-debconf/pot#adjtimex).

Após baixá-lo para um diretório, decompacte-o e terá o seguinte conteúdo no arquivo templates.pot do adjtimex:

Atenção para os campos em *itálico*, pois são nestes locais que os tradutores deverão acrescentar (quando de " ") e mudar se necessário:

```
#
# Translators, if you are not familiar with the PO format, gettext
# documentation is worth reading, especially sections dedicated to
# this format, e.g. by running:
# info -n '(gettext)PO Files'
# info -n '(gettext)Header Entry'
#
# Some information specific to po-debconf are available at
# /usr/share/doc/po-debconf/README-trans
# or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Developers do not need to manually edit POT or PO files.
#
#, fuzzy

msgid ""
msgstr ""
"Project-Id-Version:PACKAGE VERSION\n"''
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-08-02 09:29+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Description
#: ../templates:4
msgid "Should adjtimex be run at installation and at every startup?"
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"

#. Description
#: ../templates:4
msgid ""
"adjtimex can run at system startup to set the kernel time parameters to the "
"values in /etc/default/adjtimex. Don't accept if you just want to use "
"adjtimex to inspect the current parameters."
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"

#. Description
#: ../templates:12
msgid "Should adjtimexconfig be run at installation time?"
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"

#. Description
#: ../templates:12
msgid ""
"The adjtimexconfig script will use adjtimex to find values for the kernel "
"variables tick and frequency that will make the system clock approximately "
"agree with the hardware clock (also known as the CMOS clock). It then saves "
"these values in the configuration file /etc/default/adjtimex so the settings "
"will be restored on every boot, when /etc/init.d/adjtimex runs."
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"

#. Description
#: ../templates:12
msgid ""
"The script takes 70 sec to run. Alternatively, you can run adjtimexconfig "
"yourself at a later time, or determine the kernel variables one of several "
"other ways (see the adjtimex man page) and install them in /etc/default/"
"adjtimex."
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"
```

------------INÍCIO DA SUA TRADUÇÃO--------------

Veja como ficaria este **templates.pot** já traduzido e atualizado para o **pt_BR**:

```
#Brazilian Portuguese translation for adjtimex
#Copyright © 2003 Free Software Foundation
#This file is distributed under the same license as the adjtimex package.
#Rodrigo Tadeu Claro <rlinux@horizon.com.br>, 2003.
#, fuzzy

msgid ""
msgstr ""
"Project-Id-Version:adjtimex 1.16-1\n"''
"Report-Msgid-Bugs-To:rlinux@horizon.com.br\n"
"POT-Creation-Date: 2003-08-02 09:29+0200\n"
"PO-Revision-Date: 2003-08-03 09:35+0200\n"
"Last-Translator: Rodrigo Tadeu Claro <rlinux@horizon.com.br>\n"
"Language-Team: l10  Portuguese <debian-l10n-portuguese@lists.debian.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Description
#: ../templates:4
msgid "Should adjtimex be run at installation and at every startup?"
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"

#. Description
#: ../templates:4
msgid ""
"adjtimex can run at system startup to set the kernel time parameters to the "
"values in /etc/default/adjtimex. Don't accept if you just want to use "
"adjtimex to inspect the current parameters."
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"

#. Description
#: ../templates:12
msgid "Should adjtimexconfig be run at installation time?"
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"

#. Description
#: ../templates:12
msgid ""
"The adjtimexconfig script will use adjtimex to find values for the kernel "
"variables tick and frequency that will make the system clock approximately "
"agree with the hardware clock (also known as the CMOS clock). It then saves "
"these values in the configuration file /etc/default/adjtimex so the settings "
"will be restored on every boot, when /etc/init.d/adjtimex runs."
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"

#. Description
#: ../templates:12
msgid ""
"The script takes 70 sec to run. Alternatively, you can run adjtimexconfig "
"yourself at a later time, or determine the kernel variables one of several "
"other ways (see the adjtimex man page) and install them in /etc/default/"
"adjtimex."
msgstr "INSIRA aqui sua(s) frase(s) do(s) TEXTO(S) ACIMA traduzido"
```

--------------FIM DA SUA TRADUÇÃO--------------

## Muita atenção

- Não esqueça de renomear o arquivo **templates.pot** para **pt_BR.po** antes de enviar para o mantenedor do pacote adjtimex
- Use o **reportbug** (veja como na página principal do Projeto) em modo texto para enviar esta tradução na forma de Bug com severidade **whishlist** ao mantenedor
- Não esqueça de anexar o arquivo **pt_BR.po** no Bug.
- Mande um email para o mantenedor, escrito em inglês é claro, para que ele saiba que você abriu um Bug para uma nova tradução do templates do pacote dele (uma garantia a mais, não custa nada)!

Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).