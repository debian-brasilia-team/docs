---
title: Debian Installer
description: 
published: true
date: 2025-03-09T22:36:03.986Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:19:46.232Z
---

# Tradução do Debian Installer

Felizmente, nossos esforços de tradução estão bastante avançados e atualmente os pacotes **apt, dpkg, dselect**, aptitude e outros, todos parte do sistema básico Debian, estão traduzidos para o idioma Português do Brasil. Sim, traduzidos, não somente seus [templates debconf](/traduzir/debconf), mas sim os próprios programas.

Como meta para o lançamento da próxima versão estável do Debian, estamos trabalhando para conseguir um processo de instalação 100% internacionalizado, bem como ter todos os pacotes que fazem parte do sistema básico Debian com seus templates debconf totalmente traduzidos.

Também considere ajudar a traduzir [outras partes](/traduzir) do Debian. Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

## Situação das traduções do debian-installer

O instalador Debian, chamado de debian-installer, é totalmente baseada na tecnologia **debconf**. Com isso, conseguimos atingir um alto nível de flexiblidade e modularidade, com todo o instalador sendo composto de inúmeros micro-pacotes Debian, conhecidos como **udebs**.

Com a decisão da adoção da tecnologia **debconf** para o instalador e da divisão das inúmeras partes do instalador em diversos micro-pacotes, a tarefa de tradução do mesmo se resumiu em traduzir os diversos templates debconf que são fornecidos por cada **udeb**.

A tradução do primeiro estágio da instalação, o debian-installer propriamente dito, está completa e estamos atualmente com **100%** das traduções em dia. Só precisamos trabalhar para mantê-las assim , atualizadas (o que não é um trabalho fácil).

## Prioridades de tradução

Adicionalmente a tradução dos diversos templates debconf do instalador Debian, a tradução dos templates debconf dos diversos pacotes que compõem o segundo estágio da instalação é altamente desejável para que possamos oferecer ao usuário um processo de instalação totalmente internacionalizado.

Templates de diversos pacotes são exibidos durante o segundo estágio da instalação. Além do evidente **base-config**, diversos outros pacotes devem ter seus templates traduzidos. Para uma lista atualizada com as prioridades do que deve ser traduzido, com instruções detalhadas ou links para instruções sobre como fazê-lo, confira a seção de nome *About the levels* na [página de status das traduções do debian-installer](http://d-i.debian.org/l10n-stats/translation-status.html).

## Como ajudar

Qualquer pessoa pode colaborar no projeto debian-installer, bastando para isso se inscrever na lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese). 

## Como estamos atualmente

A situação atual da tradução para Português do Brasil (pt_BR), bem como o a situação de todos os outros idiomas suportados pelo mesmo, pode ser conferida na [página de status das traduções do debian-installer](http://d-i.debian.org/l10n-stats/translation-status.html).