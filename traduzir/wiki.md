---
title: Wiki
description: 
published: true
date: 2025-03-09T23:43:47.716Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:38:07.020Z
---

#language pt-br

# Tradução da Wiki

![icon-moinmoin-32x32.png](/assets/icon-moinmoin-32x32.png) O wiki é um importante recurso de documentação e comunicação do Debian, que complementa a [documentação oficial](https://www.debian.org/doc).

Abaixo estão algumas particularidades sobre o processo de tradução de páginas wiki. É um guia sintético e mais detalhes podem ser buscados nas referências indicadas em cada seção.

Também considere ajudar a traduzir [outras partes](/traduzir) do Debian. Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

Qualquer dúvida referente a este wiki e outras situações, leia sobre as [diretrizes de conteúdo](https://wiki.debian.org/pt_BR/DebianWiki/Content).

## O que é um wiki?

Uma [WikiWikiWeb](https://wiki.debian.org/WikiWikiWeb) é um ambiente de hipertexto colaborativo, com ênfase em facilidade de acesso e alteração de informação.

Você pode alterar qualquer página pressionando o link "editar". Dependendo do tema selecionado, este link fica no topo, na barra lateral ou no fim da página. Palavras iniciadas com letra maiúscula e sem espaços entre si formam os [WikiNomes](https://wiki.debian.org/WikiNomes), que funcionam como links para outras páginas. Clicar no título da página atual executa uma busca por todas as páginas com links para a mesma. Links para páginas inexistentes têm uma cor diferente (geralmente cinza), ou um ponto de interrogação: para criá-las basta clicar nos links (veja mais detalhes sobre criação de páginas em [HelpOnPageCreation](https://wiki.debian.org/HelpOnPageCreation)).

Você pode editar a [PáginaDeTestes](https://wiki.debian.org/P%C3%A1ginaDeTestes) à vontade. Evite editar outras páginas até se acostumar com o modo de funcionamento das wikis.

Para aprender mais sobre o que é uma [Wiki:WikiWikiWeb](http://c2.com/cgi/wiki?WikiWikiWeb), leia [Wiki:WhyWikiWorks](http://c2.com/cgi/wiki?WhyWikiWorks) e [Wiki:WikiNature](http://c2.com/cgi/wiki?WikiNature). Consulte também [Wiki:WikiWikiWebFaq](http://c2.com/cgi/wiki?WikiWikiWebFaq) e [Wiki:OneMinuteWiki](http://c2.com/cgi/wiki?OneMinuteWiki). Usando o recurso de [InterWiki](https://wiki.debian.org/InterWiki), é possível fazer referência a uma vasta gama de informações de outras wikis.

Pontos de partida interessantes:
 * [MudançasRecentes](https://wiki.debian.org/Mudan%C3%A7asRecentes): veja onde as pessoas estão trabalhando atualmente.
 * [EncontrarPágina](https://wiki.debian.org/EncontrarP%C3%A1gina): encontre conteúdo ou navegue pela wiki.
 * [PáginaDeTestes](https://wiki.debian.org/P%C3%A1ginaDeTestes): modifique à vontade para testar a edição de páginas.
 * [HelpOnEditing](https://wiki.debian.org/HelpOnEditing): o ponto de partida para aprender tudo sobre marcação wiki.

Para mais ajuda, veja [ConteúdoDaAjuda](https://wiki.debian.org/Conte%C3%BAdoDaAjuda) e [ÍndiceDaAjuda](https://wiki.debian.org/%C3%8DndiceDaAjuda).

## Criar uma página para ser traduzida

- Chama-se de **versão base** a página "padrão" em inglês que será a origem da tradução;
- Utilize como ponto de partida todo o texto da versão base;
  - Acesse o texto no formato wiki no menu lateral *Mais ações* --> *Visualizar texto*;
  - Copie e cole todo o código da página base e traduza a partir dele;
- Nomeie a página em português para "pt_BR/NomeEmIngles";
  - Se for o caso, troque o caminho, mas não o nome original. Por exemplo: "Brasil/NomeEmIngles";
- **Nunca use acentos e "ç" no nome da página. Por exemplo, uma página com o nome ApresentaçãoPortuguês ficaria ApresentacaoPortugues**

Veja mais dicas no [guia de edição](https://wiki.debian.org/pt_BR/DebianWiki/EditorGuide#translation) do wiki.

### Cabeçalho

Vá na página base, clique em **Informações** e veja qual o número da última versão em inglês. Substitua essa informação nos comentários da página traduzida:

```
#language pt-br
## English version: [número da revisão]
```

:warning: Atenção:

- O código ```pt-br``` deve estar obrigatoriamente em **minúsculas e com traço** (sim, está fora do padronizado com sublinha).
- Mantenha **uma** cerquilha (#) para o idioma e **duas** para a versão da página base.

Também no cabeçalho, na parte de links para traduções, está sendo usado um excerto retirado da página em inglês. Com isso, basta adicionar idiomas na página em inglês que as modificações se propagarão. Primeiro, verifique se o cabeçalho foi criado **na página em inglês**; se não existe, crie-o. Por exemplo, para a página [Locale](https://wiki.debian.org/Locale):

```
##For Translators - to have a constantly up to date translation header in you page, you can just add a line like the following (with the comment's character at the start of the line removed)
## <<Include(Locale, ,from="^##TAG:TRANSLATION-HEADER-START",to="^##TAG:TRANSLATION-HEADER-END")>>
##TAG:TRANSLATION-HEADER-START
~-[[DebianWiki/EditorGuide#translation|Translation(s)]]: [[Locale|English]] - [[es/Locale|Español]] - [[it/Locale|Italiano]] - [[pt_BR/Locale|Português (Brasil)]] - [[uk/Locale|Українська]]-~
##TAG:TRANSLATION-HEADER-END
```

Na **página em português** correspondente, adicione a diretiva ```<<include>>``` no lugar das traduções, conforme o modelo acima:

```
<<Include(Locale, ,from="^##TAG:TRANSLATION-HEADER-START",to="^##TAG:TRANSLATION-HEADER-END")>>
```

### Tradução

- Coloque um título principal e em português para a página. Insira-o logo após o cabeçalho de traduções. Siga de perto o NomeWiki em inglês. Por exemplo, para a página [DontBreakDebian](https://wiki.debian.org/pt_BR/DontBreakDebian):

```
# Não quebre o Debian
```

- Mantenha a formatação e a estrutura originais do texto;
- Se não terminou a tradução, antes de salvar acrescente o comentário:
  - \## A ATUALIZAÇÃO DA TRADUÇÃO PAROU AQUI

Utilize o [Manual de estilo](/traduzir/manual-de-estilo) para orientações de redação e o [vocabulário padronizado](/traduzir/manual-de-estilo) para verificar a tradução de termos específicos. Em caso de dúvidas, pergunte na nossa lista de tradução [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese).

### Links

O mecanismo do wiki oferece facilidades no estabelecimento de links, tanto para edição quanto para acessibilidade:

- Sintaxes como ```[[PáginaTraduzida]]```, ```[[UmaPáginaSuperiorde/PáginaTraduzida|PáginaTraduzida]]``` e ```[[PáginaTal|Uma página traduzida]]``` criam links para páginas que estão neste wiki;
- Links externos são feitos com ```[[https://www.páginatraduzida.com.br]]``` ou ```[[https://www.páginatraduzida.com.br|Página traduzida]]```;
- Prefira usar **https** a http nos links;
- Use links [InterWiki](https://wiki.debian.org/InterWiki) como ```DebianPkg:stable/bash``` para pacotes, ```DebianBug:bash``` para bugs ou ```DebianList:debian-l10n-portuguese``` para listas;

Não esqueça de direcionar os links para as páginas wiki em **português** quando estas existirem, e não para suas versões em inglês, para que tenhamos uma estrutura interconectada da documentação. Também é uma boa prática colocar uma observação, logo após o link, se este estiver em língua estrangeira.

Duas dicas rápidas para **não** criar links ou para destacar textos e códigos:

- Para escrever NomesWiki sem que o mecanismo crie o link, utilize o sinal de exclamação: ```!NãoCriaLink```;
- Para apresentar um texto literalmente, utilize chaves:

```
{{{ texto literal, sem links }}}
```

Veja mais dicas no [guia de edição](https://wiki.debian.org/pt_BR/DebianWiki/EditorGuide#Links) do wiki.

### Imagens

Caso uma página possua imagens anexadas sendo exibidas, é preciso referenciá-las à página original e não à página traduzida. Por exemplo:

Na página original:

``` {{attachment:mascot1.png|Color version by Valessio Brito}} ```

Na página traduzida:

``` {{attachment:DebianArt/Mascot/mascot1.png|Versão em cores por Valessio Brito}} ```

### Rodapé

Estabeleça uma seção [Veja também](https://wiki.debian.org/pt_BR/DebianWiki/EditorGuide#Links_para_sites_externos) com links específicos à comunidade em português do Debian.
Insira categorias relacionadas à comunidade Debian em português, como tags de [tarefas a serem feitas](https://wiki.debian.org/pt_BR/DebianWiki/EditorGuide#tags).

## Voltando à página base: cabeçalho

Ao terminar a tradução, salvar e revisar, volte à página que serviu de base para a tradução e, na seção de traduções do cabeçalho, insira o link para a versão traduzida:

```
- ... - [[CaminhoDaPágina/PáginaBase|Português (Brasil)]] - ... -
```

## Monitore as alterações

Utilize a funcionalidade *Acompanhar* que fica no topo das páginas para receber notificações por e-mail. Marque tanto a página em português quanto a página base em inglês.

## Página de apresentação pessoal

Uma [WikiHomePage](https://wiki.debian.org/WikiHomePage) (PaginaDeApresentacao ou PaginaPrincipal) é sua página pessoal em um [WikiWikiWeb](https://wiki.debian.org/WikiWikiWeb), onde você pode inserir informações de contato, seus interesses e habilidades, o que está fazendo atualmente no wiki, etc. É importante que os(as) tradutores(as) tenham uma página de apresentação no próprio wiki.

### Criação de uma PaginaDeApresentacao

Crie sua página com informações relevantes:

- Recomenda-se que o nome de usuário(a) esteja no formato !NomeSobrenome;
- **Nunca use acentos e "ç" no nome da página. Por exemplo, uma página com o nome !JoãoFrança ficaria !JoaoFranca**
- Você pode inserir seu e-mail, IRC e outras formas de contato;
- Apresente algumas contribuições suas para o wiki e para o Debian em geral, para o software livre e outros interesses;
- Não esqueça de insirar as categorias [CategoryHomepage](https://wiki.debian.org/CategoryHomepage) e [CategoryWikiTranslator](https://wiki.debian.org/CategoryWikiTranslator) na parte inferior da página;

Veja mais dicas no [guia de edição](https://wiki.debian.org/pt_BR/DebianWiki/EditorGuide#Sua_conta) do wiki.

### Edição de uma PaginaDeApresentacao

Se a página não é sua, cuidado ao editá-la! Note que essas páginas "pertencem" a alguém e assim não devem ser editadas por outras pessoas. Porém, pode-se editá-la para deixar uma mensagem: adicione após os quatro traços:

```
----
Salve! Continue com o ótimo trabalho de tradução! -- !MariaJose
```

## Revisão

O processo de revisão de páginas wiki não tem um fluxo definido. Por isso deve ser entendida como uma releitura atenta em que se busca por erros ortográficos, desvios de sentido, coesão, coerência, diagramação, etc. Por ser uma ferramenta aberta, não é preciso avisar a equipe, basta se autenticar e alterar.

Em caso de dúvidas de sentido ou de padronização, entre em contato e converse conosco.

## Status das traduções e páginas para traduzir

- Trabalho colaborativo de revisão das páginas em pt_BR: [status](/traduzir/wiki/status)
- Páginas que precisam ser criadas ou atualizadas anualmente: <https://wiki.debian.org/pt_BR/Year>.

## Veja também

- A [página principal](https://wiki.debian.org/pt_BR/DebianWiki/), o [FAQ](https://wiki.debian.org/pt_BR/DebianWiki/FAQ) e as [diretrizes de conteúdo](https://wiki.debian.org/pt_BR/DebianWiki/Content) do wiki.
- [Imagens](https://wiki.debian.org/pt_BR/Portal/IDB) para utilizar nas páginas.
- [Guia de edição](https://wiki.debian.org/pt_BR/DebianWiki/EditorGuide) e [Guia rápido de edição](https://wiki.debian.org/pt_BR/DebianWiki/EditorQuickStart), instruções gerais sobre o wiki.
- [Manual de estilo](/traduzir/manual-de-estilo) direcionado à equipe de tradução em português.
- [Vocabulário padrão](/traduzir/manual-de-estilo) e outras referências para suporte à tradução.
