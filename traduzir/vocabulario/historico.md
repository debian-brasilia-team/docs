---
title: Histórico
description: 
published: true
date: 2025-03-11T17:52:11.264Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:36:49.951Z
---

# Histórico

:warning:  Diversos projetos envolvendo vocabulários em português do Brasil estão **parados**. Foi acrescido um histórico para contextualização. Se você tem interesse em continuá-lo, por favor, entre em contato pela lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese).

# Histórico de construção de um vocabulário padrão pt-BR

A criação de um **vocabulário padrão** da equipe de tradução do Debian em português brasileiro já tem longos anos. Como o projeto está parado no momento, foram retirados alguns marcos do [histórico da lista de discussão](https://lists.debian.org/cgi-bin/search?P=vocabul%C3%A1rio&DEFAULTOP=and&B=Gdebian-l10n-portuguese&SORT=0&HITSPERPAGE=100&%5B=2&xP=vocabul%C3%A1rio&xFILTERS=Gdebian-l10n-portuguese%7E.%7E%7E0) da equipe de tradução pt-br.

- Em **2001**, o Hilton Fernandes [menciona](https://lists.debian.org/debian-l10n-portuguese/2001/07/msg00004.html0) a elaboração de uma lista de termos de tradução em meio às discussões sobre a documentação geral do projeto Debian-BR Desktop. O Ricardo Grützmacher [comenta](https://lists.debian.org/debian-l10n-portuguese/2001/09/msg00004.html) a existência do [vocabulário padrão da LDP-BR](https://web.archive.org/web/20011223224845/http://ldp-br.linuxdoc.org/ferramentas/vp/vpimport.html), fomentado pela Conectiva. O Luis Alberto [esquematiza](https://lists.debian.org/debian-l10n-portuguese/2001/09/msg00005.html) a construção do vocabulário em três níveis de abrangência: uma mais geral, contendo termos gerais de informática; uma segunda parte com termos do universo do software livre e distribuições GNU/Linux; e uma terceira parte contendo termos específicos ao Debian.

- No ano de **2002**, o Marcio Teixeira [justifica](https://lists.debian.org/debian-l10n-portuguese/2002/02/msg00074.html) a necessidade de um vocabulário devido à utilização de termos técnicos. Diz ele: "Um texto técnico é, por definição, aquele que contém expressões técnicas isto é, expressões cujo sentido é explicitamente determinado por uma certa comunidade". O Rodrigo Claro (rlinux), em **2003**, [diz](https://lists.debian.org/debian-l10n-portuguese/2003/08/msg00002.html) ter enviado uma intenção de empacotamento do bsgloss, um glossário e dicionário padrão utilizado pela Conectiva, para futura adoção pelo time. A Michelle Ribeiro [sugere](https://lists.debian.org/debian-l10n-portuguese/2003/05/msg00061.html) a possibilidade de tradução do [Debian Dictionary](https://www.debian.org/doc/manuals/dictionary/) como alternativa ao vocabulário.

- O Marcio Teixeira, em **2004**, novamente [levanta](https://lists.debian.org/debian-l10n-portuguese/2004/09/msg00271.html) a necessidade de um vocabulário padrão autônomo e paralelo àquele elaborado pela TLDP, específico ao Debian. Sobre a possibilidade de uso de wiki para isso, ele [responde](https://lists.debian.org/debian-l10n-portuguese/2004/10/msg00012.html) pela preferência por uma base de dados automatizada, com possibilidade de inclusão de dados, empacotamento .deb e uso de postgresql. Antonio Terceiro (terceiro) [argumenta](https://lists.debian.org/debian-l10n-portuguese/2004/10/msg00001.html) pelo uso simplificado do wiki. O Marcio Teixeira [manda](https://lists.debian.org/debian-l10n-portuguese/2004/10/msg00040.html) um link da lista de discussão alioth (agora desabilitada) sobre o anúncio do projeto "Vocabulário Técnico da Debian - português brasileiro". Partindo para questões práticas da implementação da versão 1 do vocabulário padrão, o Marcio [apresenta](https://lists.debian.org/debian-l10n-portuguese/2004/10/msg00100.html) uma proposta de estrutura do banco de dados, bem como de alguns procedimentos de acesso ao mesmo. A mensagem é seguida por outras com várias propostas de utilização de soluções já prontas em software livre.

- Em **2005**, o Felipe Augusto van de Wiel (faw) [informa](https://lists.debian.org/debian-l10n-portuguese/2005/11/msg00054.html) que o projeto do vocabulário padrão está parado, mesmo ainda sendo utilizado. Alguns anos depois, em **2013**, o Marcelo Santana (msantana) [relata](https://lists.debian.org/debian-l10n-portuguese/2013/07/msg00009.html) que a referência que estava sendo utilizada naquele momento era o vocabulário presente no wiki.

- Na retomada dos trabalhos da equipe de tradução em **2018**, a Tassia Araujo [envia](https://lists.debian.org/debian-l10n-portuguese/2018/03/msg00013.html) a ata da reunião onde a equipe de tradução se propõe a retomar o vocabulário com base no conteúdo atual do wordlist do DDTSS. O Daniel Lenharo (lenharo) menciona que na reunião o Leandro sugeriu utilizar a versão de 2014 do vocabulário padrão que estava no web.archive.org. Há ainda a sugestão do Marcelo Santana (msantana) de utilizar o software compendium. Ainda ali, [menciona-se](https://lists.debian.org/debian-l10n-portuguese/2018/04/msg00002.html) que o Leandro Pereira (clkw) criaria na wiki uma seção de termos mais usados a partir da wordlist do DDTP de 2014 e da página da equipe de tradução espanhol.

- Em **2020**, o Daniel Lenharo (lenharo) [cria](https://lists.debian.org/debian-l10n-portuguese/2020/06/msg00121.html) um repositório no [salsa](https://salsa.debian.org/l10n-br-team/vocabulario) para o vocabulário padrão, sendo esta a solução utilizada pela equipe de tradução atualmente.

> Esta seção é a antiga página "Brasil/Traduzir/VtdBrWeb", sem histórico de modificações, que foi transferida para dentro da estrutura [traduzir](/traduzir).
{.is-info}

## Automatização via aplicativo e interface web (2004)

### Programando para o vtd-br

A implantação envolve três fases distintas e consecutivas. Na primeira, desenvolveremos o suporte para acessar on-line os registros, fazendo consultas, edições, adições e votação.

Na segunda, o vtd-br deverá ser portado para uso off-line. Desenvolveremos os pacotes necessários.

Na terceira fase, procuraremos desenvolver soluções para integrar todo o time de tradução da Debian para o português brasileiro ao vtd-br. Esta será planejada após concluída as duas primeiras fases.

#### Fase 1: vtd-br na WWW

Nosso ponto de partida é um sofware chamado glossword (uma sugestão do colega Miguel (cf. [essa mensagem](http://lists.debian.org/debian-l10n-portuguese/2004/10/msg00119.html).

O glossword pode ser acessado [aqui](http://glossword.info/index.php) e [aqui](http://sourceforge.net/projects/glossword).

O software está sendo portado para Debian. Para testá-lo, adicione as linhas

```
deb http://www.marciotex.pro.br/debian experimental main
deb-src http://www.marciotex.pro.br/debian experimental main
```

ao seu sources.list. Após, rode

```
apt update
apt install glossword (para instalar o binário)
apt source glossword (para baixar os fontes)
```

Ou se preferir, acesse diretamente os arquivos a partir das urls indicadas. O mantenedor do pacote agradece por toda contribuição que lhe seja enviada para melhorar o estado do pacote e do programa.

Devemos adicionar a este software a capacidade de votação nos registros, o que envolve uma correção (patch) considerável. Devemos, ainda, adicionar o suporte a sqlite (originalmente, apenas mysql é suportado), uma estratégia para execução da segunda fase de implantação apontada acima (sqlite é uma sugestão do colega kov, cf. [essa menasgem](http://lists.debian.org/debian-l10n-portuguese/2004/10/msg00060.htm).

Estas duas tarefas estão órfãs. Você conhece -- ou, se não conhece quer conhecer -- php+*sql+como fazer pacotes .deb? Seja bem-vindo: junte-se a nós. Precisamos de voluntários.

Nós estamos em fase inicial. O projeto pode ser alterado. Se você tem sugestões sobre como poderíamos implantar o vtd-br, fale conosco na l10n.

> Esta seção é a antiga página "StandardTranslationsPtBr", com última edição em 16.03.2009, que foi renomeada e transferida para dentro da estrutura [traduzir](/traduzir).
{.is-info}

## Lista de termos na wiki (2005)

For translation in the l10n project, it is useful to have a list of standard translations.

Here is such a list.  I'm keeping the structure from StandardTranslations, an English-Dutch glossary. From now on, this wiki is aimed for brazilian users, so we speak portuguese.

### Glossário de tradução - l10n-pt_BR

Se você não conhece o projeto de tradução de descrições, visite a [esta página](http://debian-br.alioth.debian.org/index.php?id=DDTP).

Aqui temos um espaço para construir um glossário de tradução EN -> pt_BR. Vou começar a acrescentar algumas palavras, e estou propondo seguir o padrão do equivalente dinamarquês, em StandardTranslations.

Comentários podem ser colocados ao lado de cada palavra, fora do negrito/itálico, entre parênteses. Para discussões mais longas, proponho a criação de mais uma página, DebatesGlossarioPtBr, por exemplo, para arquivar e objetivar os debates.

- **application**: ''programa'', ''aplicativo''

- **bug**: ''bug'' ou ''erro''?

- **daemon**: ''demônio''?

- **directory**: ''diretório'' (não: pasta)

- **emacsen** (plural de emacs): ''vários emacs'', ''emacses'' (em discussão na lista)

- **facilities**: ''utilitários'', ''componentes'' (em discussão na lista)

- **full screen utilities** (em modo texto, mas com uma interface do tipo ncurses): ''ferramentas de tela cheia'' (Não gosto da solução, alguém tem outra idéia?)

- **kernel**: ''núcleo''

- **legacy**: ''antigo'', ''obsoleto''

- **log**: ''registro'', ''log''

- **runtime library**: ''biblioteca de tempo de execução''

- **to login** (verbo): ''se logar''? que tal ''autenticar''?

- **standalone**: ''autônomo'' (programa)

- **window**: ''janela''

- **wizard**: ''assistente''

> Esta seção é a antiga página !DebatesGlossarioPtBr, sem histórico de modificações, que foi transferida para dentro da estrutura Brasil/Traduzir.
{.is-info}

## Debates sobre padronização de traduções (2005)

This page is for brazilian users. See StandardTranslationsPtBr for more info.


### 1. Qual texto devo usar, "verificador ortográfico Aspell do GNU" ou "verificador ortográfico GNU Aspell" ?

Esta pergunta foi feita no seguinte e-mail:

```
To: debian-l10n-portuguese@lists.debian.org
Subject: [[ddtp]] aspell-pt-br
From: Regis Fernandes Gontijo <regisfg@no-spam.com.br>
Date: Sun, 06 Feb 2005 17:19:25 -0200
```

As seguintes opiniões foram defendidas:

### 1. Daniel, em 08/02

```
Acho que eu fui um dos que mudou para "Aspell do GNU". Seria bom adotarmos um
padrão para todos os pacotes "do GNU".

Eu dei uma olhada na versão em português da página do projeto GNU (Devia ter
feito isso antes de mudar na revisão) e lá a referência aos softwares é com o
termo "GNU" antes, como nos exemplos abaixo:

- ambiente de edição de textos GNU Emacs
- sétima versão maior do GNU Bayonne

Dessa forma, apoio manter como "GNU Aspell" e fazer o mesmo com os outros
pacotes "do GNU".
```

### 2. Fred, em 8/2

```
Sou a favor do Emacs da GNU. Um leigo que leia GNU Emacs não vai saber que é
o emacs da gnu. Se na página da gnu está traduzido errado, a gente não
precisa levar este erro para a frente. Na verdade, eu diria que está
semi-traduzido.
```

### 3. Régis, 9/2

```
Matutando um pouco mais eu pensei nos problemas de usar `Software da GNU' em vez
de `GNU Software' (substituindo Software pelo produto em questão). Acho que isso
não seria diferente de, por exemplo, traduzir `Mozilla Browser' por `Navegador
web do Mozilla', visto que tanto o termo Mozilla quanto GNU são nomes de projetos
ou instituições responsáveis pelo desenvolvimento dos softwares citados e para
mim não soa bem `Navegador do Mozilla' (ou `Browser do Mozilla') em vez de
Mozilla Browser, `Firefox do Mozilla' (em vez de Mozilla Firefox), `Composer do
Mozilla' (em vez de Mozilla Composer), etc. Creio que a diferença é que o nome
do projeto também faz parte do nome do produto e não acho correto traduzir nomes
de produtos. Por outro lado talvez eu esteja equivocado e seja interessante (ou
até mesmo necessário) traduzir, em algumas situações, nomes de produtos. Fiquei
realmente confuso pois a questão não é tão simples quanto dúvidas de vocabulário
que eu sempre posto aqui...
```

### 4. Fred, 9/2

```
Acho que você tocou num ponto importante. Não é uma simples questão de
vocabulário, mas de estrutura gramatical.

Anglicanismos estão muito no nosso vocabulário, mas não na nossa
estrutura gramatical. deixando GNU aspell nós estaríamos colocando o
adjetivo de posse antes do substantivo. E isto para mim é que é
esquisito. Quer ver? Vou dar alguns exemplos:

sansung monitor (em bom português: monitor sansung)
black&decker ferro (em bom português: ferro black&decker)
genius mouse (em bom português: mouse genius)
tramontina faca (em bom português: faca tramontina)

Mas tem alguns contra-exemplos, vindos de empresas sem compromisso com a
língua brasileira, o que não é o nosso caso. Estas empresas são
transnacionais, a comunidade de SL é multinacional. Mas vamos aos
exemplos:

microsoft word
norton antivirus

E agora? Deu para sacar o meu ponto de vista?

Paro por aqui, Fred
```

### 5. Régis, 10/2

```
Fred, não entendi exatamente o significado do termo `anglicanismos'
na frase acima, mas considerando o contexto creio que refira-se ao nosso
mal costume de respeitarmos a estrutura da língua inglesa em
detrimento do desrespeito que temos para com a língua portuguesa,
estou certo?

fum> sansung monitor (em bom português: monitor sansung)
fum> black ferro (em bom português: ferro black&decker)
fum> genius mouse (em bom português: mouse genius)
fum> tramontina faca (em bom português: faca tramontina)

Tem razão, são exemplos bem claros mesmo... Não sei como demorei
tanto para perceber... :-/

fum> E agora? Deu para sacar o meu ponto de vista?

Bom, eu saquei sim mas parece que a falta de compromisso/atenção ao
traduzir/controle de qualidade é generalizada mesmo e vai muito além
do software proprietário. Bom, não vou extender mais esta mensagem
por agora para evitar fugir do tópico, visto que por agora não tenho
idéia formada de qual é a melhor opção para solucionar este problema.
```

### 6. Fred, em 10/2

```
> Bom, aqui também não entendi essa questão dos contra-exemplos. Eu
> inclusive tentei comparar os termos `GNU software' e `Microsoft
> software' com `Software da GNU' e `Software da Microsoft' antes de
> perguntar na lista. Agora a pouco pensei em outros contra-exemplos,
> que são nacionais e incluem distribuições Linux ou talvez alguém até
> possa citar outros softwares livres... :-(   Pra não citar os nomes
> reais usarei nomes fictícios, veja:

Este 'da' não é necessario. Exemplo: café são braz. Não café da são
braz.

> Fulano Linux
> EmpresaTal Linux (existem casos de sucesso no Brasil que usam uma
> destas formas e que são bem conhecidos por muitos aqui)
>
> Seria o caso de também usarmos em português `GNU/Linux da(o) Debian'
> ?

Ou GNU/Linux Debian. Sem o 'da'.
```

### 7. Daniel, em 10/2 - Proposta de votação

```
Depois de ter lido as opiniões de todos eu acho que um bom resumo seria:

*GNU Aspell: Não é a forma como deve ser escrito em português do Brasil, mas
INFELIZMENTE é como a maioria dos softwares são referenciados por aqui.
*Aspell do GNU: É a forma correta e acho que ainda deixaria até uma dúvida no
usuário sobre o que viria a ser GNU e acho que isso é bom. Não sei porque mas
lendo 'GNU Aspell' parece que o GNU passa desapercebido.

E aí? Alguém tem mais algum comentário defendendo alguma das duas opções? A
gente tem que votar em qual vai ser a utilizada pra haver uma consistência nas
traduções.
```

### 8. Felipe, em 11/2

```
(...)
As citações e os exemplos são bastante relevantes,
no entanto um aspecto importante é o '''nome'''. Você não
troca nomes, eu me chamo Felipe aqui ou na Indonésia, não
é porque viajo para os Estados Unidos que passo a me
chamar Philip.

Sendo assim, GNU Aspell é o nome do software e,
apesar das característica de posicionamento gramatical,
que eu concordo plenamente, o nome dele está escrito
dessa maneira e devemos respeitar. :o)

Aqui tem um outro aspecto, embora o Aspell seja
parte do Projeto GNU, dizer que ele é da/do GNU pode
soar um tanto quanto estranho, até porque GNU é um
projeto relativamente complexo pra se definir pois
envolve um conceito, uma filosofia e um conjunto de
aplicações; IMHO a idéia é demonstrar que o Aspell faz
parte desse projeto, como uma forma de carimbo.

Assim como GNU/Linux, se utilizássemos Linux
da GNU estaríamos descaracterizando alguns pontos
importantes (o tema é polêmico, e GNU/Linux já gera
uma discussão grande, imagine Linux da GNU). :o)

O uso "do" ou "da" pode gerar uma idéia de
posse que não é real, o que, nesse caso, fugiria do
motivo pelo qual se utiliza GNU <alguma-coisa>.

Na minha interpretação isso não quer dizer
que o software seja __da__ GNU, mas ele é GNU compatível
e merece um tipo de "carimbo" que diga que ele segue
as filosofias. :)
```

### 9. Régis, em 14/2

```
Por um lado eu concordo com o Fred a respeito dos anglicismos; por outro lado os
nomes de produto são nomes próprios, conforme o Faw comentou. No entanto estes
nomes, mesmo sendo próprios, têm significados relevantes para o usuário, coisa
que pode não ocorrer com nomes próprios exceto em contextos muito específicos,
ainda que cada palavra do nome tenha significados tão comuns quanto qualquer
outro substantivo. Isso porque o nome de uma pessoa envolve questões que às
vezes são completamente subjetivas, como história da família, humor dos
pais/parentes/outros, etc, coisa que é bem diferente do nome auto-explicativos
de aplicativos. Jogando mais lenha na fogueira... Se a opção for feita pela
tradução dos nomes de produtos, deveríamos também traduzir siglas e o próprio
termo GNU? Creio que haverá quase unanimidade a respeito de não traduzir GNU,
mas isso é o meu chute. :-) A intenção é que usemos estas questões para definir
os limites do que é aceitável ou não no trabalho, de tal forma que alguém possa
até abrir um bug em qualquer tradução que fizermos, seja no DDTP ou outras
frentes de trabalho dentro do Debian-BR, solicitando corrigir algo que estiver
fora das diretrizes que forem estabelecidas.
```

### 10. Em 16/2, Daniel propôs encerrar o debate

```
E aí? Acho que tá bom finalizar a discussão. Então a maioria optou por deixar
sem mudar a ordem. Hora de fazer as revisões necessárias então!

Ah, eu só considerei quem deixou claro em que opção votou.
```