---
title: Services
description: 
published: true
date: 2025-03-10T22:52:15.205Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:27:36.445Z
---


# Tradução de serviços

O Debian utiliza muitos [subsistemas e infraestruturas](https://wiki.debian.org/Services) para dar conta da complexidade de suas operações. Grande parte desses mecanismos não estão localizados para o português do Brasil. E muitos ainda não se encontram nem mesmo internacionalizados.

Abaixo foram separados, como sugestão, alguns serviços que poderiam ser priorizados pela [equipe de tradução pt_BR](/traduzir) em seus esforços de localização. Há também indicações da situação de internacionalização. Uma lista mais completa de serviços pode ser encontrada [nesta página do wiki](https://wiki.debian.org/Services) com seus respectivos [pseudopacotes](https://www.debian.org/Bugs/pseudo-packages quando existirem.

Também considere ajudar a traduzir [outras partes](/traduzir) do Debian. Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

Legenda::
- ✅ Serviço localizado pela equipe
- ⚫ Serviço ainda não localizado
- ❌ Serviço não internacionalizado, isto é, não há infraestrutura ou código que permita traduções

## Chaveiro Debian

Servidor de chaves públicas que fornece pesquisas HKP e adiciona requisições para chaves públicas de mantenedores(as) e desenvolvedores(as) Debian.
- [Serviço on-line](https://keyring.debian.org) e [código-fonte](https://salsa.debian.org/debian-keyring/website)
- ❌ Situação: não está internacionalizado.

## Novos(as) Membros(as)

Site web para candidatar-se para mantenedor(a) ou desenvolvedor(a), e para gerenciar esse status no projeto.
- [Serviço on-line](https://nm.debian.org) e [código-fonte](https://salsa.debian.org/nm-team/nm.debian.org)
- ⚫ Situação: em processo de tradução.

## Equipe de Garantia de Qualidade (QA)

Interface para o Equipe de Garantia de Qualidade (Quality Assurance - QA) que serve como ponto central de discussões sobre melhorias e problemas relativos à qualidade em toda a distribuição.
- [Serviço on-line](https://qa.debian.org) e [bugs com etiqueta l10n](https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=qa.debian.org;dist=unstable;tag=l10n)
- ❌ Situação: não está internacionalizado, mas há uma [referência na lista de discussão i18n](https://lists.debian.org/debian-i18n/2004/05/msg00085.html) sobre a possibilidade de fazê-lo por estar em uma infraestrutura própria.

## Rastreador de Pacotes

Serviço que permite acompanhar a evolução do Debian, tanto por atualizações de e-mail quanto por uma interface web.
- [Serviço on-line](https://tracker.debian.org), [código-fonte](https://salsa.debian.org/qa/distro-tracker_) e [bugs com etiqueta l10n](https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=tracker.debian.org;dist=unstable;tag=l10n)
- ❌ Situação: não está internacionalizado.


## Listas de discussão

Servidor das listas de discussão do Debian que utiliza o software !SmartList.
- [Serviço on-line](https://lists.debian.org) e [bugs com etiqueta l10n](https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=lists.debian.org;dist=unstable;tag=l10n)
- ❌ Situação: não está internacionalizado. Há [um bug #446703 aberto](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=446703) (wishlist) pedindo a internacionalização.

## Sistema de Rastreamento de Bugs (BTS)

Registro de detalhes de bugs reportados por usuários(as) e desenvolvedores(as). A cada bug é dado um número, que é mantido em registro até que marcado como tratado.
- [Serviço on-line](https://bugs.debian.org), [código-fonte](https://bugs.debian.org/debbugs-source) e [bugs com etiqueta l10n](https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=bugs.debian.org;dist=unstable;tag=l10n)
- ❌ Situação: não está internacionalizado.

## Autenticação Única (SSO)

Sistema de Autenticação Única (Single Sign-On - SSO) para reconhecimento em serviços web, baseado em certificados de clientes. Os certificados são expedidos por este site e podem ser usados em qualquer site que esteja configurado para aceitá-los.
- [Serviço on-line](https://sso.debian.org), [código-fonte](https://salsa.debian.org/debsso-team/debsso) e [bugs com etiqueta l10n](https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=sso.debian.org;dist=unstable;tag=l10n)
- ❌ Situação: não está internacionalizado.

## Pacotes

Site web para visualização de informações sobre pacotes.
- [Serviço on-line](https://packages.debian.org) e [código-fonte](https://salsa.debian.org/webmaster-team/packages)
- ✅ Situação: traduzido e em produção.

## Debian Tracker (Distro Tracker)

Site web para monitorar a evolução da distribuição, consolidando todas as informações sobre determinado pacote.
- [Serviço on-line](https://tracker.debian.org) e [código-fonte](https://salsa.debian.org/qa/distro-tracker)
- ❌ Situação: não está internacionalizado.

## Working-Needed and Prospective Packages (WNPP)

Interface web que reúne informações sobre pacotes que precisam de algum retrabalho, que estão órfãos ou estão na iminência de se tornarem, e solicitações de novos empacotamentos.
- [Serviço on-line](https://wnpp.debian.net) e [código-fonte](https://github.com/hartwork/wnpp.debian.net)
- ❌ Situação: não está internacionalizado.


## Satélite do Servidor de Traduções Distribuídas Debian (DDTSS)

Interface web para o sistema DDTP, responsável pela tradução das descrições de pacotes.
- [Serviço on-line](https://ddtp.debian.org/ddtss) e [código-fonte](https://salsa.debian.org/l10n-team/ddtp/-/tree/master/ddtss)
- ❌ Situação: não está internacionalizado.

## Debian Contributors

Interface web que reúne diferentes formas de contribuição ao Debian a partir de fontes de dados.
- [Serviço on-line](https://contributors.debian.org) e [código-fonte](https://salsa.debian.org/nm-team/contributors.debian.org)
- ❌ Situação: não está internacionalizado.

## Debian Mentors (Debexpo)

Aplicativo web que auxilia as pessoas a colaborar na revisão de pacotes.
- [Serviço on-line](https://mentors.debian.net), [código-fonte](https://salsa.debian.org/mentors.debian.net-team/debexpo) e [plataforma on-line para tradução](https://hosted.weblate.org/languages/pt_BR/debexpo)
- ✅ Situação: traduzido e em produção.

## DebTags

Interface web para categorização de pacotes do Debian.
- [Serviço on-line](https://debtags.debian.org) e [código-fonte](https://salsa.debian.org/debtags-team/debtagsd)
- ❌ Situação: não está internacionalizado.


## Manpages

Interface web para procurar por páginas de manual (man) em diferentes versões do Debian.
- [Serviço on-line](https://manpages.debian.org), [código-fonte](https://github.com/Debian/debiman) e [bugs com etiqueta l10n](https://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=both;dist=unstable;package=manpages.debian.org;tag=l10n)
- ❌ Situação: não está internacionalizado.

## Site web FTP-Master

Mantém o repositório Debian.
- [Serviço on-line](https://ftp-master.debian.org), [código-fonte](https://salsa.debian.org/ftp-team/website) e [bugs com etiqueta l10n](https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=ftp.debian.org;dist=unstable;tag=l10n)
- ❌ Situação: não está internacionalizado.

## Snapshot Debian

Interface web que disponibiliza acesso a pacotes com versões antigas e atuais, permitindo análise histórica dos pacotes.
- [Serviço on-line](https://snapshot.debian.org) e [código-fonte](https://salsa.debian.org/snapshot-team/snapshot)
- ❌ Situação: não está internacionalizado. Há [um bug #577446 aberto](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=577446) (wishlist) pedindo a internacionalização.


## Pesquisa de código do Debian

Interface web para pesquisa de código dentro dos pacotes-fonte do Debian.
- [Serviço on-line](https://codesearch.debian.net) e [código-fonte](https://github.com/Debian/dcs)
- ❌ Situação: não está internacionalizado.

## Debian Trends

Interface web com visão histórica do empacotamento no Debian.
- [Serviço on-line](https://trends.debian.net) e [código-fonte](https://salsa.debian.org/lucas/debian-trends)
- ❌ Situação: não está internacionalizado.

## Debian Janitor

Bot que faz manutenção automatizada em pacotes do repositório Debian, publicando resultados na web.
- [Serviço on-line](https://janitor.debian.net) e [código-fonte](https://salsa.debian.org/jelmer/debian-janitor)
- ❌ Situação: não está internacionalizado.

----

# Veja também

- [Serviços e infraestruturas](https://wiki.debian.org/Services) do projeto Debian.
- [Pseudopacotes](https://www.debian.org/Bugs/pseudo-packages) relacionados a serviços.
- [Manual de estilo](/traduzir/manual-de-estilo) direcionado à equipe de tradução em português.
- [Vocabulário padrão](/traduzir/manual-de-estilo)] e outras referências para suporte à tradução.
