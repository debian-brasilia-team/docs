---
title: Debconf
description: 
published: true
date: 2025-03-11T22:43:22.711Z
tags: 
editor: markdown
dateCreated: 2025-03-08T15:05:41.162Z
---

# Tradução de arquivos-modelo utilizados pelo debconf

O projeto consiste na tradução dos arquivos templates.pot utilizados pelo [debconf](https://wiki.debian.org/debconf), o sistema de configuração de pacotes da distribuição Debian, durante a instalação de pacotes no sistema.

Também considere ajudar a traduzir [outras partes](/traduzir) do Debian. Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

## Documentação e listas de discussão

>**Documentação oficial**: [Localização de modelos Debconf com arquivos PO — Dicas para tradutores(as)](https://www.debian.org/international/l10n/po-debconf/README-trans)
{.is-info}

Nas documentações oficiais encontra-se orientações, organização dos arquivos e rankings de traduções. Além disso, interessados(as) em colaborar com este projeto devem se inscrever nas seguintes listas de discussão:

- [debian-i18n](https://lists.debian.org/debian-i18n/): lista referente à internacionalização (i18n) da distribuição Debian.
- [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese): lista referente à localização (l10n) para o português do Brasil.

Acompanhe as discussões, se apresente para a equipe local e tire suas dúvidas antes de começar. Leia as referências apresentadas nesta página. Todo o trabalho é feito **colaborativamente**, por isso é preciso conhecer os fluxos já estabelecidos pela equipe.

## Inscrição no irc, matrix e/ou telegram

Você também pode se inscrever no canal #debian-l10n-br  no IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) e/ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR) para uma conversa em tempo real com a equipe.

## O que são arquivos po-debconf

O Debian oferece aos(às) seus(suas) usuários(as) um sistema de gerenciamento de configuração de pacotes chamado [debconf](https://wiki.debian.org/debconf).

Uma das formas de interação do debconf com os(as) usuários(as) do Debian é a apresentação de mensagens com perguntas antes da instalação, da atualização ou da reconfiguração dos pacotes. Por exemplo, veja esta captura de tela:

![po-debconf.png](/assets/po-debconf.png)

O texto dessas mensagens apresentadas aos(às) usuários(as) fica contido em arquivos no código-fonte dos pacotes Debian. Chamamos esses arquivos de **po-debconf**.

Para que as mensagens sejam apresentadas nos seus idiomas nativos, os arquivos po-debconf devem ser traduzidos. O trabalho da equipe brasileira de tradução do Debian é traduzir os arquivos po-debconf para o português do Brasil.

----

## Exemplo de fluxo de tradução

### dpo-tools

Talvez você queira usar os scripts [dpo-tools](https://salsa.debian.org/l10n-br-team/dpo-tools), pois eles podem auxiliar na tradução de arquivos podebconf. Note que isso é completamente opcional.

### Lista de discussão

O processo de coordenação do trabalho entre tradutores(as) e revisores(as) ocorre através de e-mails enviados para nossa lista de discussão. Utilizamos etiquetas (tags) que são rastreadas automaticamente para geração de relatórios. Os detalhes desse processo podem ser lidos [na seção "visão geral da coordenação pela lista de e-mails"](/traduzir/webwml) da tradução de páginas web. Leia com atenção!

Para o processo específico dos po-debconf, é importante saber:

1. Mantenha a thread. Todos os e-mails relativos a um mesmo pacote devem ser respostas a um e-mail anterior sobre o pacote em questão. O primeiro e-mail sobre o pacote deve sempre ser um e-mail novo, e não uma resposta.
1. Para facilitar a tarefa de quem revisa, envie os arquivos sempre com o nome ```pacote_pt_BR.po ```, substituindo a palavra *pacote* pelo nome do pacote em questão. Não envie arquivos com o nome ```pt_BR.po ``` para não haver conflitos de nomes de arquivos entre múltiplos pacotes. Mas lembre-se que ao enviar o arquivo para a **abertura de bugs**, aí sim deve ser usado o nome ```pt_BR.po ```, e o arquivo deve ser compactado com o ```gzip ```, gerando o arquivo ```pt_BR.po.gz ```.
1. Quebre as linhas dos arquivos po-debconf no máximo na **coluna 80** para caber em um terminal ou emulador de terminal.

:speech_balloon: Isso pode ser feito assim com o dpo-tools:

```
dpo-wrap pacote_pt_BR.po
```

### Para revisar

1. Escolha um pacote para revisar a partir de um e-mail com a pseudo-URL ```[RFR] ```, ''Request for Review - Requisição de Revisão''. O robô que captura as etiquetas mantém uma lista atualizada [nesta página](http://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html#rfr). Tome o cuidado de escolher arquivos do tipo **po-debconf**.

2. Salve o arquivo ```pacote_pt_BR.po ``` anexado no e-mail ```[RFR] ```.

3. Faça uma cópia do arquivo a ser revisado. Insira no nome algum identificador, como seu apelido ou as iniciais do seu nome, para facilitar:
```
cp pacote_pt_BR.po pacote_pt_BR.REVISOR.po
```

4. Revise o arquivo ```pacote_pt_BR.REVISOR.po ``` usando o seu editor preferido. Se encontrar erros ou quiser sugerir alterações, modifique somente o arquivo ```pacote_pt_BR.REVISOR.po ```. Mantenha inalterado o arquivo ```pacote_pt_BR.po ```.

:speech_balloon: Nós atualmente estamos usando dois editores, o **poedit** e o **gtranslator**. Lembre-se de verificar se o poedit está configurado para quebrar as linhas em 79 caracteres.

5. Se não fez modificações no arquivo ```pacote_pt_BR.REVISOR.po ```, responda o e-mail ```[RFR] ``` avisando que você revisou o arquivo e que concorda com a tradução.

6. Se você fez modificações no arquivo ```pacote_pt_BR.REVISOR.po ```, crie um patch e envie para a lista, respondendo o e-mail ```[RFR] ```:

:speech_balloon: Isso pode ser feito assim:

```
diff -u pacote_pt_BR.po pacote_pt_BR.REVISOR.po > pacote_pt_BR.AAAAMMDD-HHMM.REVISOR.po.patch
```

:speech_balloon: ou pode ser feito assim com o dpo-tools:

```
dpo-make-patch pacote_pt_BR.po
```

### Para traduzir

O processo geral de tradução se inicia pela **escolha** de um arquivo po-debconf. É necessário verificar se não houve tradução recente, evitando retrabalho, ou se já não há alguém com intenção de traduzir ```[ITT] ``` o referido arquivo. Confirmados ambos os casos, segue-se com a **tradução** e posterior submissão do arquivo para **revisão**. Finaliza-se com uma atualização no sistema de acompanhamento de **bugs** do Debian e **fechamento do trabalho** na lista de discussão.

1. Escolha um pacote em <http://www.debian.org/international/l10n/po-debconf/pt_BR#i18n>

2. Consulte em <http://l10n.debian.org/coordination/brazilian/pt_BR.by_package.html> se não há ninguém trabalhando neste pacote.

3. Consulte no [histórico da lista](https://lists.debian.org/cgi-bin/search?B=Gdebian-l10n-portuguese&SORT=0) se houve atividade sobre este pacote.

4. Copie o arquivo ```templates.pot ``` do pacote escolhido, descompacte e renomeie para ```pacote_pt_BR.po ```.

:speech_balloon: Isso pode ser feito assim com o dpo-tools:

```
dpo-get-templates pacote
dpo-init-po pacote_versao_templates.pot
```

5. Dê uma olhada no arquivo ```pacote_pt_BR.po ``` para ver se quer mesmo traduzi-lo.

6. Mande um e-mail para [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) com o assunto neste modelo:

```
[ITT] po-debconf://pacote/pt_BR.po
```

7. Traduza o arquivo ```pacote_pt_BR.po ```.

8. Acerte o cabeçalho: ```PO-Revision-Date ``` (isso é feito automaticamente por alguns editores).

9. Teste com:
```
msgfmt -c -v -o /dev/null pacote_pt_BR.po
```

:speech_balloon: Isso pode ser feito assim com o dpo-tools:

```
dpo-check pacote_pt_BR.po
```

10. Teste com:

```
podebconf-display-po -fdialog pacote_pt_BR.po
```

11. Mande um e-mail para [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) e anexe o arquivo ```pacote_pt_BR.po ``` pedindo pela revisão. Não esqueça que este e-mail deve ser uma resposta do e-mail ```[ITT] ``` enviado anteriormente, criando assim uma thread. O campo assunto deve ter este modelo:

```
[RFR] po-debconf://pacote/pt_BR.po
```

12. Espere a resposta dos(as) revisores(as) para seu ```[RFR] ```.

13. Aplique os patches que forem enviados e envie um novo ```[RFR] ```.

:speech_balloon: Isso pode ser feito assim:

```
patch -p0 <pacote_pt_BR.po.patch
```

14. Quando receber uma confirmação final dos(as) revisores(as), mande um e-mail para [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) com o assunto no modelo abaixo e com o arquivo ```pacote_pt_BR.po ``` anexado, respondendo o e-mail do ```[RFR] ```:

```
[LCFC] po-debconf://pacote/pt_BR.po
```

15. Espere de 2 a 3 dias para comentários.

16. Quando o ```[LCFC]``` for aprovado, ou ninguém mais se manifestar, faça o seguinte:

- Renomeie o arquivo para ```pt_BR.po ```
- Compacte com o ```gzip ``` para ```pt_BR.po.gz ```
- Responda ao ```[LCFC]``` anterior mandando o email para <submit@bugs.debian.org> com cópia para: <debian-l10n-portuguese@lists.debian.org> com o assunto:

```
pacote: [INTL:pt_BR] Brazilian Portuguese debconf templates translation
```

- Anexe o arquivo ```pt_BR.po.gz ```
- No corpo do e-mail escreva:

```
Package: pacote
Tags: l10n patch
Severity: wishlist

Hello,

Could you please update this Brazilian Portuguese translation?

Attached you will find the file pt_BR.po. It is UTF-8 encoded and
tested with msgfmt and podebconf-display-po.

Kind regards.
```

17. Quando o bug for aceito (espere a resposta do BTS), mande um e-mail para <debian-l10n-portuguese@lists.debian.org> com o assunto:

```
[BTS#xxxxxx] po-debconf://pacote/pt_BR.po
```

substituindo xxxxxx com o número do bug, e adicionando o texto do corpo do e-mail:

```
Link para o BTS: http://bugs.debian.org/xxxxxx
```

## Mais informações

 * [Estado atual das traduções](http://www.debian.org/international/l10n/po-debconf/pt_BR)
 * [Dicas para tradutores(as) de modelos debconf](http://www.debian.org/international/l10n/po-debconf/README-trans)
 * [Ranking entre idiomas de arquivos PO para modelos debconf](http://www.debian.org/international/l10n/po-debconf/rank)

# Veja também

 * [Manual de estilo](/traduzir/manual-de-estilo) direcionado à equipe de tradução em português.
 * [Vocabulário padrão](https://debianbrasil.org.br/traduzir/vocabulario) e outras referências para suporte à tradução.