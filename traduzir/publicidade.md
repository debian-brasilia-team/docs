---
title: Publicidade
description: 
published: true
date: 2025-03-11T23:48:20.627Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:26:11.731Z
---

# Tradução das notícias do Debian

> As orientações nessa página precisam ser melhoradas para diferenciar melhor a tradução do Bits e das Notícias do site.
{.is-warning}

![icon-community-32x32.png](/assets/icon-community-32x32.png) As notícias sobre o projeto Debian e sua comunidade são divulgadas pela [equipe de publicidade](https://wiki.debian.org/Teams/Publicity), responsável pela reunião, tratamento e divulgação do conteúdo. Os principais canais de divulgação são:

- As [notícias e comunicados de imprensa](https://www.debian.org/News) (Debian Press Release e Debian Project News)
- O blog oficial [Bits do Debian](https://bits.debian.org) (Bits from Debian)
- As [Micronotícias do Debian](https://micronews.debian.org) (Debian micronews) - *não traduzimos*

A equipe de tradução pt_BR contribui com alguns desses conteúdos de forma **pontual**, já que temos poucas pessoas participando em ambas as equipes. Se você tem interesse em participar da elaboração dos textos em inglês, e de sua tradução de maneira mais próxima à elaboração do conteúdo original, cogite em juntar-se a [equipe de publicidade](https://wiki.debian.org/Teams/Publicity).

Também considere ajudar a traduzir [outras partes](/traduzir) do Debian. Qualquer dúvida pode ser esclarecida na lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), no canal #debian-l10n-br do IRC (OFTC), na sala no Matrix [debian-l10n-br:matrix.debian.social](https://matrix.to/#/%23debian-l10n-br:matrix.debian.social) ou no grupo do Telegram [debl10nptBR](https://t.me/debl10nptBR).

## Notícias e comunicados de imprensa

Veja a página [tradução de páginas web](/traduzir/webwml) para traduzir as notícias antigas que já foram publicadas no site e estão no [repositório](https://salsa.debian.org/webmaster-team/webwml/-/tree/master/portuguese/News).

## Bits do Debian

O processo de tradução das entradas do blog é bem simples. Em resumo, quando um novo arquivo começa a ser elaborado pelo time de publicidade, a equipe de tradução aguarda o texto quase acabado. Com um ou dois dias antes da publicação, os(as) tradutores(as) trabalham no arquivo e o enviam para o [repositório no Salsa](https://salsa.debian.org/publicity-team/bits). A própria equipe de publicidade combina os arquivos para publicação.

No caso em que o texto seja publicado antes do término da tradução, **não** deve-se continuar. Como o arquivo já foi enviado ao site, deve-se seguir o processo de [tradução de páginas web](/traduzir/webwml), aproveitando o texto já traduzido no arquivo .md (markdown) correspondente.

:speech_balloon: Os prazos são curtos, pois o ideal seria que tivéssemos mais pessoas na equipe de publicidade. Contudo, com esta ação conseguimos traduzir anúncios importantes rapidamente, como o lançamento de versões pontuais, por exemplo.

### Orientações gerais do processo de tradução

:warning: Este processo está em fase de **elaboração e discussão**.

O processo de tradução segue as [orientações](https://wiki.debian.org/Teams/Publicity/bits.debian.org/translation) fornecidas pelo time de publicidade, adaptadas para a equipe brasileira de tradução.

- O processo de trabalho é coordenado pela lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese)
- Arquivos de trabalho do Bits residem no repositório Salsa [publicity-team/bits](https://salsa.debian.org/publicity-team/bits) e o controle de versões é feito pelo git.
- O acesso aos arquivos é liberado, mas é necessário autorização para enviá-los ao repositório.
- Utiliza-se o formato [MarkDown](https://pt.wikipedia.org/wiki/Markdown) para edição. Esta pode ser feita localmente ou via interface web do Salsa.
- Após publicação, os arquivos passam para o repositório do site web [webmaster-team/webwml](https://salsa.debian.org/webmaster-team/webwml).
- A partir deste momento, deve-se seguir o processo de [tradução de páginas web](/traduzir/webwml).

### Passo a passo

1. Acompanhar arquivos novos ou modificados pela lista de discussão da equipe de publicidade [debian-publicity](https://lists.debian.org/debian-publicity) ou pelo canal #debian-publicity no IRC (OFTC).

2. Fazer um fork do repositório [publicity-team/bits](https://salsa.debian.org/publicity-team/bits)

3. Copiar o arquivo original com o mesmo nome e adicionando o sufixo **-pt-BR**:

```
cp bits/content/2025/NOMEORIGINAL.md bits/content/2025/NOMEORIGINAL-pt-BR.md
```

4. Enviar uma mensagem para a lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese), informando sobre o início do trabalho com o assunto:

```
[ITT] md://publicity/bits/content/2025/NOMEORIGINAL-pt-BR.md
```

5. Editar o arquivo NOMEORIGINAL-pt-BR.md e alterar o cabeçalho:

```
Tile: [traduzir]
Date: [não alterar]
Tags: [não alterar]
Slug: [não alterar]
Author: [traduzir "and" se aparecer]
Translator: [nome+sobrenome ou apelido]
Lang: pt-BR
Status: draft
```

6. Traduzir o corpo do texto mantendo a formatação original.

7. Subir para o seu repositório o arquivo ainda em elaboração, e fazer Merge Request para [publicity-team/bits](https://salsa.debian.org/publicity-team/bits):

- Como boa prática, se utiliza um branch em separado para a tradução.
- Definir um nome padrão para este branch (:warning: precisa ser definido).
- Manter o padrão da mensagem de commit.
- Insere-se "WIP:" (Work in Progress) para evitar merge requests antes da finalização.

```
git add bits/content/2025/NOMEORIGINAL-pt-BR.md
git commit -m 'WIP: (pt-BR) initial translation post NOMEORIGINAL-pt-BR.md'
git checkout -b newptbr
git push origin newptbr
```

8. Pedir revisão pela lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) mandando um email com o link do arquivo no repositório do bits e o assunto:

```
[RFR] md://publicity/bits/content/2025/NOMEORIGINAL-pt-BR.md
```

9. Atualizar o cabeçalho do arquivo com nome do revisor(a) após terminada a revisão:

```
Translator: [tradutor(a), revisor(a)]
```

10. Mandar um email para a lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) sobre últimas verificações antes do envio:

```
[LCFC] md://publicity/bits/content/2021/NOMEORIGINAL-pt-BR.md
```

11. Subir para o seu repositório o arquivo final e fazer um Merge Request final para [publicity-team/bits](https://salsa.debian.org/publicity-team/bits) mantendo o padrão da mensagem de commit:

```
git add bits/content/2025/NOMEORIGINAL-pt-BR.md
git commit -m '(pt-BR) NOMEORIGINAL-pt-BR.md, proofread REVISOR(A), status draft'
git push meubits revptbr
```

12. Mandar um email para a lista de discussão [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) finalizando o processo de tradução:

```
[DONE] md://publicity/bits/content/2021/NOMEORIGINAL-pt-BR.md
```

13. A equipe de publicidade se encarrega de reunir as traduções e alterar status para "published" (publicado).

----

# Veja também

- [Orientações de tradução](https://wiki.debian.org/Teams/Publicity/bits.debian.org/translation) do time de publicidade.
- [Manual de estilo](/traduzir/manual-de-estilo) direcionado à equipe de tradução em português.
- [Vocabulário padrão](/traduzir/manual-de-estilo) e outras referências para suporte à tradução.
