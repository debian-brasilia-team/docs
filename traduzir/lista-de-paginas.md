---
title: Lista de páginas de tradução
description: 
published: true
date: 2025-03-11T21:52:39.807Z
tags: 
editor: markdown
dateCreated: 2025-03-11T21:30:54.919Z
---

# Lista de páginas do projeto de tradução

- [/traduzir](/traduzir)
- [/traduzir/boas-praticas](/traduzir/boas-praticas)
- [/traduzir/campanhas](/traduzir/campanhas)
- [/traduzir/campanhas/campanha-novos-colaboradores](/traduzir/campanhas/campanha-novos-colaboradores)
- [/traduzir/campanhas/campus-party-2022](/traduzir/campanhas/campus-party-2022)
- [/traduzir/campanhas/debconf](/traduzir/campanhas/debconf)
- [/traduzir/campanhas/oficina-ddtp-2023](/traduzir/campanhas/oficina-ddtp-2023)
- [/traduzir/campanhas/sprint-ddtp](/traduzir/campanhas/sprint-ddtp)
- [/traduzir/ddtp](/traduzir/ddtp)
- [/traduzir/debconf](/traduzir/debconf)
- [/traduzir/debian-edu](/traduzir/debian-edu)
- [/traduzir/debian-installer](/traduzir/debian-installer)
- [/traduzir/lista-de-prioridades](/traduzir/lista-de-prioridades)
- [/traduzir/manpages](/traduzir/manpages)
- [/traduzir/manual-de-estilo](/traduzir/manual-de-estilo)
- [/traduzir/pacotes-nativos-debian](/traduzir/pacotes-nativos-debian)
- [/traduzir/pseudo-urls](/traduzir/pseudo-urls)
- [/traduzir/publicidade](/traduzir/publicidade)
- [/traduzir/reuniao-20230123](/traduzir/reuniao-20230123)
- [/traduzir/servicos](/traduzir/servicos)
- [/traduzir/templates-pot](/traduzir/templates-pot)
- [/traduzir/vocabulario](/traduzir/vocabulario)
- [/traduzir/vocabulario/historico](/traduzir/vocabulario/historico)
- [/traduzir/webwml](/traduzir/webwml)
- [/traduzir/wiki](/traduzir/wiki)
- [/traduzir/wiki/status](/traduzir/wiki/status)

Atualizado em 11/03/2025

Lista na wiki antiga: <https://wiki.debian.org/Brasil/ListaDePaginas>