---
title: pseudo-URLs
description: 
published: true
date: 2025-03-12T00:07:30.021Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:48:14.454Z
---

# Entendendo as pseudo-URLs

Visando controle de qualidade, a [equipe brasileira](/traduzir) de tradução do Debian, assim como outras que fazem parte da [força-tarefa de internacionalização](http://wiki.debian.org/I18n), adotou um procedimento padrão que utiliza *pseudo-URLs* na troca de mensagens por e-mail. Esse processo possibilita a coleta e a geração de informações sobre o [estado das traduções](http://l10n.debian.org/coordination/brazilian/pt_BR.by_type.html) utilizando certas expressões no assunto dos e-mails enviados para a lista de discussão  [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese). Esta nomenclatura é comumente conhecida como *pseudo-url*.

## Pseudo-URLs

Existe um programa (robô) que acompanha a lista [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) e que consegue compreender as pseudo-URLs no campo assunto. Uma pseudo-url tem o seguinte formato:

```
	[<estado>] <tipo>://<pacote>/<arquivo>
```

### Estado

O estado pode ser escolhido entre um dos seguintes:

**TAF** ***(Travail À Faire - Trabalho a Fazer)***
- Enviado pelo(a) coordenador(a), não por qualquer participante da lista, para indicar que há um documento que precisa ser trabalhado.

**MAJ** ***(Mise À Jour - Últimas atualizações)***
- Enviado pelo(a) coordenador(a), não por qualquer participante da lista, para indicar que existe um documento que precisa ser atualizado e que o trabalho está reservado para o(a) tradutor(a) anterior.

**ITT** ***(Intent To Translate - Intenção de Traduzir)***
- Indica que você planeja trabalhar na tradução, assim evitando tradução em paralelo.
- Se você enviar uma mensagem [ITT] e alguém enviar outro [ITT] para o mesmo arquivo, por favor, envie uma nova mensagem imediatamente para a lista de discussão para lembrar que você tem a prioridade. O objetivo é evitar retrabalho.

**RFR** ***(Request For Review - Requisição para Revisão)***
- A tradução inicial está pronta e foi anexada ao e-mail, outras pessoas podem revisar.
- É possível que seja seguida por outras RFRs (RFR2, RFR3...) quando mudanças substanciais forem feitas e talvez seja necessária nova revisão.
- NOTA: envie uma resposta mesmo se não encontrou falhas.

**ITR** ***(Intent To Review - Intenção de Revisar)***
- Reserva do arquivo para revisão quando você espera que ela possa demorar alguns dias.
- O corpo da mensagem deve conter a indicação de quando você espera terminar a revisão.
- NOTA: este estado não é interpretado pelo ''spider'' (robô).

**LCFC** ***(Last Chance For Comment - Última Chance Para Comentários)***
- Indica que a tradução está pronta, que as revisões foram incorporadas e que a tradução está na iminência de ser enviada para o local apropriado.
- Envia-se geralmente após a última RFR ter terminado há 3 dias.
- Não deve ser enviado antes de pelo menos uma revisão.

**BTS#\<bug number>** ***(Bug Tracking System - Sistema de Rastreamento de Bugs)***
- Usado para registrar o número do bug sob o qual você submeteu a tradução ao BTS.
- Regularmente o ''spider'' (robô) verifica se um bug aberto foi fechado.

**WONTFIX#\<bug number>** ***(bug marcado como WONTFIX)***
- Usado quando um bug foi marcado como ''wontfix''.

**FIX#\<bug number>** ***(bug corrigido)***
- Usado quando um bug foi marcado como corrigido (após um NMU).

**DONE#\<bug number>**
- Usado para fechar um bug quando a tradução foi levada em conta, útil quando não foi enviado ao BTS.

**HOLD** ***(em espera)***
- Usado para colocar uma tradução em espera, quando a versão original foi alterada mas não há necessidade de atualizar a tradução. Por exemplo, você sabe que outras modificações serão feitas em breve na tradução e você não quer que alguém a atualize no momento.

### Tipo

O **tipo** relaciona-se ao documento, por exemplo: po-debconf, debian-installer, po, man ou wml.

### Pacote

O **pacote** é o nome do pacote de onde vem o documento.

### Arquivo

O **arquivo** é o nome do arquivo do documento e pode conter outras informações como o caminho para o arquivo ou a seção para a página de manual. Desse modo, nenhum outro documento no mesmo pacote será referenciado da mesma maneira.

## Exemplos de pseudo-URLs

A estrutura do nome depende da escolha do tipo. Em princípio é apenas um identificador, mas é fortemente recomendado seguir as seguintes regras:

- Para a interface de configuração do instalador:

**po-debconf://nome-do-pacote/pt_BR.po**

- Para arquivo po clássico:

**po://nome-do-pacote/caminho-no-fonte-do-pacote/nomedoarquivo.po**

- Para documentação convertida para formato po:

**po4a://nome-do-pacote/caminho-no-fonte-do-pacote/nomedoarquivo.po**

- Páginas do site web:

**wml://www.debian.org/caminho_do_original_em_inglês_no_repositório_git**

- Para documentação do Debian:

**ddp://documento/nomedoarquivo.po**

- Para o guia de instalação:

**xml://guia-de-instalação/idioma/caminho-no-fonte-do-pacote/arquivo.xml**

## Processo pela lista de e-mails

Obs: uma síntese desse procedimento pode ser encontrada em [como usar a lista de e-mails](/traduzir/webwml#visão-geral-da-coordenação-pela-lista-de-e-mails).

Inicialmente, você envia um **ITT** para registrar no que vai trabalhar. Atente para o formato da pseudo-url. Quando estiver com a tradução pronta, responda ao seu próprio ITT e altere o campo de assunto da mensagem para **RFR**. Alguém vai revisar o seu trabalho, vocês poderão debater até chegar a um consenso. A seguir, mude o assunto para **LCFC**. Após algum tempo sem comentários, é hora de mandar sua tradução para o BTS.

Note que a ''pseudo-url'' é mantida, mudando-se apenas a **etiqueta do estado**. Não troque a etiqueta e nem o formato dela, senão o robô não entenderá. Mantenha-se fiel ao formato, seja cuidadoso com o nome do pacote. Se houver muitas modificações entre um RFR e um LCFC, você poderá registrar um **RFR2**.

### Sistema de rastreamento de bugs

:warning: ToDo: esta seção deve ser movida para a [frente de tradução](/traduzir) correspondente.

O estado do BTS é algo especial, é utilizado para registrar o número do BUG. Assim, o ''l10n-bot'' pode rastrear o estado da tradução desde que você envie para o BTS. Diariamente ele verificará se um bug aberto foi fechado. Um exemplo desse comando:

```
	[BTS#1234] po-debconf://cups/pt_BR.po
```

Se você tem intenção de traduzir vários pacotes, poderá fazer **ITT** para todos de uma só vez. Um exemplo:

```
	[ITT] po-debconf://{cups,courier,apache}/pt_BR.po
```
  
Coloque os pacotes entre chaves e separados por vírgulas. Sem espaços extras!

## Detalhes importantes ao realizar uma tradução

:warning: ToDo: esta seção deve ser movida para a [frente de tradução](/traduzir) correspondente.

Sobre a tradução em si, independente do arquivo, mantenha as linhas com até 80 colunas. Algumas pessoas trabalham no terminal em servidores remotos via SSH e não é legal ter linhas quebradas. Teste seus **potfiles** usando:

```
$ msgfmt -c -v -o /dev/null <arquivo.po>
```

Se for um po-debconf, teste também com:

```
$ podebconf-display-po <arquivo.po>
```

Fique atento(a) com a codificação do arquivo, pois usamos **UTF-8** tanto para o site quanto para os po-debconf e demais POs. Preencha os arquivos com todos os cabeçalhos necessários, se tiver dúvidas não hesite em perguntar na lista de discussão.

Não existe um tempo máximo para finalizar as traduções. Você terá que aguardar que pelo menos uma revisão aconteça. Elas podem demorar ou podem ser bem rápidas, depende da disponibilidade da equipe.

Nas primeiras vezes, comece devagar, com um ou dois arquivos para evitar erros iguais em vários arquivos. Na primeira vez que for reportar o bug, mande o modelo para a [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) primeiro para que possamos revisar e mandá-lo pronto para o(a) mantenedor(a).

Se estiver atualizando uma tradução, mande o arquivo original, o diff e o seu arquivo traduzido. Se estiver fazendo uma nova tradução, mande essa informação no e-mail.