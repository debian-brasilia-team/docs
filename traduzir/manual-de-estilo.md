---
title: Manual de estilo
description: 
published: true
date: 2025-03-11T21:23:12.465Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:23:10.025Z
---

# Manual de estilo

Este **manual de estilo**, também chamado de manual ou livro de redação, é um guia de referência para a [equipe de tradução do Debian para o português do Brasil](/traduzir). Nele reunimos regras gramaticais e de estilo, padronizações de termos, diagramação e conceitos. O objetivo do manual é alinhar os esforços de tradução e auxiliar a equipe no trabalho cotidiano.

## Orientações gerais

### Condutas de tradução

A tradução, como qualquer outra forma de comunicação do projeto Debian, se pauta pelo [Código de Conduta](https://www.debian.org/code_of_conduct.pt.html) e pela [Declaração de Diversidade](https://www.debian.org/intro/diversity.pt.html).

### Diretrizes

**Quanto ao trabalho colaborativo**: procure conhecer a rotina e os procedimentos da equipe de tradução. Antes de propor alterações ou soluções para expressões complicadas ou polêmicas, verifique se já houve discussões a respeito e que encaminhamentos foram tomados.

**Quanto ao ato de traduzir**: a tradução não deve ser entendida como um processo imediato. O **objetivo** é determinar qual o melhor significado dentre as possíveis opções a partir da combinação das vertentes lingüística e técnica (Guia de estilo de tradução, TLDP-BR):

- compreender e expressar exatamente e sem ambiguidades a informação contida no texto original;
- escrever no idioma de destino com clareza, concisão e simplicidade;
- respeitar a competência do(a) leitor(a);
- evitar estilo ou termos que possam ser ofensivos;

**Quanto às alterações do texto em português**: não tenha receio em alterar o texto em português, mesmo que ele fique ligeiramente diferente do texto em inglês. Essa diferença muitas vezes é fundamental para que a mensagem fique mais clara, evitando traduções literais sem sentido. Se você fizer alterações mais significativas, ou são informações que podem ser úteis à maioria das pessoas, por favor avise a lista de discussão [debian-www](https://lists.debian.org/debian-www/) (em inglês) e a lista DebianList:debian-l10n-portuguese, já que é desejado manter o conteúdo o mais similar possível entre os idiomas ([Sugestões úteis para tradução](https://www.debian.org/devel/website/translation_hints.pt.html)).

**Quanto às ferramentas de tradução**: não há um procedimento ou ferramenta únicos. Cada tradutor(a) tem seus métodos próprios, que podem envolver ou não o uso de documentos, softwares e sites. Uma referência que deve sempre ser consultada é o vocabulário padronizado, construído coletiva e continuamente pela equipe.

## Edição e formatação

- No editor, manter o texto em até 80 colunas.
- Não deixar espaços ao fim de linha.
- O editor de texto entende que há um espaço entre a última palavra de uma linha e a primeira palavra da próxima linha. Não é preciso deixar espaços entre essas palavras.
- Não quebrar links HTML `<a href=...></a>`, manter numa única linha.
- Sempre traduzir `<img alt="TRADUZIR-ESTE-TEXTO">`
- Nos subdiretórios de `users/` não é preciso traduzir os nomes das empresas e instituições.

## Palavras e expressões específicas do Debian

Algumas palavras e expressões são específicas ao Projeto Debian. Outras já são resultado de convenções sedimentadas pela comunidade.

### A palavra Debian é masculino

**Debian** é indicado pelo **gênero masculino** quando isolado. Outras línguas adotaram o gênero feminino, por isso o cuidado:

*O Debian é um ótimo sistema operacional.* ~(masculino)~ <br />
~~*A Debian é um ótimo sistema operacional.*~~ ~(feminino~ ~incorreto)~ <br />
*A distribuição Debian é um ótimo sistema operacional.* ~(feminino~ ~correto)~

### Versão, lançamento e distribuição

Atenção ao diferenciar **versão**, **lançamento** e **distribuição** quando traduzir as palavras *release* ou *distribution*. Muitas vezes os textos em inglês são ambíguos, e o sentido deve ser retirado do contexto. Recomenda-se:

**Distribuição** quando se referir ao sistema operacional Debian completo.

*A distribuição GNU/Linux Debian 10 foi lançada.* <br />
~~(A versão GNU/Linux Debian 10 foi lançada.)~~

**Versão** quando se referir às etapas de desenvolvimento: experimental, instável (unstable), teste (testing) e estável (stable);

*A versão teste vem antes da estável.* <br />
~~(A distribuição teste vem antes da estável.)~~ 

Pode ocorrer sobreposição dos sentidos de distribuição e versão quando nos referimos às **versões pontuais** (point releases):

*Já foram lançadas as versões pontuais Debian 10.3 e 10.4.* <br />
~~(Já foram lançadas as distribuições Debian 10.3 e 10.4)~~ 

A tradução de *release* também pode ser feita pelo genérico **lançamento** (de versão, da distribuição...):

*Em breve deve acontecer o lançamento (release) do Debian 10.5.*

Os **nomes de versão**, de modo geral, devem ser digitadas em **minúsculas**. Claro que há flexibilidade a depender da situação:

*Correções de segurança foram feitas nas versões jessie, stretch e buster.* <br />
*A Buster foi considerada uma ótima distribuição!*

### main, contrib e non-free

Os termos *main*, *contrib* e *non-free* usualmente são acompanhados do termo inglês **archive**, que deve ser traduzido como **repositório**.

*Encontre essas descrições no repositório contrib (contrib archive).*

Muitas dúvidas podem ser facilmente resolvidas com o vocabulário inglês-português elaborado coletivamente pela equipe de tradução. Os termos mais recorrentes e problemáticos são mapeados e inseridos na lista. Consulte-a sempre.

## Contato em idioma estrangeiro

Ao se deparar com o envio de e-mails e outras formas de contato, explicite, quando necessário, a utilização do idioma inglês.

*Please send an email to the oss-security mailing list.* <br />
*Por favor, mande um e-mail (em inglês) para a lista de discussão oss-security.*

Se disponível, acrescente a possibilidade de contato alternativo em português:

*Por favor, mande um e-mail (em inglês) para a lista de discussão oss-security (ou peça ajuda em português pela lista debian-devel-portuguese).*

## Títulos e subtítulos

Apenas a primeira letra em maiúscula:

**Um Título Ruim** ~(uso~ ~exagerado~ ~de~ ~letras~ ~maiúsculas)~ <br />
**Título mais adequado** ~(somente~ ~o~ ~primeiro~ ~termo~ ~em~ ~letra~ ~maiúscula)~

## Artigos definidos

Na língua inglesa, é comum a supressão de artigo definido. Na tradução para o português, o uso do artigo pode ser recomendado. Fica a critério do(a) tradutor(a) tomar a decisão conforme a melhor clareza do conteúdo:

*Recently released free software like eSpeak are important to the accesibility team.* ~(não~ ~há~ ~artigo~ ~antes~ ~de~ ~eSpeak)~ <br />
*Softwares livres recentemente lançados como **o** eSpeak são importantes para o time de acessibilidade.* ~(colocado~ ~artigo~ ~antes~ ~de~ ~eSpeak)~

## Pontuação

As grafias do **ponto final** e da **vírgula** têm especificidades no inglês que não correspondem ao português:

- No inglês, o ponto final vem <ins>antes</ins> de parêntesis e aspas; no português, vem <ins>depois</ins>:

...(and this is a sentence in english **.)** ~(ponto~ ~final,~ ~parêntesis)~ <br />
...(e esta é uma sentença em português **).** ~(parêntesis,~ ~ponto~ ~final)~ <br />

...Debian is a “informational amalgam **,”** in her words...* ~(vírgula~  ~dentro~  ~das~ ~aspas)~ <br />
...o Debian é uma "amálgama informacional **”,** nas palavras dela...* ~(vírgula~  ~fora~ ~das~  ~aspas)~

- No caso de <ins>vírgulas em uma lista</ins>, o sinal não aparece depois do penúltimo elemento:

...and I have used Debian 7, 8, 9 **, and** 10! ~(vírgula~ ~aparece~ ~no~ ~fim)~ <br />
...e eu usei o Debian 7, 8, 9 **e** 10! ~(vírgula~ ~não~ ~aparece~ ~no~ ~fim)~ <br />

...and you can translate web pages, PO files **, or** wiki pages. ~(vírgula~ ~aparece~ ~no~ ~fim)~ <br />
...e você pode traduzir páginas web, arquivos PO **ou** páginas wiki. ~(vírgula~ ~não~ ~aparece~ ~no~ ~fim)~

## Hífen

Alguns casos recorrentes do uso ou não do **hífen** podem causar confusão.

### Consulta rápida

| Palavra |  Palavra | Palavra | Palavra |
| --- | --- | --- | --- |
|código-fonte |pacote-fonte|palavra-chave |servidor-espelho |
|afrodescendente |afro-brasileiro |eurocêntrico |euro-asiático |
|versão recém-lançada |contribuição bem-vinda | | |
|passo a passo |dia a dia |fim de semana |código aberto |
|porte multiarquitetura |uso de pseudocabeçalho |coautor(a) |infraestrutura |
|coordenação |preenchido |reescrita |reeditar |
|pré-lançamento da versão |pós-evento do Dia do Debian |pacote pré-candidato ||
|e-mail |wi-fi |on-line ||
|software não livre |versão não oficial |uso não autorizado ||

### Situações recorrentes

- Primeiro termo composto por forma <ins>substantiva</ins>, <ins>adjetiva</ins>, <ins>numeral</ins> ou <ins>verbal</ins>:

| Palavra |  Palavra | Palavra | Palavra |
| --- | --- | --- | --- |
| código-fonte | pacote-fonte | palavra-chave | servidor-espelho |


- Atenção aos termos <ins>afro, sino, euro</ins> e situações com duas etnias:

| Palavra |  Palavra | Palavra | Palavra |
| --- | --- | --- | --- |
|afrodescendente |afro-brasileiro |eurocêntrico |euro-asiático |
 
- Formas <ins>além, aquém, recém, bem, sem</ins>:

| Palavra |  Palavra |
| --- | --- |
|versão recém-lançada |contribuição bem-vinda |
 
- Não se emprega o hífen nas <ins>locuções</ins> (com exceção de expressões já consagradas):

| Palavra |  Palavra |  Palavra |
| --- | --- | --- |
|passo a passo |dia a dia |fim de semana |
 
- Se o primeiro elemento <ins>termina com vogal diferente do segundo elemento</ins>, escreve-se sem hífen:

| Palavra |  Palavra | Palavra | Palavra |
| --- | --- | --- | --- |
|porte multiarquitetura |uso de pseudocabeçalho |coautor(a) |infraestrutura |

- Uma exceção à regra acima, os prefixos <ins>co-, pro-, pre-, re-</ins> não acentuados:

| Palavra |  Palavra | Palavra | Palavra |
| --- | --- | --- | --- |
|coordenação |preenchido |reescrita |reeditar |


- Outra exceção, os prefixos acentuados <ins>pós-, pré-, pró-, sem-</ins> demandam o hífen:

| Palavra |  Palavra | Palavra |
| --- | --- | --- |
|pré-lançamento da versão |pós-evento do Dia do Debian |pacote pré-candidato |
 
- <ins>Anglicismos</ins>:

| Palavra |  Palavra |
| --- | --- |
|e-mail |wi-fi |
 
- O <ins>não como advérbio</ins> também não demanda o hífen:

| Palavra |  Palavra | Palavra |
| --- | --- | --- |
|software não livre |versão não oficial |uso não autorizado |

## Data e hora, classes numéricas e separador decimal

No formato em português para **datas**, ao contrário do inglês, os nomes dos dias da semana e dos meses são escritos com inicial minúscula. Quando as datas aparecem em formato numérico, a ordem correta é dia, mês e ano, separados por uma barra inclinada. É aconselhável usar os valores indicando **horas** (de 0 a 23), minutos e segundos separados por dois pontos Guia de estilo de tradução, TLDP-BR:

*...on Wednesday, 21 February 2001...* (em inglês) <br />
*...na quarta-feira, 21 de fevereiro de 2001...* (em português) <br />

*02/21/01* (em inglês) <br />
*21/02/01* (em português) <br />

*2:00 pm* (em inglês) <br />
*14:00* ou *14 horas* (em português)

Adotamos a **vírgula decimal** para separar a parte inteira da parte não inteira. Adotamos o **ponto** ou um **espaço em branco** para separação de classes numéricas (dezena, centena, milhar...). O cuidado é importante pois o idioma inglês inverte essa utilização:

1.300,56 ~(padronização)~ <br />
1 300,56 ~(alternativa~ ~utilizando~ ~espaço~ ~em~ ~vez~ ~do~ ~ponto)~

"3.972.317.184 bytes é o tamanho exato da imagem debian-10.6.0-amd64-DVD-1.iso" ~(o~ ~ponto~ ~é~ ~a~ ~melhor~ ~opção~ ~devido~ ~ao~ ~tamanho~ ~do~ ~número)~

## Siglas

As **siglas** devem ser traduzidas na primeira vez que aparecem no documento:

*Há dois papéis na equipe do Sistema de Administração do Debian (Debian System Administration - DSA): membro e trainee.*

*Procure por URL (Uniform Resource Locator - localizador uniforme de recursos).*

(seção baseada no Guia de estilo de tradução, TLDP-BR)

## Tempos verbais, imperativo

O tempo **futuro**, utilizado com grande frequência nos manuais técnicos em inglês, admite uma tradução no tempo **presente**, mais comum em português:

*This chapter will describe the procedure to install a client/server system.* ~(futuro)~ <br />
*Este capítulo descreve os procedimentos para a instalação de um sistema cliente/servidor.* ~(presente)~

O inglês costuma usar as formas modais do tipo "deveria", "poderia", "certifique-se de", etc, que em português podem ser substituídas pelo **imperativo** na terceira pessoa:

*You should save data before proceeding to the next step.* ~(deveria/poderia)~ <br />
*Salve os dados antes de ir para o próximo passo.* ~(imperativo)~

(seção baseada no Guia de estilo de tradução, TLDP-BR)

## Outros erros comuns

- **Língua** e **idioma** são sinônimos, dialeto e linguagem não devem ser usados.
- "em vez de" e não ~~ao invés de~~
- caractere, não ~~caracter~~
- não fazer **ênfase dupla**, ou seja, usar dois marcadores de realce:
  - stable (estável) e não ~~stable ("estável")~~
  - Suporte de Longo Prazo (Long Term Support) e não ~~Suporte de Longo Prazo ("Long Term Support")~~
  - Já nesta forma "...a estável (em inglês, "stable")..." está correta, já que os parêntesis tem uma função explicativa e as aspas dirigem-se à tradução em si.
- **logotipo** Debian ou **logomarca** Debian e não ~~logo Debian~~
- manter "direitos de autor" e não flexionar para ~~direitos de autor(a)~~, pois é uma expressão jurídica.

## Diversidade e Debian 

![logotipo Debian Diversidade](https://www.debian.org/logos/diversity-2019.png)

A comunidade Debian preocupa-se com a temática da [diversidade](https://www.debian.org/intro/diversity.pt.html) e promove a visibilidade e o trabalho colaborativo a partir desta perspectiva. Nesse sentido, a [comunidade](https://wiki.debian.org/Teams/Community) orienta-se pela adoção de uma linguagem respeitosa e igualitária. A língua é uma construção social e histórica, importante fator de socialização. No trabalho de tradução, é preciso atentar para como o idioma representa a diversidade de gênero no contexto de uma sociedade machista.

Para explicitar a diversidade de gênero no [site do Debian](/traduzir/webwml) e nas [notícias publicadas](/traduzir/publicidade), nossa solução **atual** é a utilização **obrigatória** de sufixos como estes a seguir. Esta solução por enquanto não está sendo usada nas traduções das [descrições de pacotes](/traduzir/ddtp), [templates debconf](https://debianbrasil.org.br/traduzir/debconf) e [manpages](https://debianbrasil.org.br/traduzir/manpages).

*um(a) autor(a)* <br />
*aqueles(as) desenvolvedores(as)* <br />
*alguns(mas) amigos(as)*

Contudo, há momentos em que a grafia esconde relações sociolinguísticas importantes:

**Mulher pública**: *prostituta* <br />
**Homem público**: *homem que se dedica à política* <br />
**Governanta**: *mulher que cuida da casa* <br />
**Governante**: *homem que governa um povo*

Em especial, deve-se evitar duas práticas que podem produzir <ins>efeitos discriminatórios</ins> na tradução:

**Androgenismo**: o processo pelo qual situa-se o masculino como a perspectiva universal, pressupondo abranger, mas efetivamente excluindo, todas as outras perspectivas: mulheres, pessoas intersexo e não binárias.

*Os desenvolvedores criaram uma solução e duas desenvolvedoras participaram dela.* <br />
~(A~ ~contribuição~ ~feminina~ ~aparece~ ~como~ ~acessória)~

**Sexismo**: a atribuição de papeis, valores e capacidades de acordo com o sexo biológico. Uma classificação binária que desconsidera, e novamente marginaliza, identidades que não se encaixam em tal categorização.

*Os contribuidores terminaram o trabalho.* <br />
~(Pressupõe-se~ ~que~ ~pessoas~ ~de~ ~todos~ ~os~ ~gêneros~ ~terminaram~ ~o~ ~trabalho)~

Observe-se que estas práticas são comuns, pois acontecem de maneira <ins>consciente ou inconsciente</ins>. Um dos objetivos deste manual de estilo está em apontar para essas barreiras discriminatórias e evitá-las, de forma consciente ao se realizar a tradução, promovendo ativamente a inclusão na comunidade Debian Brasil. O recurso utilizado atualmente pela equipe de tradução é a justaposição da desinência de gênero:

*o(a) autor(a)* <br />
*os(as) contribuidores(as)*

Contudo, incentivamos que toda a equipe se aproprie de <ins>outros recursos linguísticos</ins> que, além de suplantar as barreiras discriminatórias, também enriquecerão os textos produzidos e a própria habilidade estilística de quem traduz.

### Estratégias de escrita não sexista e não andrógena

(Esta seção foi baseada no [manual para o uso não sexista da linguagem](https://edisciplinas.usp.br/pluginfile.php/3034366/mod_resource/content/1/Manual%20para%20uso%20n%C3%A3o%20sexista%20da%20linguagem.pdf)).

- Não usar <ins>frases estereotipadas</ins> que consolidem papéis tradicionais que imputem ideias e práticas depreciativas:

*Joana e Mário são tradutores, e Mário ainda ajuda nas tarefas domésticas.* ~(estereotipia)~ <br />
*Joana e Mário são tradutores, e dividem as tarefas domésticas.* ~(alternativa~ ~mais~ ~adequada)~

- Não usar o masculino como <ins>universal</ins>, <ins>genérico</ins> ou <ins>neutro</ins>:

*Graças ao suporte desses parceiros...* ~(masculino~ ~genérico)~ <br />
*Graças ao suporte desses(as) parceiros(as)...* ~(alternativa)~ <br />
*Graças ao suporte de parcerias...* ~(alternativa)~ <br />

*Todos os membros da organização...* ~(masculino~ ~universal)~ <br />
*O pessoal da organização... / As pessoas da organização..* ~(alternativa)~ <br />

*Quatro servidores Debian comprometidos por um atacante desconhecido.* ~(masculino~ ~neutro)~ <br />
*Quatro servidores Debian comprometidos por um ataque de autoria desconhecida.* ~(alternativa)~ <br />

*João e Maria são contribuidores...* ~(masculino~ ~universal)~ <br />
*João e Maria contribuíram...* ~(alternativa)~

- Cuidado com o gênero em <ins>funções</ins>, <ins>cargos</ins> ou <ins>profissões</ins>:

*o(a) contribuidor(a)* <br />
*o contribuidor e a contribuidora* <br />

*Os gerentes de lançamento avisaram...* <br />
*A gerência de lançamento avisou...* <br />
*Gerentes de lançamento avisaram...*

- <ins>Elipses</ins> são boas opções estilísticas:

*Há 26 anos todos viviam sem o Debian...* <br />
*Há 26 anos se vivia sem o Debian...* <br />

*Eles eram participativos na tradução...* <br />
*Eram participativos na tradução...* <br />

*Ele informou um problema de licença...* <br />
*Informou um problema de licença...*

- Trocar o verbo pela <ins>terceira pessoa do singular ou plural</ins>, e usar o pronome de tratamento <ins>você/vocês</ins>:

*O usuário estará mais seguro se...* <br />
*Você sentirá mais segurança se...* <br />

*Os contribuidores poderão...* <br />
*Se vocês contribuírem poderão...*

- Evitar o uso de <ins>aquele/aqueles</ins>:

*Aqueles que saibam traduzir...* <br />
*Quem souber traduzir...* 

- Trocar pronomes e advérbios com gênero masculino por palavras que têm <ins>o mesmo sentido</ins> e <ins>sem a marca de gênero específico</ins>:

*Você deve defender os seus...* <br />
*Você deve defender sua gente...* <br />

*Muitos têm dúvidas sobre segurança...* <br />
*Muitas pessoas têm dúvidas sobre segurança...* <br />
*A maioria tem dúvidas sobre segurança...*

- Evitar <ins>saltos semânticos</ins>, o uso do masculino genérico seguido de características especificamente masculinas:

*Todos os desenvolvedores poderão comparecer com suas companheiras.* ~(salto~ ~semântico)~ <br />
*O pessoal poderá comparecer acompanhado.* ~(alternativa)~ <br />

*Ser um contribuidor significa não ter discriminação contra pessoas não binárias.* ~(salto~ ~semântico)~ <br />
*Não se permite discriminação contra pessoas não binárias.* ~(alternativa)~

- Manter o significado, mas alterar <ins>sujeitos</ins> e <ins>verbos</ins>:

*Os contribuidores têm um bom comprometimento...* <br />
*O comprometimento da contribuição está bom...*

- O <ins>gerúndio</ins> é uma solução para evitar o uso de palavras exclusivamente identificadas ao masculino:

*Os contribuidores geram estatísticas.* <br />
*Contribuindo geram-se estatísticas.*

- Alguns termos não variam, então basta <ins>retirar o artigo caracterizador de gênero</ins>:

*Os representantes locais do Debian...* <br />
*Representantes locais do Debian...*

- Faça uso de <ins>orações passivas reflexivas</ins>:

*Os membros do Debian produzem alternativas...* <br />
*Serão produzidas alternativas pelo Debian...*

### Novas terminologias inclusivas

Em uma [recente manifestação](https://lkml.org/lkml/2020/7/4/229), a equipe de desenvolvimento do kernel Linux optou por uma nova terminologia, mais inclusiva e menos discriminatória. Como são palavras de uso historicamente difundido, existirão dificuldades, e mesmo oposição, para sua adoção. Os(As) tradutores(as) devem ficar atentos(as) à nova terminologia para que sejam plenamente utilizadas.

Apresentamos abaixo a lista de alguns desses vocábulos. Sua tradução pode ser encontrada no vocabulário.

- Nova terminologia para *master/slave*:

*{primary, main} / {secondary, replica, subordinate}* <br />
*{initiator, requester} / {target, responder}* <br />
*{controller, host} / {device, worker, proxy}* <br />
*leader / follower* <br />
*director / performer*

- Nova terminologia para *blacklist/whitelist*:

*denylist/allowlist* <br />
*blocklist/passlist*

## Acessibilidade e Debian

![logotipo acessibilidade](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Twemoji12_267f.svg/200px-Twemoji12_267f.svg.png)

O site do Debian é majoritariamente composto por texto e isto facilita o acesso a seu conteúdo. Consequentemente, traz poucas dificuldades ao trabalho de tradução quando se pensa em estratégias de acessibilidade (a11y). Contudo, é necessário atentar para as **práticas ou omissões** que podem dificultar, e até mesmo impedir, que muitas pessoas possam usufruir das informações contidas na documentação do Debian.

É importante salientar que um conteúdo acessível não só possibilita a comunicação de pessoas com deficiência, como também ajuda **pessoas idosas e usuários(as) em geral**. De outra perspectiva, um trabalho de tradução pautado em preocupações de acessibilidade traz ao(à) tradutor(a) novas competências estilísticas.

Retiramos do [Glossário do Programa de Inclusão da Pessoa com Deficiência](https://www4.planalto.gov.br/ipcd/assuntos/sobreoprograma/glossario) três conceitos que sempre devem estar presentes no trabalho dos(as) tradutores(as):

**Acessibilidade**

(a11y) condição para utilização, com segurança e autonomia, total ou assistida, dos espaços, mobiliários e equipamentos urbanos, das edificações, dos serviços de transporte e dos dispositivos, sistemas e meios de comunicação e informação, por pessoa com deficiência ou com mobilidade reduzida.

**Barreiras**

São obstáculos visíveis ou invisíveis, no ambiente ou na atitude, que impedem a plena e efetiva participação dessas pessoas na sociedade, em igualdade de oportunidades com as demais pessoas. Qualquer entrave que limite ou impeça o acesso, a liberdade de movimento, a circulação com segurança e a possibilidade de as pessoas se comunicarem ou terem acesso à informação.

**Pessoa com Deficiência**

É aquela que têm impedimentos de longo prazo de natureza física, mental, intelectual ou sensorial, os quais, em interação com diversas barreiras, podem obstruir sua participação plena e efetiva na sociedade em igualdades de condições com as demais pessoas.

### Estratégias de acessibilidade

Uma primeira abordagem às práticas abaixo descritas foram retiradas das [Diretrizes de Acessibilidade para Conteúdo Web (WCAG) 2.0](https://www.w3.org/Translations/WCAG20-pt-br/WCAG20-pt-br-20141024) e do blog [Texting Etiquette for Low Vision](https://www.perkinselearning.org/technology/blog/texting-etiquette-low-vision).

- **Referência**: utilize a expressão **pessoa com deficiência**. Evite quaisquer outros termos depreciativos (excepcional, incapacitado, inválido, deficiente, etc). Evite também a expressão "pessoa portadora de deficiência", pois foi substituída.

- **Vocabulário**: use uma linguagem clara, simples e direta. Divida assuntos por blocos, para facilitar o entendimento.

- **Erros ortográficos e linguagem informal**: erros sempre acontecem e as pessoas poderão inferir seu significado também a partir da sentença como um todo. De qualquer modo, evite jogos de palavras ou adição de letras que atrapalhem a interpretação.

- **Marcadores de plural**: evite o uso desnecessário de plural utilizando marcadores como (s), (as), (os), (es), pois causam problemas de interpretação por tecnologias assistivas:

*Verifique: disco(s) rígido(s), memória(s) e monitor(es)* ~(plural~ ~desnecessário~ ~para~ ~o~ ~entendimento)~ <br />
*Verifique: discos rígidos, memórias e monitores* ~(mais~ ~simples~ ~e~ ~com~ ~sentido~ ~original~ ~mantido)~

- **Abreviações**: leitores de tela podem ler abreviações letra por letra, dificultando o entendimento. Como qualquer grande projeto, o Debian possui inúmeras abreviações. Se não é possível ausentar-se de utilizá-las, lembre-se sempre de definir as abreviaturas no começo do texto.

- **Pontuação**: é comum o uso seguido de pontuações, por exemplo !!! ou ?!?. Evite essa ênfase, pois leitores de tela vocalizarão pontuação por pontuação, deixando a leitura monótona.

- **Emoticons e emojis**: como parte da comunicação contemporânea, são interpretados por tecnologias assistivas. Contudo, desenhos pouco convencionais podem ser um problema. Além disso, leitores de tela vocalizarão gráfico por gráfico, então evite-os no meio de frases. Uma regra geral é deixar emoticons e emojis para o fim do parágrafo e evitar a comunicação de informação essencial através deles.

- **Elementos não textuais**: gráficos, áudios, vídeos, botões, etc. devem ser acompanhados por descrições textuais.

- **Imagens**: verifique se é possível que as imagens possam ser aumentadas. Sempre defina o atributo alt da tag HTML de imagem. Uma prática ainda mais inclusiva é inserir uma descrição sumária da imagem abaixo da mesma, na forma de legenda.

- **Tabelas**: devem ter cabeçalhos de colunas e linhas. Se possível, fornecer uma versão linear da tabela e um resumo da mesma.

- **Cores**: estabeleça contrastes suficientes entre cores de fundo e frente.

- **Links**: verifique se o link é claro sobre seu destino, e que a navegação pelo pressionamento da tecla TAB seja feita na ordem determinada.

- **Páginas em série**: se a página faz parte de um conjunto temático, certifique-se de que há indicações textuais de localização (em que página se está e o conjunto de páginas).

### Um método para descrição de imagem

Este processo foi adaptado do blog [Tire todas as suas dúvidas sobre o Projeto #PraCegoVer](https://palavrachaveonline.blogspot.com/2017/06/tire-todas-as-suas-duvidas-sobre-o.html).

1. Anuncie o tipo de imagem: fotografia, logotipo, ilustração, etc.
1. Descreva da esquerda para a direita, de cima para baixo (a ordem natural de escrita e leitura ocidental).
1. Informe as cores: "logotipo em cores vermelha e preta, com letras pretas, em um fundo branco"; evite reafirmar que a imagem é colorida.
1. Descreva todos os elementos de um determinado ponto da imagem e só depois passe para o próximo ponto, criando uma sequência lógica.
1. Descreva com períodos curtos, use poucas palavras.
1. Pode-se começar pelos elementos menos importantes, contextualizando a cena, e afunilar até chegar ao clímax, no ponto-chave da imagem.


## Referências

### Vocabulários

[Vocabulário inglês-português](https://salsa.debian.org/l10n-br-team/vocabulario) da equipe de tradução, com termos reunidos pela equipe. Mais informações podem ser encontradas na [página wiki](/traduzir/vocabulario) que possui um histórico de iniciativas anteriores.

[Lista de termos](https://www.ime.usp.br/~kon/ResearchStudents/traducao.html) do Fabio Kon, com comentários a respeito de boas práticas e erros comuns.

[AmaGama](https://amagama-live.translatehouse.org), memória de tradução com campo para pesquisa.

[Vocabulário padrão](https://vp-br.herokuapp.com) com campo para pesquisa.

[Glossário Orca de inglês-português](https://quark.fe.up.pt/cgi-bin/orca/glossario) com termos de informática. Possui campo para pesquisa, mas não é atualizado desde 2000.

### Dicionários

[Dicionário Priberam de Língua Portuguesa]([https://www.priberam.pt/dlpo)

[Michaelis On-line](https://michaelis.uol.com.br): diversos dicionários, incluindo português e inglês <-> português.

[The Free Dictionary](https://www.thefreedictionary.com)

[Interactive Terminology for Europe (IATE)](https://iate.europa.eu/home): um banco de dados de termos para a União Europeia, onde pode-se buscar padronizações entre várias línguas, incluindo relações entre os idiomas inglês e português.

### Manuais de redação

[Manual para o uso não sexista da linguagem](https://edisciplinas.usp.br/pluginfile.php/3034366/mod_resource/content/1/Manual%20para%20uso%20n%C3%A3o%20sexista%20da%20linguagem.pdf) do Estado do Rio Grande do Sul.

[Novo manual de redação](https://www1.folha.uol.com.br/folha/circulo/manual_redacao.htm) do Jornal Folha de São Paulo.

[Guia de estilo de tradução](https://web.archive.org/web/20021014085013/http://br.tldp.org/ferramentas/vp/estilo/index.html) do projeto TLDP-BR (inativo).

Verbete [Manual de redação](https://pt.wikipedia.org/wiki/Manual_de_reda%C3%A7%C3%A3o) da Wikipedia.

A página [La typographie française](https://www.debian.org/international/french/typographie) do time francês de tradução do Debian é um bom exemplo de manual de estilo.

A página [English for software localisation](http://jbr.me.uk/linux/esl.html) tem algumas dicas legais para vários âmbitos da escrita no idioma inglês.

### Gramática

BECHARA, Evanildo. **Moderna gramática portuguesa**. Rio de Janeiro: Nova Fronteira, 2009.

CUNHA, Celso; CINTRA, Lindley. **Nova gramática do português contemporâneo**. Rio de Janeiro: Lexikon, 2017.