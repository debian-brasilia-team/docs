---
title: Debian Edu
description: 
published: true
date: 2025-03-10T22:26:07.170Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:17:02.838Z
---


# Tradução do DebianEdu

O DebianEdu é um [Debian Pure Blend](https://wiki.debian.org/DebianPureBlends) voltado para ambientes educacionais com diversos serviços pré-configurados e facilidade de implementação para públicos não especializados. A frente de tradução inclui o site web, wiki, documentos e pacotes. A descrição de pacotes é feita pelo [DDTP](/traduzir/ddtp) e já está praticamente completa. O [site web](https://blends.debian.org/edu) do projeto é traduzido através de arquivos PO que estão em um [repositório no Salsa](https://salsa.debian.org/blends-team/website/tree/master/www/edu).

Vídeo da palestra no [Debian Reunion em Hamburgo](https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2022/Debian-Reunion-Hamburg/debian-reunion-hamburg-2022-9-an-update-on-debian-edu-aleksis-and-foss-in-education.webm)

## Tradução do manual

O manual do DebianEdu é realizado no site do [weblate](https://hosted.weblate.org/projects/debian-edu-documentation/).

## Tradução do site web do DebianEdu

O site já foi traduzido e aguarda atualização dos(as) mantenedores(as) da página.

A [página principal do Debian Edu](https://blends.debian.org/edu/) é traduzida através de um arquivo PO. As [orientações encontram-se no Salsa](https://salsa.debian.org/blends-team/website/-/blob/master/www/edu/README.translations) e seguem traduzidas abaixo:

1. Clone o repositório git para seu ambiente local;
1. Copie o arquivo ```index.pot```, renomeando-o para ```pt_BR.po```;
1. É possível adicionar conteúdos específicos - consulte o arquivo ```de.add```;
1. Realize a tradução normalmente;
1. Envie o arquivo ```pt_BR.po``` '''compactado''' para DebianList:debian-edu.

## Teste local da tradução do manual do Debian Edu

Passo a passo para testar localmente a tradução do manual do Debian Edu

Instale as dependências necessárias à compilação, e o ```wget``` para baixar o arquivo PO:

```
sudo apt install dpkg-dev po4a xsltproc docbook-xsl wget git
```

Faça a clonagem rasa do repositório Git do manual do Debian Edu, somente o ramo ```master```:

```
git clone --depth 1 --single-branch https://salsa.debian.org/debian-edu/debian-edu-doc
```

Entre no diretório da versão atual (atualmente, <<DebianCodename(stable)>>):

```
cd debian-edu-doc/documentation/debian-edu-bookworm/
```

Baixe o arquivo PO atualizado do Weblate:

```
wget https://hosted.weblate.org/download/debian-edu-documentation/bookworm-manual/pt_BR/ -O debian-edu-bookworm-manual.pt-br.po
```

Construa a documentação para o português brasileiro:

```
make build LINGUA=pt-br
```

No diretório deve haver o arquivo ```debian-edu-bookworm-manual.pt-br.html``` e as respectivas versões xml e epub.

Em caso de erros na construção, tente limpar para depois construir novamente:

```
make clean
```

----

## Veja também

 * [Manual de estilo](/traduzir/manual-de-estilo) direcionado à equipe de tradução em português.
 * [Vocabulário padrão](/traduzir/manual-de-estilo) e outras referências para suporte à tradução.
