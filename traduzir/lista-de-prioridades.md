---
title: Lista de prioridades
description: 
published: true
date: 2025-03-08T19:13:23.881Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:21:41.377Z
---

# Prioridades e encaminhamentos da equipe de tradução

Esta página é baseada [neste e-mail](https://lists.debian.org/debian-l10n-portuguese/2023/01/msg00160.html), resultado da reunião realizada on-line no dia **23 de janeiro de 2023**, na qual definimos uma lista de prioridades para a equipe de tradução. Os encaminhamentos não são obrigatórios, não há problema nenhum em se traduzir outras áreas que não estejam listadas aqui. As orientações servem para alinharmos nosso trabalho enquanto equipe.

## Plano de trabalho 2025

Metas de tradução para o lançamento do [Debian 13 Trixie](https://wiki.debian.org/DebianReleases) em 2025. Métricas atualizadas podem ser consultadas executando-se localmente o script *debian-l10n-report.sh* que está [neste repositório Salsa](https://salsa.debian.org/tico/l10n-utils).

:star: Data estimada para lançamento da distribuição [Debian Trixie](https://wiki.debian.org/DebianTrixie): JULHO DE 2025.

Legenda:

- ⚫ ⚫ ⚫ é preciso organizar ou iniciar essa frente de tradução
- 🟢 ⚫ ⚫ trabalho de tradução iniciado; contribuições podem estar baixas
- 🟢 🟢 ⚫ trabalho de tradução em bom andamento
- 🟢 🟢 🟢 tradução completa para lançamento da próxima [versão estável](https://wiki.debian.org/DebianStable)

| Grupo | Nível | Item |
| -------- | ----- | ----------- |
| Instalação e configuração de pacotes | 🟢 🟢 🟢 | [Instalador Debian](https://d-i.debian.org/l10n-stats/translation-status.html) |
| | 🟢 🟢 ⚫ | [Todos os arquivos debconf](https://www.debian.org/international/l10n/po-debconf/pt_BR) |
| Arquivos po de pacotes importantes ([lista completa](https://www.debian.org/international/l10n/po/pt_BR)) | 🟢 🟢 ⚫ | adduser - po/pt_BR.po (57%) |
| | 🟢 🟢 🟢 | apt - po/pt_BR.po (98%) |
| | 🟢 🟢 🟢 | dpkg - po/pt_BR.po (99%) |
| | 🟢 🟢 🟢 | dpkg - dselect/po/pt_BR.po (99%) |
| | 🟢 🟢 ⚫ | debconf - po/pt_BR.po (89%) |
| | 🟢 🟢 ⚫ | apt-listchanges - po/pt_BR.po (97%) |
| | 🟢 🟢 ⚫ | apt-listbugs - po/pt_BR.po (100%) |
| Arquivos **man** importantes ([lista completa](https://www.debian.org/international/l10n/po4a/pt_BR)) | 🟢 🟢 🟢 | adduser [Bug aberto com a atualização da tradução](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1094210) |
| | 🟢 🟢 🟢 | apt (3%) |
| | 🟢 🟢 🟢 | debconf (34%) |
| | 🟢 🟢 🟢 | dpkg (1%) |
| Descrições de pacotes | 🟢 🟢 🟢 | Required |
| | 🟢 🟢 🟢 | Important |
| | 🟢 🟢 🟢 | Standard |
| | 🟢 🟢 🟢 | [500 pacotes mais instalados - POPCON500](https://popcon.debian.org/by_inst) |
| Site web | 🟢 🟢 🟢 | [Status das páginas do site](https://www.debian.org/devel/website/stats/pt.en) |
| | 🟢 🟢 ⚫ | [Notícias no site](https://www.debian.org/News/) dos lançamentos de versão e pontuais |
| | 🟢 🟢 ⚫ | [Micronotícias](https://micronews.debian.org) |
| | 🟢 🟢 ⚫ | [Bits do Debian](https://bits.debian.org) (blog) |
| Documentos | ⚫ ⚫ ⚫ | [Debian Developer's Reference](https://www.debian.org/doc/manuals/developers-reference), [mensagem na lista](https://lists.debian.org/debian-devel/2021/08/msg00232.html), [repositório](https://salsa.debian.org/debian/developers-reference) |
| | ⚫ ⚫ ⚫ | [Debian Reference](https://www.debian.org/doc/manuals/debian-reference), [repositório](https://salsa.debian.org/debian/debian-reference) |
| | ⚫ ⚫ ⚫ | [Debian Policy Manual](https://www.debian.org/doc/debian-policy), [mensagem na lista](https://lists.debian.org/debian-devel/2021/08/msg00444.html), [repositório](https://salsa.debian.org/dbnpolicy/policy) |



Anote abaixo as sugestões para auxiliar nas discussões e futuras inserções na tabela de prioridades:

- [nome] Sugestão
- [tico] [Debian beginners handbook](https://salsa.debian.org/arpinux/debian-beginners-handbook), usar weblate
- [demanda2023] [História do Projeto Debian](https://www.debian.org/doc/manuals/project-history/index.pt.html), [repositório](https://salsa.debian.org/ddp-team/project-history), usar weblate
-  [demanda2022] [devscripts](https://packages.debian.org/sid/devscripts), [e-mail na lista](https://lists.debian.org/debian-devel/2021/08/msg00450.html), [repositório](https://salsa.debian.org/debian/devscripts)


## Histórico de encaminhamentos de 2023-2024

**Pautas de reunião**:

- foco na tradução do BITS e nas notícias que são publicadas na pagina principal [Berlim]
- como simplificar e automatizar o processo de contribuição, sempre focando em atrair e reter novos/as contribuidores/as [Charles]
- rever as ações de 2023 e a situação atual da equipe [Tico]
- elencar iniciativas para o ano de 2024, como sprints e oficinas [Tico]
- modificar esta página (!ListaPrioridades) para se tornar a referência de trabalho da equipe [Tico]
- definir como meta para 2025 [[#plano|traduções que vão impactar no próximo lançamento]] [Paulo] - [mensagem original na lista de discussão](https://lists.debian.org/debian-l10n-portuguese/2023/06/msg00014.html)
  - Arquivos debconf;
  - Pacotes como apt, dpkg, debconf, adduser, etc;
  - Algumas arquivos de man;
  - "n" descrições de pacotes. Definir "n" com um número a ser alcançado;
  - (secundariamente) A parte web (site, micrononews, notícias, etc);
  - (secundariamente) Documentos prioritários;
 
 **Trabalhos em processo de finalização**:

:warning: [DDTPSS](https://ddtp.debian.org/ddtss/index.cgi/pt_BR): devagar, há centenas de revisões em aguardo - responsável ??? (Fred?)

:warning: [Debian New Members](https://nm.debian.org): em RFR2, será passado para LCFC e enviado para produção.

:warning: [dpkg](https://wiki.debian.org/dpkg) e [adduser](https://wiki.debian.org/PkgAdduser): em processo de tradução; apt está com o [Drebs para revisão](https://lists.debian.org/debian-l10n-portuguese/2022/08/msg00015.html), mas não andou - vale a pena dar um ping nele para ver se precisa de ajuda. Para entrar no Bookworm (Debian 12), precisam estar nos repositórios até o fim do Soft Freeze (2023-03-11). Ver os [comentários do Terceiro](https://lists.debian.org/debian-l10n-portuguese/2021/08/msg00017.html)sobre essas traduções - responsáveis Charles (dpkg) e Paulo (adduser).

**Iniciativas para o ano de 2023**:

⭐ [Debian Bits](https://bits.debian.org): poucas postagens, mas traduções paradas; será retomado - responsável Charles

⭐ [Manual do(a) Administrador(a) Debian](https://debian-handbook.info/browse/pt-BR/stable): [hospedado no Weblate](https://hosted.weblate.org/languages/pt_BR/debian-handbook), pode ser uma boa entrada de iniciantes; também deve ser uma prioridade da equipe como forma de divulgação do Projeto Debian em pt_BR; marcar um sprint - responsável Charles

⭐ [DDTP/DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_BR): os sprints com iniciantes atraem público, mas devemos pensar em estratégias para reter essas pessoas; marcar um sprint - responsável Tico (e Fred?); Mandar email para as pessoas que contribuíram alguma vez (é possível ver lista de pessoas); como meta da frente, sistematizar/elaborar a lista de pacotes mais importantes (Required, Important, Standard, POPCON500).

⭐ [História do Projeto Debian](https://www.debian.org/doc/manuals/project-history/index.pt.html) ([repositório](https://salsa.debian.org/ddp-team/project-history)): nova frente de trabalho, mais textual e menos código, pode ser uma boa para iniciantes; necessita organização dos arquivos PO; marcar início do projeto - responsável Tico;

⭐ Divulgação Debian nas escolas: retomar a iniciativa, pois teremos maior aproximação com institutos federais; há novas normativas educacionais que podem facilitar; reorganizar projeto e conteúdo focando num trabalho de longo prazo com as instituições - responsável Tico;

⭐ Encontros com a comunidade internacional: videochamadas com pessoas da tradução de outros países; algo como a série "[People Behind Debian](https://raphaelhertzog.com/2011/03/03/people-behind-debian-christian-perrier-translation-coordinator)" do Raphael Hertzog; foco na divulgação da comunidade não DD, bem como na prática de idioma estrangeiro - responsável Tico;

**Outras discussões**:

💬 Uso mais intensivo de tradução automática: usar plataformas e extensões de navegador; precisamos de ajuda para criação de scripts (Javascript e CSS) para facilitar as interfaces DDTSS e wiki - responsável Tico e Charles

💬 Processo MIA: criar procedimento de ping, de forma agradável, mantendo a comunicação e coesão da equipe; uma possibilidade é começar usando o banco de dados do DDTSS

💬 [Divulgação das ações da equipe](https://salsa.debian.org/debian-brasil-team/debiannews): montar cronograma de postagens (e-mail, Telegram, etc), bem simples e com frequência razoável - responsável Tico

**Eventos em 2023 para participação da equipe**:

Legenda: ✔ participamos - ❌ não participamos - ⚫ não aconteceu

- ❌ [12-16/fev: miniDebConf Portugal](https://pt2023.mini.debconf.org): organização parece devagar, haverá localização? Deve haver transmissão de acordo com a wiki
- ✔ [04-05/fev: Fosdem](https://fosdem.org/2023/schedule/track/translations): organização da Translation !DevRoom por Paulo (phls) e Lenharo
- [15/abr: Flisol (várias cidades)](https://flisol.info/FLISOL2023/BrasilCidades): participações em diferentes cidades.
- [5-6/mai: CryptoRave (São Paulo, SP)](https://www.cryptorave.org): data-limite para atividades é 10/abril/2023.
- [25-27/maio: MiniDebConf Brasília](https://brasilia.mini.debconf.org/): participação de Paulo e Charles; Possibilidade de pedir para o Debian uma ajuda para pagar as passagens das pessoas que estão contribuindo nas traduções.
- [25-30/julho: Campus Party 15 SP](https://brasil.campus-party.org/cpbr15)
- [16/agosto: Dia do Debian 30 anos](https://debianbrasil.gitlab.io/debian30anos): on-line.
- [10-17/set: DebConf 23 (Koichi, Índia)](https://debconf23.debconf.org/): quem vai? O investimento será grande.
- [11-13/out: LatinoWare (Foz do Iguaçu, PR)](https://latinoware.org): que tal um encontro presencial da equipe? Possibilidade de uma palestra ou painel, e de um estande. Possibilidade de pedir para o Debian uma ajuda para pagar as passagens das pessoas que estão contribuindo nas traduções.
- ??-??/???: !MiniDebConfs Maceió, Salvador e Curitiba: vão acontecer?

## Histórico de encaminhamentos de 2022

Esses tópicos não foram discutidos. Ficaram como sugestão para início de trabalho:

- [Debian Developer's Reference](https://www.debian.org/doc/manuals/developers-reference):
  - <https://lists.debian.org/debian-devel/2021/08/msg00232.html>
  - Repositório: <https://salsa.debian.org/debian/developers-reference>
- [Debian Reference](https://www.debian.org/doc/manuals/debian-reference)
  - Repositório: <https://salsa.debian.org/debian/debian-reference>
- Debian Policy Manual](https://www.debian.org/doc/debian-policy)
  - <https://lists.debian.org/debian-devel/2021/08/msg00444.html>
  - Repositório: <https://salsa.debian.org/dbnpolicy/policy>
- [devscripts](https://packages.debian.org/sid/devscripts)
  - <https://lists.debian.org/debian-devel/2021/08/msg00450.html>
  - Repositório: <https://salsa.debian.org/debian/devscripts>
