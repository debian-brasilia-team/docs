---
title: pacotes nativos Debian
description: 
published: true
date: 2025-03-09T22:53:57.776Z
tags: 
editor: markdown
dateCreated: 2025-03-08T14:24:15.118Z
---

# Pacotes nativos Debian

Os pacotes a seguir existem apenas no Debian, por isso seus arquivos .po podem ser traduzidos no próprio Debian.

A lista é um compilado completo da página [Arquivos PO gerenciados com po4a](https://www.debian.org/international/l10n/po4a/pt_BR) e parcial da página [Arquivos PO](https://www.debian.org/international/l10n/po/pt_BR) (existem vários arquivos dessa última página que não estão listados aqui, quem sabe no futuro a gente lista também).

adduser
- doc/po4a/po/pt_BR.po
- po/pt_BR.po

apt
- doc/po/pt_BR.po
- po/pt_BR.po

apt-build
- man/po/pt_BR.po
	
apt-listchanges
- doc/po/pt_BR.po
- po/pt_BR.po

apt-show-versions
- man/po/pt_BR.po

apt-src
- man/po/pt_BR.po

base-passwd
- man/po4a/po/pt_BR.po

debarchiver
- po4a/po/pt_BR.po

debconf
- doc/man/po4a/po/pt_BR.po
- po/pt_BR.po

debdelta - native
- po/pt_BR.po

debhelper
- man/po4a/po/pt_BR.po

debian-history
- po4a/po/pt_BR.po

debianutils (não é native)
- po4a/po/pt_BR.po
	
debmake-doc (não é native)
- po4a/po/pt_BR.po
- examples/debhello-3.3/po/pt_BR.po

deborphan
- doc/po/pt_BR.po
- po/pt_BR.po
	
debsums
- man/po/pt_BR.po

deb-gview
- po/pt_BR.po

debian-edu-config
- www/pt_BR.po

debian-edu-router
- po/pt_BR.po/LC_MESSAGES/debian-edu-router-config.po
- po/pt_BR.po/LC_MESSAGES/debian-edu-router-fai.po

debian-faq

debian-handbook

debian-policy

debian-reference
- po/pt_BR.po

debian-security-support
- po/pt_BR.po

devscripts
- po4a/po/pt_BR.po

developers-reference - native
- source/locales/pt_BR/LC_MESSAGES/best-pkging-practices.po
- source/locales/pt_BR/LC_MESSAGES/beyond-pkging.po
- source/locales/pt_BR/LC_MESSAGES/developer-duties.po
- source/locales/pt_BR/LC_MESSAGES/index.po
- source/locales/pt_BR/LC_MESSAGES/new-maintainer.po
- source/locales/pt_BR/LC_MESSAGES/pkgs.po
- source/locales/pt_BR/LC_MESSAGES/resources.po
- source/locales/pt_BR/LC_MESSAGES/tools.po
- source/locales/pt_BR/LC_MESSAGES/l10n.po
- source/locales/pt_BR/LC_MESSAGES/scope.po

dhelp
- po/pt_BR.po

dpkg
- man/po/pt_BR.po
- dselect/po/pt_BR.po
- po/pt_BR.po

doc-base
- po/pod/pt_BR.po
- po/bin/pt_BR.po

fortunes-debian-hints
- po4a/po/pt_BR.po
	
multistrap
- doc/po/pt_BR.po
- po/pt_BR.po
	
packaging-tutorial
- po4a/po/pt_BR.po

po-debconf
- doc/po4a/po/pt_BR.po

pppconfig
- man/po/pt_BR.po
- po/pt_BR.po
