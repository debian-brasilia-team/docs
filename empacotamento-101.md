---
title: Empacotamento 101
description: 
published: true
date: 2025-03-01T17:55:43.504Z
tags: 
editor: markdown
dateCreated: 2025-02-27T20:42:46.025Z
---

- [1. GPG](/howto/gpg)
  - [1.1 Sobre](/howto/gpg/sobre)
  - [1.2 Verificando as chaves GPG existentes](/howto/gpg/verificando-as-chaves-GPG-existentes)
  - [1.3 Gerando uma nova chave](/howto/gpg/gerando-uma-nova-chave)
  - [1.4 Adicionando uma chave GPG a sua conta do Salsa](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa)
  - [1.5 Assine seus commits do Git](/howto/gpg/assine-seus-commits-do-git)
- [2. Configurando ambiente](/howto/sbuild)
- [3. Construindo primeiro pacote]()
  - [3.1 Clone]()
 	```bash
	# Clone via HTTPS
	gbp clone https://salsa.debian.org/debian-brasilia-team/packages/hello-world.git
    
 	# ou via SSH
 	gbp clone git@salsa.debian.org:debian-brasilia-team/packages/hello-world.git
 	```
  - [3.2 Build]()
 	```bash
 	# Construindo o pacote
 	gbp	buildpackage
 	#ou
 	sbuild
 	```