---
title: Empacotamento 101
description: 
published: true
date: 2025-03-01T17:42:56.197Z
tags: 
editor: markdown
dateCreated: 2023-09-20T19:19:59.457Z
---

Se você está aqui, é porque quer aprender sobre empacotamento, certo? Bem, veio ao lugar certo! Para começar nessa área, é preciso ter um ambiente configurado a fim de permitir a construção dos pacotes. Entender como funciona o processo de empacotamento, quais são as atividades regularmente feitas pelos mantenedores e como um pacote é gerenciado ao longo do tempo são fundamentais. Felizmente tudo isto está documentado nesta wiki e elencado nos passos abaixo.

1. [Configurando seu ambiente](/empacotamento/configurando-seu-ambiente)
2. [Anatomia de um pacote Debian](/empacotamento/anatomia-de-um-pacote-debian)
3. [Atualizando o pacote para nova versão upstream](/empacotamento/atualizar-pacote-para-nova-versao-upstream)
4. [Gerenciando patches](/empacotamento/gerenciando-patches)
5. [Checklist antes de pedir revisão](/empacotamento/checklist)
6. [Processo de contribuição](/processo-de-contribuicao-com-pacotes)
7. [Outros tutoriais interessantes](/howto)

Se gostar de uma abordagem mais prática, é melhor seguir a ordem `1`, `3`, `4`, `5`, `6`, `2`, `7`. Do contrário, pode seguir a ordem elencada na lista.

Além disso, recomendamos que [entre em contato conosco](/home#comunicação).