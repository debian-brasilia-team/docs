---
title: Configurando seu Ambiente de Desenvolvimento
description: Descubra como configurar seu ambiente de desenvolvimento para o empacotamento Debian
published: true
date: 2025-03-01T17:42:54.030Z
tags: 
editor: markdown
dateCreated: 2024-11-05T02:36:10.717Z
---

# Configurando seu Ambiente de Desenvolvimento

Ter um ambiente configurado para construir pacotes Debian é essencial para quaisquer novos contribuidores(as), portanto indicaremos as configurações essenciais necessárias para você ter um sistema pronto para construir pacotes.

> Assumimos que você já tem uma máquina com Debian instalado
{.is-warning}

## Instalando os pacotes necessários

Nós utilizaremos vários programas para construir pacotes, então é necessário instalá-los no seu sistema Debian:

```shell
apt install debootstrap sbuild schroot blhc devscripts dh-make git-buildpackage git sbuild-debian-developer-setup
```

> O comando acima deve ser executado como `root` ou prefixado por `sudo`
{.is-warning}

## Configurando os programas necessários

Além de instalar os programas, precisamos realizar alguns passos de configuração para deixar tudo pronto.

### Bash

Edite o arquivo `~/.bashrc` e adicione as seguintes linhas:

```shell
export DEBFULLNAME="Seu Nome"
export DEBEMAIL="seuemail@mail.com"
```

Essas variáveis serão utilizadas por várias ferramentas para preencher informações como, por exemplo, a entrada do changelog de empacotamento.

### Git

Edite o arquivo `~/.gitconfig` e adicione as seguintes linhas:

```
[user]
        name = Seu Nome
        email = seuemail@mail.com
```

Essas variáveis serão utilizadas pelo `git` para preencher informações como, por exemplo, o autor(a) do commit.

### Sbuild

O `sbuild` é a ferramenta principal que utilizaremos para construir pacotes Debian e ele possui uma [página inteira com muito mais informações](/howto/sbuild), mas tentaremos condensar o mínimo que será necessário aqui.

Se você quiser criar o *chroot* rapidamente, pode seguir os seguintes passos para criar um chroot da distribuição `unstable`:

```shell
sbuild-debian-developer-setup
# or
sudo sbuild-debian-developer-setup
```

Seu usuário precisa ser adicionado ao grupo `sbuild` para que ele possa utilizar o *chroot*.

``` shell
sudo sbuild-adduser $USER
```

O comando acima aplicará o efeito somente após reiniciar a máquina, para testar o sbuild sem reiniciar pode usar o seguinte comando temporário:

``` shell
newgrp sbuild
```

Primeiro, vamos copiar (para a `$HOME`) o arquivo que contém exemplos de várias configurações aceitas pelo `sbuild`:

``` shell
cp /usr/share/doc/sbuild/examples/example.sbuildrc ~/.sbuildrc
```

E adicionar as seguintes diretivas ao arquivo `~/.sbuildrc`:

``` perl
# Faz o build de pacotes arch-independent.
$build_arch_all = 1;

# Por padrão realiza o build para unstable
$distribution = 'unstable';

# Produz um source package (.dsc) além do pacote binário.
$build_source = 1;

# Uploads para o Debian precisam ser, na grande maioria das vezes, source-only.
$source_only_changes = 1;

# Não executa o "clean" target fora do chroot.
$clean_source = 0;

# Sempre executa o lintian no final dos builds.
$run_lintian = 1;
$lintian_opts = ['--info', '--display-info'];
```

### Git-buildpackage

O `git-buildpackage` é uma ferramenta criada para auxiliar o uso do `git` no empacotamento e precisamos adicionar as seguintes diretivas no arquivo `~/.gbp.conf`:

``` python
[DEFAULT]
builder = sbuild
pristine-tar = True
```

## Testando a Configuração

Para testar a configuração do sistema vamos usar o pacote [hello](https://tracker.debian.org/pkg/hello).


``` shell
sbuild --dist=unstable hello
```
