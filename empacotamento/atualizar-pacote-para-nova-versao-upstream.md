---
title: Atualizar pacote para nova versão upstream
description: 
published: true
date: 2025-03-01T17:42:49.416Z
tags: upstream, new, version, update
editor: markdown
dateCreated: 2024-04-14T17:54:57.371Z
---

Um dos processos mais comuns no Debian é atualizar um pacote para a versão mais recente lançada pelo(a) autor(a) *upstream*.

Os passos listados aqui são genéricos e tentam abranger uma grande diversidade de cenários. No entanto, atualizar a versão pode ser simples e gastar poucos minutos ou muito complexo e demandar a ajuda de um conjunto de pessoas.

<br/>

## Atualizar o chroot


```shell
sudo sh /usr/share/doc/sbuild/examples/sbuild-debian-developer-setup-update-all
```

Caso encontre problemas ao atualizar o chroot, verifique o arquivo `/var/log/sbuild-update-all.log`. Pode ocorrer da sua sessão do schroot estar ativa e o comando não funcionar devidamente.

Para encerrar qualquer sessão ativa, basta executar o seguinte comando:

```shell
schroot --end-session --all-sessions 
```

Após isso, basta executar novamente o comando de atualização do chroot.

## Verificar esforço

Antes de seguirmos para a atualização de um pacote é preciso verificar a viabilidade de atualizá-lo. Segue um checklist do que precisa ser verificado antes de atualizar um pacote:

### 1. Revisar changelog do upstream

Normalmente o autor upstream fornece algum tipo de changelog e, mesmo que não forneça, é possível analisar o diff das versões. Com essa revisão identificamos quais mudanças podem quebrar outros pacotes que são dependentes deste.

### 2. Conferir impacto nos pacotes que dependem deste pacote

Com o comando `apt rdepends [nome-do-pacote]` serão listados todos os pacotes que dependem do pacote candidato a atualização. Assim, é possível conferir se os pacotes são compatíveis com a nova versão para, caso não sejam compatíveis, decidir como prosseguir.

Como conferir se os pacotes são compatíveis com a nova versão? Verifique se a versão da dependência reversa no repositório do Debian satisfaz o que está especificado pelo upstream na versão nova (por exemplo, no `*.gemspec`, `go.mod`, `setup.py`).

### 3. Checar se a licença continua a mesma para as novas modificações

A main do Debian só aceita software livre, portanto é sempre bom garantir que o pacote está de acordo com a DFSG.

### 4. Rever lista de dependências da nova versão

Isso serve tanto para remover quanto para adicionar novas dependências. Adicionar uma nova pode implicar um esforço extra caso a dependência não esteja no Debian.

<br/>

## Passos para atualizar um pacote

No geral, os pacotes do Debian são versionados no [Salsa](https://salsa.debian.org). É interessante, nesse primeiro momento, trabalhar fora do repositório oficial. Para isso, crie um *fork* do repositório original dentro do seu *namespace* pessoal utilizando a interface do salsa.

1. Em seguida, clone o repositório.

``` shell
gbp clone git@salsa.debian.org:[seu_usuário]/[nome-do-pacote].git
```

2. Manter o repositório oficial acessível é bastante útil também. Exemplo dos passos para trabalhar com o fork e o repositório oficial:

```shell
cd [nome-do-pacote]
```

```shell
git remote -v
origin	git@salsa.debian.org:[seu_usuário]/[nome-do-pacote].git (fetch)
origin	git@salsa.debian.org:[seu_usuário]/[nome-do-pacote].git (push)
```

```shell
git remote add oficial git@salsa.debian.org:[namespace_oficial]/[nome-do-pacote].git
git remote -v

oficial	git@salsa.debian.org:[namespace_oficial]/[nome-do-pacote].git (fetch)
oficial	git@salsa.debian.org:[namespace_oficial]/[nome-do-pacote].git (push)
origin	git@salsa.debian.org:[seu_usuário]/[nome-do-pacote].git (fetch)
origin	git@salsa.debian.org:[seu_usuário]/[nome-do-pacote].git (push)
```

Nesse exemplo, usamos a origin para edição enquanto o oficial apenas para leitura.

3. Feito isso, vamos baixar a versão atual do pacote e realizar o seu build. É importante conferir o estado atual do pacote antes de seguir com as modificações.

```shell
gbp buildpackage
```

* **Obs.:** Caso a branch original tenha outro nome, você pode utilizar esta opção abaixo:

    ```shell
    gbp buildpackage --git-debian-branch=branch_name
    ```

4. Se tudo estiver ok podemos prosseguir para atualizar o pacote.

```shell
gbp import-orig --uscan --pristine-tar
```

Esse comando utiliza a ferramenta uscan que usa as regras definidas no arquivo `debian/watch` para buscar a nova versão e importar no git. Só é necessário usar a opção `--pristine-tar` caso não exista a linha `pristine-tar=True` no arquivo `~/.gbp.conf`.

5. Gerar o changelog dessa nova versão, já que o build precisa dele para saber qual versão construir.

```shell
gbp dch --commit
```

* **Obs.** Os pacotes do Debian normalmente são administrados por uma equipe (e.g. Python Team, Go Team, Ruby Team). Por isto, ao realizar a atualização da versão upstream em um pacote que tenha uma equipe no campo `Maintainer` do arquivo `debian/control`, adicione a flag --team descrita no comando abaixo:

```shell
gbp dch --commit --team
```

6. Revisão

Essa parte é essencial na atualização upstream. Você irá revisar as mudanças feitas e ficar atento às modificações como:

-  **Dependências novas e alteradas:**

    Verifique no arquivo `debian/control` se as dependências declaradas estão atualizadas de acordo com as exigências da nova versão do software. Isso inclui dependências adicionadas, removidas ou alteradas. A Wiki também oferece informações mais detalhadas da estrutura de um pacote [aqui](anatomia-de-um-pacote-debian.md)

-  **Mudanças e de licenças:**

    O arquivo `debian/copyright` contém informação de licença de software e copyright de todos os arquivos do pacote fonte, inclusive do empacotamento (diretório `debian/`). Uma tarefa bastante comum é atualizar também o Standards-Version (atualmente na versão 4.7.0), localizado no arquivo `debian/control`. Porém, antes de realizar esta mudança, leia atentamente o [checklist de atualização](https://www.debian.org/doc/debian-policy/upgrading-checklist.html) da wiki oficial Debian.

-  **Erros e alertas do Lintian:**
    
    O **Lintian** é uma ferramenta utilizada no ecossistema Debian para verificar a conformidade de pacotes `.deb` com as boas práticas e políticas do projeto Debian. Ele analisa pacotes binários e de origem, reportando potenciais problemas que podem variar de simples advertências (warnings) a erros críticos que impediriam a inclusão do pacote nos repositórios oficiais.

    Confira o manual oficial do Lintian [aqui](https://lintian.debian.org/manual/index.html).

    Os avisos de erro e alerta do lintian ficam disponíveis no log de build, quando você executa o comando `gbp buildpackage`. No entanto, para uma análise mais detalhada, execute a linha de comando abaixo:

    ```shell
    lintian -EvIL +pedantic
    ```

     `-EvIL` inclui todos os detalhes possíveis (Erros, Avisos, Informações e Pedantic), enquanto o `+pedantic` relata problemas menores que não são obrigatórios.

    O comando `lintian-explain-tags` exibe o significado de uma ou mais tags, podendo te direcionar assim para uma possível solução do problema. E.g.

    ```shell
    lintian-explain-tags update-debian-copyright
    ```
- **Boas práticas de git:**

    **Obs.** Esta parte não é necessariamente obrigatória, mas se possível, tente seguir estas orientações.

    Uma boa prática de git para a atualização da versão upstream de um pacote no Salsa é commitar separadamente cada arquivo alterado do diretório `debian/`. Escreva uma mensagem de commmit concisa e significativa, evidenciando no início da mensagem do commit o arquivo em questão e tentando manter o mesmo "padrão" em todas as mensagens. Por exemplo, todos os commits abaixo começam com uma letra maiúscula e terminam com um ponto final: 

    ```shell
    d/changelog: Update changelog for 3.0.1-1 release.
    d/copyright: Update the upstream license.
    d/control: Bump Standards-Version to 4.7.0, no changes needed.
    ```


7. Construir novamente o pacote.

```shell
gbp buildpackage
```

* **Obs.** Após esse passo muitos cenários diferentes podem acontecer, cabe ao empacotador analisar o que precisa ser feito a partir daqui. Note que o Lintian vai reclamar com um erro de "*unreleased-changes*", isso é esperado. O Lintian acha que você já vai fazer o upload, mas na verdade o upload será feito por algum DD, e essa pessoa é quem altera o UNRELEASED do changelog.

8. Push dos commits

É necessário fazer o push de 3 branches (`debian` branch, `upstream` branch, `pristine-tar` branch), além da tag gerada pelo gbp import-orig. Felizmente o `gbp` tem um comando para automatizar esse processo:

```shell
gbp push --debian-tag=''
```

> Esse comando é equivalente a fazer o push de cada branch e das tags com o `git`
{.is-info}

9. Merge request no salsa

Agora o pacote está pronto para a revisão. Você pode fazer um MR da branch de empacotamento (geralmente `master`, `debian/master`, `debian/latest`, `debian/unstable` ou `debian/sid`) no salsa, mencionando na descrição que as outras branches estão no seu fork. Atente-se aos comentários, pois o seu revisor poderá sugerir alterações.