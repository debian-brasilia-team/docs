---
title: Checklist
description: Um checklist para empacotamento Debian.
published: true
date: 2025-03-01T17:42:51.707Z
tags: empacotamento
editor: markdown
dateCreated: 2024-06-26T13:11:35.468Z
---

# Checklist para revisão antes de RFS

[RFS seria abreviação para Request for Sponsor](https://mentors.debian.net/sponsors/rfs-howto).

Veja como seria o processo do Debian Brasilia em [processo de contribuicao com pacotes nesta wiki](/processo-de-contribuicao-com-pacotes).

Baseando em um [vídeo do Eriberto sobre empacotamento avançado que fornece uma breve lista do que revisar](https://www.youtube.com/watch?v=Hkhnza6uWeI) e em experiências relacionadas ao empacotamento, eu vou sugerir alguns itens para revisão antes de solicitar upload.

[Itens sugeridos no vídeo do Eriberto](https://youtu.be/Hkhnza6uWeI?si=uPs5JY8PqTMwvOmR&t=5432).

## Item 1 - verificando bugs

O Debian possui o https://tracker.debian.org e neste link seria possível verificar os bugs do pacote em https://bugs.debian.org que também seria possível neste segundo link diretamente. Se conseguir resolver 1 ou mais bugs é sempre muito importante ao solicitar patrocínio para o seu pacote.
Lembrando que bugs com severidades grave, critical e serious são consideradas release critical pelo Release Team do Debian e causa remoção em testing.

Veja também [sites uteis](/sites-uteis).

## Item 2 - double build

A construção do pacote ao ser realizada com sucesso e sem execução do comando `debclean` quando for executada novamente precisa obter êxito na(s) construção(ões) seguintes.
Esta revisão evita o bug com título `fails to build source after successful build` no pacote.

Mais informações em [Double Build](https://wiki.debian.org/qa.debian.org/FTBFS/DoubleBuild?action=show&redirect=qa.debian.org%2FFTBFS%2FSourceAfterBuild).

## Item 3 - verificação no arquivo de direitos autorais debian/copyright

Seria um arquivo importante, nele contém as licenças dos arquivos contidos no pacote e os autores destes arquivos.
Em pacotes órfãos (ou até em outros pacotes) é comum encontrar problemas neste arquivo e sugiro revisar olhando o link com ferramentas que possam ajudar nisso [Copyright Review Tools](https://wiki.debian.org/CopyrightReviewTools).

## Item 4 - debian/changelog

Neste item recomendo a leitura e seguir o que determina em [best practices for debian/changelog](https://www.debian.org/doc/manuals/developers-reference/best-pkging-practices.html#best-practices-for-debian-changelog).

Deve conter `New upstream version` se possuir nova versão do upstream, `Team upload` se for upload para um time, `QA upload` para pacotes órfãos e [`Non-maintainer upload` para NMU](https://www.debian.org/doc/manuals/developers-reference/pkgs.html#nmu).
**Pode executar um spell em busca de erro ortográfico.**

## Item 5 - Lintian reports

Uma feramenta que auxilia na revisão dos pacotes durante o empacotamento, é sempre recomendado corrigir o que for reportado para que o pacote seguir o que é determinado em [Debian Policy](https://www.debian.org/doc/debian-policy/) e evitar violações.
Em caso de Lintian que pode ser ignorado utiliza o arquivo `debian/lintian-overrides` (mas use apenas para casos específicos que o Lintian reporta de forma incorreta e adicione um comentário para futuras revisões/manutenções colaborativas de outras pessoas).

## Item 6 - uscan

Este comando executa o `debian/watch` para verificar novas versões upstream. Execute `uscan -v` (em modo verboso) para verificar se há problemas e corrigir (se houver). Veja [debian/watch](https://wiki.debian.org/debian/watch).

Veja também [atualizar para nova versão upstream nesta wiki](/empacotamento/atualizar-pacote-para-nova-versao-upstream).

## Item 7 - detalhes importantes

- Comparar mudanças novas em caso de update da versão upstream do pacote para verificar se há necessidade de alguma mudança.
- Analisar cada arquivo no diretório Debian, recomendado executar um wrap-and-sort do pacote devscripts para quebrar as linhas longas e classificar itens em arquivos de empacotamento e analisar eventuais erros que possam passar despercebido.

Veja também [anatomia de pacotes Debian nesta Wiki](/empacotamento/anatomia-de-um-pacote-debian).

- Uso de blhc para [Hardening](https://wiki.debian.org/Hardening) e veja também [implementing the hardening](https://eriberto.pro.br/blog/2015/09/07/debian-how-to-use-blhc-to-solve-hardening-issues-when-packaging/).
- Gerenciar patches e isso consta em [gerenciando patches](/empacotamento/gerenciando-patches).

**Arquivo em revisão e atualização.**

## Referências

- Debian Policy
- Debian Wiki
- Eriberto Mota
- Debian Developers Reference
- Debian Brasília Wiki

## Autor

Leandro Cunha <leandrocunha016@gmail.com>

Este arquivo pode ser licenciado com Creative Commons Atribuição.
