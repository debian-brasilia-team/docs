---
title: Versionando o empacotamento com git
description: Descrição da organização de um repositório git para empacotamento
published: true
date: 2025-03-01T17:43:00.861Z
tags: git, gbp, dep-14, empacotamento
editor: markdown
dateCreated: 2024-06-13T16:58:21.567Z
---

# Versionando o empacotamento com git

> Essa página está em construção.
{.is-warning}

Falar sobre os benefícios de usar git para versionar o empacotamento.

## Pré-requisitos
Apontar links para leituras recomendadas e outras documentações

## Dep-14
Falar sobre a DEP-14

### Branch de empacotamento
Exemplo de `debian/latest`

### Branch upstream
Exemplo de `upstream/latest`

### Branch pristine-tar
Exemplo de `pristine-tar`

### Alguns exemplos
Mostrar git diff, git log, git blame e alguns outros comandos úteis para nós.