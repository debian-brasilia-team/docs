---
title: O que é Debian
description: 
published: true
date: 2025-03-01T17:43:49.196Z
tags: debian
editor: markdown
dateCreated: 2025-02-27T20:51:10.492Z
---

# O que é o Debian?

O Debian é uma distribuição de **GNU/Linux** que tem como característica principal a universalidade. Ou seja, tentamos fazer com que ele seja um Sistema Operacional de caráter livre, e que possa ser usado em qualquer lugar do mundo, por qualquer pessoa. O Debian é feito por mais de 1500 voluntários(as) [ao redor de todo o globo](https://www.debian.org/devel/developers.loc). Cada pacote, seja ele um programa, uma biblioteca ou documento, tem como responsável o(a) chamado(a) mantenedor(a), cujas tarefas incluem a compilação e a correção de bugs do pacote.

O Debian tem sido adotado por muitas entidades e tem atraído especialmente os governos, por ser independente de fornecedor, uma iniciativa 100% comunitária e ter um desenvolvimento aberto.

## Mas por que usar o Debian?

Abaixo temos algumas razões que poderiam levar você a usar o **Debian GNU/Linux:**

- Website completo: <https://www.debian.org>
- Definição clara de Software Livre: [DFSG](http://www.debian.org/social_contract.html)
- Compromisso com a comunidade: [Contrato Social](http://www.debian.org/social_contract.html)
- Alto compromisso com a estabilidade e segurança: [Debian-QA](http://qa.debian.org/)
- Banco de dados de falhas aberto ao público: [Bug Tracking System](http://bugs.debian.org/)
- Dezenas de listas de discussão de ajuda e desenvolvimento, inclusive nacionais: [Listas do Debian](https://wiki.debian.org/Brasil/Listas)
- Boletins semanais traduzidos para várias línguas, inclusive o português brasileiro: [Debian Weekly News](http://www.debian.org/News/weekly)
- Mais de 15.000 pacotes na versão estável, e mais de 17.000 na versão de desenvolvimento.
- Desenvolvido por mais de 1500 voluntários(as) ao redor do mundo: o maior grupo de uma distribuição de GNU/Linux.
- Facilidade de instalação, remoção e atualização de programas, através do mais completo sistema de gerenciamento de pacotes (DPKG) e sua consagrada interface (APT), adotados por outras distribuições.
- Suporte a 14 arquiteturas de processadores: Intel x86 / IA-32 (i386), Motorola 680x0 (m68k), Sun SPARC/UltraSPARC (sparc), DEC Alpha (alpha), Motorola/IBM PowerPC (powerpc), ARM, MIPS (mips/mipsel), HP PA-RISC (hppa), Intel IA-64 (ia64), IBM S/390 (s390), AMD64 (amd64) e Hitachi SuperH (sh).
- Centenas de mirrors (cópia completa) ao redor do mundo inteiro: [Mirrors do Debian](http://www.debian.org/mirror/list)
- Menu centralizado (com todos os programas) em todos os Window Managers.

O Debian é um sistema que procura fazer as coisas do jeito certo. Por muitas vezes, mostrou o caminho para que grandes migrações ocorressem na comunidade, como em relação à localização de arquivos. É improvável ver algum pacote trocar flexibilidade e qualidade por facilidade. A facilidade é um objetivo, mas tem de ser uma adição, não um substituto.

## Como instalar o Debian?

1 - Links úteis no sítio oficial do Debian (se os links a seguir não estiverem em português, leia o rodapé das páginas para saber como configurar seu sistema para isso):

- Versões do Debian: Entenda por que existem Stable, Testing e Unstable: <https://www.debian.org/releases>
- Obtendo o Debian: Visão geral das possibilidades a partir de onde instalar: <https://www.debian.org/distrib>
- Manual de instalação do Debian 10, atual versão estável: <https://www.debian.org/releases/stable/installmanual>
- Imagens em CD (ISOs): <https://www.debian.org/CD>

2 - Tutoriais e dicas:

- Se você optar pela versão stable e quiser usar pacotes mais novos não deixe de procurar repositórios de backports para stable de suas aplicações favoritas em <https://www.apt-get.org>
- Se você gosta do ambiente GNOME, provavelmente compensa instalar a unstable, com um GNOME extremamente bem integrado.
- Não desista tão facilmente: consulte a seção Documentação, e, caso não encontre uma resposta, procure ajuda no Rau-Tu, nas Listas e até no IRC.
- Se você pretende instalar a versão instável (sid), uma boa estratégia é instalar um sistema básico a partir dos CDs do Debian-BR ou do D-I e atualizar para unstable. Um tutorial sobre como fazer essa atualização pode ser encontrado aqui: <http://www.vivaolinux.com.br/artigos/verArtigo.php?codigo=551>

3 - Voluntários para ajudar na instalação

Confira nossos voluntários(as) que podem te ajudar instalar seu primeiro Debian.

## Casos de sucesso

Alguns exemplos de uso do Debian como base são:

- [Ubuntu](http://www.ubuntu.com/)
- Knoppix e o Kurumin
- GNU/LinEx - que obteve sucesso mundial com seus 80 mil computadores rodando GNOME instalados em Extremadura, Espanha.
- Distribuição dos Telecentros de São Paulo.
- Porto Alegre GNU/Linux - criado para o IV FISL (Fórum Internacional de Software Livre).