---
title: Eventos
description: 
published: true
date: 2025-03-05T16:12:28.187Z
tags: 
editor: markdown
dateCreated: 2024-04-17T11:16:19.177Z
---

# Eventos da Comunidade Debian Brasil

## 2025

- [MiniDebCamp e MiniDebConf em Maceió - AL](https://wiki.debian.org/Brasil/Eventos/MiniDebConfMaceio2025): 1 a 4 de maio de 2025

## 2024

- [Debian Day 2024](https://wiki.debian.org/DebianDay/2024)

- [MiniDebConf em Belo Horizonte](https://wiki.debian.org/Brasil/Eventos/MiniDebConfBeloHorizonte2024): 27 a 30 de abril de 2024

## 2023

- [Debian Day 2023 em Brasília](/eventos/debian-day-2023): de 28 de agosto a 1 de setembro

- [Debian Day Brasil 2023 / Debian 30 anos online](https://debianbrasil.gitlab.io/debian30anos/): 14 a 18 de agosto de 2023

- [Debian Day 2023](https://wiki.debian.org/DebianDay/2023)

- [Festa de lançamendo do Debian 12 bookworm online no Brasil](https://wiki.debian.org/Brasil/Eventos/FestaLancamentoBookworm): 10 de junho de 2023

- [Release Party Bookworm](https://wiki.debian.org/ReleasePartyBookworm): 10 de junho de 2023

- [MiniDebConf em Brasília](https://wiki.debian.org/Brasil/Eventos/MiniDebConfBrasilia2023): 25 a 27 de maio de 2023


## 2022

- [Estande Debian Brasil no Latinoware 2022](https://debianbrasil.org.br/blog/participacao-da-comunidade-debian-no-latinoware-2022): 2 a 4 de novembro de 2022. Obs: esquecemos de fazer uma chamada pública chamando as pessoas para ajudarem no estande.

- [Debian Day 2022](https://wiki.debian.org/DebianDay/2022)

- [Apresentação na Release Party do Fedora 37](https://gelos.club/2022/10/10/fedora-release-party.html): 22 de outubro de 2022

## 2021

- [Debian Day 2021](https://wiki.debian.org/DebianDay/2021)

- [Release Party Bullseye](https://wiki.debian.org/ReleasePartyBullseye): 14 de agosto de 2021

## 2020

- [MiniDebConf Online Brasil 2020](https://mdcobr2020.debian.net): 28 e 29 de novembro de 2020

- [Debian Day Brasil 2020 online](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2020): 15 e 16 de agosto de 2020

- [Debian Day 2020](https://wiki.debian.org/DebianDay/2020)

- [Fique em casa use Debian](https://debianbrasil.gitlab.io/FiqueEmCasaUseDebian/): 3 de maio a 6 de junho de 2020

## 2019

- [Debian Day 2019](https://wiki.debian.org/DebianDay/2019)

- [DebCamp e DebConf19 em Curitiba](http://debconf19.debconf.org/): 14 a 28 de julho de 2019

- [Release Party Buster](https://wiki.debian.org/ReleasePartyBuster): 6 de junho de 20219

## 2018

- [Minievento no FTSL em Curitiba](https://wiki.debian.org/Brasil/Eventos/MiniEventoFTSL2018): 24 a 26 de outubro de 2018

- [MicroDebConf Brasília](http://microdebconf.debianbrasilia.org): 08 de setembro de 2018

- [Debian Day 2018](https://wiki.debian.org/DebianDay/2018)

- [MiniDebConf em Curitiba](https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2018): 11 a 14 de abril de 2018

- [1º Encontro Debian Women em Curitiba](https://wiki.debian.org/Brasil/Eventos/1oEncontroDebianWomenCuritiba): 10 de março de 2018

## 2017

- [Minievento no FTSL em Curitiba](https://wiki.debian.org/Brasil/Eventos/MiniEventoFTSL2017): 27 a 29 de setembro de 2017

- [Debian Day Brasil 2017](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2017)

- [Release Party Stretch](https://wiki.debian.org/ReleasePartyStretch): 17 de junho de 2017

- [2º Encontro da Comunidade Debian Curitiba](https://wiki.debian.org/Brasil/Eventos/2oEncontroDebianCuritiba): 20 de maio de 2017

- [MiniDebConf em Curitiba](https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017): 17 a 19 de março de 2017


## 2016

- [Encontro da Comunidade Debian Curitiba](https://www.meetup.com/Comunidade-Curitiba-Livre/events/235059743): 29 de outubro de 2016

- [MiniDebConf na 13ª Latinoware](https://wiki.debian.org/Brasil/Eventos/MiniDebConfLatinoware2016): 19 e 20 de outubro de 2016

- [Debian Day Brasil 2016](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2016)

- [Encontro Comunitário no FISL17](https://wiki.debian.org/Brasil/Eventos/EncontroComunitarioFISL2016): 16 de julho de 2016

- [MiniDebConf em Curitiba](https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2016): 05 e 06 de março de 2016

## 2015

- [MiniDebConf na 12ª Latinoware](https://wiki.debian.org/Brasil/Eventos/MiniDebConfLatinoware2015): 15/10/2015

- [Debian Day Brasil 2015](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2015)

- [MiniDebConf no FISL16](https://wiki.debian.org/Brasil/Eventos/MiniDebConfFISL2015): 10/07/2015

- [MicroDebConf em Brasília](https://wiki.debian.org/Brasil/Eventos/MicroDebConfBSB2015): 31/05/2015

- [Release Party Jessie](https://wiki.debian.org/ReleasePartyJessie): 25 de abril de 2015

## 2014

- [Debian Day Brasil 2014](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2014)

- FISL15: - [Espaço de usuários](http:/softwarelivre.org/debianbrasil/blog/proposta-para-grupo-de-usuarios-debian-no-fisl-15) e - [Encontro comunitário](http:/softwarelivre.org/debianbrasil/blog/proposta-para-encontro-comunitario-debian-brasil-no-fisl-15)

## 2013

- [Debian Day Brasil 2013](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2013)

- [FISL14](https://wiki.debian.org/Brasil/Eventos/Fisl2013)

- [Release Party Wheezy](https://wiki.debian.org/ReleasePartyWheezy): 4 de maio de 2013

## 2012

- [Debian Day Brasil 2012](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2012)

## 2011

- [8ª Latinoware](https://wiki.debian.org/Brasil/Eventos/Latinoware2011)

- [Debian Day Brasil 2011](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2011)

- [FISL12](https://wiki.debian.org/Brasil/Eventos/Fisl2011)

- [Release Party Squeeze](https://wiki.debian.org/ReleasePartySqueeze): 6 de fevereiro de 2011

## 2010

- [Debian Day Brasil 2010](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2010)

- [FISL11](https://wiki.debian.org/Brasil/Eventos/Fisl2010)

- [Área de Software Livre da Campus Party 2011](https://wiki.debian.org/Brasil/Eventos/CampusParty2010)

## 2009

- [Debian Day Brasil 2009](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2009)

- [FISL10](https://wiki.debian.org/Brasil/Eventos/Fisl2009)

## 2008

- [Debian Day Brasil 2008](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2008)

## 2007

- [Debian Day Brasil 2007](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2007)

## 2006

- [Debian Day Brasil 2006](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2006)

## 2005

- [Debian Day Brasil 2005](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2005)

- [FISL 6.0](https://wiki.debian.org/Brasil/Eventos/Fisl2005)

## 2004

- [Debian Day Brasil 2004](https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2004)


## Material de apoio

- [Checklist para equipe do estande](https://wiki.debian.org/Brasil/Eventos/CheckList)
- [Dicas do GUD-RS sobre como planejar um evento](https://wiki.debian.org/Brasil/Eventos/ComoPlanejarRS)
- [Dicas do GUD-GO sobre como planejar um evento](https://wiki.debian.org/Brasil/Eventos/ComoPlanejarGO)

