---
title: default
description: 
published: true
date: 2025-03-01T17:29:55.074Z
tags: 
editor: markdown
dateCreated: 2023-07-27T23:23:29.861Z
---

<!---

Leia: https://wiki.debianbsb.org/pt-br/processo-de-contribuicao-com-pacotes

Remova abaixo o que não for necessário.

--->

**Link do Tracker**: https://tracker.debian.org/pkg/<nome_do_pacote>

**Link do Salsa**: https://salsa.debian.org/...

**Bugs relacionados**:
- WNPP, se aplicável

**Link do pacote em lintian.debian.org**: https://lintian.debian.org/sources/<nome_do_pacote>
