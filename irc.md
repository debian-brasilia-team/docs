---
title: irc
description: IRC
published: true
date: 2025-03-01T17:55:57.338Z
tags: 
editor: markdown
dateCreated: 2025-02-26T19:00:22.766Z
---

# IRC

Para aqueles que gostam de usar o [IRC (Internet Relay Chat)](http://pt.wikipedia.org/wiki/Internet_Relay_Chat) o Debian Brasil mantém um canal na rede (http://www.oftc.net)[OFTC].

## Como participar

Para participar do canal Debian Brasil no IRC acesse:
- Server: **irc.debian.org** ou **irc.oftc.net**
- Canais:
   - **#debian-br**: principal canal da comunidade Debian no Brasil, reúne desenvolvedores(as), usuários (as) e entusiastas para coordenação do Projeto Debian-BR
  - **#debian-br-eventos**: questões gerais sobre eventos com a comunidade Debian Brasil.
  - **#debian-l10n-br**: coordenação dos esforços de localização e tradução para o português do Brasil.
  - **#debian-devel-br**:  discussões sobre desenvolvimento em Debian, seria a versão em português da debian-devel. É usada pela equipe de empacotamento Debian, serve como uma versão brasileira da debian-mentors, tanto para dúvidas como para encontrar sponsors que possam enviar pacotes para o repositório. 

Preferencialmente use **irc.debian.org**.

## Histórico e objetivo

Há alguns anos, o *alias* **irc.debian.org** apontava para os servidores da Freenode, mas o Projeto Debian mudou para a [OFTC](http://www.oftc.net) e o Debian Brasil acompanhou a mudança e migrou também.

Ao longo dos anos o foco principal do canal **#debian-br** mudou, em especial nos anos do [Debian-BR-CDD](http://pt.wikipedia.org/wiki/Debian-BR-CDD), passando a seguir a linha do **#debian**, ou seja, um canal voltado para discussões sobre Debian, incluindo ajuda técnica.

O canal é frequentado por um público variado e o tópico principal é o Projeto Debian e o Debian Brasil.

Lembramos que o canal é parte do Debian Brasil e que, por maior liberdade que se dê nos tópicos que podem ser discutidos, os princípios do projeto precisam ser mantidos. Não deixe de ler as [recomendações para utilização do IRC](http://debianbrasil.org.br/recomendacoes-para-utilizacao-do-irc).

Confira a página [IRCamigos](https://web.archive.org/web/20071019075748/http://www.debianbrasil.org/ircamigos/) para ver as fotos de alguns usuários.

## Canal ##debian-br na Freenode

Quando o Debian mudou da Freenode para a OFTC, alguns usuários da Freenode continuaram a frequentar o canal. Devido às regras da Freenode o canal foi renomeado para ##debian-br (pois deixou de ser um canal oficial do Debian).

Alguns chamam de "canal underground", mas ele é um canal em uma rede de IRC, provavelmente há outros canais #debian-br em outras redes mas o canal oficial é o que fica em **irc.debian.org**.
