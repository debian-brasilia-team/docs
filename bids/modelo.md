---
title: Modelo
description: 
published: true
date: 2025-03-07T19:12:05.654Z
tags: 
editor: markdown
dateCreated: 2025-03-07T18:58:55.318Z
---

# Modelo para Bid da MiniDebConf

Estou colocando aqui algumas perguntas e itens que seria legal os proponentes para a realização da MiniDebConf preencherem.

Nada aqui é obrigatório, então não se sinta obrigado a responder todas as questões :-)

Vou usar a edição de 2024 em Belo Horizonte para servir de exemplo.

## Equipe e coordenação

Você sempre poderá contar com a ajuda remota das pessoas que já ajudaram a organizar uma MiniDebConf. Mas não acredite que o pessoal remoto irá fazer a maior parte do trabalho porque no fim, será o time local que irá realmente fazer a maior parte das tarefas.

Então é muito importante ter pessoas ajudando localmente.

Indique quem será o coordenador, ou quais pessoas serão os coordenadores. E quem faz parte do time local que irá ajudar.

## Qual a cidade, local e salas?

Não precisa ter o local já definido agora. Você pode colocar o local que você acredita que poderá acontecer, e depois acabar mudando para outro.

Por exemplo:
   * Belo Horizonte
   * Campus Pampulha da UFMG
   * No sábado: salas 2076, 2077, 2014, 2013, Sala 2006, 2007. Demais dias: auditórios B101 e B102

## Quantos dias e data provável

Não precisa ter a data definitiva, mas pelo menos quando você pretende fazer.

A quantidade de dias pode variar. Em [Brasília](https://brasilia.mini.debconf.org/programacao) foram 3 dias, em [Belo Horizonte](https://bh.mini.debconf.org/schedule) foram 4.


## Hospedagem

Se você pretende oferecer bolsas de hospedagem, ou mesmo se você pretende apenas indicar um hotel pro pessoal fical, é legal indicar o valor da diária.

Em Belo Horizonte os custos foram:
   * Quarto duplo diária: R$ 320,25
   * Sala de eventos pro domingo a noite: R$ 630,00

## Alimentação

Se você pretende oferecer bolsas de alimentação, ou mesmo se você pretende apenas indicar um restaurante pro pessoal fical, é legal indicar o valor do almoço e jantar.

Em Belo Horizonte o jantar variou porque fomos em diferentes restaurantes. O almoço na cantina custou R$ 15,50.

== Material gráfico

Sempre é bom produzir os banners de lona para indicação e principalmente para divulgar os patrocianadores.

Em Belo Horizonte fizemos:
   * 0,50 x 0,70 - corredores/portas das salas no sábado = 6
   * 0,70 x 1,10 - dentro salas no sábado/hall do B101/B102 = 2
   * 0,90 x 1,50 - dentro das salas/fundo do palco B101/B102 = 3 
   * 1,70 x 0,60 - (horizontal) mesas dos palcos B101 e B102 = 2

Produzimos 250 crachás em papel couchê 150g, tamanho 10cm x 14cm, 4x0 cores: R$ 153,60

Foram impressos cartazes A3 para divulgar principalmente na UFMG. Não teve custo.

## Brindes

De forma alguma é obrigatório ter brindes para os participantes. Em Curitiba a gente só deu cordão de crachá, em Brasília o pessoal produziu camisetas e em Belo Horizonte conseguimos produzir também.

É legal você ter uma estimativa de custo do que você gostaria de entregar.

Em Belo Horizonte os custos fora,:
   * Cordao de cracha feito em Curitiba (porque eu sempre fiz nessa empresa de lá): 200 x R$ 3,20
   * Copos de vidro: 120 x R$ 8,00
   * Porta crachá: 100 custaram R$ 88,30 no Mercado Livre
   * Camiseta poliviscosi: 220 x R$ 29,92. A de algodão custava 32,88

## Day Trip / Passeio

Novamente, não é um item obrigatório. Mas você pode escrever que pretende fazer em tal local.

## Custos

Podemos usar o ICTL para receber os patrocínios e doações, e ajudar nos pagamentos. Só lembrando que o ICTL fica com 10% do que for arrecadado como uma taxa de manutenção.

Você pode ver tudo o que [gastamos](https://salsa.debian.org/debconf-team/public/mini/belo-horizonte/-/blob/main/budget-completo/expenses.ledger) e tudo o que [recebemos](https://salsa.debian.org/debconf-team/public/mini/belo-horizonte/-/blob/main/budget-completo/income.ledger). E a [prestação de conta](https://salsa.debian.org/debconf-team/public/mini/belo-horizonte/-/blob/main/accounting.md#full-accounting) final.
