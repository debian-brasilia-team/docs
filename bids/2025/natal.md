---
title: Natal
description: 
published: true
date: 2025-03-07T19:14:54.889Z
tags: 
editor: markdown
dateCreated: 2025-03-07T19:00:24.253Z
---

# Brasil Bids Natal

## Equipe e coordenação

## Qual a cidade, local e salas?

   * Natal-RN
   * SEBRAE-RN
   * Salas

## Quantos dias e data provável

Três dias de evento, dia 25 até 27 de Abril  

## Hospedagem

Ibis:
   * Quarto duplo diária: R$ 212

## Alimentação
   * Lanchonete SEBRAE-RN: R$ 20

## Material gráfico
   * Crachá de papel 200 = R$ 400

## Brindes

   * Cordão de crachá: 200 x R$ 3,20
   * Copos de 400ml: 200 x R$ 3,67
   * Porta crachá: 200 custaram R$ 176,66
   * Camiseta algodão: 200 x R$ R$ 40.
