---
title: Fotos
description: 
published: true
date: 2025-03-01T17:55:48.491Z
tags: fotos
editor: markdown
dateCreated: 2025-02-27T14:07:53.174Z
---

# Fotos da Comunidade Debian Brasil

## 2024

### Debian na CryptoRave 

- Local: São Paulo - SP
- Data: 10 e 11 de maio de 2024
- [Google photos](https://photos.app.goo.gl/Qa6FHzBDZ2UAMdnr6)

### MiniDebConf Belo Horizonte

- Local: Belo Horizonte - MG
- Data: 27 a 30 de abril de 2024
- [Google photos](https://photos.google.com/share/AF1QipMe5TBwEcuPjxBytenZo4uW9LjZ_dmQTT3UWi3qE3sjqP8HrDSk5bMqcOpqKuPZ0Q?key=b0E2TDRqNmpfZXVfMUVoaF9kVXNRNnBvTVhPTWJB)
- [Nextcloud](https://cloud.debianbsb.org/s/wgzBtzKX2kbLCYP)

## 2023

### MiniDebConf Brasília

- Local: Brasília - DF
- Data: 25 a 27 de maio de 2023
- [Google photos](https://photos.google.com/share/AF1QipPrTEsh9f7br5KpQh7vT9RjGCmbaBlMIQlo9UbjQDeKpHWqD3dfnUM23VFEbMtFVQ?key=VzdJeGMza1RuWDdDWDlxVE5rVzNIOUI2S2RlREFR)
- [Nextcloud](https://cloud.gelos.club/s/C8dx3KzZQYet7Pq)

## 2019

### DebConf19 e DebCamp

- Local: Curitiba - PR
- Data: 14 a 28 de julho de 2019
- [Fotos feitas por brasileiros(as)](https://photos.google.com/share/AF1QipPekZe8o40CK9YpAu3zXcmFQW28WNMfk_x9R86I9vdFQjhwsIpyatG9f7WN2GXvCw?key=b0lJUXVmLUJ2MjJQYTJ4c1NZdmtQT0hxT2JSZlBR)
- [Foto do grupo](https://wiki.debian.org/DebConf/19/Photos?action=AttachFile&do=view&target=debconf19_group.jpg)
- [Álbum Aigars Mahinovs](https://photos.google.com/share/AF1QipMCfsAbkjgKDooxWNIPxz9KeANWyRPcl_9-PWZNXRMd-qj7X7CiwrRbo-nJAe7-Ig?key=MWZNWTM0bGY2MjBaLUM3eFRHMXpzN1B6b1VteW13)
- [Debconf GIT LFS share](https://salsa.debian.org/debconf-team/public/share/debconf19/tree/master/photos/aigarius/)

### Debian na CryptoRave 2019

- Local: São Paulo - SP
- Data: 3 e 4 de maio de 2019
- <<https://www.flickr.com/photos/debianbrasil/albums/72157705002349142>

### Debian na Campus Party Brasil 2019 (CPBR12) e sprint da organização da DebConf19

- Local: São Paulo - SP
- Data: 12 a 16 de fevereiro de 2019
- <<https://www.flickr.com/photos/debianbrasil/albums/72157689988483053>

## 2018

### Sprint do time local da DebConf19

- Local: Curitiba - PR
- Data: 23 a 27 de outubo de 2018
- <<https://www.flickr.com/photos/curitibalivre/albums/72157702955904085>

### Debian Day Curitiba 2018

- Local: Curitiba - PR
- Data: 18 de agosto de 2018
- <<https://www.flickr.com/photos/debianbrasil/albums/72157716035334247>

### Debian Day Taquaritinga 2018

- Local: Taquaritinga - SP
- Data: 15 de agosto de 2018
- Descrição: A 1a edição do evento ocorreu na Faculdade de Tecnologia de Taquaritinga (FATEC) e teve como principal objetivo introduzir o primeiro contato da distribuição para os discentes. Contamos com palestras, instalações e auxílio aos novos usuários.
- <https://flic.kr/s/aHsmqihLMd>

### DebConf18

- Local: Hsinchu - Taiwan
- Data: 29 de julho a 05 de agosto de 2018
- <https://www.flickr.com/photos/curitibalivre/albums/72157698374023511>

### MinDebConf Curitiba 2018

- Local: Curitiba - PR
- Data: 11 a 14 de abril de 2018
- <https://www.flickr.com/photos/curitibalivre/albums/72157690015019780>

## 2017

### Lançamento do Stretch em Curitiba

- Local: Curitiba - PR
- Data: 17 de junho de 2017
- <https://www.flickr.com/photos/curitibalivre/albums/72157667521525969>

### Reunião MiniDebConf 2018 e BID 2019

- Local: Curitiba - PR
- Data: 9 de dezembro de 2017
- <https://www.flickr.com/photos/curitibalivre/albums/72157667392611349>

### Debian no FTSL 2017

- Local: Curitiba - PR
- Data: 27 a 29 de setembro de 2017
- <https://www.flickr.com/photos/curitibalivre/albums/72157661046698288>

### Debian Day Curitiba 2017

- Local: Curitiba - PR
- Data: 19 de agosto de 2017
- <https://www.flickr.com/photos/curitibalivre/albums/72157685044712731>

### Debian Day Maceió 2017

- Local: Maceió - AL
- Data: agosto de 2017
- <https://www.flickr.com/photos/debianbrasil/albums/72157716019609901>

### MinDebConf Curitiba 2017

- Local: Curitiba - PR
- Data: 17 a 19 de março de 2017
- <https://www.flickr.com/photos/curitibalivre/albums/72157679698906961>

### Debian na Campus Party Brasil 2017 (CPBR10)

- Local: São Paulo - SP
- Data: 31 de janeir a 05 de fevereiro de 2017
- <https://www.flickr.com/photos/slcampusparty/albums/72157676339619643>

## 2016

### Encontro da Comunidade Debian Curitiba

- Local: Curitiba - PR
- Data: 29 de outubro de 2016
- <https://www.flickr.com/photos/curitibalivre/albums/72157672337907374>

### MiniDebConf na 13ª Latinoware

- Local: Foz do Iguaçu - PR
- Data: 19 a 21 de outubro de 2016
- <https://www.flickr.com/photos/debianbrasil/albums/72157716022510362>

### Debian Day Curitiba 2016

- Local: Curitiba - PR
- Data: 13 de agosto de 2016
- <https://www.flickr.com/photos/curitibalivre/albums/72157672324106196>

### Debian no FISL17

- Local: Porto Alegre - RS
- Data: 13 a 16 de julho de 2016
- <https://www.flickr.com/photos/debianbrasil/albums/72157716018790553>

### MinDebConf Curitiba 2016

- Local: Curitiba - PR
- Data: 05 e 06 de março de 2016
- <https://www.flickr.com/photos/curitibalivre/albums/72157665695548695>

### Debian na Campus Party Brasil 2016 (CPBR9)

- Local: São Paulo - SP
- Data: 26 a 31 de janeiro de 2016
- <https://www.flickr.com/photos/debianbrasil/albums/72157716019289536>

## 2015

### Debian Day Curitiba 2015

- Local: Curitiba - PR
- Data: 15 de agosto de 2015
- <https://www.flickr.com/photos/curitibalivre/albums/72157657314437622>

### MiniDebConf no FISL16

- Local: Porto Alegre - RS
- Data: 08 a 11 de julho de 2015
- <https://www.flickr.com/photos/curitibalivre/sets/72157653569094123>

### MicroDebConf Brasília 2015

- Local: Brasília - DF
- Data: 31 de maio de 2015
- <https://wiki.debian.org/Brasil/Eventos/MicroDebConfBSB2015/Report?action=AttachFile&do=get&target=BSB_group_photo.JPG>

## 2014

### Debian Day Curitiba 2014

- Local: Curitiba - PR
- Data: 16 de agosto de 2014
- <https://www.flickr.com/photos/curitibalivre/albums/72157657323659392>

## 2013

### Lançamento Wheezy em Curitiba

- Local: Curitiba - PR
- Data: 11 de maio de 2013
- <https://www.flickr.com/photos/curitibalivre/albums/72157655012357874>

## 2012

### Debian Day Curitiba 2012

- Local: Curitiba - PR
- Data: 18 de agosto de 2012
- <https://www.flickr.com/photos/curitibalivre/albums/72157657322595552>

## Muito antigas ==

### Debian Day em Salvador em 2005 e 2006

- <https://debianbrasil.org.br/fotos/eventos-salvador/index.html>

### III FISL - Fórum Internacional de Software Livre

- Local: Porto Alegre
- Data: 2002
- Descrição: Fotos tiradas por participantes do fórum que aconteceu em Porto Alegre, RS. Foi o maior fórum de informática do Brasil, reunindo quase 3.000 participantes.
- http://debianbrasil.org.br/fotos/fisl-iii-2002-poa

### Install Fest Debian-SP

- Local: Unicamp
- Data: 2002
- Descrição: Fotos tiradas no primeiro Install Fest do Debian-SP, em conjunto com o CIPSGA, com a Unicamp e com a IMA. Houve palestras de pessoas importantes do movimento do software livre, apresentações musicais e, claro, instalação de Debian em diversas máquinas.
- <http://debianbrasil.org.br/fotos/install-fest-debian-sp-unicamp-2002>

### Cultura Hacker UERGS

- Local: UERGS
- Data: 2002
- <http://debianbrasil.org.br/fotos/cultura-hacker-uergs-2002>

### I Debian-BR/CIPSGA Install Fest

- Local: Rio de Janeiro
- Data: 2001
- Descrição: Fotos tiradas no Primeiro Debian-BR Install Fest, realizado na cidade do Rio de Janeiro para apoiar e com o apoio do CIPSGA. Foram instaladas as máquinas da rede da escola de software livre do CIPSGA e máquinas de algumas pessoas que estiveram presentes.
- <http://debianbrasil.org.br/fotos/install-fest-debian-br-cipsga-rj-2001>

### Vídeo do I Debian-BR/CIPSGA Install Fest - 8 anos do Debian

- Local: Rio de Janeiro
- Data: 2001
- Descrição: Também comemoramos dois eventos importantes para o mundo do software livre, nesse encontro: o unixtime 1000000000 e os 8 anos de Debian. Este é um pequeno vídeo gravado com todos os presentes no final do evento.
- [Arquivo mpg](https://gitlab.com/DebianBrasil/debianbrasil.org.br/-/raw/master/content/arquivos/8-anos-debian.mpg)

### II FISL - Fórum Internacional de Software Livre

- Local: Porto Alegre
- Data: 2001
- Descrição: Fotos tiradas no campus da UFRGS e em "happy hours".
- <http://debianbrasil.org.br/fotos/fisl-ii-2001-poa>

### Fórum Internacional de Software Livre

- Local: Brasília
- Data: 2001
- Descrição: Fotos tiradas no anexo 2 da Câmara dos Deputados, onde almoçamos.
- <http://debianbrasil.org.br/fotos/forum-internacional-sl-2001-brasilia>
