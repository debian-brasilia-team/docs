---
title: MiniDebConf Curitiba 2018 - 11 a 14 de abril
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:41:16.720Z
tags: blog, geral
editor: markdown
dateCreated: 2017-11-11T14:40:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

**Está confirmada a 3a edição do maior encontro brasileiro de usuários e desenvolvedores Debian.** 
**Será de 11 a 14 de abril no Campus central da UTFPR - Universidade Tecnológica Federal do Paraná em Curitiba - PR.** 
Nos dias 11 e 12 (quarta e quinta) acontecerá a **MiniDebCamp** - período para
colaboradores(as) do Debian se encontrarem e trabalharem conjuntamente em um ou
mais aspectos do projeto. Essa será a nossa versão da brasileira da
[DebCamp](https://wiki.debconf.org/wiki/DebConf17/DebCamp), que acontece
tradicionalmente antes da Debconf. Não haverá palestras, debates e oficinas
nesses dias, teremos apenas "mão na massa" como empacotamentos de softwares,
traduções e BSP - Bug Squashing Party :-)

E nos dias 13 e 14 (sexta e sábado) acontecerá a **MiniDebConf** propriamente
dita - palestras, debates, oficinas, e mais "mão na massa" :-D

Em breve divulgaremos os formulários para chamada de atividades e para inscrição gratuita.

Mais informações:

<https://minidebconf.curitiba.br/2018/>

![Banner debconf curitiba 2018](https://debianbrasil.org.br/blog/imagens/banner-debconf-curitiba-2018.png =400x)