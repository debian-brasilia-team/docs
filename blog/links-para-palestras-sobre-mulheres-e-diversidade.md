---
title: Links para palestras sobre mulheres e diversidade
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:32.526Z
tags: blog, geral
editor: markdown
dateCreated: 2020-07-02T19:11:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Links para palestras que abordam mulheres e diversidade de gênero no Debian.

**Is Debian (and Free Software) gender diverse enough?**

- DebConf19 26/07/2019:
    - <https://www.youtube.com/watch?v=aEDlCoHKs0M>
    - <https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2019/DebConf19/is-debian-and-free-software-gender-diver.webm>     - <https://peertube.debian.social/videos/watch/ed1ee1f2-b901-4d69-9958-e0f48f57cdd0> 
**Debian Diversity BoF**

- DebConf19 22/07/2019:
    - <https://www.youtube.com/watch?v=hCZAPVjuL-A>
    - <https://caesar.ftp.acc.umu.se/pub/debian-meetings/2019/DebConf19/debian-diversity-bof.webm> 
**Debian Women - mulheres no Debian**

- MiniDebConf Curitiba 2018:
    - <https://www.youtube.com/watch?v=VbBH-HkfrqM>
    - <https://gensho.ftp.acc.umu.se/pub/debian-meetings/2018/mini-debconf-curitiba/12-debian-women-mulheres-no-debian.webm> 
**Diversity and inclusion BoF**

- MiniDebConf Cambridge 2016:
    - <https://www.youtube.com/watch?v=a7yyWpIpeUg>
    - <https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2016/miniconf_cambridge16/Diversity_and_Inclusion_BoF.webm> 
**Debian Women Assembly**

- MiniDebConf Barcelona 16/03/2014:
    - <https://www.youtube.com/watch?v=aOjd4ZvOTyk>
    - <https://gensho.ftp.acc.umu.se/pub/debian-meetings/2014/mini-debconf-barcelona/Debian_Women_Assembly.webm> 
**Women in Debian 2013**

- DebConf13:
    - <https://www.youtube.com/watch?v=AwYmbu-1t6Q>
    - <https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2013/debconf13/webm-high/1012_Women_in_Debian_2013.webm> 
**Debian-Women BoF**

- DebConf11:
    - <https://www.youtube.com/watch?v=Q6wu43tyt40>
    - <https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2011/debconf11/high/723_Debian-Women_BoF.ogv> 
**The Debian Women Project - past and present**

- DebConf11:
    - <https://www.youtube.com/watch?v=oOgmqel76Vk>
    - <https://caesar.ftp.acc.umu.se/pub/debian-meetings/2011/debconf11/high/722_Debian-Women_-_The_Past_and_the_Present.ogv> 
**Debian Women and Women in Free Software**

- DebConf5:
    - <https://www.youtube.com/watch?v=3QyRrAacgQ8>
    - <https://caesar.ftp.acc.umu.se/pub/debian-meetings/2005/debconf5/ogg_theora/720x576/2005-07-15/03-Debian_Women_and_Women_in_Free_Software-Erinn_Clark_Magni_Onsoien.ogg>