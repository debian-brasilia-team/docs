---
title: Encontro via IRC para tradução do Debian durante a MiniDebConf Curitiba 2016
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:39:28.389Z
tags: blog, geral
editor: markdown
dateCreated: 2016-03-02T16:51:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A Comunidade Debian Brasil convida a todos para um encontro via IRC para
tradução do Debian durante a MiniDebConf Curitiba 2016.

Membros da equipe de tradução do Debian para português do Brasil se encontrarão
para traduzir. O encontro é aberto para novas pessoas que queiram conhecer o
trabalho e/ou fazer parte da equipe.

Data: 06/03/2016 (domingo)

Horário: das 14:00 às 17:00 (horário de Brasília)

Presencial:
[MiniDebConf Curitiba 2016](http://br2016.mini.debconf.org/programacao.shtml)

Remoto:
irc://irc.debian.org/debian-l10n-br ou <https://webchat.oftc.net/?channels=debian-l10n-br> 
Mais informações:

<https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2016/Atividades#Encontro_para_Tradu.2BAOcA4w-o_do_Debian> 
Para participar, basta comparecer ou conectar a um dos locais do encontro.