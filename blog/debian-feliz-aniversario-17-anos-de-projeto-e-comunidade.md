---
title: Debian, Feliz Aniversário! 17 anos de projeto e comunidade!
description: por Valessio Brito
published: true
date: 2025-03-01T17:38:31.705Z
tags: blog, geral
editor: markdown
dateCreated: 2010-08-16T12:00:00.000Z
---

<!-- author: Valessio Brito -->

Cartão feito por mim utilizando [Inkscape](http://www.inkscape.org) e
alguns cliparts livre do [OpenClipart.org](http://www.openclipart.org).

![](https://debianbrasil.org.br/blog/imagens/cardebianptbr.jpg)


Participe também do viral "Balões do Debian em seu site", ~~acesse
aqui~~ (*link não funciona mais*) e cole o código em seu blog ousite.
