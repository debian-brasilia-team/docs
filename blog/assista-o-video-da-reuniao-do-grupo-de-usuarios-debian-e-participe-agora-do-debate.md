---
title: Assista o vídeo da reunião do grupo de usuários Debian e participe agora do debate
description: por Andre Felipe Machado
published: true
date: 2025-03-01T17:57:04.741Z
tags: blog, geral
editor: markdown
dateCreated: 2014-05-11T10:55:00.000Z
---

<!-- author: Andre Felipe Machado -->

Você pode assistir o vídeo da reunião do grupo de usuários Debian Brasil
durante o FISL 15 e participar também agora mesmo.

Primeiro assista o vídeo da reunião comunidade Debian Brasil no FISL15 em um
destes links:

- [YouTube](https://www.youtube.com/watch?v=bSTTqjqOBr4)
- [Peertube](https://peertube.debian.social/videos/watch/f032d380-d235-449b-8c43-786a2f6acf0f) 
Foi bem participativa e várias idéias e planos surgiram lá mesmo.

Depois poste suas sugestões de planos de ação como comentário aqui abaixo neste
artigo, para centralizar idéias de todas as redes sociais, listas e fóruns.

Precisa ter conta gratuita no site e entrar para a comunidade Debian Brasil
para postar e receber cópias das respostas também.

O arquivo da apresentação utilizada pode ser encontrado neste
[link](https://drive.google.com/file/d/0B-eUGbmJJnbEZTdWY2tIN0RGTVE/edit?usp=sharing). 