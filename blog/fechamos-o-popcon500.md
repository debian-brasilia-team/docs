---
title: Fechamos o POPCON500!
description: por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:39:37.756Z
tags: blog, geral
editor: markdown
dateCreated: 2024-10-15T09:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

por Thiago Pezzo

Há mais de 2 anos a [equipe de tradução](https://wiki.debian.org/Brasil/Traduzir/) do Debian para o português do Brasil (pt_BR) tem utilizado o trabalho de tradução das descrições de pacotes, via sistema
[DDTP/DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_BR), como porta de entrada para iniciantes. Com isso, tivemos um aumento significativo nas estatísticas dessa frente de tradução.

Hoje (9 de outubro de 2024) fechamos em 100% as descrições da lista [POPCON500!](https://ddtp.debian.org/stats/stats-sid.html)

Essa lista sinaliza os pacotes classificados em quantidade de pessoas que os utilizam [regularmente](https://popcon.debian.org/by_vote).

As estatísticas completas no momento seguem abaixo, nos colocando como quinto idioma mais traduzido nessa frente, logo atrás do polonês:

- Required 34 (100%)
- Important 30 (100%)
- Standard 38 (100%)
- Optional 7380 (10%)
- Extra 5 (2%)
- Popcon500 500 (100%)
- PopconRank 82.95M (78%)

Bom trabalho, pessoal! Continuemos!
