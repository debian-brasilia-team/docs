---
title: Porque você deve ir à MiniDebConf Curitiba 2016
description: por Giovani Ferreira
published: true
date: 2025-03-01T17:41:53.782Z
tags: blog, geral
editor: markdown
dateCreated: 2016-02-27T17:01:00.000Z
---

<!-- author: Giovani Ferreira -->

Às vezes as pessoas próximas a nós, amigos e familiares, não entendem porque
nos envolvemos e dedicamos tanto à projetos de Software Livre. O trabalho de
colaboração dentro de uma comunidade é um trabalho voluntário como qualquer
outro e é da natureza do ser humano ter essa motivação, talvez seja por nos
deixar mais conscientes do nosso papel na sociedade e na formação do mundo.

O motivo pelo qual participamos destas comunidades a princípio pode ser a busca
na solução de nossos problemas técnicos ou ainda ter acesso a projetos com
tecnologia de ponta. Mas com o passar do tempo parecem que essas comunidades
conquistam o nosso coração e outros valores assumem um papel importante, seja
o compartilhamento livre do conhecimento, valorização de pessoas acima da
tecnologia, sensação de pertencimento, amadurecimento pessoal e profissional,
vivência de outras culturas, etc.

Comunidades são feitas de pessoas e a relação entre essas pessoas e seus
propósitos é o que dita o ritmo e o futuro desta comunidade. Por isso que quero
te falar alguns bons motivos para participar da Mini DebConf Curitiba 2016:

**- Maior encontro da comunidade Debian no Brasil desde a DebConf4**. Desde
a realização da DebConf4 em 2004, não se tem um evento de expressão exclusivo
da comunidade Debian. Até o momento temos 11 Palestras, 5 Workshops, além de
várias Light Talks, Install Fest, Painel de Debate, Festa de assinatura de
Chaves, é uma super programação para dois dias de evento.

**- Imersão nas trilhas de colaboração do projeto.** A proposta é promover
atividades nas principais linhas de colaboração dentro do projeto Debian. Você
terá acesso a palestras e workshops sobre tradução produção gráfica,
empacotamento, infra estrutura ou ainda projetos com sistemas embarcados.
Membros oficiais dos times de Publicidade, Ruby, Perl, Forense, Tradução e
outros, estarão presentes e você terá acesso direto à estas pessoas para
aprender como contribuir com Debian.

**- Fazer amizades e aprender muito.** Como havia dito, uma comunidade é
feita de pessoas, então estar em eventos é uma ótima oportunidade de conhecer
muita gente e também muitos outros projetos. Você revisar um pacote, aplicar
um patch ou submeter uma tradução junto de outro membro com mais experiência
é uma oportunidade de aprendizado única e além disso o contato com pessoas e
culturas diferentes é enriquecedor. Já estamos com mais de 180 inscrições,
temos pessoas dos 4 cantos do brasil, além de participantes de outros países
da América Latina, Europa e África.

É isto que te espera na Mini DebConf Curitiba 2016, um evento relacionado à
distribuição que comecei a utilizar em 2007, mas que somente a partir de 2015
comecei a colaborar com o desenvolvimento. Nestes poucos meses muita coisa
aconteceu e posso dizer que aprendi muito. O que digo à você que tem interesse
em participar de uma comunidade é que não deixe o tempo passar como eu deixei
(8 anos de usuário até a minha primeira contribuição), saia da inércia e
aproveite este mundo de oportunidades. A Mini DebConf acontecerá nos dias 05
e 06 de março (sábado e domingo), na sede da Aldeia Coworking, na Avenida
Cândido de Abreu, 381, no bairro Centro Cívico, Curitiba/PR.
Mais informações: <http://br2016.mini.debconf.org>.

Nos vemos lá! ;-)