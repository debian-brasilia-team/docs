---
title: How was the Debian mini-event at FTSL 2017 in Curitiba
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:03.557Z
tags: blog, english
editor: markdown
dateCreated: 2017-10-03T12:40:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

The ninth edition of
[Free Software Technology Forum - (FTSL in portuguese)](http://ftsl.org.br)
took place from September 27 to 29, 2017 at Campus Curitiba of the
[Federal University of Technology - Paraná - UTFPR](http://www.utfpr.edu.br/estrutura-universitaria/pro-reitorias/prorec/diretoria-de-relacoes-interinstitucionais/international-affairs-eng/about-us), the same venue of
[MiniDebConf 2017 Curitiba](http://br2017.mini.debconf.org/index-en.shtml)
in march. And Debian community participated with some lectures on the third
day of the event.

A few months ago the FTSL organization looked for members of the Free Software
communities in Curitiba and proposed that they organize small events within
the FTSL. Each community would have a room available to organize their
activities on one of the three days of the event (Wednesday, Thursday or
Friday).

Some of these communities that organized activities were:
[PHP](https://phppr.org/),
[Python](https://grupypr.github.io/) and
[Debian](/blog/debian-brasil-estara-no-ftsl-2017-em-curitiba).


Debian community Debian held six lectures throughout the third day:


### 1 - Why am I a GNU/Linux/DEBIAN user?

- Speaker: Antonio C C Marques

![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-antonio.jpg =400x)


### 2 - How to become an official member of Debian (DD or DM)

- Presentation file: [pdf](http://www.eriberto.pro.br/palestras/debian-dd.pdf)
- Video of the talk: [youtube](https://www.youtube.com/watch?v=UGuv-PZYr3w) and
[peertube](https://peertube.debian.social/videos/watch/f41aefde-f01e-4f71-be37-2854f63d3cb1) - Speaker:
[João Eriberto Mota Filho](https://contributors.debian.org/contributor/eriberto@debian.org/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-eriberto.jpg =400x)


### 3 - Debian - the universal system

- Presentation file: [pdf](http://softwarelivre.org/arquivos-de-apresentacoes/debian-o-sistema-universal-daniel-lenharo-ftsl-2017.pdf) - Video of the talk: [youtube](https://www.youtube.com/watch?v=McUhtsBhu6E&feature=youtu.be) and
[peertube](https://peertube.debian.social/videos/watch/164d1535-7128-40ca-b02f-93313d21d643) - Speaker:
[Daniel Lenharo de Souza](https://contributors.debian.org/contributor/lenharo@debian.org/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-daniel-1.jpg =400x)


### 4 - The Debian Project wants you!

- Presentation file:
[pdf](http://softwarelivre.org/arquivos-de-apresentacoes/o-projeto-debian-quer-voce-paulo-santana-ftsl-2017.pdf) and
[odp](http://softwarelivre.org/arquivos-de-apresentacoes/o-projeto-debian-quer-voce-paulo-santana-ftsl-2017.odp) - Speaker:
[Paulo Henrique de Lima Santana](https://contributors.debian.org/contributor/phls-guest@alioth/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-paulo.jpg =400x)


### 5 - Knowing the work of the Debian translation team

- Presentation file:
[pdf](http://softwarelivre.org/arquivos-de-apresentacoes/conhecendo-o-trabalho-da-equipe-de-traducao-do-debian-daniel-lenharo-ftsl-2017.pdf) - Speaker: [Daniel Lenharo de Souza](https://contributors.debian.org/contributor/lenharo@debian.org/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-daniel-2.jpg =400x)


### 6 - Debian 101

- Presentation file: pdf
- Speaker: [Samuel Henrique](https://contributors.debian.org/contributor/samueloph-guest@alioth/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-samuel.jpg =400x)


We took advantage of the FTSL to sell several Debian branded merchandise
produced by [Curitiba Free Community](http://curitibalivre.org.br/) to
attendees. Just to remember that the profit is reverted to organize future
Free Software events. You can see Debian branded merchandise in the photos
below:


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-1.jpg =400x)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-2.jpg =400x)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-3.jpg =400x)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-4.jpg =400x)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-5.jpg =400x)


Among the new products we made, there is the "Debian Women" t-shirt to
encourage the participation of more women in the Debian Project. To learn more
about Debian Women Project see
[https://www.debian.org/women](https://www.debian.org/women/index.pt.html) and
the brazilian wiki <https://wiki.debian.org/MulheresBrasil>.


Adriana da Costa secured her t-shirt :-)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-adriana.jpg =400x)


To buy the products online, see: <http://loja.curitibalivre.org.br>


We put the Debian community together to take a photo:


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-todos.jpg =400x)

Photo left to right: Eriberto, Paulo, Adriana, Daniel, Antonio and Samuel


We are very grateful to the organizers of FTSL 2017 for their commitment to the
organization of the event and the availability of space for our activities.
Congratulations to the organization for the success of FTSL 2017.


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-alisson-christian.jpg =400x)

Alisson Coelho and Christian Mendes - coordinators of the FTSL 2017
organization^


You can see all photos here:


<https://www.flickr.com/photos/curitibalivre/sets/72157661046698288>
