---
title: An interview with Chris Lamb, the current Debian Project Leader
description: by Giovani Ferreira
published: true
date: 2025-03-01T17:56:57.775Z
tags: blog, english
editor: markdown
dateCreated: 2017-07-17T02:02:00.000Z
---

<!-- author: Giovani Ferreira -->

![Chris lamb](https://debianbrasil.org.br/blog/imagens/chris-lamb.png)


In the last week [Chris Lamb](https://chris-lamb.co.uk/), current
[Debian Project Leader (DPL)](https://www.debian.org/devel/leader) gave an
interview to Dionatan Vidal Simioni, owner of
[Diolinux](http://www.diolinux.com.br/) - a brazilian blog of great audience
with several themes FLOSS.

We contacted Dionatan and he sent us this interview now share in full with the
Debian community.

The original post in Portuguese is available at:
<http://www.diolinux.com.br/2017/07/entrevistamos-chris-lamb-atual-lider-do-Debian.html> 

**Diolinux: What is your name and function within the Debian project?**

**Chris:**
*My name is Chris Lamb and I am the current Debian Project Leader
(DPL), the official representative of the Debian Project. I go by the nickname
"lamby" on IRC and @lolamby on Twitter.*
*The DPL has two main functions, one internal and one external. In the external
function, I represent the Debian Project to others. This involves giving talks
and presentations about Debian and attending conferences as well as forging
relationships with other organisations and companies.*
*Internally, the Project Leader manages the project and defines its vision to
some degree. They talk to other Debian developers to see how they can assist in
their work, removing any potential blockers. A main task of the Project Leader
therefore involves coordination and communication. Herding cats, as they say!*


**Diolinux: Since when do you perform this function?**

**Chris:**
*After a 6-week campaigning and voting period, I was elected on April 17th.*


**Diolinux: What made you part of the Debian community?**


**Chris:**
*My first experience with Debian was a happy accident. I had sent off for a
five CD set of Red Hat from "The Linux Emporium", a company in the UK that
shipped CDROMs of GNU/Linux distrbutions, an essential service prior to fast
internet connections.*
*However, I was to discover I lacked the 12MB of RAM required to run the Red
Hat installer. Annoyed, I reached for a Debian "potato" CD that was included
(free-of-charge) in my order due to it being rather outdated, even at the
time...*
*Fast-forwarding a few years, whilst my first contribution was trivial (a patch
to the Lilypond music software editor) it was a fellow Debian Developer's
infectious enthusiasm that led me to contribute more, becoming a Google Summer
of Code student and eventually becoming an official Debian Developer in
September 2008. Attending my first DebConf (the annual Debian conference) in
Edinburgh, Scotland cemented my good view of the community.*


**Diolinux: How would you define the Debian project for people who already use
linux?**


**Chris:**
*At the time of writing, more than 10% of the web is powered by Debian. How
many web sites would you have missed today without Debian? Debian is the
operating system of choice on the international space station and countless
universities, companies and public administrations rely on Debian to deliver
services to millions of users around the world and beyond. Debian is a highly
successful and is far more pervasive in our lives than people are aware of,
even within the GNU/Linux community. It would be fair to say that Debian has a
reputation for being an "expert"*
*Linux distribution. However, this is somewhat misleading and perhaps a little
unfair; whilst there are many distributions that extend Debian to improve its
user interface in various directions, the underlying core of Debian should not
be feared. Our efforts are generally focused around releasing every 18 months
or so; something we call Debian "stable". However, Debian also maintains the
"testing" distribution, perhaps better understood as the "staging area" for
the next release. There is also the "unstable" distribution which (despite the
misleading name!) is completely usable as a day-to-day system. It is akin to
the rolling variants of other distributions.*


**Diolinux: We all know that Debian's main goals are to have as stable a
system as possible and be a multiarchitecture system, including support for
different kernel types, not just Linux. Also, is there any future target for
Debian that falls outside these standards? Is there any other goal that you
are working to achieve?**


**Chris:**
*You are right to point out that Debian supports non-Linux kernels,
specifically the kfreebsd kernel. You may also be aware that there is highly
experimental Hurd port too. Debian's technical diversity stretches beyond
kernels however meaning that we are available on sorts of small devices,
notably as the base of Raspbian on the Raspberry Pi, as well as supporting
very large machines with 100GBs of RAM. We also ship with a huge choice of
desktop environments and don't "require" you to use one or the other.
And for those who care about such things, it is perfectly possible to use
Debian without systemd... "*g"


**Diolinux: We recently had information that there were several problems and
bugs in Debian ISOs Live, which is really something unusual, little by little
the problems are being corrected, however, through the mailing lists we were
able to prove that there were missing testers for these ISOs. Would you like to
know how the public can help debug Debian to make it better?**


**Chris:**
*Clearly, the issue with the live ISOs show that there are gaps in our QA
process. However, the team are introducing processes, both technical and
non-technical, to ensure it cannot happen again.*
*In terms of how the community can help, testing beta versions of the
images/installer is always appreciated; whilst the day-to-day distribution
gets a lot of testing prior to release, the very context of an installation
process means that it does not see as many "eyeballs."*


**Diolinux: You use Debian exclusively or you also use other distributions and
operating systems? What is your point of view on using proprietary systems?**


**Chris:**
*Currently, I not only exclusively use free software operating systems, they
are all running Debian!*


**Diolinux: Listening to our readers, some have argued that there is no
"easy-to-access" channel between Debian developers, the community, and the
"end-user". What is the best way to get involved with the project? What kind
of professionals and enthusiasts can be part of the Debian community?**


**Chris:**
*There are many ways you can start. For users, try the "Welcome" page on the
wiki:*

 *  <https://wiki.debian.org/Welcome/Users>

*For people interested in contributing, check out the overview here:*

* [https://www.debian.org/devel](https://www.debian.org/devel/)

*...but also do check the "New Maintainer's Guide":*

 *  <https://www.debian.org/doc/manuals/maint-guide>


**Diolinux: How do you imagine Debian will be in 10 years?**


**Chris:**
*Firstly, I would to imagine myself as still a contributor to the Debian
project and I trust that many of the current developers will be too.*
*However, in terms of what the distribution itself will look like, Debian has
always had a tradition of being shaped by user and developer demand as well as
by the general efforts and standardisation in the free software community
around certain software.
This is contrast to having a vision imposed on it "from high", one of the
principal reasons why people choose Debian to begin with. There is no reason
to think why this would be any different in the upcoming decade, meaning that
it is extremely difficult to predict!*


**Diolinux: Leave a final message to the Diolinux blog audience.**


**Chris:**
*Thanks for reading!*


------------------------------------------------------------------------


The Debian Brasil community thanks Diolinux for the content available for this
publication and thanks Chris Lamb for his interview. This publication is of
great importance for the promotion of the
[Debian Project in Brazil](https://wiki.debian.org/Brasil/).
