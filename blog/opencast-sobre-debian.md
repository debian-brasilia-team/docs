---
title: Opencast sobre Debian
description: por Helio Loureiro
published: true
date: 2025-03-01T17:41:33.084Z
tags: blog, geral
editor: markdown
dateCreated: 2013-09-30T09:11:00.000Z
---

<!-- author: Helio Loureiro -->

Participei, junto com o Guaraldo, de um opencast pra falar um pouco sobre o Debian. 
Foi mais direcionado às pessoas que usam Ubuntu mas que gostariam de conhecer
as origens desse pelo Debian.

<https://youtu.be/K6-oNg_I3wY>