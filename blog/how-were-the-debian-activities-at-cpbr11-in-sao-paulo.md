---
title: How were the Debian activities at
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:06.431Z
tags: blog, english
editor: markdown
dateCreated: 2018-02-05T16:10:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

The Debian community was present at
[Campus Party Brasil 2018 - CPBR11](http://brasil.campus-party.org/), it was
held from january 30th to february 4th at Anhembi in São Paulo.

![Cpbr11 2018 001](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-001.jpg  =400x)

## Activities realized

**1 - Workshop for signing OpenPGP keys**

It was held on january 31th from 6pm to 8pm

Speaker:

- Giovani Ferreira (DD)

<https://campuse.ro/events/campus-party-brasil-2018/workshop/oficina-para-assinatura-de-chaves-openpgp-cpbr11-comunidades-softwarelivre> 
![Cpbr11 2018 037](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-037.jpg  =400x)

**2 - Workshop: Debian web server: HTML, PHP and MySQL in practice**

It was held on february 1st from 10:30am to 1pm

Speaker:

- Wellton Costa

<https://campuse.ro/events/campus-party-brasil-2018/workshop/servidor-web-em-debian-html-php-e-mysql-na-pratica-cpbr11-comunidades-softwarelivre> 
![Cpbr11 2018 059](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-059.jpg  =400x)

**3 - Workshop: Teaching and practicing the "transplant" of a Debian GNU/Linux installation** 
It was held on february 2nd from 3pm to 5pm

Speaker:

- Paulo Roberto Alves de Oliveira (Kretcheu) (DM)

<https://campuse.ro/events/campus-party-brasil-2018/workshop/ensinando-e-praticando-o-transplante-de-uma-instalacao-debian-gnulinux-cpbr11-comunidades-softwarelivre> 
**4 - Workshop: Debian on the desktop, installing and knowing the options**

It was held on february 2nd from 5:45pm to 7:45pm

Speaker:

- Daniel Lenharo (DD)

<https://campuse.ro/events/campus-party-brasil-2018/workshop/debian-no-desktop-instalando-e-conhecendo-as-opcoes-cpbr11-comunidades-softwarelivre> 
![Cpbr11 2018 063](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-063.jpg  =400x)

**5 - Panel: Debian - listening to experiences and contributing to the project** 
It was held on february 3rd from 0am to 1am

Speakers:

- Daniel Lenharo (DD)
- Paulo Henrique de Lima Santana (DM)
- Antonio C. C. Marques

[Vídeo](https://www.youtube.com/watch?v=AXiiyuCexao)

<https://campuse.ro/events/campus-party-brasil-2018/talk/debian-ouvindo-experiencias-e-contribuindo-para-o-projeto-cpbr11> 
![Cpbr11 2018 079](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-079.jpg  =400x)

Photo from left to right: Paulo, Antonio and Daniel

**6 - Workshop: Contributing to the Debian Project**

It was held on february 3rd from 10:30am to 1pm

Speakers:

- Daniel Lenharo (DD)
- Lucas Kanashiro (DD)
- Paulo Henrique de Lima Santana (DM)

<https://campuse.ro/events/campus-party-brasil-2018/talk/workshop-contribuindo-para-o-projeto-debian> 
![Cpbr11 2018 086](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-086.jpg  =400x)

Photo from left to right: Paulo, Lucas and Daniel

After an initial presentation done by Paulo on how to contribute to the Debian
Project, the team was separated into two groups to learn and/or ask questions
about packaging with Lucas and about translation with Daniel.

![Cpbr11 2018 099](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-099.jpg  =400x)

Photo Lucas teaching packaging

![Cpbr11 2018 089](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-089.jpg  =400x)

Photo Daniel teaching translation

![Cpbr11 2018 012](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-012.jpg  =400x)

Photo from left to right: Rafael, Kretcheu, Giovani, Daniel and Paulo

## Others photos

![Cpbr11 2018 139](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-139.jpg  =400x)

Photo from left to right: Paulo, Antonio and Daniel

![Cpbr11 2018 017](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-017.jpg  =400x)

Photo Antonio Marques - Debian contributor and Maker

![Cpbr11 2018 140](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-140.jpg  =400x)

Photo Giovani Ferreira

![Cpbr11 2018 142](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-142.jpg  =400x)

Photo stickers to sell

![Cpbr11 2018 143](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-143.jpg  =400x)

Photo t-shits to sell

![Cpbr11 2018 144](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-144.jpg  =400x)

Photo t-shirts to sell

![Cpbr11 2018 145](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-145.jpg  =400x)

Photo tweet from OSI

![Cpbr11 2018 146](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-146.jpg  =400x)

Photo tweet from Bruce Perens

![Cpbr11 2018 110](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-110.jpg  =400x)

**You can see more photos on the link below:**

<https://www.flickr.com/photos/slcampusparty/albums/72157669221197639>