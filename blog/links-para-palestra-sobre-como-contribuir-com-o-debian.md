---
title: Links para palestra sobre como contribuir com o Debian
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:30.174Z
tags: blog, geral
editor: markdown
dateCreated: 2020-07-02T16:52:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Listamos abaixo link para palestras mostrando como você pode contribuir com o Debian. 

**O Projeto Debian quer você!**

- FiqueEmCasaUse Debian 08/05/2020:
    - <https://www.youtube.com/watch?v=xSJAkT1oR1Y>
    - <https://peertube.debian.social/videos/watch/fea77679-2e68-4308-b8b8-b6f7c2d88eb1> - DebConf19 20/07/2019:
    - <https://www.youtube.com/watch?v=M8DAEqVpmzw>
    - <https://peertube.debian.social/videos/watch/0b7dd3f0-1f23-498f-aaf0-0a96684d4958> - Campus Party Brasília 2017:
    - <https://www.youtube.com/watch?v=Qb9vFG0TrPs>
    - <https://peertube.debian.social/videos/watch/65af3a67-7aef-4b7d-9004-573d55aa056d> - Campus Party Bahia 2017:
    - <https://www.youtube.com/watch?v=JY78vCbp2go>
    - <https://peertube.debian.social/videos/watch/5a888bf3-1fd0-474f-8c94-709551b2df20> 
**Como se tornar um membro oficial do Debian (DD ou DM)**

- Debian Day Brasil 2020 online:
    - <https://www.youtube.com/watch?v=nAQZCsJhufs>
    - <https://peertube.debian.social/videos/watch/98ab298c-b3a1-46ed-9e9d-6b1e3871bd0a> - MiniDebConf Curitiba 2018 13/04/2018:
    - <https://www.youtube.com/watch?v=f8Cu3DFC-s0>
    - <https://peertube.debian.social/videos/watch/e2c4952f-589d-4cc3-b5eb-a41c7355aa25> - IX FTSL 2017 29/09/2017:
    - <https://www.youtube.com/watch?v=UGuv-PZYr3w>
    - <https://peertube.debian.social/videos/watch/f41aefde-f01e-4f71-be37-2854f63d3cb1> - FISL16 10/07/2017:
    - <https://www.youtube.com/watch?v=sAheZirzK7U>
    - <https://peertube.debian.social/videos/watch/d6520ecc-5788-4e37-a11e-ce37443f0199> 
**Não sou programador, como posso ajudar o projeto Debian?**

- FiqueEmCasaUse Debian 22/05/2020:
    - <https://www.youtube.com/watch?v=1zXEW6OtrCY>
    - <https://peertube.debian.social/videos/watch/64abf5e1-8b2e-4a2c-8c6b-83582d860bf6> - FISL16 10/07/2017:
    - <https://www.youtube.com/watch?v=yH8aDSkob8g>
    - <https://peertube.debian.social/videos/watch/857bdc28-47c2-4c4d-a3e2-5060d85ad628> 
**Sou programador{a,}, como posso ajudar o Debian?**

- FiqueEmCasaUse Debian 05/06/2020:
    - <https://www.youtube.com/watch?v=kanDL540lVc>
    - <https://peertube.debian.social/videos/watch/7669bc85-5ef9-4085-b885-f542621c265f> 
**Tratando bugs no Debian ao vivo**

- FiqueEmCasaUse Debian 18/05/2020:
    - <https://www.youtube.com/watch?v=Ibax4tP9nNM>
    - <https://peertube.debian.social/videos/watch/a107a116-e4ae-4d6a-bfe1-2b0316612d1e> 
**De noviço a tradutor: uma caminhada com Debian**

- FiqueEmCasaUse Debian 26/05/2020:
    - <https://www.youtube.com/watch?v=75Ouhkm31d0>
    - <https://peertube.debian.social/videos/watch/41fe8e11-4dd5-4323-9a04-679aec6b629c> 
**Colabore com o Projeto Debian e impulsione sua carreira**

- FiqueEmCasaUse Debian 28/05/2020:
    - <https://www.youtube.com/watch?v=gl4t_WLwJXg>
    - <https://peertube.debian.social/videos/watch/26273d54-b307-4e1f-a56e-c30d9b20c5cd> 
**A minha primeira experiência com empacotamento e tradução no Debian**

- FiqueEmCasaUse Debian 05/06/2020:
    - <https://www.youtube.com/watch?v=i0XoAzL1KHI>
    - <https://peertube.debian.social/videos/watch/bf31fad4-9814-4581-bd9a-3495335299fb> 
**Conheça o trabalho da equipe de tradução do Debian**

- MiniDebConf Curitiba 2017 17/03/2017:
    - <https://www.youtube.com/watch?v=jkMIYpkhQ6Y>
    - <https://peertube.debian.social/videos/watch/df60b71b-7d44-4149-8f00-f11056597b11> 
**Debian: um universo em construção**

- MiniDebConf Curitiba 2017 17/03/2017:
    - <https://www.youtube.com/watch?v=wksPHH8noDw>
    - <https://peertube.debian.social/videos/watch/831719ce-e13a-4837-9af6-e474a78ed38e> 
**Painel: Debian Teams**

- MiniDebConf Curitiba 2017 18/03/2017:
    - <https://www.youtube.com/watch?v=SGLf1yHgNno>
    - <https://peertube.debian.social/videos/watch/6e7d772d-a378-43ed-8bad-6172bc35ebad> 
**Curti o Debian, quero contribuir! E agora?**

- MiniDebConf Curitiba 2018 13/04/2018:
    - <https://www.youtube.com/watch?v=TjOehR2Vdl4>
    - <https://peertube.debian.social/videos/watch/aa5f6b9c-0fe1-4825-8c84-3ecd9ebd15e4> 
**Um guia de newbie para newbie sobre o Debian**

- MiniDebConf Curitiba 2018 13/04/2018:
    - <https://www.youtube.com/watch?v=8BtWTRumhrA>
    - <https://peertube.debian.social/videos/watch/9bc25cea-a88b-4688-a028-3adc02b65363> 
**5 motivos para você celebrar o Debian Day**

- MiniDebConf Curitiba 2018 14/04/2018:
    - <https://www.youtube.com/watch?v=xAbifGImODU>
    - <https://peertube.debian.social/videos/watch/a7d166a0-e4b5-4e0d-9169-1051b46ccfc2> 
**Tradução do Debian para português do Brasil**

- FISL16 10/07/2017:
    - <https://www.youtube.com/watch?v=W8A5JDmZrDc>
    - <https://peertube.debian.social/videos/watch/9debe152-634e-4f0e-8ca7-722ce4166d55> 
## Internacionais

**I'm a programmer. How can I help Debian?**

- MiniDebConf online 2020:
    - <https://www.youtube.com/watch?v=RcTwWJ5C6uw>
    - <https://peertube.debian.social/videos/watch/87c422a4-8f4e-4bdc-84ea-e634ad568f9e> 
**A newbie's newbie guide to Debian**

- DebConf17 5/08/2017:
    - <https://www.youtube.com/watch?v=tT39aHeqgfE>
    - <https://saimei.ftp.acc.umu.se/pub/debian-meetings/2017/debconf17/a-newbie-s-newbie-guide-to-debian.vp9.webm> 
**Welcome team and non-packaging contribution opportunities in Debian (inglês)** 
- MiniDebConf Marseille 2019 25/05/2019:
    - <https://www.youtube.com/watch?v=M9NMR0hmk-I>
    - <https://peertube.debian.social/videos/watch/fd1d9360-6df6-4ed9-9bcf-4f4843a0e458> 
**Panel Discussion: Story of Debian contributors around the world (inglês)**

- DebConf18 28/07/2018:
    - <https://www.youtube.com/watch?v=hx8eS_USL_E>
    - <https://peertube.debian.social/videos/watch/c833e8f3-97fd-4819-b997-ac9e870c3958> 
**Learn how to triage bugs (inglês)**

- DebConf18 30/07/2018:
    - <https://www.youtube.com/watch?v=yl4Exsw2T08>
    - <https://peertube.debian.social/videos/watch/68968e65-d78d-434a-957d-ba821eef8e88> 
- MiniDebConf Barcelona 15/03/2014:
    - <https://www.youtube.com/watch?v=nKAPPNQ2-Xw>
    - <https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2014/mini-debconf-barcelona/Bug_triaging_and_bug_closing_by_Solveig.webm> 
**A newbie's perspective towards Debian (inglês)**

- DebConf19 20/07/2019:
    - <https://www.youtube.com/watch?v=N35aKEKO4Mo>
    - <https://peertube.debian.social/videos/watch/c430ddc9-5fcc-4de2-afac-77790fbfd0d8> 
**Non uploading DD process and a couple of tricks that saved my volunteer life** 
- MiniDebConf Barcelona 16/03/2014:
    - <https://www.youtube.com/watch?v=P6qesujjnUY>
    - <https://caesar.ftp.acc.umu.se/pub/debian-meetings/2014/mini-debconf-barcelona/Non_uploading_DD_process_and_a_couple_of_tricks_that_saved_my_volunteer_life_by_Francesca_Ciceri.webm>