---
title: DebianDay 2014 Brasil
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:38:38.520Z
tags: blog, geral
editor: markdown
dateCreated: 2014-07-28T14:50:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

![](https://debianbrasil.org.br/blog/imagens/banner-debianday2014-2.png)

### Cidades brasileiras que estão organizando o Debian Day 2014

**Belém - PA**

-   Data: 16 de agosto de 2014
-   Horário:
-   Local:
-   Contato: contato@ausla.com.br
-   Site:

**Campo Grande - MS**

-   Data: 16 de agosto de 2014
-   Horário: 8:00h às 17:00h
-   Local: Campus central da Universidade Tecnológica Federal do Paraná (UTFPR)
-   Contato:
-   Site: <http://www.debian-ms.org>

**Curitiba - PR**

-   Data: 16 de agosto de 2014
-   Horário: 13:00h às 18:00h
-   Local: Campus central da Universidade Tecnológica Federal do Paraná (UTFPR)
-   Contato: contato@curitibalivre.org.br
-   Site: <http://www.softwarelivre.org/debianday2014-curitiba>

**Francisco Beltrão - PR**

-   Data: 16 de agosto de 2014
-   Horário: 9:00h às 16h30min
-   Local: Campus Francisco Beltrão da Universidade Tecnológica Federal do Paraná (UTFPR) -   Contato: debian.fb@gmail.com
-   Site: [http://eventos.fb.utfpr.edu.br/debianday](http://eventos.fb.utfpr.edu.br/debianday/) -   [Programação](https://debianbrasil.org.br/blog/imagens/programacao-francisco-beltrao.png)

**Juiz de Fora - MG**

-   Data: 16 de agosto de 2014
-   Horário:
-   Local:
-   Contato:
-   Site: [http://www.debiandayjf.blogspot.com.br](http://www.debiandayjf.blogspot.com.br/) 
**Luziânia - GO**

-   Data: 16 de agosto de 2014
-   Horário: 7:00h
-   Local: Câmpus Luziânia do Instituto Federal de Góias
-   Contato: wendell.geraldes@ifg.edu.br
-   Site:

**Recife - PE**

-   Data: 16 de agosto de 2014
-   Horário: 18:00h
-   Local: Pizzaria Atlântico das Graças
-   Contato:
-   Site: <http://www.konesans.com.br/?p=199>

**Salvador - BA**

-   Data: [16 de agosto] 23 de agosto de 2014
-   Horário:
-   Local: Raul HackerClub - <http://raulhc.cc/Doc/Sede#ComoChegar>
-   Contato:
-   Site: [http://raulhc.cc/Projetos/DebianDay2014](http://raulhc.cc/Projetos/DebianDay2014%20) 
**Taguatinga - DF**

-   Data: 16 de agosto de 2014
-   Horário: 9:00h às 17:00h
-   Local: Universidade Católica de Brasília - UCB
-   Contato: alcyon@portaltic.com
-   Site: <https://www.doity.com.br/debian-day-df-2014>

**Trindade - GO**

-   Data: 16 de agosto de 2014
-   Horário: 8:00h às 18:00h
-   Local: Campus Trindade da Universidade Estadual de Goiás - UEG
-   Contato: george.mendes@gmail.com
-   Site: <http://www.debian-go.org>
-   [Programação](https://debianbrasil.org.br/blog/imagens/programacao-trindade.pdf)

Obs: está lista está sendo atualizada porque outras cidades podem ser incluídas. 
Veja outras cidades no mundo que estão organizando também:

<https://wiki.debian.org/DebianDay/2014>