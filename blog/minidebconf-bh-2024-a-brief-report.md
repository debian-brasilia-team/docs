---
title: MiniDebConf Belo Horizonte 2024 - a brief report
description: by Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-08T21:44:22.132Z
tags: blog, english
editor: markdown
dateCreated: 2025-02-27T19:37:14.939Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

From April 27th to 30th, 2024,
[MiniDebConf Belo Horizonte 2024](https://bh.mini.debconf.org/) was held at
the Pampulha Campus of
[UFMG - Federal University of Minas Gerais](https://ufmg.br), in Belo
Horizonte city.

![MiniDebConf BH 2024 banners](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-banners.jpg =400x) 

This was the fifth time that a MiniDebConf (as an exclusive in-person event
about Debian) took place in Brazil. Previous editions were in Curitiba
([2016](https://br2016.mini.debconf.org/),
[2017](https://br2017.mini.debconf.org), and
[2018](https://br2018.mini.debconf.org)), and in
[Brasília 2023](https://brasilia.mini.debconf.org). We had other MiniDebConfs
editions held within Free Software events such as
[FISL](http://fisl.org.br) and [Latinoware](https://latinoware.org/), and other
online events. See our
[event history](https://bh.mini.debconf.org/evento/edicoes-anteriores/).

Parallel to MiniDebConf, on 27th (Saturday)
[FLISOL - Latin American Free Software Installation Festival](https://flisol.info/FLISOL2024/Brasil) took place. It's the largest event in Latin America to promote Free Software,
and It has been held since 2005 simultaneously in several cities.

MiniDebConf Belo Horizonte 2024 was a success (as were previous editions) thanks to the participation of everyone, regardless of their level of knowledge about
Debian. We value the presence of both beginner users who are familiarizing
themselves with the system and the official project developers. The spirit of
welcome and collaboration was present during all the event.

![MiniDebConf BH 2024 flisol](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-flisol.jpg =400x) 
## 2024 edition numbers

During the four days of the event, several activities took place for all
levels of users and collaborators of the Debian project. The official schedule
was composed of:

- 06 rooms in parallel on Saturday;
- 02 auditoriums in parallel on Monday and Tuesday;
- 30 talks/BoFs of all levels;
- 05 workshops for hands-on activities;
- 09 lightning talks on general topics;
- 01 Live Electronics performance with Free Software;
- Install fest to install Debian on attendees' laptops;
- BSP (Bug Squashing Party);
- Uploads of new or updated packages.


![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-palestra-1.jpg =400x) 
The final numbers for MiniDebConf Belo Horizonte 2024 show that we had a
record number of participants.

- Total people registered: 399
- Total attendees in the event: 224

Of the 224 participants, 15 were official Brazilian contributors,
10 being DDs (Debian Developers) and 05 (Debian Maintainers), in addition to
several unofficial contributors.

The organization was carried out by 14 people who started working at the end of
2023, including Prof. Loïc Cerf from the Computing Department who made the event possible at UFMG, and 37 volunteers who helped during the event.

As MiniDebConf was held at UFMG facilities, we had the help of more than
10 University employees.

See the [list](https://bh.mini.debconf.org/evento/organizadores/) with the
names of people who helped in some way in organizing MiniDebConf Belo Horizonte
2024.

The difference between the number of people registered and the number of
attendees in the event is probably explained by the fact that there is no
registration fee, so if the person decides not to go to the event, they will
not suffer financial losses.

The 2024 edition of MiniDebconf Belo Horizonte was truly grand and shows the
result of the constant efforts made over the last few years to attract more
contributors to the Debian community in Brazil. With each edition the numbers
only increase, with more attendees, more activities, more rooms, and more
sponsors/supporters.

![MiniDebConf BH 2024 grupo](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-grupo-1.jpg =400x) <br />
<br />
![MiniDebConf BH 2024 grupo](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-grupo-2.jpg =400x) 
## Activities

The MiniDebConf schedule was intense and diverse. On the 27th, 29th and 30th
(Saturday, Monday and Tuesday) we had talks, discussions, workshops and many
practical activities.

![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-palestra-2.jpg =400x) 
On the 28th (Sunday), the Day Trip took place, a day dedicated to sightseeing
around the city. In the morning we left the hotel and went, on a chartered bus,
to the
[Belo Horizonte Central Market](https://mercadocentral.com.br/). People took
the opportunity to buy various things such as cheeses, sweets, cachaças and
souvenirs, as well as tasting some local foods.

![MiniDebConf BH 2024 mercado](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-mercado.jpg =400x) 
After a 2-hour tour of the Market, we got back on the bus and hit the road for
lunch at a typical Minas Gerais food restaurant.

![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-restaurante.jpg =400x) 
With everyone well fed, we returned to Belo Horizonte to visit the city's
main tourist attraction: Lagoa da Pampulha and Capela São Francisco de Assis,
better known as
[Igrejinha da Pampulha](http://portalbelohorizonte.com.br/o-que-fazer/arte-e-cultura/igrejas/igreja-sao-francisco-de-assis). 
![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-igrejinha.jpg =400x) 
We went back to the hotel and the day ended in the hacker space that we set up
in the events room for people to chat, packaging, and eat pizzas.

![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-hotel.jpg =400x) 
## Crowdfunding

For the third time we ran a crowdfunding campaign and it was incredible how
people contributed! The initial goal was to raise the amount equivalent to a
gold tier of R$ 3,000.00. When we reached this goal, we defined a new one,
equivalent to one gold tier + one silver tier (R$ 5,000.00). And again we
achieved this goal. So we proposed as a final goal the value of a gold + silver
+ bronze tiers, which would be equivalent to R$ 6,000.00. The result was that
we raised R$7,239.65 (~ USD 1,400) with the help of more than 100 people!

Thank you very much to the people who contributed any amount. As a thank you, we [list the names of the people who donated](https://bh.mini.debconf.org/doacoes/). 
![MiniDebConf BH 2024 doadores](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-doadores.jpg =400x) 
## Food, accommodation and/or travel grants for participants

Each edition of MiniDebConf brought some innovation, or some different benefit
for the attendees. In this year's edition in Belo Horizonte, as with DebConfs, we [offered bursaries for food, accommodation and/or travel](https://bh.mini.debconf.org/evento/bolsas/) to help those people who would like to come to the event but who would need
some kind of help.

In the registration form, we included the option for the person to request a
food, accommodation and/or travel bursary, but to do so, they would have to
identify themselves as a contributor (official or unofficial) to Debian and
write a justification for the request.

Number of people benefited:

- Food: 69
- Accommodation: 20
- Travel: 18

The food bursary provided lunch and dinner every day. The lunches included
attendees who live in Belo Horizonte and the region. Dinners were paid for
attendees who also received accommodation and/or travel. The accommodation was
held at the [BH Jaraguá Hotel](https://www.bhjaraguahotel.com.br/). And the
travels included airplane or bus tickets, or fuel (for those who came by car or
motorbike).

Much of the money to fund the bursaries came from the Debian Project, mainly
for travels. We sent a budget request to the former Debian leader Jonathan
Carter, and He promptly approved our request.

In addition to this event budget, the leader also approved individual requests
sent by some DDs who preferred to request directly from him.

The experience of offering the bursaries was really good because it allowed
several people to come from other cities.

![MiniDebConf BH 2024 grupo](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-jantar.jpg =400x) 
## Photos and videos

You can watch recordings of the talks at the links below:

- [Youtube](https://www.youtube.com/playlist?list=PLU90bw3OpxLpu7hJO8TzySFX32_UD8Eh4) - [Peertube](https://peertube.debian.social/w/p/3X7BY1MyH686QNCS2A6wkd)
- [video.debian.net](https://meetings-archive.debian.net/pub/debian-meetings/2024/MiniDebConf-Belo-Horizonte/) 
And see the photos taken by several collaborators in the links below:

- [Google photos](https://photos.app.goo.gl/ZV14bZ9AQFaKsgio6)
- [Nextcloud](https://cloud.debianbsb.org/s/wgzBtzKX2kbLCYP)

## Thanks


We would like to thank all the attendees, organizers, volunteers, sponsors and
supporters who contributed to the success of MiniDebConf Belo Horizonte 2024.

![MiniDebConf BH 2024 grupo](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-ate-2025.jpg =400x) 
## Sponsors

**Gold:**

- [Collabora](https://www.collabora.com/)

**Silver:**

- [Jedai.ai](https://jedai.ai/)
- [Toradex Brasil](https://www.toradex.com/pt-br)
- [Globo.com](https://www.globo.com)
- [Policorp Tecnologia](https://policorp.com.br)

**Bronze:**

- [BRDSoft - Tecnologias para TI e Telecomunicações](https://brdsoft.com.br/)
- [EITA - Cooperativa de Trabalho Educação, Informação e Tecnologia para Autogestão](https://eita.coop.br/) 
## Supporters

- [ICTL - Instituto para Conservação de Tecnologias Livres](https://ictl.org.br) - [Nazinha Alimentos](https://www.nazinha.com.br/)
- DACOMPSI

## Organizers

- [Projeto Debian](https://www.debian.org/)
- [Comunidade Debian Brasil](https://debianbrasil.org.br/)
- [Comunidade Debian MG](https://debian-minas-gerais.gitlab.io/site/)
- [DCC/UFMG - Departamento de Ciência da Computação da Universidade Federal de Minas Gerais](https://dcc.ufmg.br/) 