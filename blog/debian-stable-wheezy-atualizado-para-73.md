---
title: Debian stable (Wheezy) atualizado para 7.3
description: por Helio Loureiro
published: true
date: 2025-03-01T17:38:36.197Z
tags: blog, geral
editor: markdown
dateCreated: 2013-12-16T09:54:00.000Z
---

<!-- author: Helio Loureiro -->

![Debian Wheezy](http://2.bp.blogspot.com/-TrwpygdALME/UYW7yyjknZI/AAAAAAAAAj8/Kt8eh9vRXno/s1600/debian-wheezy.png) 

O projeto Debian anunciou o lançamento da terceira atualização da sua
distribuição estável Debian 7 (codenome Wheezy).  Essa atualização adiciona
principalmente correções para problemas de segurança naversão estável, assim
como alguns ajustes para problemas mais sérios.

Mais informações:

<http://www.debian.org/News/2013/20131214>