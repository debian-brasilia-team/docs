---
title: Resultado do sprint DDTP-DDTSS da equipe l10n-pt-BR
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:42:19.063Z
tags: blog, geral
editor: markdown
dateCreated: 2021-04-10T18:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A [equipe de tradução do Debian para o português do Brasil](https://wiki.debian.org/Brasil/Traduzir) realizou de 20 a 28 de março de 2021 um
[sprint](https://wiki.debian.org/Brasil/Traduzir/Campanhas/SprintDDTP) para
traduzir as descrições de pacotes do Debian e encorajar a participação de
novos(as) colaboradores(as). Este é o segundo ano consecutivo em que um sprint
é realizado pela equipe de tradução pt-BR.
O [primeiro](https://wiki.debian.org/Sprints/2020/l10n-portuguese) aconteceu em
maio de 2020 quando a equipe focou na tradução dos arquivos do site <debian.org> e resultou na tradução/atualização de todas as páginas principais durante os
meses seguintes.

A primeira atividade do grupo foi um encontro virtual pelo Jitsi para uma
rápida apresentação da equipe l10n-pt-BR . Depois  os veteranos apresentaram
aos(às) novatos(as) o
[DDTP - Projeto de Tradução de Descrições Debian](https://www.debian.org/international/l10n/ddtp) e mostraram na prática como utilizar o
[DDTSS - Debian Distributed Translation Server Satellite](https://ddtp.debian.org/ddtss/index.cgi/pt_BR). Durante a semana ainda foram realizados mais dois encontros com os mesmo
objetivos para dar oportunidade a quem não pôde participar do encontro anterior. 
Durante os dias deste sprint foram realizados **mais de 100 novas traduções e
mais de 650 revisões de traduções** por 15 pessoas, para as quais deixamos nosso muito obrigado por suas contribuições e listamos seus nomes abaixo:

* Alexandro Souza
* Carlos Henrique Lima Melara
* Daniel Lenharo de Souza
* Felipe Viggiano
* Fred Maranhão
* Gabriel Thiago H. dos Santos
* Luis Paulo Linares
* Michel Recondo
* Paulo Henrique de Lima Santana
* Ricardo Berlim Fonseca
* Thiago Hauck
* Thiago Pezzo
* Tiago Zaniquelli
* Victor Moreira
* Wellington Almeida

Para finalizar o sprint, realizamos um último encontro virtual para avaliação e
discussão sobre os
[encaminhamentos](https://wiki.debian.org/Brasil/Traduzir/Campanhas/SprintDDTP#Encaminhamentos) futuros.

Nunca é tarde para começar a contribuir com o Debian! Se você também quer
ajudar com tradução, leia a
[nossa página](https://wiki.debian.org/Brasil/Traduzir) e venha fazer parte da
nossa equipe.
