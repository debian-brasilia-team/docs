---
title: Debian Brazil at Campus Party Brazil 2023
description: by Daniel Lenharo
published: true
date: 2025-03-01T17:38:17.880Z
tags: blog, english
editor: markdown
dateCreated: 2023-08-02T10:00:00.000Z
---

<!-- author: Daniel Lenharo -->

Another edition of [Campus Party Brasil](https://brasil.campus-party.org/cpbr15/) took place in the city of São Paulo between the 25th and 30th of July 2023.
One more time the Debian Brazil Community was present. During the days in the
available space, we carry out some activities:  
  - Gifts for attends (stickers, cups, lanyards);  
  - Workshop on how to contribute to the translation team;  
  - Workshop on packaging;  
  - Key signing party;  
  - Information about the project;  
 
Every day, there was always someone available to pass on information about what
Debian is and the different ways to contribute. During the entire event, we
estimate that at least 700 people interacted in some way with our community.
Several people took the opportunity to benefit from the excellent work done by
the project on Debian 12 - Bookworm.
 
Here are some photos taken during the event!


![CPBR15](https://debianbrasil.org.br/blog/imagens/cpbr15/CPBR15-01.jpg =400x)  
Community Space at the Event.  
![CPBR15](https://debianbrasil.org.br/blog/imagens/cpbr15/CPBR15-03.jpg =400x)  
Romulo, visitor from space with Daniel Lenharo.  
![CPBR15](https://debianbrasil.org.br/blog/imagens/cpbr15/CPBR15-04.jpg =400x)  
Some gifts for attends.  
![CPBR15](https://debianbrasil.org.br/blog/imagens/cpbr15/CPBR15-05.jpg =400x)  
Communit Space at the event.  
![CPBR15](https://debianbrasil.org.br/blog/imagens/cpbr15/CPBR15-06.jpg =400x)  
Sticker made with Jefferson artwork.  
![CPBR15](https://debianbrasil.org.br/blog/imagens/cpbr15/CPBR15-08.jpg =400x)  
People in the community space.  
![CPBR15](https://debianbrasil.org.br/blog/imagens/cpbr15/CPBR15-10.jpg =400x)  
Packaging workshop, held by Charles.  
![CPBR15](https://debianbrasil.org.br/blog/imagens/cpbr15/CPBR15-09.jpg =400x)  
people who were involved in the Community activities.  
