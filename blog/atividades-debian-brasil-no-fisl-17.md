---
title: Atividades Debian Brasil no FISL 17
description: por Giovani Ferreira
published: true
date: 2025-03-01T17:57:13.624Z
tags: blog, geral
editor: markdown
dateCreated: 2016-07-08T19:58:00.000Z
---

<!-- author: Giovani Ferreira -->

A comunidade Debian Brasil está com uma série de atividades programadas para o
FISL 17.

A programação está disponível na Wiki do projeto Debian, que pode ser acessada
[aqui.](https://wiki.debian.org/Brasil/Eventos/EncontroComunitarioFISL2016)

Nos vemos no FISL!
