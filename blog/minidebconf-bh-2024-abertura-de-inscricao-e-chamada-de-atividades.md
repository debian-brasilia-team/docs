---
title: MiniDebConf BH 2024 - abertura de inscrição e chamada de atividades
description: por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:40:53.207Z
tags: blog, geral
editor: markdown
dateCreated: 2024-01-14T11:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

![MiniDebConf BH 2024](https://debianbrasil.org.br/blog/imagens/logo-debconf-bh-600px.svg)

Está aberta a inscrição de participantes e a
[chamada de atividades](https://bh.mini.debconf.org/evento/chamada-de-atividades/) para a
[MiniDebConf Belo Horizonte 2024](https://bh.mini.debconf.org) e para o
[FLISOL - Festival Latino-americano de Instalação de Software Livre](https://flisol.info/FLISOL2024/Brasil/BeloHorizonte). 
Veja abaixo algumas informações importantes:

## Data e local da MiniDebConf e do FLISOL

A MiniDebConf acontecerá de 27 a 30 de abril no
[Campus Pampulha da UFMG - Universidade Federal de Minas Gerais](https://bh.mini.debconf.org/evento/local/). 
No dia 27 (sábado) também realizaremos uma edição do
[FLISOL - Festival Latino-americano de Instalação de Software Livre](https://flisol.info/FLISOL2024/Brasil/BeloHorizonte), evento que acontece no mesmo dia em várias cidades da América Latina.

Enquanto a MiniDebConf terá atividades focados no Debian, o FLISOL terá
atividades gerais sobre Software Livre e temas relacionados como linguagem de
programação, CMS, administração de redes e sistemas, filosofia, liberdade,
licenças, etc.

## Inscrição gratuita e oferta de bolsas

**Você já pode realizar a sua inscrição gratuita para a MiniDebConf Belo Horizonte 2024.**

A MiniDebConf é um evento aberto a todas as pessoas, independente do seu nível
de conhecimento sobre Debian. O mais importante será reunir a comunidade para
celebrar um dos maiores projeto de Software Livre no mundo, por isso queremos
receber desde usuários(as) inexperientes que estão iniciando o seu contato com o Debian até Desenvolvedores(as) oficiais do projeto. Ou seja, estão todos(as)
convidados(as)!

Este ano estamos ofertando
[bolsas de hospedagem e passagens](https://bh.mini.debconf.org/evento/bolsas/)
para viabilizar a vinda de pessoas de outras cidades que
[contribuem para o Projeto Debian](https://www.debian.org/intro/help.pt.html).
Contribuidores(as) não oficiais, DMs e DDs podem solicitar as bolsas usando o
[formulário de inscrição](https://bh.mini.debconf.org/register/). 

Também estamos ofertando bolsas de alimentação para todos(as) os(as)
participantes, mesmo não contribuidores(as), e pessoas que moram na região de
BH.

Os recursos financeiros são bastante limitados, mas tentaremos atender o máximo
de pedidos.

Se você pretende pedir alguma dessas bolsas, acesse
[este link](https://bh.mini.debconf.org/evento/bolsas) e veja mais informações
antes de realizar a sua inscrição:

A inscrição (sem bolsas) poderá ser feita até a data do evento, mas temos uma
[data limite](https://bh.mini.debconf.org/evento/datas-importantes/) para o
pedido de bolsas de hospedagem e passagens, por isso fique atento(a) ao
**prazo final: até 18 de fevereiro.**

Como estamos usando mesmo formulário para os dois eventos, a inscrição será
válida tanto para a MiniDebConf quanto para o FLISOL. 

Para se inscrever, acesse o site, vá em
[Criar conta](https://bh.mini.debconf.org/accounts/login/). Criei a sua conta
(preferencialmente usando o [Salsa](https://salsa.debian.org/)) e acesse o
seu perfil. Lá você verá o botão de
[Se inscrever](https://bh.mini.debconf.org/register/).

https://bh.mini.debconf.org

## Chamada de atividades

**Também está aberta a
[chamada de atividades](https://bh.mini.debconf.org/evento/chamada-de-atividades/) tanto para MiniDebConf quanto para o FLISOL.**

Para mais informações, acesse
[este link](https://bh.mini.debconf.org/evento/chamada-de-atividades/).

**Fique atento ao prazo final para enviar sua proposta de atividade: até 18 de
fevereiro.**

## Contato

Qualquer dúvida, mande um email para <contato@debianbrasil.org.br>

## Organização

[![Debian Brasil](https://bh.mini.debconf.org/media/pages_files/logo-debian-brasil-150-100.svg)](https://debianbrasil.org.br) [![Debian ](https://bh.mini.debconf.org/media/pages_files/logo-debian-150-100.svg)](https://debian.org) [![Debian MG](https://bh.mini.debconf.org/media/pages_files/logo-debian-mg-150-100.svg)](https://debian-minas-gerais.gitlab.io/site/) [![DCC](https://bh.mini.debconf.org/media/pages_files/logo-DCC-370-80.svg)](https://dcc.ufmg.br) 