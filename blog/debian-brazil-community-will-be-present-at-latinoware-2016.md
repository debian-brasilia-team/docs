---
title: Debian Brazil Community will be present at Latinoware 2016
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:38:29.393Z
tags: blog, english
editor: markdown
dateCreated: 2016-09-29T01:19:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

The 13th edition of the [Latin American Open Source Software Conference - Latinoware 2016](http://latinoware.org/), will be held on october 19-21 at the premises of the Itaipu Technological Park
(PTI), located at Itaipu power plant, in Foz do Iguaçu city - Brazil. Around
4.000 people, among students, professionals and specialists from the area are
expected for this edition of the event.

During Latinoare 2016 the brazilian community of users and Debian developers
will be present to realize some activities on track "MiniDebConf". These
activities will be 01 workshop, 05 lightning talks and 07 lectures, all in
portuguese. See below:

## Workshop:

**Title: Teaching and practicing the "transplant" of a Debian GNU/Linux installation** 
- Speaker: Paulo Roberto Alves de Oliveira (DM)

## Panel:

**Title: Me, you and the Debian: how was my entry in Debian and how you can
enter**

- Speakers: Various.

## Lightning talks:

**Title: Debian to who want to be big**

- Speaker: Gilmar dos Reis

**Title: Meet Debian Forensics Team**

- Speaker: Giovani Augusto Ferreira (DM)

**Title: News of the APT (Advanced Package Tool)**

- Speaker: Raphael Mota

**Title: Understand the Debian Policy**

- Speaker: Thadeu Lima de Souza Cascardo (DD)

**Title: Women of my Brazil, where are you???**

- Speaker: João Eriberto Mota Filho (DD)

**Titlle: Ultimate debian database (udd) and debian.net: a show of information
e curiosities**

- João Eriberto Mota Filho.

## Lectures:

**Title: Debian - a universe under construction**

- Speaker: Giovani Augusto Ferreira (DM)

**Title: Debian Backstage: understand how the distribution works**

- Speaker: João Eriberto Mota Filho (DD)

**Title: Installing a GNU Debian 100% free**

- Speaker: Alessandro Moura

**Title: Joining Debian Translation Team**

- Speaker: Daniel Lenharo de Souza

**Title: Knowing the Debian Bug Tracking System (BTS)**

- Speaker: Giovani Augusto Ferreira (DM)

**Title: Organizing events in Debian**

- Speaker: Daniel Lenharo de Souza

**Title: Software packaging in Debian**

- Speaker: João Eriberto Mota Filho (DD)

## More informations:

<http://latinoware.org>

