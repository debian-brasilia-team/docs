---
title: Afinal de contas, o que é o Debian?
description: por DebianBrasil
published: true
date: 2025-03-01T17:56:55.504Z
tags: blog, geral
editor: markdown
dateCreated: 2012-07-17T12:07:00.000Z
---

<!-- author: DebianBrasil -->

O [Projeto Debian](https://debian.org/) é uma associação de indivíduos que têm
como causa comum criar um sistema operacional livre. O sistema operacional que
criamos é chamado Debian GNU/Linux, ou simplesmente Debian.

Um sistema operacional é o conjunto de programas básicos e utilitários que fazem seu computador funcionar. No núcleo do sistema operacional está o kernel. O
kernel é o programa mais fundamental no computador e faz todas as operações mais básicas, permitindo que você execute outros programas.

Os sistemas Debian atualmente usam o kernel [Linux](http://www.kernel.org/). O
Linux é uma peça de software criada inicialmente por
[Linus Torvalds](http://www.cs.helsinki.fi/u/torvalds/) com a ajuda de milhares
de programadores espalhados por todo o mundo.

No entanto, há trabalho em andamento para fornecer o Debian com outros kernels,
primeiramente com o [Hurd](http://www.gnu.org/software/hurd/hurd.html). O Hurd é um conjunto de servidores que rodam no topo de um micro kernel (como o Mach), os quais implementam diferentes características. O Hurd é software livre produzido
pelo [projeto GNU](http://www.gnu.org/).

Uma grande parte das ferramentas básicas que formam o sistema operacional são
originadas do [projeto GNU](http://www.gnu.org/); daí os nomes: GNU/Linux e
GNU/Hurd. Essas ferramentas também são ferramentas livres.

Claro que o que todos queremos são aplicativos: programas que nos ajudam a
conseguir fazer o que desejamos fazer, desde edição de documentos até a
administração de negócios, passando por jogos e desenvolvimento de mais
software. O Debian vem com mais de 29000
[pacotes](http://www.debian.org/distrib/packages) (softwares pré-compilados e
empacotados em um formato amigável, o que faz com que sejam de fácil instalação
em sua máquina) — todos eles são [livres](http://www.debian.org/intro/free).


É mais ou menos como uma torre: Na base dela está o kernel. Sobre ele todas as
ferramentas básicas e acima estão todos os outros softwares que você executa em
seu computador. No topo da torre está o Debian — organizando e arrumando
cuidadosamente as coisas, de modo que tudo funcione bem quando todos esses
componentes trabalham em conjunto.

Fonte:
[http://softwarelivre.org/debian-day-df-2012/blog/afinal-de-contas-o-que-e-o-debian](http://softwarelivre.org/debian-day-df-2012/blog/afinal-de-contas-o-que-e-o-debian) 