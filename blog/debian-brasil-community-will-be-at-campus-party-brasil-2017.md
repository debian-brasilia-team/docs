---
title: Debian Brasil Community will be at Campus Party Brasil 2017
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:38:22.487Z
tags: blog, english
editor: markdown
dateCreated: 2017-01-20T16:53:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

[Campus Party Brasil 2017 (CPBR10)](http://brasil.campus-party.org/) will take
place from january 31st to february 4th in São Paulo, and Debian Brasil
Community will be there with some activies. See below:

**Worskhop: Firts steps with Debian (and derivatives)**

- Schedule: february 2 - 16h15min
- Workshop Inovação
- Speaker: [Samuel Henrique](https://contributors.debian.org/contributor/samueloph-guest%40alioth/) 
[http://campuse.ro/events/campus-party-brasil-2017/workshop/primeiros-passos-debian-e-derivados](http://campuse.ro/events/campus-party-brasil-2017/workshop/primeiros-passos-debian-e-derivados/) 
**Talk: Debian - The Universal Operation System**

- Schedule: february 3 - 11h00min
- Stage: Curador I
- Speaker: [Daniel Lenharo de Souza](https://contributors.debian.org/contributor/lenharo-guest%40alioth/) 
<http://campuse.ro/events/campus-party-brasil-2017/talk/debian-o-sistema-universal> 
**Panel: Debian - Listening experiences and contributing to the project**

- Schedule: february 3 - 23h15min
- Stage: Inovação
- Speakers:
    - [Daniel Lenharo de Souza](https://contributors.debian.org/contributor/lenharo-guest%40alioth/)     - [Samuel Henrique](https://contributors.debian.org/contributor/samueloph-guest%40alioth/)     - [Paulo Henrique de Lima Santana](https://contributors.debian.org/contributor/phls-guest%40alioth/) 
<http://campuse.ro/events/campus-party-brasil-2017/talk/painel-debian-no-brasil> 
**Every day we will be selling Debian t-shirts and stickers on some of the
benches near the Inovação Stage.**

The profit obtained with the sales will be reversed in the organization of
[MiniDebConf Curitiba 2017](http://br2017.mini.debconf.org/index-en.shtml).

See the models below:

![Camisa preta 2](https://debianbrasil.org.br/blog/imagens/camisa-preta-2.jpg =400x)

![Camisa preta 1](https://debianbrasil.org.br/blog/imagens/camisa-preta-1.jpg =400x)

![Camisa vermelha](https://debianbrasil.org.br/blog/imagens/camisa-vermelha.jpg =400x)

![Camisa branca](https://debianbrasil.org.br/blog/imagens/camisa-branca.jpg =400x)