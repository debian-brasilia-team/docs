---
title: Debian Day 2024 em Belém e Poços de Caldas - Brasil
description: por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:39:10.186Z
tags: blog, geral
editor: markdown
dateCreated: 2024-08-28T11:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

por Paulo Henrique de Lima Santana (phls)

Listamos abaixo os links para os relatos e notícias do Debian Day 2024 realizado em Belém e Poços de Caldas:

- [Belém - PA](https://paralivre.org/noticias/primeiro-debianday-belem-celebrando-31-anos-de-debian-com-palestras-e-networking) - [Poços de Caldas - MG](https://portal.pcs.ifsuldeminas.edu.br/noticias/5270)
