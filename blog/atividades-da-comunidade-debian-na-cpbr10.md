---
title: Atividades da Comunidade Debian na #CPBR10
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T19:08:51.800Z
tags: blog, geral
editor: markdown
dateCreated: 2017-01-20T15:54:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Durante a [Campus Party Brasil 2017 - CPBR10](http://brasil.campus-party.org/),
que acontecerá de 31 de janeiro a 04 de fevereiro no Anhembi em São Paulo,
acontecerão algumas atividades da comunidade Debian. Veja abaixo a lista:

**Worskhop: Primeiros passos Debian (e derivados)**

- Data e horário: 02/02 - 16h15min
- Workshop Inovação
- Descrição: Aprenda tudo que você precisa para sair destruindo no Debian e seus sistemas derivados! Vamos abordar desde a instalação do sistema e primeiros
passos até dicas de segurança e compilação do kernel. É recomendável que o
participante já tenha o Debian instalado no seu notebook. Se precisar, pode
passar na bancada do software livre nos dias anteriores a oficina para mais
informações.
- Samuel Henrique é técnico em Informática pela UTFPR, atualmente estudando
Engenharia de Computação na UTFPR e estagiando na MadeiraMadeira. Debian
Maintainer atuando principalmente no time Debian Security Tools Packaging
Team <pkg-security-team>, também contribuiu com alguns pacotes no AUR,
repositório comunitário do Arch Linux, além de contribuir para projetos de
software livre sempre que possível; aircrack-ng, pixiewps, t50.

[http://campuse.ro/events/campus-party-brasil-2017/workshop/primeiros-passos-debian-e-derivados](http://campuse.ro/events/campus-party-brasil-2017/workshop/primeiros-passos-debian-e-derivados/) 

**Palestra: Debian - O sistema universal**

- Data e horário: 03/02 - 11h00min
- Palco Curador I
- Palestrante: Daniel Lenharo de Souza, Analista de Informática, Especialista em Redes e Segurança da Informação. Membro das Comunidades Curitiba Livre e Debian, promovendo o uso do Software Livre em diversos eventos pelo Brasil. Tenho
auxiliado na organização de diversos eventos (FISL, MiniDebConf, SFD, FLISOL,
entre outros).
- Descrição: Esta palestra expositiva, tem como objetivo apresentar o Projeto
Debian - O Sistema Operacional Universal. Mostrar ao público como ele é
organizado, sua estrutura, forma de trabalho. Também apresentar a Comunidade
Debian, comunidade que tem como objetivo manter e melhorar o maior projeto de
Software Livre do Mundo. Uma parte da palestra é reservada para apresentar
maneiras para colaborar com a comunidade Debian.

<http://campuse.ro/events/campus-party-brasil-2017/talk/debian-o-sistema-universal> 
**Painel: Debian - Ouvindo experiências e contribuindo para o projeto**

- Data e horário: 03/02 - 23h15min
- Palco de Inovação
- Descrição: A atividade tem por objetivo divulgar os trabalho desenvolvido pela comunidade Debian no Brasil. Apresentamos onde a comunidade Debian está
atualmente e para onde está indo, as formas distintas de como contribuir para
comunidade Debian Brasil e o projeto Debian. Principais objetivos: incentivar
mais pessoas a contribuir com o projeto, integrar colaboradores e usuários e
trocar conhecimentos.
- Palestrantes:
    - Daniel Lenharo de Souza, Analista de Informática, Especialista em Redes e
    Segurança da Informação. Membro das Comunidades Curitiba Livre e Debian,
    promovendo o uso do Software Livre em diversos eventos pelo Brasil. Tenho
    auxiliado na organização de diversos eventos (FISL, MiniDebConf, SFD,
    FLISOL, entre outros).
    - Paulo Henrique de Lima Santana é bacharel em Ciência da Computação pela
    UFPR, trabalha com administração de redes e sistemas GNU/Linux em Curitiba.
    Entusiasta de Software Livre, participa de diversos grupos de atuação como a     Comunidade Curitiba Livre e a Associação Software Livre.Org. Palestrou em
    edições do FISL, da Latinoware e da Campus Party Brasil. Coordenou a
    organização de eventos em Curitiba como o Festival Latino-americano de
    Instalação de Software Livre (FLISOL), o Software Freedom Day (SFD), o
    Education Freedom Day, o Document Freedom Day, e o Circuito Curitibano de
    Software Livre. Coordenou a organização da grade de programação do Fórum
    Internacional de Software Livre 2015 (FISL16) e 2016 (FISL17). Curador de
    Software Livre da Campus Party Brasil desde 2013, da Campus Party Recife
    desde 2012 e da Campus Party Minas Gerais em 2016.
    - Samuel Henrique é técnico em Informática pela UTFPR, atualmente estudando
    Engenharia de Computação na UTFPR e estagiando na MadeiraMadeira. Debian
    Maintainer atuando principalmente no time Debian Security Tools Packaging
    Team <pkg-security-team>, também contribuiu com alguns pacotes no AUR,
    repositório comunitário do Arch Linux, além de contribuir para projetos de
    software livre sempre que possível; aircrack-ng, pixiewps, t50.

<http://campuse.ro/events/campus-party-brasil-2017/talk/painel-debian-no-brasil> 
**Durante todos os dias estaremos vendendo camisetas e adesivos do Debian em
alguma das bancadas próximas ao Palco Inovação.**

O lucro obtido com as vendas será revertido na organização da
[MiniDebConf Curitiba 2017](http://br2017.mini.debconf.org/) que acontecerá de
17 a 19 de março.

Os modelos das camisetas são esses abaixo:

![Camisa preta 2](https://debianbrasil.org.br/blog/imagens/camisa-preta-2.jpg =400x)

![Camisa preta 1](https://debianbrasil.org.br/blog/imagens/camisa-preta-1.jpg =400x)

![Camisa vermelha](https://debianbrasil.org.br/blog/imagens/camisa-vermelha.jpg =400x)

![Camisa branca](https://debianbrasil.org.br/blog/imagens/camisa-branca.jpg =400x)