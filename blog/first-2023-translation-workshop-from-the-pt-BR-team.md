---
title: First 2023 translation workshop from the pt_BR team
description: by Thiago Pezzo
published: true
date: 2025-03-01T17:39:44.551Z
tags: blog, english
editor: markdown
dateCreated: 2023-04-01T10:00:00.000Z
---

<!-- author: Thiago Pezzo -->

The Brazilian translation team [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese/) had [their first workshop of 2023](https://wiki.debian.org/Brasil/Traduzir/Campanhas/OficinaDDTP2023) in February, with great results:

 * The workshop was aimed at beginners, working in [DDTP/DDTSS](https://ddtp.debian.org/ddtss).  * Two days of a hands-on workshop via Jitsi.
 * In the following days, translation work continued independently, with team support. 
 * Subscribers: 29
 * New contributors to DDPT/DDTSS: 22

 * Translations from new participants: 175
 * Revisions from new participants : 261

Our focus was to complete the descriptions of the 500 most popular packages
(popcon).
Although we were unable to reach 100% of the translation cycle, much of these
descriptions are in progress and with a little more work will be available to
the community.

[The Brazilian Portuguese translation team](https://wiki.debian.org/Brasil/Traduzir)