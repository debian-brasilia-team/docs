---
title: Chamada de atividades para Debian Day Brasil 2020
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:57:43.220Z
tags: blog, geral
editor: markdown
dateCreated: 2020-07-10T21:22:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Este ano a Comunidade Debian Brasil promoverá um evento online durante dois
dias para celebrar o 27º aniversário do Projeto Debian.
**O Debian Day Brasil 2020 online acontecerá nos dias 15 e 16 agosto** e será
transmitido pelo
[canal Debian Brasil no YouTube](https://www.youtube.com/channel/UCc6Yr77vh0vVh-yDXYwKLXg). 
Estamos com uma chamada de atividades aberta para receber propostas da
comunidade. Você pode enviar propostas de atividades relacionadas ao Debian até às 23:59 do dia 26 de julho de 2020. 
Teremos 3 tipos de atividades.

- Palestras longas: palestra expositiva, com tempo de 50 minutos de duração,
incluindo o tempo para responder perguntas.
- Palestras curtas: palestra expositiva, com tempo de 20 minutos de duração,
incluindo o tempo para responder perguntas.
- Oficina: atividade para mão na massa, com duração a definir.

Para enviar sua proposta, acesse o formulário abaixo:

<https://form.curitibalivre.org.br/index.php/329429>

![Debianday 2020 banner simples](https://debianbrasil.org.br/blog/imagens/debianday-2020-banner-simples.png =400x) 
