---
title: Evento: Debian Day - você tem que prestigiar o aniversariante!
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:39:33.209Z
tags: blog, geral
editor: markdown
dateCreated: 2014-07-03T16:39:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

---
title: Evento: Debian Day - você tem que prestigiar o aniversariante!
description: 
published: true
date: 2025-03-01T17:20:13.845Z
tags: blog
editor: markdown
dateCreated: 2025-02-27T19:35:17.553Z
---

Olá!

O **[Debian Day](https://wiki.debian.org/DebianDay/2014)** ou em português Dia
do Debian é comemorado todo dia 16 de agosto. A comemoração é feita nesse dia
ou no final de semana mais próximo. Em 2014 o dia 16/08 será em um sábado!

![](https://debianbrasil.org.br/blog/imagens/debian-logo.png)

**E o que é esse tal de Debian?**

Abaixo as explicações que encontrei no site
[debian.org](https://www.debian.org/intro/about)

Um sistema operacional é o conjunto de programas básicos e utilitários que
fazem seu computador funcionar. No núcleo do sistema operacional está o kernel.
O kernel é o programa mais fundamental no computador e faz todas as operações
mais básicas, permitindo que você execute outros programas.

Os sistemas Debian atualmente usam o
[kernel Linux](http://pt.wikipedia.org/wiki/Linux_(n%C3%BAcleo)).
O Linux é uma peça de software criada inicialmente por Linus Torvalds com a
ajuda de milhares de programadores espalhados por todo o mundo.

**Como consigo o Debian?**

Compre um CD faça o download do mesmo pela Internet. Depois confira nossa
documentação de instalação.

O método de instalação do Debian mais popular é através de CDs, os quais você
pode adquirir pelo preço das mídias de um dos nossos vários vendedores de CDs.
Caso você disponha de acesso rápido a Internet é possível fazer o download e
instalar o Debian (...)

**Como isso tudo começou?**

O Debian foi iniciado em agosto de 1993 por
[Ian Murdock](http://pt.wikipedia.org/wiki/Ian_Murdock), como uma nova
distribuição que seria feita abertamente, no espírito do Linux e do projeto GNU. O Debian deveria ser feito cuidadosamente e conscienciosamente e ser mantido e
suportado com cuidado similar. Ele começou como um grupo pequeno de
desenvolvedores de Software Livre e cresceu gradualmente para se tornar uma
comunidade grande e bem-organizada de desenvolvedores e usuários (...)

![](https://debianbrasil.org.br/blog/imagens/debian1.jpg)

**E o Debian Day?**

É um evento internacional organizado para comemorar o aniversário do Debian.
Nesse dia acontecem diversas atividades variando de acordo com o grupo que
está promovendo a comemoração do aniversário, tais, como de divulgação,
contribuições e até palestras.

Organize o Debian Day na sua cidade!
[AQUI](https://wiki.debian.org/DebianDay/2014) você encontra a lista das
cidades que estão organizando para este ano.

E não esqueça do bolo para o aniversariante do dia!

![](https://debianbrasil.org.br/blog/imagens/bolo-debian.jpeg)

Fonte:
[www.mulheres.eti.br](http://mulheres.eti.br/blog/evento-debian-day-voce-tem-que-prestigiar-o-aniversariante)