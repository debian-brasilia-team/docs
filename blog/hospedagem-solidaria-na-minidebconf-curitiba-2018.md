---
title: Hospedagem Solidária na MiniDebConf Curitiba 2018
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:39:58.130Z
tags: blog, geral
editor: markdown
dateCreated: 2018-02-09T17:21:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A Comunidade Debian quer dar uma ajuda para quem precisa de um cantinho para
ficar durante [MiniDebConf Curitiba 2018](https://minidebconf.curitiba.br/2018/) que acontecerá de 11 a 14 de abril.

Assim, criamos 2 tópicos no fórum para que você possa oferecer ou pedir um
quarto (cama, sofá, etc) durante a MiniDebConf.

Não há coisa melhor que aproveitar um evento próximo à pessoas que compartilham
dos mesmo valores!

O fórum foi desativado.

![Banner casa](https://debianbrasil.org.br/blog/imagens/banner-casa.png =400x)