---
title: Como foi a participação da equipe de localização na DebConf21
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:57:58.916Z
tags: blog, geral
editor: markdown
dateCreated: 2021-08-30T16:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A [equipe de localização](https://wiki.debian.org/Brasil/Traduzir) do Debian
para português do Brasil realizou um BoF com o título
[trabalho das comunidades brasileiras na tradução de softwares livres ](https://debconf21.debconf.org/talks/59-trabalho-das-comunidades-brasileiras-na-traducao-de-softwares-livres/) no dia 25 de agosto de 2021 durante a [DebConf21](https://debconf21.debconf.org) 
O que era para ser um bate-papo mais amplo entre várias equipes de localização
de projetos de software livre acabou se tornando uma conversa entre os membros
da equipe brasileira de tradução do Debian contando com a participação de:
Carlos Melara, Daniel Lenharo, Paulo Santana (phls), Ricardo Fonseca (Berlim) e
Thiago Pezzo (tico).

Para assistir a gravação, acesse o arquivo
[aqui](https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-151-trabalho-das-comunidades-brasileiras-na-traducao-de-softwares-livres.webm). 
Durante o BoF foram citados os seguintes pontos:

1) Atualizar a tradução dos pacotes apt e dpkg.
Mais detalhes [nessa mensagem](https://lists.debian.org/debian-l10n-portuguese/2021/08/msg00017.html) do Terceiro para a lista de discussão.

2) Pedir aos(as) DDs brasileiros(as) notícias para gerar conteúdo para
publicações da comunidade brasileira. Coisas como "o que eles(as) estão fazendo
que podemos divulgar?"

3) Realizar encontros mais descontraídos, para conversar e
fomentar a comunidade local.

4) Reativar a publicação "BITS da equipe debian-l10n-portuguese" coordenada pelo Daniel. Publicações passadas:

* [Dezembro de 2018](https://debianbrasil.org.br/blog/bits-da-equipe-debian-l10n-portuguese-dez2018) * [Novembro de 2018](https://debianbrasil.org.br/blog/bits-da-equipe-debian-l10n-portuguese-nov2018) * [Outubro de 2018](https://debianbrasil.org.br/blog/bits-da-equipe-debian-l10n-portuguese-out2018) * [Setembro de 2018](https://debianbrasil.org.br/blog/bits-da-equipe-debian-l10n-portuguese-set2018) * [Agosto de 2018](https://debianbrasil.org.br/blog/bits-da-equipe-debian-l10n-portuguese-ago2018) 
5) Começar a usar o [OmegaT](https://omegat.org/pt_BR/). Está
disponível no repositório do Debian.

6) Novas demandas para tradução levantadas pelo Thiago na lista devel, e que
pode ser usado o OmegaT integrado com o Git (salsa.debian.org):

* Debian Developer's Reference
    * [Mensagem](https://lists.debian.org/debian-devel/2021/08/msg00232.html)
    * [Site](https://www.debian.org/doc/manuals/developers-reference)
    * [Repositório](https://salsa.debian.org/debian/developers-reference)
* Debian Policy Manual
    * [Mensagem](https://lists.debian.org/debian-devel/2021/08/msg00444.html)
    * [Site](https://www.debian.org/doc/debian-policy)
    * [Repositório](https://salsa.debian.org/dbnpolicy/policy)
* devscripts
    * [Mensagem](https://lists.debian.org/debian-devel/2021/08/msg00450.html)
    * [Site](https://packages.debian.org/sid/devscripts)
    * [Repositório](https://salsa.debian.org/debian/devscripts)

7) Encontros virtuais para socialização da equipe e debate de temas
relacionados à tradução: idioma como fenômeno individual, social, cultural e
político, tradução profissional, troca de práticas e experiências entre a
equipe Debian e outras (LibreOffice, OmegaT, WordPress).

8) Relembrando as diversas frentes de tradução que temos hoje:

* Descrições de pacotes
    * [DDTSS](https://ddtp.debian.org/ddtss/index.cgi/pt_BR)
* Release Notes
    * [Site](https://www.debian.org/releases/bullseye/amd64/release-notes)
    * [Repositório](https://salsa.debian.org/ddp-team/release-notes/-/tree/master/pt-br) * Arquivos po:
    * [Estado dos arquivos](https://www.debian.org/international/l10n/po-debconf/pt_BR)     * [Arquivos PO originais](https://www.debian.org/international/l10n/po-debconf/pot)     * [Weblate para teste](https://hosted.weblate.org/projects/debconf-templates-ptbr) * Manual do DebianEdu
    * [Site](https://wiki.debian.org/DebianEdu/Documentation/Manuals)
    * [Weblate](ttps://hosted.weblate.org/projects/debian-edu-documentation/debian-edu-bullseye/pt_BR) * Site debian.org
    * [Repositório](https://salsa.debian.org/webmaster-team/webwml)
* Bits do Debian
    * [Site](https://bits.debian.org)
    * [Repositório](https://salsa.debian.org/publicity-team/bits)
* Announcements
    * [Repositório](https://salsa.debian.org/publicity-team/announcements)
* Wiki do Debian
    * [Site](https://wiki.debian.org)
