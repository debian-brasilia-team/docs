---
title: Debian Day 2024 em Natal/RN - Brasil
description: por Allythy
published: true
date: 2025-03-01T17:39:12.479Z
tags: blog, geral
editor: markdown
dateCreated: 2024-08-22T13:00:00.000Z
---

<!-- author: Allythy -->

por Allythy

O Debian Day é um evento anual que celebra o aniversário do Debian, uma das
distribuições GNU/Linux mais importante do Software Livre, criada em 16 de
Agosto de 1993, por Ian Murdock.

No último sábado (17/08/2024) no Sebrae-RN comemoramos os 31 anos Debian em
Natal, no Rio Grande do Norte. A celebração, foi organizada pela
PotiLivre(Comunidade Potiguar de Software Livre), destacou os 31 anos de
história do Debian. O evento contou com algumas palestras e muitas discussões
sobre Software Livre. Tivemos 70 inscrições, 40 estiverem presentes.

O Debian Day em Natal foi uma ocasião para celebrar a trajetória do Debian e
reforçar a importância do Software Livre.

## Palestrantes

Agradecemos imensamente a Isaque Barbosa Martins, Eduardo de Souza Paixão,
Fernando Guisso,que palestraram nessa edição! Obrigado por compartilhar tanto
conhecimento com a comunidade. Esperamos ver vocês novamente em futuros
encontros!

![foto da palestra conhecendo projeto Debian](https://debianbrasil.org.br/blog/imagens/debian-day-natal-2024/debian-day-natal-palestras.jpg =400x) 
- 09:00 - 09:40 - [Conhecendo projeto Debian](https://slides.com/allythy/conhecendo-projeto-debian) - Allytthy e Clara Nobre - 09:40 - 10:20 - [Proxmox e Homelab: Como Transformei um Mini PC em um Servidor de Respeito]((https://guisso.dev/posts/proxmox-debian-day/)) - Fernando Guisso - 10:20 - 10:40   Intervalo
- 10:40 - 11:20 - Analisando a aplicação de algoritmos criptográficos em pacotes de redes - Isaque Barbosa Martins - 11:20 - 12:00 - Introdução a escalação de privilégio em sistemas GNU/Linux - Eduardo de Souza Paixão 
[Link dos slides do Debian Day](https://drive.proton.me/urls/K357YMHWCM#CmuK3rRDT96m) 
## Participantes

Um grande obrigado também a todos os participantes, nós fazemos isso por vocês!
Esperamos que tenham aprendido, se divertido e feito novas conexões entre a
comunidade

![Participantes do Debian Day Natal-RN](https://debianbrasil.org.br/blog/imagens/debian-day-natal-2024/debian-day-natal-participantes.jpg =1000x) 
Essa edição do Debina Day Natal foi organizada por: Allythy, Clara Nobre,
Gabriel Damazio e Marcel Ribeiro.
