---
title: Envie sua proposta de logo para a DebConf19 até 27 de maio
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:39:30.796Z
tags: blog, geral
editor: markdown
dateCreated: 2018-05-21T14:04:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Em julho de 2019 Curitiba sediará a 20a edição da
[DebConf - Conferência Mundial de Desenvolvedores(as) do Projeto Debian](https://www.debconf.org/) (conhecida como DC19), e você pode desde já contribuir para o sucesso do evento! 
Como acontece tradicionalmente, a organização da DebConf abriu um concurso
bastante simples para a escolha da logo que identificará a DC19 em Curitiba.
**Prorrogamos o prazo e agora você pode enviar quantas propostas quiser até às 23h59m do dia 27/05/2018**. 
Sua proposta pode ser relacionada a Curitiba ou ao Brasil. Se quiser ver quais
pontos turísticos temos em Curitiba, dê uma olhada aqui:

<http://www.turismo.curitiba.pr.gov.br/categoria/atrativos-turisticos/3>

Lá você encontrará vários prédios/monumentos como:

-   Estufa do Jardim Botânico
-   Prédio histórico da UFPR
-   Ópera de Arame
-   Museu Oscar Niemeyer
-   Rua XV de Novembro

Existem outras coisas que representam Curitiba e/ou Paraná como

-   Pinhão <https://pt.wikipedia.org/wiki/Pinh%C3%A3o>
-   Araucária (a árvore que dá o pinhão)
-   Os 4 climas em um dia (calor, frio, chuva, sol)
-   A frase "leite quente dá dor de dente na gente" com sotaque curitibano
<https://www.youtube.com/watch?v=ZBvBzofL_1g>
-   Pássaro Gralha-azul
-   Ruas feitas de petit pavê com desenhos dos símbolos
<http://levecuritiba.com.br/colecoes/petit-pave>
-   Paulo Leminski
-   Poty Lazzarotto (e suas obras)
-   Capivara

Veja mais detalhes sobre o concurso:

<https://wiki.debian.org/DebConf/19/Artwork/LogoProposals>

Qualquer dúvida, mande email para: <logo@dc19.curitiba.br>

Lembramos que não haverá premiação em dinheiro para a logo vencedora, mas você
receberá uma camiseta e o nosso "muito obrigado" da comunidade Debian :-)