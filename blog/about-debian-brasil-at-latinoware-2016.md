---
title: About Debian Brasil at Latinoware 2016
description: by Giovani Ferreira
published: true
date: 2025-03-01T17:56:51.024Z
tags: blog, english
editor: markdown
dateCreated: 2016-11-16T02:35:00.000Z
---

<!-- author: Giovani Ferreira -->

The [Debian Brazilian Community](http://debianbrasil.org.br) held another [MiniDebConf](https://wiki.debian.org/Brasil/Eventos/MiniDebConfLatinoware2016) during the 13th edition of [Latinoware](http://latinoware.org/), from 19 to 21 October 2016 in Foz do Iguaçu.

During the 19th and 20th of October, [7 lectures were held, 1 session with 4 lightning talks and 1 panel](/blog/debian-brazil-community-will-be-present-at-latinoware-2016). These activities dealt with the most varied topics within the Debian Project, ranging from technical questions about tools used in the project to the presentation of how each community works, calling more people to contribute.

In recent years the members of the project have made a special effort to attract more people, and from now on especially a movement for the inclusion of women has started and this was the theme of one of the Lightning Talks.

The Debian Brasil community wants, as a way to increase diversity, to make a [special invitation to all women in Brazil](https://wiki.debian.org/MulheresBrasil) to participate in the most varied forms of collaboration.

Throughout the event we had a booth in the exhibition area where the public could heal their doubts about the project and better understand the organization of the community in Brazil. The booth also served as a meeting point for community members.

There were distribution of gifts and marketing of some custom items (shirts, mugs and stickers) to assist the organization of community projects.

We would like to thank the Latinoware organization for the space that was given to us during the event. We would also like to thank the members of the community who have had the opportunity to hold the talks and thank the public for their activities.

Photos: <https://www.flickr.com/photos/debianbrasil/albums/72157716022510362>
