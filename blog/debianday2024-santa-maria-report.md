---
title: Debian Day 2024 in Santa Maria - Brazil
description: by Andrew Gonçalves
published: true
date: 2025-03-01T17:39:21.458Z
tags: blog, english
editor: markdown
dateCreated: 2024-08-20T13:00:00.000Z
---

<!-- author: Andrew Gonçalves -->

by por Andrew Gonçalves

[Debian Day in Santa Maria - RS 2024](https://wiki.debian.org/DebianDay/2024/Brazil/SantaMaria) was held after a 5-year hiatus from the previous version of the event. It took
place on the morning of August 16, in the Blue Hall of the
[Franciscan University (UFN)](https://site.ufn.edu.br/) with support from the
Debian community and the Computing Practices Laboratory of UFN.

The event was attended by students from all semesters of the Computer Science,
Digital Games and Informational Systems, where we had the opportunity to talk to the participants.

Around 60 students attended a lecture introducing them to Free and Open Source
Software, Linux and were introduced to the Debian project, both about the
philosophy of the project and how it works in practice and the opportunities
that have opened up for participants by being part of Debian.

After the talk, a packaging demonstration was given by local DD Francisco
Vilmar, who demonstrated in practice how software packaging works in Debian.

I would like to thank all the people who helped us:

- Debian Project
- Professor Ana Paula Canal (UFN)
- Professor Sylvio André Garcia (UFN)
- Laboratory of Computing Practices
- Francisco Vilmar (local DD)

And thanks to all the participants who attended this event asking intriguing
questions and taking an interest in the world of Free Software.

Photos:

![DD em Santa Maria 1](https://debianbrasil.org.br/blog/imagens/debianday-santa-maria-2024-1.jpg =400x)
![DD em Santa Maria 2](https://debianbrasil.org.br/blog/imagens/debianday-santa-maria-2024-2.jpg =400x)
![DD em Santa Maria 3](https://debianbrasil.org.br/blog/imagens/debianday-santa-maria-2024-3.jpg =400x)
![DD em Santa Maria 4](https://debianbrasil.org.br/blog/imagens/debianday-santa-maria-2024-4.jpg =400x)
