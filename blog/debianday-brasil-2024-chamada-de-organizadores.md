---
title: Debian Day Brasil 2024 - chamada de organizadores(as)
description: por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:39:07.996Z
tags: blog, geral
editor: markdown
dateCreated: 2024-06-09T15:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

No dia 16 agosto é comemorado o aniversário do Projeto Debian, e todos os anos
comunidades ao redor do mundo organizam encontros para celebrar esta data.

Chamado de [Debian Day (Dia do Debian)](https://wiki.debian.org/pt_BR/DebianDay), o evento sempre conta com uma quantidade expressiva de comunidadades brasileiras organizando atividades nas suas cidades no dia 16 (ou no sábado mais próximo).

Em 2024 o Debian Day celebrará os 31 anos do Projeto Debian e o dia 16 de agosto será numa sexta-feira, por isso provavelmente a maioria das comunidades
organizarão suas atividades no sábado, dia 17.

Estamos fazendo uma chamada de organizadores(as) para o Debian Day em 2024. A
ideia é reunir, em um grupo no telegram, as pessoas interessadas em coordenar
as atividades das suas comunidades locais para trocar experiências, ajudar
os(as) novatos(as), e discutir a possibilidade do Projeto Debian ajudar
financeiramente as comunidades.

O Debian Day na sua cidade pode ser desde um encontro em uma
pizzaria/bar/restaurante para promover a reunião das pessoas, até um evento mais amplo com palestras/oficinas. Então não existe obrigatoriedade sobre como deve
ser o encontro, tudo depende do que você e a sua comunidade querem e podem
fazer.

Existe a possibilidade de solicitarmos ao líder do projeto Debian para reembolsar algumas despesas. Por exemplo, para produzir adesivos, pagar as pizzas,
encomendar um bolo, etc.

Venha fazer parte do grupo **Debian Day BR** no telegram e discutir as ideias:
<https://t.me/debian_day_br>

Se você topa esse desafio e vai organizar um Debina Day na sua cidade, não deixe de adicionar a sua cidade com as informações necessárias
[aqui](https://wiki.debian.org/DebianDay/2024).

Veja na [wiki do Debian](https://wiki.debian.org/Brasil/GUD) quais grupos estão
ativos e como entrar em contato com eles. Algumas cidades ou estados possuem
grupos de telegram próprios. Se a sua cidade não estiver lá, você pode liderar
a criação do seu grupo local :-)


![Debian Day 2024](https://debianbrasil.org.br/blog/imagens/debianday-2024.png)
