---
title: MiniDebConf Maceió 2025 - abertura de inscrição e chamada de atividades
description: 10/01/2025 por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:41:21.498Z
tags: blog, geral
editor: markdown
dateCreated: 2025-01-10T11:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

[![MiniDebConf Maceió 2025](https://maceio.mini.debconf.org/media/pages_files/logo-minidebconf-maceio-nova-400px.png)](https://maceio.mini.debconf.org/)

Está aberta a inscrição de participantes e a [chamada de atividades](https://maceio.mini.debconf.org/evento/chamada-de-atividades/) para a [MiniDebConf Maceió 2025](https://maceio.mini.debconf.org).

Esta será a primeira edição em uma cidade do Nordeste!

Veja abaixo algumas informações importantes:


## Data e local da MiniDebConf e do FLISOL

A MiniDebConf acontecerá de 1 a 4 de maio no Centro de Inovação do Jaraguá em Maceió - AL.

## Inscrição gratuita e oferta de bolsas

**Você já pode realizar a sua inscrição gratuita para a MiniDebConf Maceió 2025.**

A MiniDebConf é um evento aberto a todas as pessoas, independente do seu nível de conhecimento sobre Debian. O mais importante será reunir a comunidade para celebrar um dos maiores projeto de Software Livre no mundo, por isso queremos receber desde usuários(as) inexperientes que estão iniciando o seu contato com o Debian até Desenvolvedores(as) oficiais do projeto. Ou seja, estão todos(as) convidados(as)!

Assim como aconteceu na edição de [Belo Horizonte](https://bh.mini.debconf.org) em 2024, na edição de Maceió em 2025 estamos ofertando [bolsas de alimentação, hospedagem e passagens](https://maceio.mini.debconf.org/evento/bolsas/) para viabilizar a vinda de pessoas de outras cidades que contribuem para o Projeto Debian. Contribuidores(as) não oficiais, DMs e DDs podem solicitar as bolsas usando o formulário de inscrição.

Os recursos financeiros são bastante limitados, mas tentaremos atender o máximo de pedidos.

Se você pretende pedir alguma dessas bolsas, acesso [esse link](https://maceio.mini.debconf.org/evento/bolsas) e veja mais informações antes de realizar a sua inscrição:

A [inscrição](https://maceio.mini.debconf.org/evento/inscricao/) (sem bolsas) poderá ser realizada até a data do evento, mas temos uma data limite para o pedido de bolsas de alimentação, hospedagem e passagens, por isso fique atento(a) ao **prazo final: até 31 de janeiro.**

Para se inscrever, acesso o site, vá em "Cria conta". Criei a sua conta (preferencialmente usando o Salsa) e acesse o seu perfil. Lá você verá o botão de "Se inscrever".

Para se inscrever, acesse o site, vá em [Criar conta](https://maceio.mini.debconf.org/accounts/login/). Criei a sua conta (preferencialmente usando o [Salsa](https://salsa.debian.org/)) e acesse o
seu perfil. Lá você verá o botão de [Se inscrever](https://maceio.mini.debconf.org/register/).

## Chamada de atividades

**Também está aberta a chamada de atividades para a MiniDebConf.**

Para mais informações, acesse [esse link](https://maceio.mini.debconf.org/evento/chamada-de-atividades/).

Fique atento ao prazo final para enviar sua proposta de atividade: **até 31 de janeiro**.

## Contato

Qualquer dúvida, mande um email para <contato@debianbrasil.org.br>

## Organização

[![Debian Brasil](https://maceio.mini.debconf.org/media/pages_files/logo-debianbrasil-capa.svg)](https://debianbrasil.org.br) [![Debian ](https://maceio.mini.debconf.org/media/pages_files/logo-debian-capa.svg)](https://debian.org) [![OxE](https://maceio.mini.debconf.org/media/pages_files/logo-0xE-capa.svg)](http://oxehacker.club/)
