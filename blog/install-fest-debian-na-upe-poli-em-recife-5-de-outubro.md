---
title: Install Fest Debian na UPE-POLI em Recife - 5 de outubro
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:11.354Z
tags: blog, geral
editor: markdown
dateCreated: 2019-09-26T15:04:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A Escola Politécnica da Universidade de Pernambuco (POLI), unidade mais antiga
da [Universidade de Pernambuco](http://www.upe.br/) (UPE) busca através do
[INGENIA](http://csec.poli.br/eventos/ingenia-2019/) (evento integrante da
Semana Universitária da Universidade de Pernambuco) consolidar suas relações
com a sociedade e o setor produtivo, contando com o apoio de entidades e
empresas de vanguarda que tenham interesse em difundir suas tecnologias,
produtos e serviços.

No último dia do evento (**5 de outubro - sábado**) o pessoal da UPE/POLI
realizará em Recife um **Install Fest [Debian](https://www.debian.org) Linux**.

- Local: LIP3.
- Horário: das 08h00 as 17h00
- Atenção: se você deseja participar, lembre-se que são vagas limitadas.

A agenda completa do evento pode ser encontrada aqui:

<http://csec.poli.br/eventos/ingenia-2019>

Organização: Prof. Dr. Ruben Carlo Benante -
[@drbeco](https://twitter.com/drbeco/status/1175220550844788736)

## In english:

We (UPE/POLI, Recife, BR) are holding a Install Fest Debian Linux:

- Date: Oct-05-2019, saturday.
- Location: UPE/POLI, LIP03.
- Hours: from 08h00 to 17h00
- If you want to attend, be aware that there are limited spots available.


The event complete schedule can be found here:

<http://csec.poli.br/eventos/ingenia-2019>

Thank you and welcome to our Install Fest!

Prof. Dr. Ruben Carlo Benante - organizer.

Keep in touch and share: twitter
[@drbeco](https://twitter.com/drbeco/status/1175220550844788736)


 


