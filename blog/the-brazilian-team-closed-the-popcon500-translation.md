---
title: The Brazilian team closed the POPCON500 translation!
description: by Thiago Pezzo
published: true
date: 2025-03-01T17:42:30.729Z
tags: blog, geral
editor: markdown
dateCreated: 2024-10-15T09:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

For more than 2 years the [Debian pt_BR localization team](https://wiki.debian.org/Brasil/Traduzir/) has been using the [DDTSS platform](https://ddtp.debian.org/ddtss/index.cgi/pt_BR) as a gateway for beginners. This ongoning initiative resulted in a significant increase in the translation of package descriptions.

As of today, October 9th, 2024, we finished the [POPCON500 list!](https://popcon.debian.org/by_vote)

[Statistics](https://ddtp.debian.org/stats/stats-sid.html) places Brazilian Portuguese as the fifth most translated language on this front.

Nice work, team! And keep going!
