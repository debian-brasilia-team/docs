---
title: Lançado o Debian GNU/Linux 6.0 "Squeeze"
description: por Debian Brasil
published: true
date: 2025-03-01T17:40:16.364Z
tags: blog, geral
editor: markdown
dateCreated: 2011-02-07T23:21:00.000Z
---

<!-- author: Debian Brasil -->

Após 24 meses de constante desenvolvimento, o Projeto Debian tem o prazer de
apresentar sua nova versão estável 6.0 (codinome "Squeeze"). O Debian 6.0 é um
sistema operacional livre, vindo pela primeira vez em dois sabores. Ao lado do
Debian GNU/Linux, o Debian GNU/kFreeBSD é introduzido com esta versão como uma
"prévia da tecnologia".
O Debian 6.0 inclui o KDE Plasma Desktop e Aplicativos, os ambientes de área
de trabalho GNOME, Xfce, e LXDE assim como todos os tipos de aplicativos para
servidor. Também apresenta compatibilidade com a FHS v2.3 e software
desenvolvido para a versão 3.2 da LSB.


O Debian funciona em computadores desde palmtops e sistemas  handheld a
supercomputadores, e em quase tudo entre eles.  Um total de nove arquiteturas
são suportadas pelo Debian GNU/Linux: 32-bit PC / Intel IA-32 (i386), 64-bit
PC / Intel EM64T / x86-64 (amd64), Motorola/IBM PowerPC (powerpc), Sun/Oracle
SPARC (sparc), MIPS (mips (big-endian) e mipsel (little-endian)), Intel Itanium
(ia64), IBM S/390 (s390), e ARM EABI (armel).

O Debian 6.0 "Squeeze" introduz prévias técnicas de dois novos portes para o
kernel do projeto FreeBSD usando o conhecido espaço de  aplicativos Debian/GNU:
 Debian GNU/kFreeBSD para o 32-bit PC (kfreebsd-i386) e o 64-bit PC
(kfreebsd-amd64). Estes portes são os primeiros a serem incluídos em uma versão
do Debian que não são baseados no kernel Linux. O suporte ao software comum de
servidor está robusto e combina os recursos existentes nas versões Debian
baseadas em Linux com os recursos únicos conhecidos do mundo BSD. Entretanto,
para esta versão estes novos portes são limitados; por exemplo, alguns recursos
avançados do desktop ainda não são suportados.

Outra novidade é o kernel Linux completamente livre, que não contém mais
arquivos de firmware problemáticos. Estes foram divididos em pacotes separados
e movidos do repositório main do Debian para a área non-free do nosso repositório, que não é habilitada por padrão. Desta forma, os usuários Debian têm a possibilidade de executar um sistema operacional completamente livre, mas ainda podem optar por usar arquivos de firmware não-livres se necessário. Os arquivos de firmware
necessários durante a instalação podem ser carregados pelo sistema de instalação; imagens especiais de CD e arquivos tarball para instalações baseadas em USB também estão disponíveis.  Mais informações a respeito disso podem ser encontradas na
[página wiki de Firmware do Debian](http://wiki.debian.org/Firmware).

Além disso, o Debian 6.0 introduz um sistema de inicialização baseado em
dependências, tornando a inicialização do sistema mais rápida e robusta devido
a execução paralela dos scripts de inicialização e o controle correto de
dependências entre eles. Várias outras mudanças tornam o Debian mais adequado
para notebooks de tamanho reduzido, como a introdução do shell KDE Plasma Netbook. 
Esta versão inclui numerosos pacotes de software atualizados, tais como:

-   Espaços de trabalho KDE Plasma e Aplicativos do KDE 4.4.5
-   uma versão atualizada do ambiente de área de trabalho GNOME 2.30
-   o ambiente de área de trabalho Xfce 4.6
-   LXDE 0.5.0
-   X.Org 7.5
-   OpenOffice.org 3.2.1
-   GIMP 2.6.11
-   Iceweasel 3.5.16 (uma versão sem marcas do Mozilla Firefox)
-   Icedove 3.0.11 (uma versão sem marcas do Mozilla Thunderbird)
-   PostgreSQL 8.4.6
-   MySQL 5.1.49
-   Coleção de Compiladores GNU 4.4.5
-   Linux 2.6.32
-   Apache 2.2.16
-   Samba 3.5.6
-   Python 2.6.6, 2.5.5 e 3.1.3
-   Perl 5.10.1
-   PHP 5.3.3
-   Asterisk 1.6.2.9
-   Nagios 3.2.3
-   Xen Hypervisor 4.0.1 (dom0 assim como suporte a domU)
-   OpenJDK 6b18
-   Tomcat 6.0.18
-   mais de 29.000 outros pacotes de software prontos para usar, construídos a
partir de cerca de 15.000 pacotes fonte.


O Debian 6.0 inclui mais de 10.000 novos pacotes como o navegador Chromium, a
solução de monitoramento Icinga, a interface gráfica de gerenciamento de de
pacotes Central de Aplicativos, o gerenciador de rede wicd, as ferramentas de
contêiner Linux lxc e a framework de cluster corosync.

Com esta ampla seleção de pacotes, o Debian mais uma vez permanece fiel ao seu
objetivo de ser o sistema operacional universal. É apropriado para muitos
casos diferentes de uso: de sistemas desktop a netbooks; de servidores de
desenvolvimento a sistemas de cluster; e para servidores de banco de dados,
web ou de armazenamento. Ao mesmo tempo, esforços adicionais para garantia de
qualidade como instalação automática e testes de atualização para todos os pacotes nos repositórios Debian, assegurando que o Debian 6.0 satisfaz as altas
expectativas que os usuários têm de uma versão estável do Debian. É sólido com
rocha e rigorosamente testado.

A partir do Debian 6.0, as "Distribuições Debian Customizadas" são renomeadas
para ["Debian Pure Blends"](http://blends.alioth.debian.org/). Sua cobertura
tem aumentando conforme o Debian 6.0 adiciona
[Debian Accessibility](http://www.debian.org/devel/debian-accessibility/),
[DebiChem](http://debichem.alioth.debian.org/),
[Debian EzGo](http://wiki.debian.org/DebianEzGo),
[Debian GIS](http://wiki.debian.org/DebianGis) e
[Debian Multimedia](http://blends.alioth.debian.org/multimedia/tasks/index)
às já existentes, [Debian Edu](http://wiki.debian.org/DebianEdu),
[Debian Med](http://www.debian.org/devel/debian-med/) e
[Debian Science](http://wiki.debian.org/DebianScience)
"pure blends". O conteúdo completo de todas as blends
[pode ser acessado](http://blends.alioth.debian.org/),
incluindo possíveis pacotes que os usuários são convidados a indicar para
adição na próxima versão.

O Debian pode ser instalado a partir de várias mídias de instalação tais como
discos Blu-ray, DVDs, CDs e dispositivos USB ou através da rede. O GNOME é o
ambiente de área de trabalho padrão e está contido no primeiro CD. Os outros
ambiente de áreas de trabalho KDE Plasma Desktop e Aplicativos, Xfce, ou LXDE
podem ser instalados através de duas imagens de CD alternativas. O ambiente de
área de trabalho desejado também pode ser escolhido a partir dos menus de
inicialização dos CDs/DVDs.  Novamente estão disponíveis com o Debian 6.0 os
CDs e DVDs multi-arquitetura que suportam instalação de múltiplas arquiteturas
a partir de um único disco. A criação da mídia de instalação USB inicializável
também foi extremamente simplificada; veja o
[Guia de Instalação](http://www.debian.org/releases/squeeze/installmanual)
para mais detalhes.

Além da mídia de instalação normal, o Debian GNU/Linux também pode ser
diretamente utilizado sem instalação prévia.  As imagens especiais usadas,
conhecidas como imagens live, estão disponíveis para CDs, dispositivos USB e
montagens via netboot. Inicialmente, estas são fornecidas apenas para as
arquiteturas amd64 e i386. Também é possível usar estas imagens live para
instalar o Debian GNU/Linux.

O processo de instalação para o Debian GNU/Linux 6.0 foi melhorado de várias
maneiras, incluindo seleção mais fácil do idioma e configurações do teclado, e
particionamento de volumes lógicos (LVM), RAID e sistemas criptografados.
Também foi adicionado suporte para os sistemas de arquivos ext4 e btrfs e na
arquitetura kFreeBSD o sistema de arquivos Zettabyte (ZFS). O sistema de
instalação para o Debian GNU/Linux agora está disponível em 70 idiomas.

As imagens de instalação podem ser baixadas agora mesmo via
[bittorrent](http://www.debian.org/CD/torrent-cd/)
(o método recomendado), [jigdo](http://www.debian.org/CD/jigdo-cd/#which) ou
[HTTP](http://www.debian.org/CD/http-ftp/)
veja [Debian em CDs](http://www.debian.org/CD/) para maiores informações.
Em breve também estará disponível em mídias de DVD, CD-ROM e discos Blu-ray a
partir de vários [vendedores](http://www.debian.org/CD/vendors).

As atualizações para o Debian GNU/Linux 6.0 a partir da versão anterior,
Debian GNU/Linux 5.0 (codinome "Lenny"), serão automaticamente manipuladas
pela ferramenta de gerenciamento de pacotes apt-get para a maioria das
configurações, e de certa forma, também pela ferramenta de gerenciamento de
pacotes aptitude. Como sempre, os sistemas Debian GNU/Linux devem ser
atualizados de forma indolor, no local, sem qualquer indisponibilidade forçada,
mas é fortemente recomendado que você leia as
[notas de lançamento](http://www.debian.org/releases/squeeze/releasenotes)
assim como o
[guia de instalação](http://www.debian.org/releases/squeeze/installmanual)
para possíveis problemas, e instruções detalhadas de instalação e atualização.
As notas de lançamento ainda serão melhoradas e traduzidas para idiomas
adicionais nas semanas após o lançamento.

**A respeito do Debian**

O Debian é um sistema operacional livre, desenvolvido por milhares de
voluntários de todo o mundo que colaboram através da Internet. Os pontos chave
do projeto Debian são a sua base de voluntários, a sua dedicação ao Contrato
Social do Debian e ao Software Livre, e o seu compromisso de fornecer o melhor
sistema operacional possível. O Debian 6.0 é mais um passo importante nesta
direção.

**Informações de Contato**

Para mais informações, por favor, visite as páginas web do Debian em
<http://www.debian.org/> ou envie um e-mail para <press@debian.org>.