---
title: Comunidade Debian elege novo líder do projeto
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:58:10.718Z
tags: blog, geral
editor: markdown
dateCreated: 2013-03-14T10:16:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

![](https://debianbrasil.org.br/blog/imagens/debian-small.png)

Os [candidatos foram anunciados](http://lists.debian.org/debian-devel-announce/2013/03/msg00003.html) e o processo de eleição para o novo Debian Project Leader (DPL) começou
oficialmente. Gergely Nagy, Moray Allan e Lucas Nussbaum estão em campanha para
a primeira posição no Projeto Debian. O atual líder do Projeto Debian, Stefano
Zacchiroli, não está participando para a reeleição este ano.

O Projeto Debian vota para um líder de projeto a cada ano, com os candidatos
anunciado no início de [março](http://www.debian.org/vote/2013/vote_001). Os
candidatos deste ano terão campanhas até o final de março e, em seguida, os
desenvolvedores Debian começarão a votação, que ocorre de 31 março a 13 abril.
O novo líder do Projeto Debian vai assumir os trabalhos de Zacchiroli a partir
de 17 de Abril.

Fonte:
[The H Online](http://www.h-online.com/open/news/item/Debian-community-to-elect-new-project-leader-1820624.html) 
Fonte da tradução:
[Revista Espírito Livre](http://www.revista.espiritolivre.org/comunidade-debian-elege-novo-lider-do-projeto) 