---
title: Tradução do Debian Administrator’s Handbook para pt-BR
description: por Debian Brasil
published: true
date: 2025-03-01T17:42:37.786Z
tags: blog, geral
editor: markdown
dateCreated: 2015-04-07T23:19:00.000Z
---

<!-- author: Debian Brasil -->

[![](http://debian-handbook.info/files/2012/04/front-cover-232x300.png)](http://debian-handbook.info) 

Olá a todos, é com grande satisfação que venho lhes informar que iniciamos as
traduções do [Debian Administrator’s Handbook](http://debian-handbook.info).


Como já havia postado
[anteriormente](http://earruda.eti.br/blog/2012/05/the-debian-administrators-handbook-is-out), os fontes do livro foram disponibilizados, e isso permitiu o ínicio de
contribuições da comunidade para o livro.

Se você deseja ajudar na tradução, você deve:

1 – Cadastrar-se nas listas de discussões:

- *debian-l10n-protuguese* (idioma: português): <http://lists.debian.org/debian-l10n-portuguese/> - *debian-handbook-translators* (idioma: inglês): <http://lists.alioth.debian.org/mailman/listinfo/debian-handbook-translators> 
2 – Manifestar-se na lista *debian-l10n-portuguese*: informando que deseja
ajudar na tradução do livro, para ter seu nome adicionado à lista de tradutores. 
3 – Realizar as traduções pelo Weblate: as traduções para o idioma português
brasileiro estão sendo realizadas através de uma interface web chamada [Weblate](http://weblate.org) 
O link para a interface de tradução do livro é: <http://debian.weblate.org/>

**Outras informações:**

Leia o arquivo
[README](http://http://anonscm.debian.org/gitweb/?p=debian-handbook/debian-handbook.git;a=blob;f=pt-BR/README;h=0320b60cae6158e43afdef62a9c545b9cf0a6925;hb=squeeze/master); 
Tente sanar sua dúvida primeiro pela lista de discussões *debian-l10n-portuguese.* 