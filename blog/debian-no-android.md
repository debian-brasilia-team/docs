---
title: Debian no Android
description: por Paulo Francisco Slomp
published: true
date: 2025-03-01T17:38:33.919Z
tags: blog, geral
editor: markdown
dateCreated: 2016-03-11T01:02:00.000Z
---

<!-- author: Paulo Francisco Slomp -->

Nas parte das "Observações" da tabela há o registro da possibilidade de
instalar distribuições GNU-Linux (Debian, Fedora, Gentoo, etc.) em aparelhos
Android.

Aprendizagem com tablets e celulares

Pesquisa mapeia mais de 300 aplicativos educacionais livres.

Matéria jornalística: UFRGS mapeia aplicativos para aprendizagem em dispositivos móveis

Público pode acessar relação com mais de 300 ferramentas livres para tablets e
celulares, úteis principalmente para professores, alunos e comunidade escolar

07/03/2016 10:42

Fonte: <http://miud.in/1Hiu>


