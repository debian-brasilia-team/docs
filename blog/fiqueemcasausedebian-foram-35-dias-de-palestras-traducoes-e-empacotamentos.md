---
title: #FiqueEmCasaUseDebian: foram 35 dias de palestras, traduções e empacotamentos
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:20:23.385Z
tags: blog, geral
editor: markdown
dateCreated: 2020-06-08T17:58:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A **Comunidade Debian Brasil** promoveu de 3 de maio a 6 de de junho de 2020 o
evento online chamado
[#FiqueEmCasaUseDebian](https://debianbrasil.gitlab.io/FiqueEmCasaUseDebian).
Durante 27 noites, os DDs Daniel Lenharo e Paulo Santana receberam pessoas
convidadas para compartilherem seus conhecimentos, e o que estava inicialmente
previsto para acabar no dia 30 de maio, acabou se extendendo por mais uma semana atendendo a pedidos da comunidade. O formato do evento foi fortemente inspirado
no [#FiqueEmCasaConf](https://github.com/linuxtips/FiqueEmCasaConf) realizado
durante o mês de abril pelo Jeferson Fernando do site
[LINUXtips](https://www.linuxtips.io).

## Estatísticas

Foram 27 atividades transmitidas ao vivo, sendo uma por dia, e que estão
disponíveis no canal Debian Brasil no
[YouTube](https://www.youtube.com/playlist?list=PLU90bw3OpxLo18CzFm3bEm9mNHdf8e2Ra) e o
[Peertube.](https://peertube.debian.social/my-account/video-playlists/b6dccae1-aad6-4e21-a2c5-ce108b4c5fd7) As 21 pessoas que participaram das lives transmitindo seus conhecimentos
produziram aproximadamente 35 horas de conteúdo sobre temas relacionados ao
Debian. Até o dia do encerramento, o canal no YouTube havia registrado:

- 723 novas pessoas inscritas, totalizando 1.210 assinantes.
- 12,4 mil visualizações.
- 3,4 mil espectadores únicos.
- Pessoas de 12 países diferentes.

Acreditamos que boa parte da audiência de outros países provavelmente acessou a
palestra em inglês realizada pelo atual DPL Jonathan Carter com o título
*[A mixed bag of Debian](https://www.youtube.com/watch?v=Zu6ig8lIw3k)*.

## Sprint de tradução

A equipe de localização para português do Brasil
([debian-l10n-portuguese](https://wiki.debian.org/Brasil/Traduzir)) pegou carona na mobilização do **\#FiqueEmCasaUseDebian** para promover a chamada de novos
as) contribuidores(as) para ajudarem a traduzir o site debian.org. Foi agendado
então a realização de um
[sprint de tradução](https://wiki.debian.org/Sprints/2020/l10n-portuguese)
nos dia 28 e 29 de maio, dentro da
[MiniDebCamp mundial online](https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline). 
A [lista de discussão debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese) que há muito tempo recebia poucas mensagens por mês, teve 778 novas mensagens
de 1 de maio até hoje (08/06/2020) com assuntos diversos como pedidos de
orientação sobre como traduzir, dúvidas sobre o que fazer, sugestões,
explicações, pseudo-urls que fazem parte do processo de tradução, etc.

Isso resultou em **4 novas páginas traduzidas e 58 páginas com traduções atualizadas**. A quantidade muito maior de atualizações se deve ao fato da equipe ter decidido
focar os esforços em zerar o número de páginas com esse status.

Venha fazer parte dessa equipe você também e ajudar o Projeto Debian!

## Maratona de empacotamento

No mesmo período do **\#FiqueEmCasaUseDebian**, um grupo de cinco DDs
brasileiros organizou uma maratona online de empacotamento para ensinar aquelas
pessoas que queriam aprender como empacotar softwares para o Debian. e assim se
tornarem novas contribuidoras nessa área.

Durante cinco sábados foram realizados encontros ao vivo online pelo jitsi para
os DDs ensinarem os procedimentos de empacotamento. Posteriormente durante os
dias da semana eram enviadas tarefas para essas pessoas realizarem, e toda noite havia pelo meno um desses DD de plantão para tirar dúvidas pelo jitsi.

Os números da maratona mostram que o saldo é muito positivo e que o trabalho
pode ser considerado um sucesso. Foram no total:

- 36 participantes](https://udd.debian.org/cgi-bin/new-maintainers.cgi) com pelo menos 1 upload cada.
- 70 pacotes trabalhados.
- 41 pacotes com testes de CI implementados (o time de CI agradece).
- 18 bugs sanados.
- 143 uploads.
- 03 pacotes foram descobertos com licenças inconpatíveis com a DFSG e aberto
bugs para removê-los do Debian.

Torcemos para que desse grupo de pessoas saiam novos DMs, e posteriormente novos DDs, que ajudem a manter o Debian como sendo o sistema operacional universal.

## Palavras finais

O objetivo do **\#FiqueEmCasaUseDebian** foi levar conteúdo para as pessoas que
estão enfrentando esse momento difícil de isolamento social por causa da
pandemia do COVID-19. Se temos que ficar em casa o máximo de tempo possível,
podemos tentar pelo menos tentar aprender algo novo, conversar online com
velhos(as) amigos(as) ou conhecer pessoas novas, mesmo a distância. Esperamos
que esses dias tenham nos ajudado a ficarmos mais próximos uns dos outros e em
torno do Debian.

![](https://debianbrasil.org.br/blog/imagens/assista.png =400x)