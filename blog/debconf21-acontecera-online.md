---
title: DebConf21 acontecerá online
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:58:26.462Z
tags: blog, geral
editor: markdown
dateCreated: 2021-04-05T21:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Veja abaixo a tradução da notícia
[DebConf21 to be held online](https://lists.debian.org/debian-devel-announce/2021/04/msg00000.html) 
Devido a pandemia de COVID-19, a [DebConf](https://www.debconf.org/) deste ano
será mais uma vez realizada online. As datas exatas ainda não foram definidas,
e serão  anunciadas posteriormente. No entanto, você pode esperar que a
[DebConf21](https://debconf21.debconf.org/) acontecerá durante o terceiro
trimestre de 2021, ou seja, aquele período do ano tradicionalmente conhecido
como "tempo de DebConf".

Precisamos de uma equipe para fazer a DebConf21 acontecer. No ano
passado construímos uma infraestrutura básica para DebConfs online, mas
certamente podemos fazer mais este ano. Se você quer tornar a DebConf
especial, ou apenas quer ajudar a fazê-la acontecer, por favor, seja
voluntário(a) e proponha suas idéias para a lista de discussão
<debconf-team@lists.debian.org>.

Entre outras coisas, precisamos de voluntários(as) para:

- Design: logotipo, site web, camisetas.
- Equipe de conteúdo: seleção e programação das atividades, coordenação
  dos(as) palestrantes.
- Equipe de patrocínio: contato com potenciais patrocinadores(as).
- Equipe de vídeo: executar as sessões de videoconferência e mixar as
  transmissões ao vivo.
- Geral: fazer o evento acontecer, coordenando com as outras equipes da
  DebConf.

Fique ligado(a) para mais notícias sobre a DebConf21 a medida que elas
forem sendo anunciadas, e para informações sobre a DebConf22+ asssim
que as decisões forem tomadas.

Antonio, em nome da equipe DebConf

Obs: para participar da organização da DebConf é necessário saber se comunicar
(ler e escrever) em inglês. Esse conhecimento mínimo é importante porque as
mensagens entre os(as) organizadores(as) são trocadas em inglês.
Mas não se preocupe, você não precisa ser proficiente em inglês :-)

## Contatos

Com a colaboração do Francisco Vilmar, Felipe Maia e Leandro Cunha, que fizeram
esse levantamento, listamos abaixo os meios de comunicação da organização da DebConf. 
**Listas de discussão:**

Organização:

- <https://lists.debian.org/debconf-team>
- <debconf-team@lists.debian.org>

Geral:

- <https://lists.debian.org/debconf-discuss>
- <debconf-discuss@lists.debian.org>


**IRC - rede OFTC**

Organização:

- Canal #debconf-team

Geral:

- Canal #debconf


**Rede Matrix:**

- Canal #debconf-team no matrix.debian.social

Observações:

- As salas no IRC e no Matrix estão interconectadas por ponte (bridged), o que significa que as conversas são replicadas automaticamente de uma para a outra (nas duas direções). - A sala do Matrix fica na instância oficial do Debian (matrix.debian.social) e NÃO em matrix.org.