---
title: Proposta da Área de Comunidades do FISL 17
description: por Giovani Ferreira
published: true
date: 2025-03-01T17:41:58.339Z
tags: blog, geral
editor: markdown
dateCreated: 2016-05-21T21:08:00.000Z
---

<!-- author: Giovani Ferreira -->

**Nome da comunidade/projeto/entidade/grupo;**

Comunidade Debian Brasil

**Estimativa do número pessoas movimentando as atividades no espaço do grupo na
Área das Comunidades;**

150 pessoas

**Conteúdos/informações que demonstram contribuições do grupo em questão
(continuas para o desenvolvimento do projeto, de tecnologias livres e de código
aberto, para o qual o grupo foi criado);**

A Comunidade Debian Brasil é um projeto formado por usuários, desenvolvedores,
tradutores e documentadores voluntários, com o objetivo central de tornar o
Projeto Debian mais próximo dos usuários de nosso país, o sistema Debian
localizado para o português brasileiro.

**Proposta de atividades/ações no espaço da Área das Comunidade:**

 * Apresentação dos projetos e times do Debian Brasil
 * Motivar a participação na Comunidade Debian Brasil
 * Incentivar a promoção da Comunidade Debian Brasil em eventos regionais
 * Oficinas: empacotamento / tradução
 * Install Fest
 * Festa de assinatura de chaves
 * Sorteio de brindes

**Como contribuirão com o install fest, ou seja, que projetos de software livre
vinculados à sua comunidade eles podem ajudar a instalar;**

 * Distribuição Debian GNU/Linux Stable e Testing
 * Contribuição para atividades que ajudem a movimentar o FISL, ou seja, integração com os visitantes; 
** Apoio no install Fest promovido por outras comunidades como FLISoL,
suporte à usuários, Light Talks, sorteio e distribuição de materiais Lightning
talks (18 minutos) que o grupo poderá promover na Área das Comunidades;**

 * Conhecendo o Sistema de Bugs do Debian (BTS);
 * Forensics-all - transforme o seu debian em um sistema de perícia forense;

**Deixar claro como convidarão as pessoas.**

 * Site oficial;
 * Página da comunidade no softwarelivre.org;
 * Listas de discussão;
 * Canais IRC.

**Links para as páginas e listas principais do projeto em que a comunidade/grupo faz parte, bem com a(s) lista(s) do próprio grupo;** 
 * http://debianbrasil.org.br
 * http://wiki.debianbrasil.org/
 * debian-br-geral@lists.alioth.debian.org
 * debian-user-portuguese@lists.debian.org
 * http://twitter.com/debianbrasil
 * http://www.facebook.com/debianbrasil

**Informar se enviou uma proposta de Encontro Comunitário no FISL (via a chamada de trabalhos);** 
Encontro da comunidade já aprovado para o evento

**Indicar de 2 a 4 coordenadores do grupo na Área das Comunidades (nome e e-mail de cada um).** 
 * Lucas Castro lucascastroborges@gmail.com
 * Giovani Ferreira giovani@riseup.net

**Informar outras atividades:**

 * hackathon,
 * workshop,
 * install-party,
 * festa de lançamento,
 * aniversário da comunidade,
 * mesa redonda,
 * URC,
 * Demonstrações das variadas formas de colaboração dentro do Projeto Debian.