---
title: MiniDebConf Curitiba 2017 - a brief report
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:41:09.851Z
tags: blog, english
editor: markdown
dateCreated: 2017-03-30T01:46:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

![Foto banner minidebconf2017](https://debianbrasil.org.br/blog/imagens/minidebconf-2017-pulpito.jpg =400x) 
### With this second edition of MiniDebConf Curitiba, the event is consolidated as the main meeting of the Brazilian community of users and developers Debian. 
The [MiniDebConf Curitiba](http://br2017.mini.debconf.org/index-en.shtml)
second edition was held from March 17th to 19th, 2017 at Campus Curitiba of the
[Federal University of Technology - Paraná - UTFPR](http://www.utfpr.edu.br/estrutura-universitaria/pro-reitorias/prorec/diretoria-de-relacoes-interinstitucionais/international-affairs-eng/about-us). 
On March 17th (Friday) the event happened from 2 p.m to 7 p.m, we started with
the opening of the event, after that, there were three lectures. On March 18th
and 19th (Saturday and Sunday) the activities happened from 10:00 a.m. to 7 p.m. There were some simultaneously activities like lectures, workshops in four
different rooms.

### Data about 2017 edition

During three days, many activities happened for every level of knowledge about
the Debian Project. The schedule had:

-   16 lectures (basic level, intermediate level and advanced level)
-   06 workshops.
-   06  lightning talks
-   Install Fest for install Debian in the laptop.
-   BSP (Bug Squashing Party) They resolved some critical bugs and it  was a
very important contribution from the Brazilian community to the Stretch release
that will happen soon.

The final numbers of the MiniDebConf Curitiba are the follow:

-   342 inscribed people
-   143 present people

From the 143 present people, 12 were brazilians official contributors, they are
7 DDs (Debian Developers) and 5 DMs (Debian Maintainers). The event was
organized by 9 people that started the work in December 2016 and there were 22
volunteers that were helping during the 3 days.
We believe that the difference between the number of registered people and the
present people is because there was not charge fee registration. So, if people
give up from participating on the event they won’t lose money.


The second edition of MiniDebConf Curitiba shows the result of the effort to
encourage  people to contribute for the Debian Brazilian community. This result
can be testify comparing  the data of this year with the
[MiniDebConf 2016](http://br2016.mini.debconf.org/index-en.shtml): The number of registered people increased 68%;  the number of lectures increased 50%,the
varied topics also increased  in the lectures and the technical level of the
activities was higher. And, since the MiniDebConf 2016,
[4 people became DebianDeveloper and 3 people became Debian Maintainer](/blog/brasileiros-mantenedores-e-desenvolvedores-debian-a-partir-de-julho-de-2015/). You can read the report from
[MiniDebConf Curitiba 2016](/blog/mini-debconf-curitiba-in-2016-was-a-success/). 
![Minidebconf2017 banners](https://debianbrasil.org.br/blog/imagens/minidebconf2017-banners.jpg =400x)

### Women’s participation


We had 32 women present, but unfortunately there was not female speaker. The
committee tried to invite a female speaker, but they could not find a woman to
talk about something related to Debian project in our city.The thinking of the
possibility to invite a Debian Developer woman from Europe came out, but there
was not enough financial resources to do that.


We admit this fault and support the
[campaign started last year](https://wiki.debian.org/MulheresBrasil) to
encourage women to contribute to the Debian Project and talking about that.
There are two women Debian Developers in Brazil, but nowadays they are living
abroad. For the next MiniDebConf edition we will work hard to increase the
number of women Debian Developers.

![Minidebconf2017 mulheres](https://debianbrasil.org.br/blog/imagens/minidebconf2017-mulheres.jpg =400x)

### Crowdfunding

This edition has an important innovation: we made a
[crowdfunding](http://br2017.mini.debconf.org/doacao.shtml) and got funds to
pay some things (breakfast, venue, press, badge, lanyards, the staff from UTFPR
that worked to support the activities at workshops and the auditorium). The
Crowdfunding was an unpublished experience, we did note hear about this
experience from other team before that and we are very pleased with the result.
The crowdfunding goal was R\$ 2.000,00 and we got R\$ 2.800,00, we received
donation from 52 people from Brazil and from other countries.

Each participant received: a badge, a customized lanyard, a flyer with tips
"how to contribute with Debian Project".

![Minidebconf2017 doadores](https://debianbrasil.org.br/blog/imagens/minidebconf2017-doadores.jpg =400x)

### Community Store

We had the traditional Debian community store, how it happened in other events
organized by us, we sold some products like T-shirts, button pins, keychain,
stickers and mug. The money is going to be used for the next events about Debian in Brazil and also for other events organized by
[Curitiba Livre Community](http://curitibalivre.org.br/).

![Minidebconf2017 lojinha](https://debianbrasil.org.br/blog/imagens/minidebconf2017-lojinha.jpg =400x)

### Bug Squashing Party

During the event, there was a Bug Squashing Party (BSP), we focused on resolve
bugs that were interfering with the next Debian stable release (Stretch).
Although happening at the same time as the other activities, participants were
able to solve
[3 critical bugs](https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org;tag=bsp-2017-03-br-curitiba). Besides the 3 closed bugs, there was also the discover of a critical one
affecting jessie->stretch upgrades, this bug was reported by a participant and
he had support from a DM, whom wasn't participating directly on the BSP, that
helped him report the bug and the maintainer to fix the problem
[#858200](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=858200)

We believe that it was a good result, because there were three people working
together. Considering the short time of this activity, they did a great
contribution.

![Minidebconf2017 bsp](https://debianbrasil.org.br/blog/imagens/minidebconf2017-bsp.jpg =400x)

### Photos and videos


We made our first trial to the lecture online transmission and we also recorded
the lecture to share before the event on the Debian repository. You can watch
the lecture on the following link (they are in pt-br):

[youtube](https://www.youtube.com/playlist?list=PLU90bw3OpxLoKKXpGVpGzEiITDDnHzgAu) or
[peertube](https://peertube.debian.social/videos/watch/playlist/cf8f5f6e-f4b0-4ab9-9645-942b5cb2fde1) 
Some volunteers took pictures and they helped us to keep track of the
MiniDebConf. You can see the photos on the following link:

<http://deb.li/2017br>

### Use of free social networks

Since the beginning of the organization of MiniDebConf Curitiba 2017 we have
used the free social networks to publicize the event. In addition to this prior
disclosure, we had as goal to make coverage during the event by posting news and photos on free social networks to help generate content and bring more people
to use them.

With the help of volunteers, we were able to partially achieve this goal
because unfortunately the diaspora.softwarelivre.org network went offline three
days before the start of MiniDebConf, and the identi.ca network was offline on
the second day. Check out the publications we have made and take advantage of
it to sign up for the networks:

-   GNU Social: <https://quitter.se/debianbrasil>
-   Pump.io: <https://identi.ca/debianbrasil>
-   Diaspora: <https://diaspora.softwarelivre.org/u/curitibalivre>
-   Hubzilla: <https://hub.vilarejo.pro.br/channel/debianbrasil>

### Partnership Event

During the MiniDebConf happened a partnership event called
["Inspire yourself! Women in Computer Science - Experiences and Learning"](https://www.sympla.com.br/inspire-se-mulheres-na-computacao---experiencias-e-aprendizados__123063). This event was organized by a group from UTFPR the
[Emílias - Armação em Bits](http://emilias.dainf.ct.utfpr.edu.br/) and
Adriana Cássia da Costa that was working in the
MiniDebConf committee. That activity happened on Saturday evening (18/03), the
scheduling has a discussion about "How to be a software developer", a lecture
about "Machine Learning" and other about "How to encourage women in technology". 
The most of participants were women that have been participating in groups to
encourage other women in technology and there were some people who were
interested in working with technology and went to the event to learn about what
is the best way to do that.

This is our first partnership event and the result was very nice. We discussed
about how to be a good I.T professional, how to include minorities and the
ethical and social aspects of technology.

Photos: <https://www.flickr.com/photos/curitibalivre/albums/72157679965837780>

### Acknowledgment

We would like to give many thanks for all the participants that made the
MiniDebConf Curitiba 2017 a successful event.


We thank the speakers so much that made a big presentation, the participants
that participated a lot to the activities and contributed to the discussions
with their opinion, and the volunteers that gave their best to help in the event. 
We thank the professors and employees from the Federal University of Technology
- Paraná - UTFPR, that received us very well and the venue space.

We thank our sponsors: [4Linux](https://www.4linux.com.br/) and
[Locaweb](https://www.locaweb.com.br/), that believed in our event.


**We hope that after the MiniDebConf Curitiba 2017 more people will be inspired
to contribute with the Debian Brazil Community.**

![Foto oficial minidebconf2017](https://debianbrasil.org.br/blog/imagens/foto-oficial-minidebconf2017.jpg)

![Minidebconf2017 patroc org en](https://debianbrasil.org.br/blog/imagens/minidebconf2017-patroc-org-en.png)