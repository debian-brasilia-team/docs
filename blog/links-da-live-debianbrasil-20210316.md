---
title: Links da live Debian Brasil de 16/03/2021
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:20.821Z
tags: blog, geral
editor: markdown
dateCreated: 2021-03-17T19:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A seguir estão os links citados na live do
[canal Debian Brasil no YouTube](https://www.youtube.com/debianbrasiloficial)
do dia 16 de março de 2021.

[Vídeo](https://www.youtube.com/watch?v=tbo4c8JtiVk) para assistir.

Participantes: Antonio Terceiro, Daniel Lenharo, Lucas Kanashiro, Sérgio
Durigan e Paulo Santana.

Grupos de [Brasília](https://t.me/debianbrasilia) e
[São Paulo](https://t.me/DebianSP) no Telegram.

[Anúncio](https://lists.debian.org/debian-devel-announce/2021/02/msg00003.html)
do lançamento debuginfod mantido pelo Sérgio.

[Google Summer Of Code](https://summerofcode.withgoogle.com/) e
[Outreachy](https://www.outreachy.org/)

Plataformas da [Sruthi Chandran](https://www.debian.org/vote/2021/platforms/srud) e do [Jonathan Carter](https://www.debian.org/vote/2021/platforms/jcc) para
eleição do DPL.

Como doar para o Debian: <https://www.debian.org/donations>

Usuários(as): <https://www.debian.org/users>

Consultores(as): <https://www.debian.org/consultants>

Bloco de notícias

* [Luis Teixeira](https://t.me/luisteixeira25) está chamando pessoas para
criar um GUD em Belém.
* [Vaquinha](https://www.vakinha.com.br/vaquinha/ajuda-para-equipamento-de-respiracao) para ajudar Leandro Atlas com COVID-19.
* [Debian packaging workshop](https://eventyay.com/e/fa96ae2c/session/6731)
do Andreas Tille no dia 19 de março às 10:00h.
* [Bug Squash Party (BSP) Montreal](https://wiki.debian.org/BSP/2021/03/ca/Montreal) no dia  20 de março (virtual). Veja a [mensagem](https://lists.debian.org/debian-dug-quebec/2021/03/msg00001.html) para a lista. 