---
title: MiniDebConf Brasília 2023 - a brief report
description: by Giovani Augusto Ferreira
published: true
date: 2025-03-01T17:41:05.081Z
tags: blog, english
editor: markdown
dateCreated: 2023-07-01T11:00:00.000Z
---

<!-- author: Giovani Augusto Ferreira -->

![Minidebconf2033 palco](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-palco.jpg =400x) 
From May 25th to 27th, Brasília hosted the
[MiniDebConf 2023](https://brasilia.mini.debconf.org). This gathering, composed
of various activities such as talks, workshops, sprints, BSP (Bug Squashing
Party), key signing, social events, and hacking, aimed to bring the community
together and celebrate the world's largest Free Software project: Debian.

The MiniDebConf Brasília 2023 was a success thanks to the participation of
everyone, regardless of their level of knowledge about Debian. We valued the
presence of both beginners who are getting familiar with the system and official project developers. The spirit of inclusion and collaboration was present
throughout the event.
MiniDebConfs are local meetings organized by members of the Debian Project,
aiming to achieve similar goals as DebConf but on a regional scale. Throughout
the year, events like this occur in different parts of the world, strengthening
the Debian community.

![Minidebconf2023 placa](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-placa.jpg =400x) 
## Activities

The MiniDebConf program was intense and diverse. On May 25th and 26th (Thursday
and Friday), we had talks, discussions, workshops, and many hands-on activities. On the 27th (Saturday), the Hacking Day took place, which was a special moment
for Debian contributors to come together and work collaboratively on various
aspects of the project. This was the Brazilian version of Debcamp, a tradition
preceding DebConf. On this day, we prioritized practical activities such as
software packaging, translations, key signing, install fest, and the Bug
Squashing Party.

![Minidebconf2023 auditorio](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-auditorio.jpg =400x) 
<br />
<br />

![Minidebconf2023 oficina](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-oficina.jpg =400x) 
## Edition numbers

The event numbers are impressive and demonstrate the community's involvement
with Debian. We had 236 registered participants, 20 submitted talks, 14
volunteers, and 125 check-ins. Furthermore, in the hands-on activities, we
achieved significant results, including 7 new installations of Debian GNU/Linux, the update of 18 packages in the official Debian project repository by
participants, and the inclusion of 7 new contributors to the translation team.

We also highlight the remote participation of the community through live
streams. The analytics data reveals that our website received a total of 7,058
views, with 2,079 views on the homepage (which featured our sponsors' logos),
3,042 views on the program page, and 104 views on the sponsors' page. We
recorded 922 unique users during the event.

On YouTube, the live stream reached 311 views, with 56 likes and a peak of 20
concurrent views. There were an incredible 85.1 hours of watch time, and our
channel gained 30 new subscribers. All this engagement and interest from the
community further strengthen MiniDebConf.

![Minidebconf2023 palestrantes](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-palestrantes.jpg =400x) 
## Photos and videos

To relive the best moments of the event, we have photos and recordings
available. Photos can be accessed at: https://deb.li/pbsb2023. Video recordings
of the talks are available at the following link: <https://deb.li/vbsb2023>.
To stay updated and connect with the Debian Brasília community, follow us on
our social media channels:

- Telegram: <https://t.me/debianbrasilia>
- Facebook: <https://www.facebook.com/debianbrasiliaoficial>
- Twitter: <https://twitter.com/DebianBrasilia>

## Thanks

We would like to express our deep gratitude to all the participants, organizers, sponsors, and supporters who contributed to the success of MiniDebConf Brasília
2023. In particular, we extend our thanks to our Gold sponsors:
2024.
[Pencillabs](https://pencillabs.tec.br),
[Globo](https://www.globo.com),
[Policorp](https://policorp.com.br), and
[Toradex Brasil](https://www.toradex.com/pt-br), and our Silver sponsor,
[4-Linux](https://4linux.com.br/). We also thank Finatec and the
[Instituto para Conservação de Tecnologias Livres (ICTL)](https://ictl.org.br)
for their support.

![Minidebconf2023 coffee](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-coffee.jpg =400x) 
MiniDebConf Brasília 2023 was a milestone for the Debian community,
demonstrating the power of collaboration and Free Software. We hope that
everyone enjoyed this enriching gathering and will continue to actively
participate in future Debian Project initiatives. Together, we can make a
difference!

![Minidebconf2023 fotos oficial](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-oficial.jpg =400x)