---
title: Links da live Debian Brasil de 09/03/2021
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:18.618Z
tags: blog, geral
editor: markdown
dateCreated: 2021-03-10T09:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A seguir estão os links citados na live do
[canal Debian Brasil no YouTube](https://www.youtube.com/debianbrasiloficial)
do dia 09 de março de 2021.

[Vídeo](https://www.youtube.com/watch?v=KnkThN9I4j8) para assistir.

Participantes:

* [Daniel Lenharo](https://www.sombra.eti.br/)
* [Paulo Santana](https://phls.com.br)

O que fizemos nas férias.

Perspectiva para 2021:

* Lançamento Debian 11 ([Release Party](https://wiki.debian.org/ReleaseParty))
* [DebConf21](https://debconf21.debconf.org/)
* Aniversário do Debian ([Debian Day](https://wiki.debian.org/DebianDay))

Times locais que estão fazendo atividades de empacotamento:

* [Brasília](https://wiki.debian.org/Brasil/GUD/Brasilia)
* [São Paulo](https://wiki.debian.org/Brasil/GUD/SP)

Bloco de notícias

* Novo layout do site [debian.org](https://www.debian.org/) foi lançado em 17/12. Veja a [mensagem](https://lists.debian.org/debian-www/2020/12/msg00057.html) enviada para a lista. * [MiniDebConf Índia](https://in2021.mini.debconf.org). Veja os [arquivos](https://ftp.acc.umu.se/pub/debian-meetings/2021/MiniDebConf-India/) das palestras. * Time [Debian Edu](https://wiki.debian.org/Teams/DebianEdu) chama tradutores para o manual do Debian Edu Bullseye. Veja a [mensagem](https://lists.debian.org/debian-i18n/2021/02/msg00002.html) enviada para a lista. * [Bug Squash Party (BSP) America Latina](https://wiki.debian.org/BSP/2021/03/LatinAmerica) a partir do dia 9 de março (virtual). Veja a [mensagem](https://lists.debian.org/debian-devel-announce/2021/03/msg00003.html) enviada para a lista. * [Bug Squash Party (BSP) Salzburg - Áustria](https://wiki.debian.org/BSP/2021/04/virtual-Salzburg) nos dias 24 e 25 de abril (virtual). Veja a [mensagem](https://lists.debian.org/debian-devel-announce/2021/02/msg00000.html) para a lista. * Sprint do time de tradução para o DDTP acontecerá em breve. Veja a [mensagem](https://lists.debian.org/debian-l10n-portuguese/2021/03/msg00025.html) enviada para a lista. * Chamada de candidaturas para [eleição](https://www.debian.org/vote/2021/vote_001) do Debian Project Leader (DPL). Veja a [mensagem](https://lists.debian.org/debian-devel-announce/2021/03/msg00001.html) enviada para a lista. * LibrePlanet 2021 divulgou a [grade de programação](https://libreplanet.org/2021/program/). 
Futuro do canal Debian Brasil. Em breve divulgaremos detalhes sobre como você nos enviar vídeos para publicarmos no canal. 
