---
title: Oficina de tradução do Manual do(a) Administrador(a) Debian em 13 de junho
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:41:28.419Z
tags: blog, geral
editor: markdown
dateCreated: 2023-06-04T10:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A
[equipe de tradução do Debian para o português do Brasil](https://wiki.debian.org/Brasil/Traduzir) realizará, no dia 13 de junho a partir das 20h, uma oficina de tradução do
[Manual do(a) Administrador(a) Debian (The Debian Administrator's Handbook)](https://debian-handbook.info). 
O objetivo é mostrar aos(às) iniciantes como colaborar na tradução deste
importante material, que existe desde 2004 e vem sendo traduzido para o
português ao longo dos anos. Agora a tradução precisa ser atualizada para a
versão 12 do Debian (bookworm), que será
[lançada este mês](https://wiki.debian.org/ReleasePartyBookworm).

A ferramenta usada para traduzir o Manual é o site weblate, então você já pode
criar sua conta e acessar o
[Projeto Debian Handbook](https://hosted.weblate.org/languages/pt_BR/debian-handbook/) para se ambientar.

A oficina acontecerá no formato online, e o link para participar da sala no
jitsi será divulgado no grupo
[debl10nptBR no telegram](https://t.me/debl10nptBR) e no canal
[#debian-l10n-br do IRC](https://webchat.oftc.net/?channels=debian-l10n-br).
