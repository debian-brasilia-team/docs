---
title: Report from the first meeting in Curitiba to talk about the Bid to DebConf18
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:42:12.359Z
tags: blog, english
editor: markdown
dateCreated: 2016-11-01T12:59:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Last saturday we had a presential meeting in Curitiba
at [Positivo University](http://www.up.edu.br/) - Campus Downtown, to talk about the [DebConf18](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/) bid.

Were present: [Adriana da Costa](https://contributors.debian.org/contributor/adrianacosta-guest@alioth/), [Antonio Terceiro](https://contributors.debian.org/contributor/terceiro/),
Cléber Ianes, [Daniel Lenharo](https://contributors.debian.org/contributor/lenharo), [Paulo Santana](https://contributors.debian.org/contributor/phls)
and [Samuel Henrique](https://contributors.debian.org/contributor/samueloph).

All of them were very excited to have an oportunity to organize a
[DebConf](http://debconf.org/), if Curitiba is chosen :-)

We had many questions to ask to Antonio Terceiro, our DD who lives in Curitiba
and who has gone to the last Debian Conferences, to understand how it works.
After that, we understood better about DebCamp, day trip, offical dinner, wine
and cheese party, etc.

We talked too about the advantages of Curitiba, the venue, the accommodations,
the food in Brazil, the places to visit here, etc. We distributed some tasks and now each of us is responsible for writing some topic of the Bid.

The Bid is been written here:

<https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba>

And a more complete meeting report is here (in portuguese only)

<https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/Meeting20161029>

Probably in 3 weeks we will have a virtual meeting to give opportunity to our
friends from other cities to join.

We created an IRC channel fot the team (in portugese): \#debian-br-eventos,
which will be used for the bid but also for other Debian events.

We would like to thank Marcelo Belli de Oliveira from the Python Community for
providing us the room for the meeting.

By the way, in the same time, the Curitiba Python Community was holding some
talks next door.

Photos of the meeting: <https://www.flickr.com/photos/curitibalivre/albums/72157672337907374> 