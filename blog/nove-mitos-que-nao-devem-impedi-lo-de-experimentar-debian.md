---
title: Nove mitos que não devem impedi-lo de experimentar Debian
description: por Debian Brasil
published: true
date: 2025-03-01T17:41:23.974Z
tags: blog, geral
editor: markdown
dateCreated: 2013-04-12T14:26:00.000Z
---

<!-- author: Debian Brasil -->

[![](http://1.bp.blogspot.com/-q9yyh-kMzwg/UWgp6juleEI/AAAAAAAALgo/JL_FRo4ZUIo/s1600/debian_braim.jpeg)](http://1.bp.blogspot.com/-q9yyh-kMzwg/UWgp6juleEI/AAAAAAAALgo/JL_FRo4ZUIo/s1600/debian_braim.jpeg) 
*A Reputação do Debian está fora de sintonia com o que ele realmente oferece.*

Nestes dias, o Debian parece estar desfrutando um modesto retorno entre os
usuários experientes. Dificilmente passa-se uma semana sem que não se ouça em
sites de redes sociais a notícia de que duas ou três pessoas estão olhando o
Debian com outros olhos.

Este interesse renovado pode ser reflexo da crescente desilusão com o Ubuntu,
distribuição baseada no Debian e que outrora havia tomado seu lugar em
popularidade entre os usuários Linux. Certamente, reflete uma crescente
disposição, no transcurso dos últimos dois anos, dos usuários revoltados com
o Unity do Ubuntu, em experimentarem novas distros com GNOME 3. Sendo uma das
distribuições mais antigas e especificamente focada nas escolhas do usuário,
Debian parece confiável no meio de tantas incertezas.

Ainda assim, muitos usuários hesitam em mudar para o Debian. A distribuição é
cercada por mitos, muitos deles ligados à impressão de que é indicada para
especialistas e de que é quase tão difícil de usar como o
[Gentoo](http://www.gentoo.org/) ou
[Linux from Scratch](http://www.linuxfromscratch.org/).

No entanto, a maioria desses mitos ou estão desatualizados ou são meias-verdades que precisam ser inteiramente analisadas. Como em qualquer distribuição, a
experiência do usuário no Debian vem tanto dos aplicativos como da própria
distribuição. Se você estiver confortável com o KDE ou o LXDE no Fedora ou
Mageia, você deve sentir-se também confortável com eles no Debian. Mesmo que
qualquer um desses mitos fossem verdadeiros, nenhum deles poderiam fundamentar
qualquer razão para não darmos uma chance ao Debian.

## 9. Debian é difícil de instalar

Verdade seja dita, o Debian foi uma das últimas distribuições a ter uma
instalação fácil de usar, e muito menos em modo gráfico. No entanto, uma
revisão do instalador baseado em texto foi aplicada em 2005, e uma versão
gráfica em 2007, sendo que ambos são práticos, embora esteja longe de ser
amigável.

O que pode ser intimidante é que ambas as versões do instalador exigem uma
contribuição significativa do usuário. Se você quiser, você pode ajustá-los
indefinidamente. No entanto, mesmo se você não tem ideia do que é o Linux,
você ainda pode instalar o Debian com sucesso aderindo ao nível básico de
detalhes, aceitando sugestões do instalador e ler a ajuda.

Alguma vez você já recorreu ao método especialista do instalador do Ubuntu?
Se sim, você já usou uma versão do instalador Debian, e pode usá-lo a partir
dessa experiência em primeira mão.

## 8. Os usuários devem manter um mesmo ramo de repositórios

Muitas pessoas estão conscientes de que o Debian tem três repositórios
principais: Testing, Unstable e Stable. A maioria também estão cientes de que
um pacote entra no Unstable após o cumprimento das normas básicas, então passa
para o Testing e, finalmente, para o Stable quando uma liberação geral é feita.
No entanto, os usuários potenciais receiam comprometer seus sistema por conta
da escolha de um repositório inadequado para suas preferências.

Por outro lado, usuários experientes do Debian conhecem melhor as possibilidades dos repositórios. Usuários que estão configurando um servidor ou que exigem o
máximo de confiabilidade, ou por algum outro motivo qualquer, geralmente ficam
com o repositório Stable. No entanto, outros usuários, especialmente em estações de trabalho independentes, costuma misturar e combinar os repositórios para
produzir sistemas híbridos.

Entretanto, estes sistemas híbridos exigem cautela. Geralmente, deve-se evitar
misturar e combinar pacotes do sistema central (os chamados pacotes base). A
exceção fica por conta do kernel, já que o bootloader normalmente armazena
vários kernels, de modo que se um novo não funcionar, você ainda pode reiniciar
o sistema. Da mesma forma, se você tem várias áreas de trabalho, problemas com
uma delas ainda ainda lhe permitirá usar uma outra interface, ou mesmo a linha
de comando como último recurso.

Por outro lado, ainda assim é perfeitamente seguro atualizamos os aplicativos
do desktop para aqueles dos repositórios Unstable, porque, mesmo se ocorrerem
problemas, seu sistema básico ainda deve inicializar.

Em outras palavras, desde que você tome algumas precauções, você não está
limitado a usar um único repositório, a menos que você deseje ser assim.

## 7. Unstable é instável

Sim, o repositório Unstable é instável para os padrões Debian. Mas isso
significa que, para os padrões da maioria das outras distribuições, pacotes
instáveis ​​são geralmente utilizáveis. Na verdade, os derivados
Debian muitas vezes tomam emprestados pacotes diretamente do Unstable, a fim
de dar aos usuários as mais recentes versões dos pacotes.

No entanto, o repositório Unstable passa por alguns períodos em que é melhor
deixá-lo sozinho. Desde que os pacotes cumpram algumas normas mínimas, alguns
pacotes mandados para o Unstable podem ter problemas de dependência que podem
quebrar o sistema de gerenciamento de pacotes, deixando-o incapaz de instalar
outros pacotes até que o problema seja resolvido.

Mas tais problemas geralmente são corrigidos quando submetidos ao processo de
depuração. Você também pode ter um número de
[opções](http://archive09.linux.com/articles/48910) para a correção  do seu
próprio sistema.

Da mesma forma, você deve evitar atualizações do Debian unstable quando está
no meio de uma transição de uma tecnologia para outra. Por exemplo, há alguns
anos, o Debian mudou o seu gerenciador gráfico do XFree86 para X.Org e
demorou um pouco para fazer a transição sem problemas para os usuários.

Se você usar o Unstable como repositório principal ou ocasionalmente em um
sistema híbrido, você precisa adquirir o hábito de acompanhar  o que o Projeto
[está fazendo](http://wiki.debian.org/DebianUnstable#What.27s_the_current_status_of_Unstable.3F). 
Por exemplo, quando um congelamento para o próximo lançamento é iniciado, não
é uma boa ideia atualizarmos ou instalarmos pacotes do Unstable, porque alguns
podem ter sido feitos à pressa, a fim de cumprir o prazo de congelamento.

Sob essas circunstâncias, por uma semana ou duas, você pode querer usar a opção
-s para o apt-get para simular uma atualização antes de realmente a praticá-la,
apenas para evitar problemas.

## 6. Pacotes Debian são desatualizados

A verdade desta afirmação depende do repositório e das circunstâncias.

Geralmente, o repositório Stable possui pacotes mais antigos do que do
repositório Testing e, muito provavelmente ainda mais desatualizados do que
aqueles do Unstable. Historicamente, dois ou três anos é período médio entre
os lançamentos oficiais de uma versão estável do Debian. Antes do lançamento
de uma nova versão, a versão estável corrente pode estar bastante obsoleta,
apesar de várias micro-releases, backports e patches de segurança que permitem
torná-la utilizável.

Da mesma forma, quando um congelamento é iniciado, levando a uma nova versão,
o conteúdo do Testing e Unstable vão ficando cada vez mais idênticos pelo fato
de não ser permitida a inclusão de novos pacotes no sistema.

Por outro lado, nos primeiros seis meses após o lançamento, a versão Stable
pode ser tão atual como o repositório de qualquer outra distro.

O que é verdade é que a disponibilidade de pacotes depende do entusiasmo das
equipes de mantenedores. Aplicações populares como o Amarok podem permanecer
no Unstable por um período, ​​depois que o upstream project anuncia
uma liberação.

Outros pacotes podem demorar mais para aparecer. A ênfase do Debian é a
estabilidade, e não a atualidade de seus pacotes. Se você realmente quiser o
software mais recente, você pode ativar o repositório experimental. Mas nem 
todos os pacotes que vão para o Unstable aparecem primeiro no Experimental,
e esse repositório pode causar sérios problemas.

Em geral, na melhor das hipóteses, a atualidade dos pacotes Debian é a menor
de suas preocupações. Enquanto todo mundo gosta da ideia de ter a versão mais
recente de tudo, a maioria dos aplicativos no ambiente de trabalho livre são
avançados o suficiente para que as diferenças entre uma versão e outra não
faça assim tanta diferença.

## 5. Debian não é uma distribuição livre

Você não vai encontrar o Debian na
[lista](http://www.datamation.com/open-source/www.gnu.org/distros/free-distros.html) das distribuições livres licenciadas pela Free Software Foundation por duas
razões: primeiro, porque cada repositório Debian contém uma secção não-livre
(non-free), bem como uma seção contrib consistindo de software que é livre em
si, mas depende de algum software não-livre, e, segundo, porque inclui a opção
de instalar firmware proprietário em seus kernels.

No entanto, o instalador Debian encoraja os usuários a instalar um sistema
livre. Aqueles que querem usar as seções non-free e contrib tem que
adicioná-los à lista de fontes de repositórios próprios. Da mesma forma,
os usuários podem optar por não utilizar firmware proprietário ao instalar.
Com essas opções, você pode facilmente instalar um sistema Debian totalmente
livre se for a sua escolha.

## 4. As imagens de instalação são enormes, para Download

Uma versão completa do Debian pode ser contida em [51 CDs](http://www.debian.org/CD/http-ftp/) e, é provável, poderá levar mais de 24 horas para serem baixados.

No entanto, a maioria dos usuários preferem um live CD, ou uma imagem de
instalação de rede de 180 MB, ou uma instalação de cartão de 40 MB. A
instalação levará mais tempo a partir dessas soluções porque elas têm de baixar
da Internet os pacote que não vem com as mesmas. Mas com elas, você pode estar
pronto para instalar em cinco minutos ou menos.

## 3. Os mantenedores Debian são muito hostis

Há uma década atrás, os mantenedores Debian tinham a reputação de serem
indiferentes, grosseiros e sarcásticos. Atualmente, podemos encontrar
discussões acaloradas nas listas de discussão do projeto, mas de muitas
maneiras Debian é claro em seus atos.

Uma das razão para a mudança reside no fato de que os mantenedores solitários
estão cada vez mais dando lugar a equipes, os membros do projeto estão mais
preocupados com o bom relacionamento com as pessoas.

Nos últimos anos, o projeto também instituiu um
[código de conduta](http://www.debian.org/MailingLists/index.en.html#codeofconduct) para suas listas de discussão, uma
[declaração de diversidade](http://www.debian.org/intro/diversity.en.html),
uma [equipe anti-assédio](http://wiki.debian.org/AntiHarassment) e
[padrões de respeito para eventos](https://wiki.debconf.org/wiki/StandardsOfRespect). 
Se esses esforços visam incentivar uma atmosfera amigável ou refletem apenas
uma determinação entre os líderes do Debian para aparentar isso é uma questão
aberta. Mas é verdade que o projeto Debian parece um lugar mais acolhedor hoje
do que era há cinco anos.

## 2. Debian não é compatível com o Ubuntu

Esta afirmação é importante porque o Ubuntu contém documentação e aplicativos
proprietários em seus repositórios que alguns usuários podem querer instalar.
Além disso, quando um projeto derivado deseja um ciclo de liberação mais curto,
atualmente, eles preferem fazê-lo para o Ubuntu.

O fato é que, como o Ubuntu toma emprestado muitos de seus pacotes do Debian
Testing ou dos repositórios Unstable, as duas distribuições terão sempre um
elevado grau de compatibilidade. Ao mesmo tempo, o Ubuntu continua a
diferenciar-se do Debian e outros demais distribuições debian-like, logo, essa
compatibilidade tenderá a cair com o tempo.

Até onde eu sei, ninguém controla o nível dessa compatibilidade. Mas, de acordo
com uma [apresentação](http://debian-bits-and-snips.blogspot.ca/2011/04/who-bloody-hell-cares-about-debian.html) feita pelo líder do projeto Debian, Stefano Zacchiroli, em 2011, 74% dos
pacotes do Ubuntu são tirados diretamente de repositórios do Debian e 18% são
de pacotes adaptados. (Os 7% restantes são obtidos do upstream). Estes números
sugerem que dois de cada três pacotes são compatíveis em ambos os sistemas.
Provavelmente, as chances são ainda melhores se o pacote não fizer parte do
núcleo do sistema.

## 1. Debian é irrelevante hoje

Em grande medida, o Ubuntu agora desfruta da popularidade que o Debian tinha
uma década atrás. Inovador onde Debian está preocupado com a estabilidade,
user-friendly, onde Debian tem uma reputação de ser idealizado para
especialistas, poderiam dizer que o Ubuntu tornou o Debian irrelevante.

Uma análise mais atenta, porém, mostra que, se o Debian em si está menos
popular do que era antes, a sua influência tornou-se maior do que nunca. Além
Ubuntu em si, 147 das 321 distribuições listadas na [Distrowatch](http://distrowatch.com/) baseiam-se em  Debian. Adicionando-se as distribuições derivadas do Ubuntu,
chegaremos a 234 distros derivadas direta ou indiretamente do Debian (73% das
distribuições existentes). Este é um
[acréscimo](http://www.datamation.com/osrc/article.php/3926941/Linux-Leaders-Debian-and-Ubuntu-Derivative-Distros.htm) de 10% em relação há dois anos atrás, e a inclui entre as três distribuições
das cinco principais mais procuradas - Linux Mint, Ubuntu e Debian.

Em vez dizer que tornou-se irrelevante, hoje Debian está mais influente do que
nunca. Pode-se dizer que se tornou o maior projeto upstream do desktop Linux.

## Vivendo sob os rumores

Tecnicamente e socialmente, o Debian tem muitos pontos a seu favor. O Seu
sistema mantenedor garante que apenas pessoas habituadas com projetos de
upstream  estejam habilitadas a supervisionar seus pacotes, e os testes destes
pacotes incluem padrões rigorosos.

Tão importante quanto isso, está uma das mais fortes provas de que uma
distribuição baseada na comunidade pode ser tão bem sucedida como uma comercial. Alguns usuários também a apoiam pelo simples fato de que ela oferece uma posição rígida e diferenciada quanto ao software livre, que é independente da Free
Software Foundation.

Entretanto, muitas pessoas acabam deixando-se influenciar pelos mitos quando
pensam em adotar Debian - baseadas em rumores de que nunca foram verdade ou
que há muito tempo deixam de ser verdade.

Quando você resolver olhar através dos mitos, atualmente você não terá motivo
algum para não considerar Debian ao invés de outras distros que têm uma
reputação melhor por conta da facilidade de uso. Além de umas poucas
qualificações que eu mencionei, o Debian moderno merece ser um candidato
sério quando você se decidir trocar de distro.

Fonte: [Datamation](http://goo.gl/af9qQ)

Reproduced with permission.
Copyright 1999-2013 QuinStreet, Inc. All rights reserved.

Tradução original: [Debian Maníaco](http://debianmaniaco.blogspot.com/2013/04/nove-mitos-que-nao-devem-impedi-lo-de.html) 
Fonte:
[http://debianmaniaco.blogspot.com/2013/04/nove-mitos-que-nao-devem-impedi-lo-de.html](http://debianmaniaco.blogspot.com/2013/04/nove-mitos-que-nao-devem-impedi-lo-de.html) 