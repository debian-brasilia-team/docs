---
title: BITS da equipe debian-l10n-portuguese - Dez/2018
description: por Daniel Lenharo de Souza
published: true
date: 2025-03-01T17:57:22.522Z
tags: blog, geral
editor: markdown
dateCreated: 2019-01-19T12:11:00.000Z
---

<!-- author: Daniel Lenharo de Souza -->

Mensalmente emitimos um relatório da
[equipe debian-l10n-portuguese](https://wiki.debian.org/Brasil/Traduzir) do
Projeto [Debian](https://www.debian.org) e este é o quinto deles. O objetivo é
que as atividades da equipe sejam conhecidas e possamos dar os devidos créditos
pelo trabalho produzido por nossos contribuidores. Acreditamos que isso poderá
estimular a participação de novos tradutores e revisores.

## Visão geral de Dezembro de 2018

-   02 Páginas traduzidas/atualizadas no site oficial.
-   02 Traduções PO.
-   01 Traduções de páginas wiki

## Pessoas com atividades registradas durante dezembro de 2018

-   Adriana Costa
-   Adriano Rafael Gomes
-   Daniel Lenharo de Souza
-   Diego Neves
-   Paulo Henrique de Lima Santana
-   Ricardo Fantin da Costa

Se você fez alguma colaboração de tradução que não está listada acima, nos avise na lista (debian-l10n-portuguese@lists.debian.org) para que seu trabalho seja
devidamente registrado e reconhecido!

## Itens traduzidos

-   wml://www.debian.org/events/eventsmailinglists.wml
-   wml://www.debian.org/events/talks.wml
-   po://debian-installer/level1/sublevel1/pt\_BR.po
-   po://debian-installer/level1/sublevel2/pt\_BR.po
-   <https://wiki.debian.org/pt\_BR/NFS>

A equipe de tradução para português do Brasil agradece a todos que colaboram
para que o Debian se torne mais universal!

Venha [ajudar](https://wiki.debian.org/Brasil/Traduzir) você também. Toda ajuda
é bem-vinda e estamos precisando da sua.

Não perca a [DebConf19 no Brasil](https://debconf19.debconf.org/). A DebConf
acontecerá em Curitiba entre os dias 21 e 28 de Julho de 2019. Participe!


