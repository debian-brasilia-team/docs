---
title: Report from the last meeting of the DebConf18 in Curitibia BID at UTFPR
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:42:14.563Z
tags: blog, english
editor: markdown
dateCreated: 2016-12-05T13:27:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

On last friday (December 2) a part of the core team of
[BID Curitiba](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba)
([Antonio Terceiro](https://contributors.debian.org/contributor/terceiro),
[Daniel Lenharo](https://contributors.debian.org/contributor/lenharo)
and [Paulo Santana](https://contributors.debian.org/contributor/phls))
had a meeting with representatives from The
[Federal University of Technology - Paraná (UTFPR)](http://utfpr.edu.br/),
Prof. Paulo Apelles de Oliveira - Director of
[Corporate and Community Relations](http://www.utfpr.edu.br/curitiba/estrutura-universitaria/diretorias/direc/inicio), Prof. Maria Emer and Prof. Adolfo Neto from
[Informatics Department](http://www2.dainf.ct.utfpr.edu.br),
and Prof. Christian Mendes from
[Electronics Department.](http://www.utfpr.edu.br/curitiba/estrutura-universitaria/diretorias/dirgrad/departamentos/eletronica/eng/initial-page) 
If Curitiba is chosen to host DebConf18, The University will provide the
auditoriums and rooms to the discussions, workshops and coding parties, for a
low cost. So, our prioriary venue to DebConf18 in Curtiba is UTFPR for sure.

UTFPR has a excellent network connectivity, through RNP which serves most public universities. Also, we can use the restaurant inside the Campus.

They hope that after DebConf18 many local students can use and contribute with
Debian. Prof. Maria told us about
[Emilias Project](http://emilias.dainf.ct.utfpr.edu.br/)
created to encourage women in computing, and how will be important to have girls from [The Debian Women Project](https://www.debian.org/women) here to talk to
them.

![Foto reuniao utfpr 2016 12 02](https://debianbrasil.org.br/blog/imagens/foto-reuniao-utfpr-2016-12-02.jpg =400x) 

