---
title: how-can-i-help: oportunidades de contribuição com o Debian
description: por Antonio Terceiro
published: true
date: 2025-03-01T17:40:00.910Z
tags: blog, geral
editor: markdown
dateCreated: 2014-02-11T14:56:00.000Z
---

<!-- author: Antonio Terceiro -->

---
title: how-can-i-help: oportunidades de contribuição com o Debian
description: 
published: true
date: 2025-03-01T17:20:48.073Z
tags: blog
editor: markdown
dateCreated: 2025-02-27T19:36:03.603Z
---

Já faz um tempo desde o meu último post, e pra esse novo vou retomar o assunto
do [anterior](https://terceiro.xyz/2013/06/18/participando-do-projeto-debian-como-comecar/) - como começar a colaborar com o Debian, inspirado num
[post recente do Stefano](http://upsilon.cc/~zack/blog/posts/2014/02/apt-get_install_how-can-i-help/). 
O `how-can-i-help` é um programa que te mostra oportunidades de colaboração
relacionadas aos pacotes que estão instalados no seu sistema: pacotes que
precisam de novos mantenedores, pacotes que estão com bugs críticos, pacotes
que estão com bugs críticos **e** vão ser removidos da *testing*, etc.

Outro tipo de possível contribuição que o `how-can-i-help` vai listar e que
vai ser bastante útil pra quem quer começar são bugs com a tag “gift”, ou seja
bugs que o mantenedor do pacote marcou como fáceis para serem resolvidos por
novos colaboradores.

Por padrão o `how-can-i-help` vai rodar toda vez que você instalar um pacote,
mas também você também pode rodar ele digitando `how-can-i-help` manualmente
no terminal.

Então, se você quer começar a contribuir com o Debian, você pode começar agora
mesmo com:

        $ apt-get install how-can-i-help

OBS: o how-can-i-help só estão disponível a partir do Debian jessie
(próximo release), ou seja, você precisa estar usando jessie (testing) ou
sid (unstable).

Fonte:
[Antonio Terceiro](https://terceiro.xyz/2014/02/11/how-can-i-help-oportunidades-de-contribuicao-com-o-debian/) 