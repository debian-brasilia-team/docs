---
title: Debian Day 2024 em Santa Maria/RS - Brasil
description: por Andrew Gonçalves
published: true
date: 2025-03-01T17:39:19.211Z
tags: blog, geral
editor: markdown
dateCreated: 2024-08-20T13:00:00.000Z
---

<!-- author: Andrew Gonçalves -->

por Andrew Gonçalves

O [Debian Day em Santa Maria - RS 2024](https://wiki.debian.org/DebianDay/2024/Brazil/SantaMaria) foi realizado após 5 anos de hiato, foi feito durante a manhã do dia 16/08/2024
no Salão Azul da [Universidade Franciscana (UFN)](https://site.ufn.edu.br/) com
apoio da comunidade Debian e do Laboratório de Práticas da Computação da UFN.

O evento contou com alunos de todos os semestres dos cursos de Ciência da
Computação, Jogos Digitais e Sistemas de Informação, fizemos um coffee break
onde tivemos a oportunidade de conversar com os participantes.

Cerca de 60 alunos prestigiaram uma palestra de introdução ao Software Livre e
de Código Aberto, Linux e foram introduzidos ao projeto Debian, tanto sobre a
filosofia do projeto, até como ele acontece na prática e oportunidades que se
abriram para participantes do projeto por fazerem parte do Debian.

Após a palestra foi feita uma demonstração de empacotamento pelo DD local
Francisco Vilmar, que demonstrou na prática como funciona o empacotamento de
software no [Debian](https://www.debian.org/).

Gostaria de agradecer a todas as pessoas que nos ajudaram:

- Projeto Debian
- Professora Ana Paula Canal (UFN)
- Professor Sylvio André Garcia
- Laboratório de Práticas da Computação
- Francisco Vilmar (DD local)

E um muito obrigado a todos os participantes que nos prestigiaram neste evento
fazendo perguntas intrigantes e se interessando pelo mundo  do Software Livre.

Algumas fotos de Santa Maria:

![DD em Santa Maria 1](https://debianbrasil.org.br/blog/imagens/debianday-santa-maria-2024-1.jpg =400x)
![DD em Santa Maria 2](https://debianbrasil.org.br/blog/imagens/debianday-santa-maria-2024-2.jpg =400x)
![DD em Santa Maria 3](https://debianbrasil.org.br/blog/imagens/debianday-santa-maria-2024-3.jpg =400x)
![DD em Santa Maria 4](https://debianbrasil.org.br/blog/imagens/debianday-santa-maria-2024-4.jpg =400x)
