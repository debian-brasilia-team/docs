---
title: MiniDebConf Brasília 2023 - um breve relato
description: por Giovani Augusto Ferreira
published: true
date: 2025-03-01T17:41:07.362Z
tags: blog, geral
editor: markdown
dateCreated: 2023-07-01T11:00:00.000Z
---

<!-- author: Giovani Augusto Ferreira -->

![Minidebconf2033 palco](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-palco.jpg =400x) 
No período de 25 a 27 de maio, Brasília foi palco da
[MiniDebConf 2023](https://brasilia.mini.debconf.org). Esse encontro, composto
por diversas atividades como palestras, oficinas, sprints,
[BSP (Bug Squashing Party)](https://wiki.debian.org/BSP), assinatura de chaves,
eventos sociais e hacking, teve como principal objetivo reunir a comunidade e
celebrar o maior projeto de Software Livre do mundo: o
[Debian](https://debian.org).

A MiniDebConf Brasília 2023 foi um sucesso graças à participação de todas e
todos, independentemente do nível de conhecimento sobre o Debian. Valorizamos a
presença tanto dos(as) usuários(as) iniciantes que estão se familiarizando com o sistema quanto dos(as) desenvolvedores(as) oficiais do projeto. O espírito de
acolhimento e colaboração esteve presente em todos os momentos.

As MiniDebConfs são encontros locais organizados por membros do Projeto Debian,
visando objetivos semelhantes aos da DebConf, porém em âmbito regional. Ao
longo do ano, eventos como esse ocorrem em diferentes partes do mundo,
fortalecendo a comunidade Debian.

![Minidebconf2023 placa](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-placa.jpg =400x) 
## Atividades

A programação da MiniDebConf foi intensa e diversificada. Nos dias 25 e 26
(quinta e sexta-feira), tivemos palestras, debates, oficinas e muitas atividades práticas. Já no dia 27 (sábado), ocorreu o Hacking Day, um momento especial em
que os(as) colaboradores(as) do Debian se reuniram para trabalhar em conjunto em vários aspectos do projeto. Essa foi a versão brasileira da Debcamp, tradição
prévia à DebConf. Nesse dia, priorizamos as atividades práticas de contribuição
ao projeto, como empacotamento de softwares, traduções, assinaturas de chaves,
install fest e a Bug Squashing Party.

![Minidebconf2023 auditorio](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-auditorio.jpg =400x) 
<br />
<br />

![Minidebconf2023 oficina](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-oficina.jpg =400x) 
## Números da edição

Os números do evento impressionam e demonstram o envolvimento da comunidade com
o Debian. Tivemos 236 inscritos(as), 20 palestras submetidas, 14 voluntários(as) e 125 check-ins realizados. Além disso, nas atividades práticas, tivemos
resultados significativos, como 7 novas instalações do Debian GNU/Linux, a
atualização de 18 pacotes no repositório oficial do projeto Debian pelos
participantes e a inclusão de 7 novos contribuidores na equipe de tradução.

Destacamos também a participação da comunidade de forma remota, por meio de
transmissões ao vivo. Os dados analíticos revelam que nosso site obteve 7.058
visualizações no total, com 2.079 visualizações na página principal (que contava com o apoio de nossos patrocinadores), 3.042 visualizações na página de
programação e 104 visualizações na página de patrocinadores. Registramos 922
usuários(as) únicos durante o evento.

No [YouTube](https://www.youtube.com/playlist?list=PL3LYBsdwbu0y_W-JPLROVW218WQs-ou-f), a transmissão ao vivo alcançou 311 visualizações, com 56 curtidas e um pico de
20 visualizações simultâneas. Foram incríveis 85,1 horas de exibição, e nosso
canal conquistou 30 novos inscritos(as). Todo esse engajamento e interesse da
comunidade fortalecem ainda mais a MiniDebConf.

![Minidebconf2023 palestrantes](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-palestrantes.jpg =400x) 
## Fotos e vídeos

Para revivermos os melhores momentos do evento, temos disponíveis fotos e vídeos. As fotos podem ser acessadas em: <https://deb.li/pbsb2023>. Já os vídeos com as
gravações das palestras estão disponíveis no seguinte link:
<https://deb.li/vbsb2023>. Para manter-se atualizado e conectar-se com a
comunidade Debian Brasília, siga-nos em nossas redes sociais:

- Telegram: <https://t.me/debianbrasilia>
- Facebook: <https://www.facebook.com/debianbrasiliaoficial>
- Twitter: <https://twitter.com/DebianBrasilia>

## Agradecimentos

Gostaríamos de agradecer profundamente a todos(as) os(as) participantes,
organizadores(as), patrocinadores e apoiadores(as) que contribuíram para o
sucesso da MiniDebConf Brasília 2023. Em especial, expressamos nossa gratidão
aos patrocinadores Ouro:
[Pencillabs](https://pencillabs.tec.br),
[Globo](https://www.globo.com),
[Policorp](https://policorp.com.br) e
[Toradex Brasil](https://www.toradex.com/pt-br), e ao patrocinador Prata,
[4-Linux](https://4linux.com.br/). Também agradecemos à Finatec e ao
[Instituto para Conservação de Tecnologias Livres (ICTL)](https://ictl.org.br)
pelo apoio.

![Minidebconf2023 coffee](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-coffee.jpg =400x) 

A MiniDebConf Brasília 2023 foi um marco para a comunidade Debian, demonstrando
o poder da colaboração e do Software Livre. Esperamos que todas e todos tenham
desfrutado desse encontro enriquecedor e que continuem participando ativamente
das próximas iniciativas do Projeto Debian. Juntos, podemos fazer a diferença!

![Minidebconf2023 fotos oficial](https://debianbrasil.org.br/blog/imagens/minidebconf-brasilia-2023-oficial.jpg =400x) 
