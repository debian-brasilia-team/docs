---
title: Debian Brasil na Cryptorave 2017
description: por Daniel Lenharo de Souza
published: true
date: 2025-03-01T17:38:26.954Z
tags: blog, geral
editor: markdown
dateCreated: 2017-05-09T11:57:00.000Z
---

<!-- author: Daniel Lenharo de Souza -->

A Casa do Povo recebeu durante os dias 5 e 6 de Maio a edição 2017 da
[Cryptorave](https://cryptorave.org), inspirado na ação global e descentraliza
da [CryptoParty](https://www.cryptoparty.in), a qual têm como objetivo difundir
os conceitos fundamentais e softwares básicos de criptografia. Durante 24 horas
foram apresentandas atividades sobre segurança, criptografia, hacking,
anonimato, privacidade e liberdade na rede. A Comunidade Debian esteve
presente, representada pelos contribuidores brasileiros
[Daniel Lenharo](https://contributors.debian.org/contributor/lenharo/)
e
[Giovani Ferreira](https://contributors.debian.org/contributor/giovani/).

A CryptoRave mostra afinidade com o Projeto Debian ao recomendar as
distribuições Debian GNU/Linux e Tails dentre os softwares que respeitam a
privacidade dos usuários, e também em uma homenagem ao fundaddor do projeto,
uma das salas do evento recebeu o nome de Ian Murdock.

Durante o evento, em um espaço cedido pela organização, realizou-se a venda de
produtos Debian (camisetas, chaveiros, bottons, Adesivos, etc.) e aproveitou-se
para falar sobre o projeto e buscar novas pessoas à contribuir. Nesse sentido,
Daniel Lenharo realizou uma palestra chamada "O debian quer você" em que além
de falar sobre o projeto apresentou ao públicos diversas formas de contribuir
ao projeto Debian. "Fazer o Debian estar presente em um evento da importância
da Cryptorave é sensacional. Saber que nosso projeto é util e as pessoas sentem
orgulho de exibir nossa espiral é algo incrível. Com certeza após o evento,
teremos mais pessoas utilizando e colaborando com nossa comunidade", afirma
Lenharo.

Agradecemos à organização da Cryptorave pelo espaço cedido e esperamos por
2018!!

![Lenharo Falando Debian](https://debianbrasil.org.br/blog/imagens/cryptorave2017-palestras.jpg =400x)

Palestra de Daniel Lenharo sobre o Debian.

![](https://debianbrasil.org.br/blog/imagens/cryptorave2017-mesa.jpg =400x)

Daniel Lenharo e Giovani Ferreira na Mesa, divulgando o projeto Debian.

![](https://debianbrasil.org.br/blog/imagens/cryptorava2017-banner.jpg =400x)

Daniel Lenharo e Giovani Ferreira, Junto ao Banner com nome da Sala do criador
do Projeto Debian