---
title: Online seminar at UFAL - Brasil
description: por Thiago Pezzo e Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:41:30.797Z
tags: blog, geral
editor: markdown
dateCreated: 2021-06-27T15:00:00.000Z
---

<!-- author: Thiago Pezzo e Paulo Henrique de Lima Santana -->

On June, 3rd 2021, DD Paulo Santana (phls) and contributor Thiago Pezzo (tico)
held an online seminar to the
[Social Work](https://fsso.ufal.br/extensao/programa-de-extensao)
undergraduate course at the
[Federal University of Alagoas (UFAL)](https://ufal.br/), Brazil.

The lecture had the objective to spread the word about
[Debian](https://www.debian.org)'s operational system and community, also
introducing operational systems, Free Software movement, and software licenses
to a non-specialized public. Some situations were addressed where social
workers could use Debian GNU/Linux in their professional contexts.

Teachers and students from elementary/middle/high schools in partnership with
UFAL were also present. We would like to thank Professor Telma Sasso for the
opportunity! We hope that students and social workers alike be interested in
and be involved with our [Debian Brazil community](https://debianbrasil.org.br). 
You can watch (in portuguese) [here](https://www.youtube.com/watch?v=PwPZ3rImW84) 
![Seminário UFAL](https://debianbrasil.org.br/blog/imagens/seminario-online-ufal.jpg =400x)