---
title: Chamada de voluntários para ajudar durante a MiniDebConf Curitba 2017
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:57:49.898Z
tags: blog, geral
editor: markdown
dateCreated: 2017-03-06T13:29:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Estamos com uma chamada de voluntários aberta para quem quiser ajudar durante os dias da MiniDebConf Curitiba 2017.

Tem várias tarefas que precisaremos de ajuda, então basta preencher o formulário.