---
title: Atividades Debian Brasil na Latinoware 2016
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:57:11.457Z
tags: blog, geral
editor: markdown
dateCreated: 2016-09-24T23:56:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A Comunidade Debian Brasil participará da
[13ª Conferência Latino-americana de Software Livre - Latinoware](http://latinoware.org/), que acontecerá de 19 a 21 de outubro de 2016 no Parque Tecnológico Itaipu em
Foz do Iguaçu - PR.

Este ano, as atividades que estão inseridas na trilha "MiniDebConf" da
Latinoware estão distribuídas da seguinte maneira:

### 19/10/2016 (quarta-feira)

**14:00 - Oficina: Ensinando e praticando o "transplante" de uma instalação
Debian GNU/Linux.**

- Paulo Oliveira Kretcheu.

**16:00 - Palstra: Bastidores Debian: entenda como a distribuição funciona.**

- João Eriberto Mota Filho.
- [Apresentação em pdf](http://www.eriberto.pro.br/palestras/bastidores_debian.pdf) 
**17:00 - Palestra: Debian - um universo em construção.**

- Giovani Augusto Ferreira.
- [Apresentação em pdf](/apresentacoes-latinoware-2016/giovani-debian-um-universo-em-construcao.pdf) 
### 20/10/2016 (quinta-feira)

**11:00 - Palestra: Instalando um GNU Debian 100% livre.**

- Alessandro Moura.
- [Apresentação em pdf](/apresentacoes-latinoware-2016/alessandro-instalando-um-gnu-debian-100-livre.pdf) 
**12:00 - Palestra: Entrando para o Time de Tradução do Debian.**

- Daniel Lenharo.
- [Apresentação em pdf](/apresentacoes-latinoware-2016/daniel-entrando-para-o-time-de-traducao-do-debian.pdf) 
**13:00 - Palestra: Organizando eventos no Debian.**

- Daniel Lenharo e Giovani Ferreira
- [Apresentação em pdf](/apresentacoes-latinoware-2016/giovani-daniel-organizando-eventos-no-debian.pdf) 
**14:00 - Lightning Talks (palestras de até 10 minutos)**

- 14:00 - Entenda a Debian Policy - Thadeu Cascardo.
- 14:10 - Conheça o Debian Forensics Team - Giovani Augusto Ferreira.
- [Apresentação em pdf](/apresentacoes-latinoware-2016/giovani-conheca-o-debian-forensics-team.pdf)   - 14:20 - Ultimate debian database (udd) e debian.net: Um show de informações e curiosidades - João Eriberto Mota Filho. - [Apresentação em pdf](http://eriberto.pro.br/palestras/udd_debiannet.pdf)
- 14:30 - Novidades do APT (Advance Package Tool) - Raphael Mota.
- [Apresentação em pdf](/apresentacoes-latinoware-2016/raphael-novidades-do-apt.pdf) - 14:40 - Mulheres do meu Brasil, cadê vocês??? - João Eriberto Mota Filho.
- [Apresentação em pdf](http://eriberto.pro.br/palestras/mulheres_brasil.pdf)
- 14:50 -  Debian para quem quer ser grande - Gilmar dos Reis.
- [Apresentação em pdf](/apresentacoes-latinoware-2016/gilmar-debian-para-quem-quer-ser-grande.pdf) 
**15:00 - Palestra:** Conhecendo o Debian Bug Tracking System (BTS).

- Giovani Augusto Ferreira.
- [Apresentação em pdf](/apresentacoes-latinoware-2016/giovani-conhecendo-o-debian-bug-tracking-system.pdf) 
**16:00 - Palestra: Empacotamento de software no Debian.**

- João Eriberto Mota Filho.
- [Apresentação em pdf](http://www.eriberto.pro.br/palestras/empacotamento_debian.pdf) 
**17:00 - Painel: Eu, você e o Debian: como foi a minha entrada no Debian e como você poderá entrar.** 
- Todos.

Também acontecerão as seguintes palestras com a temática do Debian em outras trilhas: 
19/10/2016 (quarta-feira)
13:00 - Palestra: Libertas, uma personalização da Debian para as escolas de
Belo Horizonte. - Frederico Guimarães.

21/10/2016 (sexta-feira)
11:00 Palestra: Debian no seu desktop - faça parte da comunidade. - Albino
Blasutti Neto.

[latinoware.org](http://www.latinoware.org)