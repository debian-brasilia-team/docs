---
title: Meeting was held in Curitiba to organize MiniDebConf 2018 and BID 2019
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:34.832Z
tags: blog, english
editor: markdown
dateCreated: 2017-12-13T19:33:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Last Saturday (2017-12-09), some participants from the Debian community in
Curitiba met at the [Positivo University](http://www.up.edu.br/) to discuss the
organization of the
[MiniDebConf Curitiba 2018](https://minidebconf.curitiba.br/2018/en/) and update the [BID from Curitiba](https://wiki.debconf.org/wiki/DebConf19/Bids/Curitiba)
to DebConf19.

MiniDebConf Curitiba 2018 will take place on April from 11 to 14 at the central
campus of Federal University of Technology - Paraná (UTFPR). Registration is
free and we have a crowdfunding campaign to get funds to the event.

More infos: <https://minidebconf.curitiba.br/2018/en>

We are applying Curitiba for the second consecutive year to host an edition of
[DebConf - The Debian Developer Conference](https://debconf.org/). For this, it
is necessary to prepare a document called "BID" where are the details of the
candidacy to host DebConf19 that will happen in 2019.

More infos: <https://wiki.debconf.org/wiki/DebConf19/Bids/Curitiba>

The meeting was attended by:

-   Alisson Coelho
-   Antonio C. C. Marques
-   Antonio Terceiro
-   Cleber Ianes
-   Daniel Lenharo de Souza
-   Paulo Henrique de Lima Santana (phls)
-   Samuel Henrique

We also count on the help of Profa. Juliana Tibães and Prof. Marcelo Szostak who kindly booked the room for our meeting. 
![Reuniao minidc bid 2017 12 09 01](https://debianbrasil.org.br/blog/imagens/reuniao-minidc-bid-2017-12-09-01.jpg =400x) 
![Reuniao minidc bid 2017 12 09 08](https://debianbrasil.org.br/blog/imagens/reuniao-minidc-bid-2017-12-09-08.jpg =400x)