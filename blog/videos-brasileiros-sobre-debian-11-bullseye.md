---
title: Vídeos brasileiros sobre o Debian 11 Bullseye
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:42:44.444Z
tags: blog, geral
editor: markdown
dateCreated: 2021-09-19T21:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Após o lançamento do Debian 11 no dia 14 de agosto de 2021 (e mesmo antes do
lançamento)  várias pessoas publicaram vídeos em português em seus canais no
YouTube  com reviews, tutoriais, dicas, comentários, etc, sobre a mais nova
versão estável (stable) conhecida como Bullseye.

Abaixo listamos os vídeos que achamos e que vale a pena você assitir, curtir
e compartilhar.

Canal Debian Brasil

* [Migrando do Buster para o Bullseye! Cuidados a serem tomados](https://www.youtube.com/watch?v=WOFRvWzEPo4) 
Canal Dois Digitus

* [Debian 11 Bullseye, instalação e overview](https://www.youtube.com/watch?v=DMjizNGMYcE) 
Canal Diolinux

* [Então, esse é o segredo da estabilidade do Debian?](https://www.youtube.com/watch?v=JK03ZcXYAoE) 
Canal Distro Hoper

* [Debian 11, como sempre no alvo!!!](https://youtu.be/8kDRleFvOTM)

Canal Eriberto Mota

* [Atualização para o Debian 11 Bullseye via linha de comando](https://www.youtube.com/watch?v=5VtX6LQFTpE) 
Canal Leandro Ramos

* [Atualização do Debian - Buster para Bullseye](https://youtu.be/Jt-Lb_dkSEU)
* [Tethering USB, brincadeiras com Debian Sid e outras coisas](https://www.youtube.com/watch?v=yHrVarnDXx4) * [Todos os ambientes do Debian Bullseye](https://www.youtube.com/watch?v=ZnGm0WNddgY) 
Canal Paulo Kretcheu

* [Atualizando Debian - curso GNU Linux - Lab GNU #32](https://www.youtube.com/watch?v=M4pMtzh8Ggg) 
Canal Rikerlinux

* [Debian 11 o segredo do rei da estabilidade](https://www.youtube.com/watch?v=SenijTZTPNI) * [Debian 11 review - dessa vez com Kde Plasma](https://www.youtube.com/watch?v=zFDbSwqltcY) 
Canal zer01ti

* [Como fazer uma customização simples no Debian 11 com XFCE](https://www.youtube.com/watch?v=qguNemHvbG4) * [Como instalar o Debian 11 server](https://www.youtube.com/watch?v=a-P8iP_GBiM) * [Upgrade Debian 10 para o novo Debian 11](https://www.youtube.com/watch?v=9mbkZ7lzxwk) * [Debian 11 testing já está bom?](https://www.youtube.com/watch?v=Sv8KDqy40P8)


Veja também alguns textos publicados no blog Linux Dicas e Suporte

* <https://linuxdicasesuporte.blogspot.com/2021/09/adicionais-para-convidado-do-virtualbox.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/kde-plasma-sempre-atual-no-debian-11.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/aberto-o-desenvolvimento-do-debian-gnu.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/debian-gnu-linux-11-bullseye-ubuntuzado.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/instalar-o-virtualbox-no-debian-11.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/programas-mais-atuais-no-debian-gnu.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/sourceslist-completa-para-distribuicao.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/resolver-erro-possible-missing-firmware.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/driver-mesa-3d-na-distribuicao-gnu.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/fonts-microsoft-e-alternativas-livre.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/plugins-e-codecs-free-para-debian-11.html> * <https://linuxdicasesuporte.blogspot.com/2021/08/lancamento-da-distribuicao-gnu-linux.html> * <https://linuxdicasesuporte.blogspot.com/2021/07/o-que-voce-precisa-saber-para-usar-o.html> * <https://linuxdicasesuporte.blogspot.com/2021/07/customizando-o-xfce-416-do-debian-11.html> * <https://linuxdicasesuporte.blogspot.com/2021/07/lancamento-do-debian-gnu-linux-11.html> 

![Debian 11 Bullseye](https://debianbrasil.org.br/blog/imagens/debian-11-bulseye.jpg =800x)
