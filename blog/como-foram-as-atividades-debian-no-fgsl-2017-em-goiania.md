---
title: Como foram as atividades Debian no FGSL 2017 em Goiânia
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:58:08.404Z
tags: blog, geral
editor: markdown
dateCreated: 2017-11-21T20:07:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

![Banner fgsl debian](https://debianbrasil.org.br/blog/imagens/banner-fgsl-debian.png =400x)

Nos dias 17 e 18 de novembro de 2017 aconteceu o
[XIV Fórum Goiano de Software Livre (FGSL)](http://2017.fgsl.net/) no Campus da
Universidade Federal de Goias (UFG) em Goiânia.

A Comunidade Debian esteve presente no FGSL por meio de 3 palestras:

 **Controle de anomalias e bloqueio de ataques em redes em tempo real**

- [João Eriberto Mota Filho](http://eriberto.pro.br)
- [Arquivo](http://www.eriberto.pro.br/palestras/controle-anomalias-rede.pdf)

**O Projeto Debian quer você!**

- [Paulo Henrique de Lima Santana](http://phls.com.br/)
- [Arquivo](https://gitlab.com/phls/arquivos-de-apresentacoes/-/blob/master/2017-11-18-FGSL-o-projeto-debian-quer-voce.pdf) 
**Bastidores Debian: entenda como a distribuição funciona**

- [João Eriberto Mota Filho](http://eriberto.pro.br)
- [Arquivo](http://www.eriberto.pro.br/palestras/bastidores_debian.pdf)

Antes do evento a organização publicou diversos banners nas redes sociais para
ajudar na divulgação das atividades confirmadas, como estes abaixo:

![Banner Eriberto](https://debianbrasil.org.br/blog/imagens/banner-fgsl-eriberto-1.png =400x)

![Banner Paulo](https://debianbrasil.org.br/blog/imagens/banner-fgsl-paulo.png =400x)

Além das palestras, a comunidade Debian teve uma mesa para conversar com os
participantes do evento e vender os produtos da
[loja da comunidade Curitiba Livre](http://loja.curitibalivre.org.br/) e que o
lucro é revertido para organizar eventos de software livre em Curitiba.

Abaixo mesa da comunidade Debian com Rodrigo Troian, Paulo Santana e Christiane
Borges - foto de Álvaro Justen (CC BY-SA 2.0)

![Mesa da comunidade Debian](https://debianbrasil.org.br/blog/imagens/mesa-debian-01.jpg =400x)

Abaixo produtos na mesa da comunidade Debian - foto de Álvaro Justen (CC BY-SA 2.0) 
![Mesa da comunidade Debian com produtos](https://debianbrasil.org.br/blog/imagens/mesa-debian-03.jpg =400x) 
Abaixo Alice, filha do Prof. Fábio - foto de Álvaro Justen (CC BY-SA 2.0)

![Alice](https://debianbrasil.org.br/blog/imagens/menina.jpg =400x)

Abaixo Paulo palestrando sobre "O Projeto Debian quer você!" - foto de Álvaro
Justen (CC BY-SA 2.0)

![Palestra Paulo](https://debianbrasil.org.br/blog/imagens/paulo-palestra-01.jpg =400x)

Abaixo Eriberto palestrando sobre "Bastidores Debian: entenda como a
distribuição funciona" - foto de Paulo Santana (CC BY-SA 4.0)

![Palestra Eribeto](https://debianbrasil.org.br/blog/imagens/eribeto-palestra-01.jpg =400x)

Abaixo alguns participantes do XIV FGSL - foto de Álvaro Justen (CC BY-SA 2.0)

![Foto todos no FGSL](https://debianbrasil.org.br/blog/imagens/foto-fgsl-todos.jpg =400x)

Veja mais fotos aqui:
<https://www.flickr.com/photos/debianbrasil/albums/72157716020023033>

Deixamos nossos agradecimentos a organização do XIV FGSL pelo convite, a ajuda
e o acolhimento que recebemos, em especial a Profa. Christiane Borges e ao Prof. Marcelo Akira. Parabenizamos a todos os envolvidos na organização pelo sucesso
de mais essa edição do FGSL.