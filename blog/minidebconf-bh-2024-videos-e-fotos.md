---
title: MiniDebConf BH 2024 - vídeos e fotos
description: por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:41:00.112Z
tags: blog, geral
editor: markdown
dateCreated: 2024-05-08T10:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

Os vídeos das palestras MiniDebConf BH 2024 estão disponíveis nos links abaixo:

* [Youtube](https://www.youtube.com/playlist?list=PLU90bw3OpxLpu7hJO8TzySFX32_UD8Eh4) * [Peertube](https://peertube.debian.social/w/p/3X7BY1MyH686QNCS2A6wkd)
* [Arquivos](https://meetings-archive.debian.net/pub/debian-meetings/2024/MiniDebConf-Belo-Horizonte) 
As fotos do evento estão nos links abaixo:

* [Google photos](https://photos.app.goo.gl/ZV14bZ9AQFaKsgio6)
* [Nextcloud](https://cloud.debianbsb.org/s/wgzBtzKX2kbLCYP)

E os arquivos usados nas apresentações estão aqui:

* [Repositório](https://salsa.debian.org/debconf-team/public/mini/belo-horizonte/-/tree/main/arquivos-das-apresentacoes) 
Em breve divulgaremos um relato sobre o evento, aguardem :-)
