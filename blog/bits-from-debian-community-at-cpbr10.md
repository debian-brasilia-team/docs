---
title: Bits from Debian Community at CPBR10
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:57:31.734Z
tags: blog, english
editor: markdown
dateCreated: 2017-02-10T18:18:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

[Campus Party Brasil 2017 - CPBR10](http://brasil.campus-party.org/) was held
from january 31st to february 4th 2017 at Anhembi in São Paulo, and during the
event the Debian community performed several activities.

On february 1st (Wednesday) [Karen Sandler,](https://nm.debian.org/person/karen) one of main internacional guests, talked on the main stage with the title
"[Cyborgs Unite!](https://sfconservancy.org/news/2017/feb/09/karen-campus-party-brasil/)". Karen is Debian Developer and
[Software Freedom Conservancy's](https://sfconservancy.org) Executive Director.

![Karen cpbr10](https://debianbrasil.org.br/blog/imagens/karen-cpbr10.jpg =400x)

On february 2nd (Thursday)
[Samuel Henrique](https://contributors.debian.org/contributor/samueloph/)
gave a workshop called "[Firts steps with Debian
(and derivatives)](http://campuse.ro/events/campus-party-brasil-2017/workshop/primeiros-passos-debian-e-derivados/)". Samuel is currently [Debian Maintainer](https://nm.debian.org/person/samueloph). 
![Samuel cpbr10](https://debianbrasil.org.br/blog/imagens/samuel-cpbr10.jpg =400x)

On february 3rd (Friday) was
[Daniel Lenharo's](https://contributors.debian.org/contributor/lenharo)
turn to present the lecture
"[Debian - The Universal Operation System](http://campuse.ro/events/campus-party-brasil-2017/talk/debian-o-sistema-universal)". Daniel is currently applying to be
[Debian Developer](https://nm.debian.org/person/lenharo).

![Daniel cpbr10](https://debianbrasil.org.br/blog/imagens/daniel-cpbr10.jpg =400x)

The last activite was a panel on february 3rd (friday) called
"[Debian - Listening experiences and contributing to the project](http://campuse.ro/events/campus-party-brasil-2017/talk/painel-debian-no-brasil)", with Sanuel Henrique, Daniel Lenharo and
[Paulo Henrique de Lima Santana](https://contributors.debian.org/contributor/phls-guest%40alioth/). Paulo is currently [Debian Maintainer](https://nm.debian.org/person/phls).

![Daniel samuel paulo](https://debianbrasil.org.br/blog/imagens/damiel-samuel-paulo.jpg =400x)

Besides the activities on the stages, the community was selling t-shirts and
stickers. The money from sales will be used by
[Comunidade Curitiba Livre](http://curitibalivre.org.br/) to organize others
free software events.

[Wellton Costa](http://campuse.ro/events/vire-um-curador-na-cpbr10-votos/talk/seja-dono-de-verdade-seu-libreoffice-online-e-sua-propria-nuvem-com-nextcloud), [Thiago Mendonça](http://campuse.ro/events/campus-party-brasil-2017/workshop/sincronizacao-e-backup-de-arquivos-com-software-livre/), Samuel Henrique and Paulo Kretcheu installed Debian at laptops from people that
went to our table to ask for help. Every year we use to promote a install fest
on the tables to all interested people to have GNU/Linux at Campus Party Brasil. 
![Bancadas cpbr10](https://debianbrasil.org.br/blog/imagens/bancadas-cpbr10.jpg =400x)

There was also the advertising of

![Adesivos cpbr10](https://debianbrasil.org.br/blog/imagens/adesivos-cpbr10.jpg =400x)

![Camisetas cpbr10](https://debianbrasil.org.br/blog/imagens/camisetas-cpbr10.jpg =400x)

Curitiba is [running to host DebConf18](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba), so we did some t-shirts to promote an action called
[Brasil loves Debian](http://debian.softwarelivre.org/). While Campus Party was
happening, we sent some photos to IRC channel \#debconf-team.

See some photos:

![Samuel daniel barbara paulo cpbr10](https://debianbrasil.org.br/blog/imagens/samuel-daniel-barbara-paulo-cpbr10.jpg =400x) 
![Samuel paulo wellton cpbr10](https://debianbrasil.org.br/blog/imagens/samuel-paulo-wellton-cpbr10.jpg =400x) 
![Thiago ana cpbr10](https://debianbrasil.org.br/blog/imagens/thiago-ana-cpbr10.jpg =400x)

Jon Maddog recorded a message to support Curitiba Bid:

You can see all photos on the album:

<https://www.flickr.com/photos/slcampusparty/albums/72157676339619643>
