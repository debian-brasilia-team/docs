---
title: 1º Encontro Debian Women - Curitiba, 10 de Março de 2018
description: por Miriam Retka
published: true
date: 2025-03-01T17:56:46.737Z
tags: blog, geral
editor: markdown
dateCreated: 2018-03-17T19:04:00.000Z
---

<!-- author: Miriam Retka -->

Foi realizado na Pipefy, no dia 10 de março de 2018, o [primeiro encontro do Debian Women Curitiba](https://debianwomenbr.github.io/).

Foi um evento gratuito com palestras sobre o que é software livre, [Debian](https://www.debian.org/) e também foi falado sobre o programa de estágio internacional [Outreachy](https://www.outreachy.org/).

Também foi feito um install fest no fim do evento. O encontro foi organizado por Kira Oliveira, Miriam Retka e Renata D'Ávila. O evento foi voltado para mulheres cis e trans e pessoas não-binárias.

Este foi o primeiro encontro do Debian Women no Brasil. Até então só haviam os grupos internacionais.

A coordenação do evento ficou contente com o público presente no dia do encontro e registrou o evento [na Debian Wiki](https://wiki.debian.org/Brasil/Eventos/1oEncontroDebianWomenCuritiba).

## Atividades

Manhã

-   09:00 - Boas-vindas e café da manhã
-   10:00 - O que é Software Livre? Copyright, licenças, compartilhamento
-   10:30 - O que é Debian?
-   12:00 - Almoço

Tarde

-   14:30 - Estágio com Debian - Outreachy e Google Summer of Code
-   15:00 - Install fest / ajuda com problemas
-   16:00 - Como fazer edição da wiki do Debian
-   17:30 - Encerramento
