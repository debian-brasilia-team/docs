---
title: #FiqueEmCasaUseDebian: it was 35 days with talks, translations and packaging
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:20:25.852Z
tags: blog, english
editor: markdown
dateCreated: 2020-06-18T15:26:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

The **Debian Brasil community** organized from May 3rd to Jun 6th, 2020, an
online event called
[#FiqueEmCasaUseDebian](https://debianbrasil.gitlab.io/FiqueEmCasaUseDebian/)
(#StayHomeUseDebian). During 27 nights, the DDs Daniel Lenharo and Paulo Santana hosted guests who shared their knowledge about Debian. The event was initially
planned to finish on May 30, but it was extended for one more week due to
attendees' request. The format of the event was inspired on
[#FiqueEmCasaConf](https://github.com/linuxtips/FiqueEmCasaConf) which took
place in April, organized by Jeferson Fernando from
[LINUXtips](https://www.linuxtips.io/).

## Statistics

There were 27 activities streamed online, one per night, which are available on
Debian Brasil's
[Youtube](https://www.youtube.com/playlist?list=PLU90bw3OpxLo18CzFm3bEm9mNHdf8e2Ra) channel and on
[Peertube](https://peertube.debian.social/my-account/video-playlists/b6dccae1-aad6-4e21-a2c5-ce108b4c5fd7). The 21 people who shared their knowledge in those activities produced
approximately 35 hours of content about themes related to Debian. Until the
closing session, the Youtube channel had registered:

- 723 new subscribers, 1210 subscribers in total.
- 12,4k views.
- 3,4k unique viewers.
- Viewers from 12 different countries.

We believe a good amount of viewers from other countries are likely due to the
talk given in English by the current DPL Jonathan Carter entitled
["A mixed bag of Debian"](https://www.youtube.com/watch?v=Zu6ig8lIw3k).

## l10n pt-BR Sprint

The Brazilian Portuguese localization team
([debian-l10n-portuguese](https://wiki.debian.org/Brasil/Traduzir)) took
advantage of the **#FiqueEmCasaUseDebian** to make a call for new contributors
to help to translate the debian.org website. A
[l10n pt-BR sprint](https://wiki.debian.org/Sprints/2020/l10n-portuguese) was
held on May 28th and 29th, in the context of the first
[MiniDebCamp online](https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline). 
The [debian-l10n-portuguese mailing list](https://lists.debian.org/debian-l10n-portuguese) has not seen many action for a while, and since May 1 until June 8 there were
778 new messages with different questions around the translate process,
suggestions, explanations, and so on.

The outcome was **4 new translated pages and 64 pages with translations updated**. The number of translations updated is greater than new translated pages because
of the team's decision to focus on lowering the number of outdated translated
pages.

Join our team and help us improve the Debian Project!

## Packaging Workshop

During the **\#FiqueEmCasaUseDebian** a group of 5 Brazilian DDs organized an
online packaging workshop to teach interested people how to package software to
Debian. The participants made their first contributions to the project and
hopefully they will keep it up!

In 5 Saturdays was held online meetings via Jitsi to teach packaging procedures. After that, during week days were sent some tasks to the participants (like a
"homework"), and every night at least one of the DDs was available in our Jitsi
videoconference room to answer any question that might come up.

The statistics of this packaging workshop was considered really good by the
organizers. It was a huge success! Check it out:

- [36 attendees](https://udd.debian.org/cgi-bin/new-maintainers.cgi) with at
least one upload each.
- 70 packages changed.
- 41 packages with DEP-8 tests added (CI team thanks)
- 18 bugs fixed.
- 143 package uploads.
- 3 packages were removed from the Debian archive due to licenses not being
DFSG-compliant (investigated by the participants during the workshop)

We hope future DMs and DDs come up from this group of great people who engaged
with us in this endeavor! Let's keep Debian as the universal operating system :) 
## Final Words

The goal of **\#FiqueEmCasaUseDebian** was to bring good content to people who
is struggling with this hard moment of social distancing due to COVID-19
pandemic. If we need to stay home the maximum we can, we might take advantage
of that and try to learn something new, chat online with old and new friends.
We hope this event had helped us to stay together around Debian.

Stay home, stay safe!

![](https://debianbrasil.org.br/blog/imagens/assista.png =400x)
