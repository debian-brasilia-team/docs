---
title: MiniDebConf Belo Horizonte 2024 - um breve relato
description: por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:40:57.803Z
tags: blog, geral
editor: markdown
dateCreated: 2024-05-20T09:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

De 27 a 30 de abril de 2024 foi realizada a
[MiniDebConf Belo Horizonte 2024](https://bh.mini.debconf.org/) no Campus
Pampulha da [UFMG - Universidade Federal de Minas Gerais](https://ufmg.br),
em Belo Horizonte - MG.

![MiniDebConf BH 2024 banners](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-banners.jpg =400x) 
Esta foi a quinta vez que uma MiniDebConf (como um evento presencial exclusivo
sobre Debian) aconteceu no Brasil. As edições anteriores foram em Curitiba
([2016](https://br2016.mini.debconf.org/),
[2017](https://br2017.mini.debconf.org), e
[2018](https://br2018.mini.debconf.org)), e em
[Brasília 2023](https://brasilia.mini.debconf.org). Tivemos outras edições
de MiniDebConfs realizadas dentro de eventos de Software Livre como o
[FISL](http://fisl.org.br) e a [Latinoware](https://latinoware.org/), e
outros eventos online. Veja o nosso
[histórico de eventos](https://bh.mini.debconf.org/evento/edicoes-anteriores/).

Paralelamente à MiniDebConf, no dia 27 (sábado) aconteceu o
[FLISOL - Festival Latino-americano de Instalação de Software Livre](https://flisol.info/FLISOL2024/Brasil), maior evento da América Latina de divulgação de Software Livre realizado desde o ano de 2005 simultaneamente em várias cidades.

A MiniDebConf Belo Horizonte 2024 foi um sucesso (assim como as edições
anteriores) graças à participação de todos(as), independentemente do nível de
conhecimento sobre o Debian. Valorizamos a presença tanto dos(as) usuários(as)
iniciantes que estão se familiarizando com o sistema quanto dos(as)
desenvolvedores(as) oficiais do projeto. O espírito de acolhimento e colaboração esteve presente em todos os momentos.

![MiniDebConf BH 2024 flisol](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-flisol.jpg =400x) 
## Números da edição 2024

Durante os quatro dias de evento aconteceram diversas atividades para todos os
níveis de usuários(as) e colaboradores(as) do projeto Debian. A
[programação oficial](https://bh.mini.debconf.org/schedule) foi
composta de:

- 06 salas em paralelo no sábado;
- 02 auditórios em paralelo na segunda e terça-feira;
- 30 palestras/BoFs de todos os níveis;
- 05 oficinas para atividades do tipo mão na massa;
- 09 lightning talks sobre temas diversos;
- 01 performance Live Eletronics com Software Livre;
- Install fest para instalar Debian nos notebook dos(as) participantes;
- BSP (Bug Squashing Party - Festa de Caça à Bugs);
- Uploads de pacotes novos ou atualizados.

![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-palestra-1.jpg =400x) 
Os números finais da MiniDebConf Belo Horizonte 2024 mostram que tivemos um
recorde de participantes.

- Total de pessoas inscritas: 399
- Total de pessoas presentes: 224

Dos 224 participantes, 15 eram contribuidores(as) oficiais brasileiros sendo 10
DDs (Debian Developers) e 05 (Debian Maintainers), além de diversos(as)
contribuidores(as) não oficiais.

A organização foi realizada por 14 pessoas que começaram a trabalhar ainda no
final de 2023, entre elas o Prof. Loïc Cerf do Departamento de Computação que
viabilizou o evento na UFMG, e 37 voluntários(as) que ajudaram durante o evento. 
Como a MiniDebConf foi realizado nas instalações da UFMG, tivemos a ajuda de
mais de 10 funcionários da Universidade.

Veja a [lista](https://bh.mini.debconf.org/evento/organizadores/) com os nomes
das pessoas que ajudaram de alguma forma na realização da MiniDebConf Belo
Horizonte 2024.

A diferença entre o número de pessoas inscritas e o número de pessoas presentes
provavelmente se explica pelo fato de não haver cobrança de inscrição, então se
a pessoa desistir de ir ao evento ela não terá prejuízo financeiro.

A edição 2024 da MiniDebconf Belo Horizonte foi realmente grandiosa e mostra o
resultado dos constantes esforços realizados ao longo dos últimos anos para
atrair mais colaboradores(as) para a comunidade Debian no Brasil. A cada edição
os números só aumentam, com mais participantes, mais atividades, mais salas, e
mais patrocinadores/apoiadores.

![MiniDebConf BH 2024 grupo](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-grupo-1.jpg =400x) <br />
<br />
![MiniDebConf BH 2024 grupo](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-grupo-2.jpg =400x) 
## Atividades

A programação da MiniDebConf foi intensa e diversificada. Nos dias 27, 29 e 30
(sábado, segunda e terça-feira) tivemos palestras, debates, oficinas e muitas
atividades práticas.

![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-palestra-2.jpg =400x) 
Já no dia 28 (domingo), ocorreu o Day Trip, um dia dedicado a passeios pela
cidade. Pela manhã saímos do hotel e fomos, em um ônibus fretado, para o
[Mercado Central de Belo Horizonte](https://mercadocentral.com.br/). O pessoal
aproveitou para comprar várias coisas como queijos, doces, cachaças e
lembrancinhas, além de experimentar algumas comidas locais.

![MiniDebConf BH 2024 mercado](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-mercado.jpg =400x) 
Depois de 2 horas de passeio pelo Mercado, voltamos para o ônibus e pegamos a
estrada para almoçarmos em um restaurante de comida típica mineira.

![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-restaurante.jpg =400x) 
Com todos bem alimentados, voltamos para Belo Horizonte para visitarmos o
principal ponto turístico da cidade: a Lagoa da Pampulha e a Capela São
Francisco de Assis, mais conhecida como
[Igrejinha da Pampulha](http://portalbelohorizonte.com.br/o-que-fazer/arte-e-cultura/igrejas/igreja-sao-francisco-de-assis). 
![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-igrejinha.jpg =400x) 
Voltamos para o hotel e o dia terminou no _hacker space_ que montamos na sala
de eventos para o pessoal conversar, empacotar, e comer umas pizzas.

![MiniDebConf BH 2024 palestra](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-hotel.jpg =400x) 
## Financiamento coletivo

Pela terceira vez fizemos uma campanha de financiamento coletivo e foi incrível
como as pessoas contribuíram! A meta inicial era arrecadar o valor equivalente
a uma cota ouro de R$ 3.000,00. Ao atingirmos essa meta, definimos uma nova,
equivalente a uma cota ouro + uma cota prata (R$ 5.000,00). E novamente
atingimos essa meta. Então propusermos como meta final o valor de uma cota
ouro + prata + bronze, que seria equivalente a R$ 6.000,00. O resultado foi que
arrecadamos R$ 7.239,65 com a ajuda de mais de 100 pessoas!

Muito obrigado as pessoas que contribuíram com qualquer valor. Como forma de
agradecimento,
[listamos os nomes das pessoas que doaram](https://bh.mini.debconf.org/doacoes/). 
![MiniDebConf BH 2024 doadores](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-doadores.jpg =400x) 
## Bolsas de alimentação, hospedagem e/ou passagens para participantes

Cada edição da MiniDebConf trouxe alguma inovação, ou algum benefício diferente
para os(a) participantes. Na edição deste ano em Belo Horizonte, assim como
acontece nas DebConfs,
[oferecemos bolsas de alimentação, hospedagem e/ou passagens](https://bh.mini.debconf.org/evento/bolsas/) para ajudar aquelas pessoas que gostariam de vir para o evento mas que
precisariam de algum tipo de ajuda.

No formulário de inscrição, colocamos a opção para a pessoa solicitar bolsa de
alimentação, hospedagem e/ou passagens, mas para isso, ela deveria se
identificar como contribuidor(a) (oficial ou não oficial) do Debian e
escrever uma justificativa para o pedido.

Número de pessoas beneficiadas:

- Alimentação: 69
- Hospedagem: 20
- Passagens: 18

A bolsa de alimentação forneceu almoço e jantar todos os dias. Os almoços
incluíram pessoas que moram em Belo Horizonte e região. Já o jantares foram
pagos para os(as) participantes que também receberam a bolsa de hospedagem e/ou
passagens. A hospedagem foi realizada no
[Hotel BH Jaraguá](https://www.bhjaraguahotel.com.br/). E as passagens incluíram de avião ou de ônibus, ou combustível (para quem veio de carro ou moto).

Boa parte do dinheiro para custear as bolsas vieram do Projeto Debian,
principalmente para as passagens. Enviamos um orçamento o então líder do Debian
Jonathan Carter, e ele prontamente aprovou o nosso pedido.

Além deste orçamento do evento, o líder também aprovou os pedidos individuais
enviados por alguns DDs que preferiram solicitar diretamente para ele.

A experiência de oferecer as bolsas foi realmente muito boa porque permitiu
a vinda de várias pessoas de outras cidades.

![MiniDebConf BH 2024 grupo](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-jantar.jpg =400x) 
## Fotos e vídeos

Você pode assistir as gravações das palestras nos links abaixo:

- [Youtube](https://www.youtube.com/playlist?list=PLU90bw3OpxLpu7hJO8TzySFX32_UD8Eh4) - [Peertube](https://peertube.debian.social/w/p/3X7BY1MyH686QNCS2A6wkd)
- [video.debian.net](https://meetings-archive.debian.net/pub/debian-meetings/2024/MiniDebConf-Belo-Horizonte/) 
E ver as fotos feitas por vários(as) colaboradores(as) nos links abaixo:

- [Google photos](https://photos.app.goo.gl/ZV14bZ9AQFaKsgio6)
- [Nextcloud](https://cloud.debianbsb.org/s/wgzBtzKX2kbLCYP)

## Agradecimentos

Gostaríamos de agradecer a todos(as) os(as) participantes, organizadores(as),
voluntários(as), patrocinadores(as) e apoiadores(as) que contribuíram para o
sucesso da MiniDebConf Belo Horizonte 2024.

![MiniDebConf BH 2024 grupo](https://debianbrasil.org.br/blog/imagens/minidebconf-bh-2024-ate-2025.jpg =400x) 
## Patrocinadores

**Ouro:**

- [Collabora](https://www.collabora.com/)

**Prata:**

- [Jedai.ai](https://jedai.ai/)
- [Toradex Brasil](https://www.toradex.com/pt-br)
- [Globo.com](https://www.globo.com)
- [Policorp Tecnologia](https://policorp.com.br)

**Bronze:**

- [BRDSoft - Tecnologias para TI e Telecomunicações](https://brdsoft.com.br/)
- [EITA - Cooperativa de Trabalho Educação, Informação e Tecnologia para Autogestão](https://eita.coop.br/) 
## Apoiadores

- [ICTL - Instituto para Conservação de Tecnologias Livres](https://ictl.org.br) - [Nazinha Alimentos](https://www.nazinha.com.br/)
- DACOMPSI

## Organização

- [Projeto Debian](https://www.debian.org/)
- [Comunidade Debian Brasil](https://debianbrasil.org.br/)
- [Comunidade Debian MG](https://debian-minas-gerais.gitlab.io/site/)
- [DCC/UFMG - Departamento de Ciência da Computação da Universidade Federal de Minas Gerais](https://dcc.ufmg.br/) 