---
title: Report from sprint DDTP-DDTSS by l10n-pt-br team
description: by Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:42:10.069Z
tags: blog, english
editor: markdown
dateCreated: 2021-04-11T11:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

The Debian Brazilian Portuguese (l10n-pt-BR) translation team organised a
[sprint](https://wiki.debian.org/Brasil/Traduzir/Campanhas/SprintDDTP) from March 20th to 28th, 2021 to translate Debian package descriptions and to encourage new contributors to join the team. This is the second consecutive
year that a sprint is organized by the l10n-pt-BR translation team . The
[first one](https://wiki.debian.org/Sprints/2020/l10n-portuguese) took place in
May 2020 when the team focused on translating the debian.org website, which
resulted in the translation/update of all main pages in the following months.

The first group activity was a virtual meeting using Jitsi for a quick
introduction of the l10n-pt-BR team. Then, the veterans introduced the
newcomers to
[DDTP - The Debian Description Translation Project] (https://www.debian.org/international/l10n/ddtp) and did a hands-on presentation of
[DDTSS - The Debian Distributed Translation Server Satellite](https://ddtp.debian.org/ddtss/index.cgi/pt_BR). During the following week, two other meetings took place with the same goals
for those who could not join the previous meeting.

During the sprint days, more than 100 new translations and more than 650
reviews were done by  15 people, whom we thank very much for their
contributions and list their names below:

* Alexandro Souza
* Carlos Henrique Lima Melara
* Daniel Lenharo de Souza
* Felipe Viggiano
* Fred Maranhão
* Gabriel Thiago H. dos Santos
* Luis Paulo Linares
* Michel Recondo
* Paulo Henrique de Lima Santana
* Ricardo Berlim Fonseca
* Thiago Hauck
* Thiago Pezzo
* Tiago Zaniquelli
* Victor Moreira
* Wellington Almeida

To finish the sprint, we held one last virtual meeting to assess and discuss
about future
[plans](https://wiki.debian.org/Brasil/Traduzir/Campanhas/SprintDDTP#Encaminhamentos). 
It's never too late to start contributing to Debian! If you would like to help
with translation, read
[our page](https://wiki.debian.org/Brasil/Traduzir) and come join our team.
