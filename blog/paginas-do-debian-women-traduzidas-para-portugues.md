---
title: Páginas do Debian Women traduzidas para português
description: por Adriana Costa
published: true
date: 2025-03-01T17:41:37.829Z
tags: blog, geral
editor: markdown
dateCreated: 2016-11-16T00:30:00.000Z
---

<!-- author: Adriana Costa -->

As páginas do projeto Debian Women foram recentemente traduzidas para o
português em um esforço coletivo do time de tradução do Debian no Brasil.

Esperamos que isso também incentive mais mulheres brasileiras a participarem do
projeto.

Acesse em <https://www.debian.org/women/>

![Banner garota debian](https://debianbrasil.org.br/blog/imagens/banner-garota-debian.png =400x)

