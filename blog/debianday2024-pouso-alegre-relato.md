---
title: Debian Day 2024 em Pouso Alegre/MG - Brasil
description: por Thiago Pezzo (Tico), Giovani Ferreira
published: true
date: 2025-03-01T17:39:14.746Z
tags: blog, geral
editor: markdown
dateCreated: 2024-08-18T15:00:00.000Z
---

<!-- author: Thiago Pezzo (Tico), Giovani Ferreira -->

por [Thiago Pezzo](https://contributors.debian.org/contributor/tico@salsa/) e
[Giovani Ferreira](https://contributors.debian.org/contributor/giovani/)

As celebrações locais do [Dia do Debian 2024](https://wiki.debian.org/DebianDay/2024) também aconteceram em
[Pouso Alegre, MG, Brasil](https://www.openstreetmap.org/relation/315431). Neste ano conseguimos organizar dois dias de palestras!

No dia 14 de agosto de 2024, quarta-feira pela manhã, estivemos no campus Pouso
Alegre do
[Instituto Federal de Educação, Ciência e Tecnologia do Sul de Minas Gerais](https://portal.ifsuldeminas.edu.br/index.php) (IFSULDEMINAS). Fizemos a apresentação introdutória do [Projeto Debian](https://www.debian.org), sistema operacional e comunidade, para os três anos do Curso Técnico de Ensino
Médio em Informática. O evento foi fechado para o IFSULDEMINAS e estiveram
presentes por volta de 60 estudantes.

Já no dia 17 de agosto de 2024, um sábado pela manhã, realizamos o evento aberto à comunidade na [Universidade do Vale do Sapucaí](https://www.univas.edu.br/)
(Univás), com apoio institucional do Curso de Sistemas de Informação. Falamos
sobre o Projeto Debian com [Giovani Ferreira](https://contributors.debian.org/contributor/giovani/) (Debian Developer); sobre a [equipe de tradução Debian pt_BR](https://wiki.debian.org/Brasil/Traduzir/) com Thiago Pezzo; sobre experiências no dia a dia com uso de softwares livres
com Virgínia Cardoso; e sobre como configurar um ambiente de desenvolvimento
pronto para produção usando Debian e Docker com Marcos António dos Santos.
Encerradas as palestras, foram servidos salgadinhos, café e bolo, enquanto os/as participantes conversavam, tiravam dúvidas e partilhavam experiências.

Gostaríamos de agradecer a todas as pessoas que nos ajudaram:

- Michelle Nery (IFSULDEMINAS) e André Martins (UNIVÁS) pelo auxílio na organização local - Paulo Santana (Debian Brasil) pela organização geral
- Virgínia Cardoso, Giovani Ferreira, Marco António e Thiago Pezzo pelas palestras - E um agradecimento especial a todas e todos que participaram da nossa comemoração! 
Algumas fotos:

![Apresentação no campus Pouso Alegre do IFSULDEMINAS 1](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-ifsuldeminas1.jpg =400x) ![Apresentação no campus Pouso Alegre do IFSULDEMINAS 2](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-ifsuldeminas2.jpg =400x) ![Apresentação no campus Fática da UNIVÁS 1](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-univas1.jpg =400x) ![Apresentação no campus Fática da UNIVÁS 2](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-univas2.jpg =400x) ![Apresentação no campus Fática da UNIVÁS 3](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-univas3.jpg =400x) ![Apresentação no campus Fática da UNIVÁS 4](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-univas4.jpg =400x) 