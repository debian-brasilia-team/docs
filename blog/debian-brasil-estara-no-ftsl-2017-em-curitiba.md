---
title: Debian Brasil estará no FTSL 2017 em Curitiba
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:38:24.676Z
tags: blog, geral
editor: markdown
dateCreated: 2017-09-14T01:57:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

![Banner ftsl 2017 469x61](https://debianbrasil.org.br/blog/imagens/banner-ftsl-2017-469x61.png)

De 27 a 29 de setembro de 2017 acontecerá o
[IX Fórum de Tecnologia em Software Livre - FTSL](http://ftsl.org.br), no
Campus central da
[Universidade Tecnológica Federal do Paraná - UTFPR](http://www.utfpr.edu.br/curitiba/o-campus/pasta2), mesmo local onde aconteceu a
[MiniDebConf Curitiba 2017](http://br2017.mini.debconf.org/) em março.

A inscrição para o FTSL é totalmente gratuita e dá acesso a todas as atividades. A [programação](https://ftsl.websiteseguro.com/ftsl9/grade/) do evento contará
com palestras, oficinas, painéis e encontros de comunidades, divididos em seis
salas e quatro laboratórios.

A comunidade [Debian Brasil](http://debianbrasil.org.br) participará do FTSL
por meio de uma trilha de palestras que acontecerão no dia 29 (sexta) das 10:00
às 17:00 na sala 4. Abaixo estão os detalhes das atividades:

**09:00**

- [Por que sou usuário do GNU/Linux/DEBIAN?](https://ftsl.websiteseguro.com/ftsl9/grade/detail.html?t=1505875049391&id=f426c77164f7052f97fdde247f54e2ea&type=1&pid=322&day=3) - Antonio C C Marques

**10:00**

- [Como se tornar um membro oficial do Debian (DD ou DM)](https://ftsl.websiteseguro.com/ftsl9/grade/detail.html?t=1505353768056&id=f426c77164f7052f97fdde247f54e2ea&type=1&pid=307&day=3) - João Eriberto Mota Filho

**11:00h**

- [Debian - O sistema universal](https://ftsl.websiteseguro.com/ftsl9/grade/detail.html?t=1505353768058&id=f426c77164f7052f97fdde247f54e2ea&type=1&pid=180&day=3) - Daniel Lenharo de Souza

**14:00h**

- [O Projeto Debian quer você!](https://ftsl.websiteseguro.com/ftsl9/grade/detail.html?t=1505353768059&id=f426c77164f7052f97fdde247f54e2ea&type=1&pid=260&day=3) - Paulo Henrique de Lima Santana

**15:00h**

- [Conhecendo o trabalho da equipe de tradução do Debian](https://ftsl.websiteseguro.com/ftsl9/grade/detail.html?t=1505353768060&id=f426c77164f7052f97fdde247f54e2ea&type=1&pid=262&day=3) - Daniel Lenharo de Souza

**16:00h**

- [debian 101](https://ftsl.websiteseguro.com/ftsl9/grade/detail.html?t=1505353768061&id=f426c77164f7052f97fdde247f54e2ea&type=1&pid=263&day=3) - Samuel Henrique


  ![Banner ftsl 2017 debian](https://debianbrasil.org.br/blog/imagens/banner-ftsl-2017-debian.png =400x)


