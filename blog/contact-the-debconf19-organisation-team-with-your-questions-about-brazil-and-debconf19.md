---
title: Contact the DebConf19 organisation team with your questions about Brazil and DebConf19
description: by Adriana Costa
published: true
date: 2025-03-01T17:58:15.055Z
tags: blog, english
editor: markdown
dateCreated: 2019-02-15T23:32:00.000Z
---

<!-- author: Adriana Costa -->

Hi Debian community!

We would like to clarify some issues related to Brazil and DebConf19. So, if
you have any concern about Brazil (food, security, immigration, people, and so
on) please don't be shy and send a message to us.

We will provide the information necessary to people that will attend DC19. The
answers will be available in the FAQ on the DC19 website
(<https://debconf19.debconf.org>) with the questions and answers without
identification the person who sent the question.

Please, don't wait to send your question few days before DebConf, send as soon
as possible. Send your message to <dc19questions@debconf.org>


