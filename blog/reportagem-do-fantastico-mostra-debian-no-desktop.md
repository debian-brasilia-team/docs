---
title: Reportagem do Fantástico mostra Debian no Desktop
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:42:16.832Z
tags: blog, geral
editor: markdown
dateCreated: 2013-04-01T21:35:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Durante uma reportagem exibida no Fantástico da Rede Globo no dia 24/03/2013
com o tema "Neurocientista mostra avanços em projeto para paraplégico andar
na Copa" é possível ver em um dos computadores do laboratório o papel de
parede do tema oficial da versão Squeezy do Debian GNU/Linux :-)

O tema se chama [SpaceFun](http://wiki.debian.org/DebianArt/Themes/SpaceFun)
e foi desenvolvida pelo nosso colega Valéssio Brito para o Debina 6.0.

Para ver, acesso o vídeo da reportagem. A imagem aparece aos 7min26s.

<http://g1.globo.com/fantastico/noticia/2013/03/neurocientista-mostra-avancos-em-projeto-para-paraplegico-andar-na-copa.html> 

![](https://debianbrasil.org.br/blog/imagens/desktop-com-debian.png =400x)


