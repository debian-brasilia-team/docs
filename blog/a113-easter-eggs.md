---
title: A113, "Easter Eggs"
description: por Francisco Aparecido da Silva
published: true
date: 2025-03-01T17:56:48.859Z
tags: blog, geral
editor: markdown
dateCreated: 2012-04-07T20:09:00.000Z
---

<!-- author: Francisco Aparecido da Silva -->

O Filme Toy Story é um filme de longa metragem, um dos primeiros da Pixar, totalmente feito em computação gráfica. É um dos meus favoritos. Perdi as contas das vezes que os vi. Por aqui, as crianças também preferem, por ser um desenho divertido e principalmente por trazer bastante conteúdo inteligente.

Na última vez que vi o filme, notei que a Van da Mãe de Andy (o menino dono dos brinquedos), possui a placa A 113, ou A1-13. Fiquei bastante curioso com isso, e imaginei logo um "Easter Egg", um tipo de piada que programadores colocam em programas, ou neste caso, colocado no filme.

Esta brincadeira inteligente faz parte de muitos filmes da [PIXAR](http://pt.wikipedia.org/wiki/Pixar) e este número, A113, faz menção à sala dos estudantes de design, também usado por John Lasseter e Brad Bird. Esta brincadeira aparece em várias partes do filme Toy Story e também em muitos outros filmes da [PIXAR](http://en.wikipedia.org/wiki/A113\#A113\_appearances)

Veja algumas das imagens ([crédito das imagens:](http://pixar.wikia.com/A113)):


[![](http://4.bp.blogspot.com/-yUbc1ym3HaQ/T4CuwGKReCI/AAAAAAABIQ8/yRP0unxqc2Q/s320/185px-Toy_Story_%2528Original%25291.jpg)](http://4.bp.blogspot.com/-yUbc1ym3HaQ/T4CuwGKReCI/AAAAAAABIQ8/yRP0unxqc2Q/s1600/185px-Toy_Story_%2528Original%25291.jpg)

[![](http://2.bp.blogspot.com/-tleRnZIvb8o/T4CuwZbqBuI/AAAAAAABIRI/cRVveTF0lLM/s320/185px-Toy_Story_2.jpg)](http://2.bp.blogspot.com/-tleRnZIvb8o/T4CuwZbqBuI/AAAAAAABIRI/cRVveTF0lLM/s1600/185px-Toy_Story_2.jpg)

[![](http://2.bp.blogspot.com/-nm5jtrLF1yE/T4CuwksdaqI/AAAAAAABIRU/Ay_onW7ubAs/s320/185px-Toystory2a113.png)](http://2.bp.blogspot.com/-nm5jtrLF1yE/T4CuwksdaqI/AAAAAAABIRU/Ay_onW7ubAs/s1600/185px-Toystory2a113.png)

[![](http://4.bp.blogspot.com/-702HN0imkbA/T4Cuw4keLBI/AAAAAAABIRg/K2gMkGyEHLY/s320/185px-Toy_Story_3.jpg)](http://4.bp.blogspot.com/-702HN0imkbA/T4Cuw4keLBI/AAAAAAABIRg/K2gMkGyEHLY/s1600/185px-Toy_Story_3.jpg)

Fonte: [Fafanet's blog](https://blog.silva.eti.br/2012/04/a113-easter-eggs.html)
