---
title: Debian Brasil at Cryptorave 2017
description: by Daniel Lenharo de Souza
published: true
date: 2025-03-01T17:38:15.671Z
tags: blog, english
editor: markdown
dateCreated: 2017-05-09T12:04:00.000Z
---

<!-- author: Daniel Lenharo de Souza -->

The House of the People (Casa do Povo) received during the days 5th and 6th of
May the 2017 edition of [Cryptorave](https://cryptorave.org), inspired by the
global and decentralized action of [CryptoParty](https://www.cryptoparty.in),
which aims to spread the fundamental concepts and basic cryptography software.
During 24 hours there were activities on security, encryption, hacking,
anonymity, privacy and freedom on the network. The Debian Community was
present, represented by the Brazilian contributors
[Daniel Lenharo](https://contributors.debian.org/contributor/lenharo/)
and
[Giovani Ferreira](https://contributors.debian.org/contributor/giovani/).

CryptoRave shows affinity with the Debian Project by recommending the Debian
GNU/Linux and Tails among the softwares that respects users privacy, and also
an tribute to the project's founder, one of the rooms of the event was named
Ian Murdock.

During the event, in a space provided by the organization, the sale of Debian
products (t-shirts, keyrings, bottons, stickers, etc.) took place and talked
about the project and finding new people to contribute. In this sense, Daniel
Lenharo held a talk called "The debian wants you" in which in addition to
talking about the project presented to the public various ways to contribute to
the debian project. "Make Debian present in an event of the importance of
Cryptorave is amazing.  In these 24 hours we can learn and collaborate in this
process of incredible exchange of knowledge that is formed in this event. Also
know that our project is useful and people are proud to display Our swirl is
something incredible. Surely after the event, we will have more people using
and collaborating with our community." Says Lenharo.

We thank Cryptorave's organization for the space provided and we look forward
to 2018!!


![Lenharo Falando Debian](https://debianbrasil.org.br/blog/imagens/cryptorave2017-palestras.jpg =400x)


Daniel Lenharo talking about Debian.


![](https://debianbrasil.org.br/blog/imagens/cryptorave2017-mesa.jpg =400x)


Daniel Lenharo and Giovani Ferreira promoting Debian.


![](https://debianbrasil.org.br/blog/imagens/cryptorava2017-banner.jpg =400x)


Daniel Lenharo and Giovani Ferreira at Debian Project Founder banner.
