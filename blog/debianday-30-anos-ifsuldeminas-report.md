---
title: Debian Day 30 years at IF Sul de Minas, Pouso Alegre - Brazil
description: by Thiago Pezzo (Tico)
published: true
date: 2025-03-01T17:38:54.030Z
tags: blog, english
editor: markdown
dateCreated: 2023-08-24T10:00:00.000Z
---

<!-- author: Thiago Pezzo (Tico) -->

by [Thiago Pezzo](https://contributors.debian.org/contributor/tico@salsa/),
Debian contributor, pt_BR localization team

This year's [Debian Day](https://wiki.debian.org/DebianDay/) was a pretty special one, we are celebrating 30 years! Giving the importance of this event, the Brazilian community planned a very
special week. Instead of only local gatherings, we had a week of online talks
streamed via Debian Brazil's [youtube channel](https://www.youtube.com/playlist?list=PLU90bw3OpxLoiEUdWm7uyTi71ATGk6oXm) (soon the recordings will be uploaded to our team's
[peertube instance](https://peertube.debian.social/w/p/r1gZyzq3anFaECh6QEmU4d)). 
Nonetheless the local celebrations happened around the country and one was
organized in [Pouso Alegre, MG, Brazil](https://www.openstreetmap.org/relation/315431), at the [Instituto Federal de Educação, Ciência e Tecnologia do Sul de Minas Gerais](https://portal.ifsuldeminas.edu.br/index.php) (IFSULDEMINAS - Federal Institute of Education, Science and Technology of the
South of Minas Gerais). The Institute, as many of its counterparts in Brazil,
specializes in professional and technological curricula to high school and
undergraduate levels. All public, free and quality education!

The event happened on the afternoon of August 16th at the Pouso Alegre campus.
Some 30 students from the High School Computer Technician class attended the
presentation about the [Debian Project](https://www.debian.org/) and the Free
Software movement in general. Everyone had a great time! And afterwards we
had some spare time to chat.

I would like to thank all people who helped us:

- Professors Michelle Nery and Ismael David Muro (IFSULDEMINAS Pouso Alegre)
- Virginia Cardoso and Melissa de Abreu (IFSULDEMINAS Rector's Office)
- [Giovani Ferreira](https://nm.debian.org/person/giovani/) (a DD living in Minas Gerais state) - Felipe Maia (from Debian São Paulo - thanks for the posters!)
- Gustavo (a clever young student who made important comments about accessibility - thanks!) - And a special thanks to all students who attended the presentation, hope to see you again! 
Here goes our group photo:

![Presentation at IFSULDEMINAS Pouso Alegre campus](https://debianbrasil.org.br/blog/imagens/ifsuldeminas.png =400x) 
