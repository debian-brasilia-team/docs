---
title: FISL17 foi especial para Debian Brasil
description: por Giovani Ferreira
published: true
date: 2025-03-01T17:39:46.851Z
tags: blog, geral
editor: markdown
dateCreated: 2016-07-26T03:12:00.000Z
---

<!-- author: Giovani Ferreira -->

Durante a
[17ª edição do Fórum Internacional Software Livre](http://softwarelivre.org/fisl17) (FISL7) que aconteceu de 13 a 16 de julho de 2016 na PUCRS em Porto Alegre, Ian
Murdock, fundador do projeto [Debian](http://www.debian.org/), recebeu uma
grande homenagem, na qual um dos palcos do evento recebeu seu nome. Na abertura
oficial este anúncio foi recebido com uma calorosa salva de palmas.

Mais detalhes estão disponíveis nesta noticia publicada no site do evento:

<http://softwarelivre.org/fisl17/noticias/comunidade-debian-brasil-fala-sobre-o-legado-de-ian-murdock> 
A [comunidade Debian Brasil](http://debianbrasil.org.br) realizou diversas
atividades durante o FISL17: foram 6 lightning talks sobre temas variados como
Forense, Debian Policy e GSoC, tivemos também oficinas (hands-on) sobre
empacotamento e Web of Trust, e ainda aconteceu um encontro comunitário. O tema
principal do encontro comunitário foi apresentar as diversas frentes de
trabalho e formas de contribuição com a comunidade, visando atrair novos
contribuidores.

Todas as atividades realizadas pela comunidade Debian Brasil podem ser vistas
neste link:

<https://wiki.debian.org/Brasil/Eventos/EncontroComunitarioFISL2016>

Assim como tem acontecido nos anos anteriores, tivemos durante o evento uma
bancada na Área de Comunidades, esse espaço foi muito importante e serviu como
ponto de encontro entre os membros da comunidade Debian. Durante os quatro dias
de evento foram distribuídos materiais promocionais e diversas pessoas
procuraram esta bancada para conhecer mais sobre a comunidade Debian, além de
outras que participaram do Install Fest que aconteceu por lá.

Enfim, mesmo não organizando um "mini-evento" dentro do FISL17 como aconteceu no [FISL do ano passado](https://wiki.debian.org/DebianEvents/br/2016/MiniDebconfFISL), a comunidade Debian Brasil conseguiu atingir o objetivo ao marcar presença em
um dos maiores eventos de Software Livre do mundo, mostrando o trabalho
realizado e convidando mais pessoas a colaborarem com o "Sistema Operacional
Universal"

Fotos:
<https://www.flickr.com/photos/debianbrasil/albums/72157716018790553>
