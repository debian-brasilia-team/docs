---
title: Organize o Debian Day 2018 na sua cidade
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:41:35.566Z
tags: blog, geral
editor: markdown
dateCreated: 2018-05-10T13:50:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Em julho 2019 Curitiba sediará a 20a edição da
[DebConf - Conferência Mundial de Desenvolvedores(as) do Projeto Debian](https://www.debconf.org/) e para celebrar a vinda do evento para o Brasil, estamos lançando esta
convocação para que as comunidades locais organizem o
[Debian Day](https://wiki.debian.org/DebianDay) em suas cidades.

## O que é Debian Day?

É um evento internacional realizado anualmente no dia 16 de agosto ou no final
de semana mais próximo para **comemorar o aniversário do Debian**. Sendo um dia
totalmente dedicado a divulgação e contribuições ao projeto.

Tendo como nome do evento "Debian Day", em português "Dia Debian", ou ainda
"Dia D".

O Brasil sempre foi o país que teve mais cidades organizando o Debian Day no
mundo, mas nos últimos anos a quantidade de cidades diminuiu bastante, como você pode ver nas [estatísticas](https://wiki.debian.org/DebianDay#Statistics).

## Como descubro se terá Debian Day na minha cidade?

Você pode acessar a página na wiki que lista todas as cidades que estão
organizando:

<https://wiki.debian.org/DebianDay/2018>

## Minha cidade não está na lista, e agora?

Você pode chamar a comunidade local de software livre para organizar o Debian
Day na sua cidade :-)

## Ok, decidimos organizar o Debian Day aqui, o que devemos fazer?

Vocẽ deve inserir a sua cidade na lista de cidades na
[wiki do Debian](https://wiki.debian.org). Para isso:

- [Criei um usuário](https://wiki.debian.org/P%C3%A1ginaPrincipal?action=newaccount) - [Insira a sua cidade na página de 2018](https://wiki.debian.org/DebianDay/2018) 
Mesmo que você use um site externo com informações sobre o Debian Day na sua
cidade, você **deve inserir as informações básica na wiki do Debian**. A sua
cidade só contará oficialmente como cidade organizadora se estiver na wiki.

Se inscreva na lista de discussão
[debian-br-eventos](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos) para conversar com outros brasileiros. Lá você pode avisar que vai ter Debian
Day na sua cidade, tirar dúvidas, discutir dicas e sugestões, etc.

## É obrigatório ser no dia 16 de agosto (quinta-feira)?

Não, mas o ideal é que seja na mesma semana. Se para vocês for melhor fazer no
sábado para aumentar a participação das pessoas, você pode fazer no sábado mais
próximo do dia 16/08, que este ano será dia 18/08.

## Deve ser um evento grande?

Não, não precisa ser um evento grande! O tamanho do Debian Day na sua cidade
será do tamanho que vocês quiserem/puderem organizar.

## Qual deve ser o formato do evento?

Não existe um formato fixo, vocês são livres para definir o formato que ficar
melhor para a comunidade.  Algumas sugestões são:

-   Um encontro em um bar, pizzaria ou restaurante.
-   Um evento com palestras e/ou oficinas sobre Debian em um local com sala(s)
e/ou laboratório(s).

É importante que vocês entendam que não existe uma competição para ver qual
cidade organizará o "maior Debian Day do mundo". Um encontro em um bar a noite
conta tanto quanto um dia inteiro de palestras e oficinas. Os dois modelos
servirão para celebrar o aniversário do Debian.

## Precisa de ter bolo?

Não precisa, mas se tiver será muito legal, seja um bolo caseiro ou profissional. 
![Bolos](https://debianbrasil.org.br/blog/imagens/bolos)

## Material gráfico

Se você fez alguma arte para este evento, compartilhe com todos! E colocar
link para o material [aqui](https://wiki.debian.org/Promote).

## Dicas e sugestões

A Adriana Costa fez uma palestra na
[MiniDebConf 2018 em Curitiba](https://minidebconf.curitiba.br/2018/) sobre
"5 motivos para celebrar o Debian Day". Você pode ver o vídeo
[aqui](https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2018/mini-debconf-curitiba/11-5-motivos-para-voce-celebrar-o-debianday.webm) ou no [YouTube](https://www.youtube.com/watch?v=xAbifGImODU).

Traduzido da wiki: Se você deseja organizar o Day na sua cidade, recomendamos
que você siga estas instruções:

- Selecione um lugar para a comemoração que tenha:
    - Fácil acesso.
    - Que tenham tomadas elétricas suficientes, se quiser fazer um install fest. 
- Patrocinadores:
    - Idealmente, você deve escolher patrocinadores que não tenham links para
produtos privados.

- Atividades: algumas atividades que você pode escolher incluem:
    - Install Fest
    - Festa de assinatura de chaves GPG
    - Palestras sobre:
        -   Debian.
        -   Software Livre.

## Façam fotos

Façam fotos do Debian Day na sua cidade e publique para a gente ver como foi.

Veja as fotos de algumas cidades de 2017:

- Curitiiba: <https://www.flickr.com/photos/curitibalivre/sets/72157685044712731> - Maceió: <https://www.flickr.com/photos/debianbrasil/albums/72157716019609901>
- Natal: <http://bit.ly/2gLqnIw>
- Recife: <http://bit.ly/2gKJi6c>

![](https://debianbrasil.org.br/blog/imagens/logo-debianday.png)


[Arquivo svg](https://debianbrasil.org.br/blog/imagens/logo-debianday-source.svg)
