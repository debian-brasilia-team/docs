---
title: Participação da comunidade Debian no Latinoware 2022
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:41:44.605Z
tags: blog, geral
editor: markdown
dateCreated: 2022-11-12T18:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

De 2 a 4 de novembro de 2022 aconteceu a 19º edição do
[Latinoware](https://latinoware.org/) - Congresso Latino-americano de Software
Livre e Tecnologias Abertas, em Foz do Iguaçu. Após 2 anos acontecendo de forma
online devido a pandemia do COVID-19, o evento voltou a ser presencial e
sentimos que a comunidade [Debian Brasil](https://debianbrasil.org.br) deveria
estar presente. Nossa última participação no Latinoware foi em
[2016](https://debianbrasil.org.br/blog/participacao-da-comunidade-debian-na-latinoware-2016/) 
A organização do Latinoware cedeu para a comunidade Debian Brasil um estande
para que pudéssemos ter contato com as pessoas que visitavam a área aberta de
exposições e assim divulgarmos o projeto [Debian](https://www.debian.org).
Durante os 3 dias do evento, o estande foi organizado por mim
([Paulo Henrique Santana](https://phls.com.br)) como Desenvolvedor Debian, e
pelo Leonardo Rodrigues como contribuidor Debian. Infelizmente o Daniel Lenharo
teve um imprevisto de última hora e não pode ir para Foz do Iguaçu (sentimos sua falta lá!).

![Latinoware 2022 estande 1](https://debianbrasil.org.br/blog/imagens/latinoware-2022-01.jpg =400x) 
Várias pessoas visitaram o estande e aquelas mais iniciantes (principalmente
estudantes) que não conheciam o Debian, perguntavam do que se tratava o nosso
grupo e a gente explicava vários conceitos como o que é Software Livre,
distribuição GNU/Linux e o Debian propriamente dito. Também recebemos pessoas
da comunidade de Software Livre brasileira e de outros países da América Latina
que já utilizavam uma distribuição GNU/Linux e claro, muitas pessoas que já
utilizavam Debian. Tivemos algumas visitas especiais como do Jon maddog Hall,
do Desenvolvedor Debian Emeritus Otávio Salvador, do Desenvolvedor Debian
[Eriberto Mota](http://eriberto.pro.br/), e dos Mantenedores Debian Guilherme de Paula Segundo e Paulo Kretcheu.

![Latinoware 2022 estande 4](https://debianbrasil.org.br/blog/imagens/latinoware-2022-04.jpg =400x) 
Foto da esquerda pra direita: Leonardo, Paulo, Eriberto e Otávio.

![Latinoware 2022 estande 5](https://debianbrasil.org.br/blog/imagens/latinoware-2022-05.jpg =400x) 
Foto da esquerda pra direita: Paulo, Fabian (Argentina) e Leonardo.

Além de conversarmos bastante, distribuímos adesivos do Debian que foram
produzidos alguns meses atrás com o patrocínio do Debian para serem distribuídos na DebConf22(e que haviam sobrado), e vendemos várias
[camisetas](http://loja.curitibalivre.org.br/) do Debian produzidas pela
comunidade [Curitiba Livre](http://curitibalivre.org.br).

![Latinoware 2022 estande 2](https://debianbrasil.org.br/blog/imagens/latinoware-2022-02.jpg =400x) 
![Latinoware 2022 estande 3](https://debianbrasil.org.br/blog/imagens/latinoware-2022-03.jpg =400x) 
Também tivemos 3 palestras inseridas na programação oficial do Latinoware.
[Eu](https://latinoware.org/paulo-henrique-de-lima-santana/) fiz as palestras:
“como tornar um(a) contribuidor(a) do Debian fazendo traduções” e “como os
SysAdmins de uma empresa global usam Debian”. E o
[Leonardo](https://latinoware.org/leonardo-rodrigues-pereira/) fez a palestra:
“vantagens da telefonia Open Source nas empresas”.

![Latinoware 2022 estande 6](https://debianbrasil.org.br/blog/imagens/latinoware-2022-06.jpg =400x) 
Foto Paulo na palestra.

Agradecemos a organização do Latinoware por receber mais uma vez a comunidade
Debian e gentilmente ceder os espaços para a nossa participação, e parabenizamos a todas as pessoas envolvidas na organização pelo sucesso desse importante
evento para a nossa comunidade. Esperamos estar presentes novamente em 2023.

Agracemos também ao Jonathan Carter por aprovar o suporte financeiro do Debian
para a nossa participação no Latinoware.

[Versão em inglês](https://debianbrasil.org.br/blog/about-debian-brasil-at-latinoware-2022/)