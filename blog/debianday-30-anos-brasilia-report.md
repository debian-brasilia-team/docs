---
title: Debian Day 30 years in Brasília - Brazil
description: by Thaís Rebouças de Araujo
published: true
date: 2025-03-01T17:38:47.368Z
tags: blog, english
editor: markdown
dateCreated: 2023-09-06T12:34:00.000Z
---

<!-- author: Thaís Rebouças de Araujo -->

This year's [Debian Day](https://wiki.debian.org/DebianDay/) was a pretty special one, we were celebrating 30 years! Given the importance of this event, the Brazilian community planned a very
special week. Instead of only local gatherings, we had a week of online talks
streamed via Debian [Brazil's YouTube channel](https://www.youtube.com/playlist?list=PLU90bw3OpxLoiEUdWm7uyTi71ATGk6oXm) (soon the recordings will be uploaded to [Debian's PeerTube instance](https://peertube.debian.social/w/p/r1gZyzq3anFaECh6QEmU4d). Nonetheless the local celebrations happened around the country and we've organized one in Brasília at [University of Brasília](https://fga.unb.br/) on the Gama campus. 
The event happened on August 29th and went on the whole afternoon. We had two
talks made to two different classes at the university, the first of 30 students and then to a class of 80. Each conversation lasted about 2 hours. The talks were about: 
- Debian and debian community
- Free software

Debian helped us with coffee break where we had the chance to talk to the participants, and finished with a group photo (check this one and many others below). 
We announced the event in instant communication groups such as Telegram of technology courses of the University of Brasília. 
![Promotional folder](https://debianbrasil.org.br/blog/imagens/debian-day-30-anos/brasilia/folder.jpeg =400x)   
Photos taken during the event:

![Photo 1](https://debianbrasil.org.br/blog/imagens/debian-day-30-anos/brasilia/present_debian_0.jpeg =400x) 
![Photo 2](https://debianbrasil.org.br/blog/imagens/debian-day-30-anos/brasilia/present_debian_1.jpeg =400x) 
![Photo 3](https://debianbrasil.org.br/blog/imagens/debian-day-30-anos/brasilia/present_debian_2.jpeg =400x) 
Presentation about debian in first class.

![Photo 1](https://debianbrasil.org.br/blog/imagens/debian-day-30-anos/brasilia/present_free_software_0.jpeg =400x) 
![Photo 2](https://debianbrasil.org.br/blog/imagens/debian-day-30-anos/brasilia/present_free_software_1.jpeg =400x) 
Presentation about free software on auditorium.

