---
title: Lançado o Debian 9 - Stretch
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:13.882Z
tags: blog, geral
editor: markdown
dateCreated: 2017-06-18T16:45:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

![Banner stretch 380](https://debianbrasil.org.br/blog/imagens/banner-stretch-380.png)

Depois de 26 meses de desenvolvimento, o projeto
[Debian](https://www.debian.org/) tem o orgulho de apresentar a sua nova versão
estável 9 (codinome Stretch), que será suportada durante os próximos 5 anos,
graças ao trabalho combinado da equipe de [Segurança do
Debian](http://security-team.debian.org)
e da equipe de [Suporte de Longo Prazo](https://wiki.debian.org/LTS) do Debian.

O **Debian 9** é
[dedicado](http://ftp.debian.org/debian/doc/dedication/dedication-9.0.pt_BR.txt) ao fundador do projeto, Ian Murdock, que faleceu em 28 de dezembro de 2015.

Na **Stretch**, a variante MySQL padrão agora é o MariaDB. A substituição dos
pacotes do MySQL 5.5 ou 5.6 pela variante MariaDB 10.1 acontecerá
automaticamente na atualização.

O Firefox e o Thunderbird retornam ao Debian com o lançamento da **Stretch**,
e substituem as suas versões com as marcas removidas Iceweasel e Icedove, que
estiveram presentes no repositório por mais de 10 anos.

Graças ao projeto "Reproducible Builds", mais de 90% dos pacotes fonte
incluídos no **Debian 9** construirão pacotes binários idênticos bit-a-bit.
Essa é uma funcionalidade de verificação importante que protege os usuários
contra tentativas maliciosas de adulterar compiladores e redes de construção.
Versões futuras do Debian incluirão ferramentas e metadados para que usuários
finais possam validar a procedência de pacotes dentro do repositório.

Administradores e aquelas pessoas que estão em ambientes sensíveis a segurança
podem se confortar em saber que o sistema gráfico X não exige mais privilégios
de "root" para executar.

A versão **Stretch** é a primeira versão do Debian a apresentar a ramificação
"modern" do GnuPG no pacote "gnupg". Isso traz criptografia de curva elíptica,
padrões melhores, uma arquitetura mais modular e suporte melhorado a smartcards. Nós continuaremos a fornecer a ramificação "classic" do GnuPG como gnupg1 para
pessoas que precisam dela, mas agora ela está obsoleta.

Os pacotes de depuração estão mais fáceis de obter e usar no **Debian 9**
**Stretch**. Um novo repositório "dbg-sym" pode ser adicionado à lista de
fontes do APT para fornecer símbolos de depuração automaticamente para vários
pacotes.

O suporte a UEFI ("Unified Extensible Firmware Interface"), introduzido
primeiramente na "Wheezy", continua a ser consideravelmente melhorado na
**Stretch**, e também suporta a instalação em firmware UEFI de 32 bits com um
kernel de 64 bits. As imagens Debian live agora incluem suporte à inicialização
UEFI como uma nova funcionalidade, também.

Esta versão inclui vários pacotes de software atualizados, tais como:

- Apache 2.4.25
- Asterisk 13.14.1
- Chromium 59.0.3071.86
- Firefox 45.9 (no pacote firefox-esr)
- GIMP 2.8.18
- Uma versão atualizada do ambiente de área de trabalho GNOME 3.22
- Coleção de Compiladores GNU 6.3
- GnuPG 2.1
- Golang 1.7
- KDE Frameworks 5.28, KDE Plasma 5.8, KDE Applications 16.08 e 16.04 para
componentes PIM
- LibreOffice 5.2
- Linux 4.9
- MariaDB 10.1
- MATE 1.16
- OpenJDK 8
- Perl 5.24
- PHP 7.0
- PostgreSQL 9.6
- Python 2.7.13 e 3.5.3
- Ruby 2.3
- Samba 4.5
- systemd 232
- Thunderbird 45.8
- Tomcat 8.5
- Xen Hypervisor
- O ambiente de área de trabalho Xfce 4.12
- Mais de 51.000 outros pacotes de software prontos para uso, construídos a
partir de pouco mais de 25.000 pacotes fonte.

Com essa ampla seleção de pacotes e seu tradicional amplo suporte de
arquiteturas, o Debian mais uma vez se mantém fiel ao seu objetivo de ser o
sistema operacional universal. Ele é apropriado para muitos casos diferentes de
uso: de sistemas desktop a netbooks; de servidores de desenvolvimento a
sistemas de cluster; e para servidores de bancos de dados, web ou
armazenamento. Ao mesmo tempo, esforços adicionais para garantia de qualidade,
como instalação automática e testes de atualização para todos os pacotes do
repositório do Debian asseguram que a **Stretch** satisfaz as altas
expectativas que os usuários têm de uma versão estável do Debian.

Um total de dez arquiteturas são suportadas: PC de 64 bits / Intel EM64T /
x86-64 (amd64), PC de 32 bits / Intel IA-32 (i386), PowerPC de 64 bits
little-endian da Motorola/IBM (ppc64el), IBM S/390 de 64 bits (s390x), para
ARM, armel e armhf para hardware de 32 bits antigo e mais recente, além de
arm64 para a arquitetura de 64 bits "AArch64", e para MIPS, em adição às duas
arquiteturas de 32 bits mips (big-endian) e mipsel (little-endian), há uma nova
arquitetura mips64el para hardware de 64 bits little-endian. O suporte para
PowerPC de 32 bits da Motorola/IBM (powerpc) foi removido na "Stretch".

Caso você queira simplesmente testar o **Debian 9** **Stretch** sem ter que
instalá-lo, você pode usar uma das imagens live disponíveis, que carregam e
executam o sistema operacional completo em um estado somente de leitura através
da memória do seu computador. Se você gostar do sistema operacional, você tem a
opção de instalá-lo no disco rígido do seu computador a partir da imagem live.
A imagem live está disponível para CDs, pendrives USB e configurações com
inicialização via rede. Inicialmente, essas imagens são fornecidas apenas para
as arquiteturas amd64 e i386. Mais informações estão disponíveis na seção de
imagens de instalação [live](https://www.debian.org/CD/live) no site web do
Debian.

Caso você prefira instalar o **Debian 9** **Stretch** diretamente no disco
rígido do seu computador, você pode escolher entre uma variedade de mídias de
instalação, tais como discos Blu-ray, DVDs, CDs e pendrives USB, ou através da
rede interna. Diversos ambientes de área de trabalho — GNOME, área de trabalho
KDE Plasma e seus aplicativos, LXDE e Xfce — podem ser instalados através
dessas imagens com a sua seleção desejada escolhida a partir dos menus de
inicialização das mídias de instalação. Além disso, CDs e DVDs multiarquitetura
estão disponíveis, os quais suportam instalação de múltiplas arquiteturas a
partir de um único disco. Ou você sempre pode criar uma mídia de instalação USB
inicializável (veja o
[Guia de Instalação](https://www.debian.org/releases/stretch/installmanual)
para mais detalhes). Para usuários de nuvem, o Debian também oferece imagens
[OpenStack pré-construídas](http://cdimage.debian.org/cdimage/openstack/current) para as arquiteturas amd64 e arm64, prontas para uso.

O Debian agora pode ser instalado em 75 idiomas, com a maioria deles disponível
tanto em interfaces de usuário baseadas em texto como gráficas.

As imagens de instalação podem ser baixadas agora mesmo via
[bittorrent](https://www.debian.org/CD/torrent-cd) (o método recomendado),
[jigdo](https://www.debian.org/CD/jigdo-cd/#which) ou
[HTTP](https://www.debian.org/CD/http-ftp); veja
[Debian em CDs](https://www.debian.org/CD) para informações adicionais. A
**Stretch** em breve estará disponível em mídias físicas de DVD, CD-ROM e
discos Blu-ray a partir de vários
[fornecedores](https://www.debian.org/CD/vendors), também.

As atualizações para o **Debian 9** a partir da versão anterior, Debian 8
(codinome "Jessie"), são tratadas automaticamente pela ferramenta de
gerenciamento de pacotes apt-get na maioria das configurações. Como sempre, os
sistemas Debian podem ser atualizados de forma indolor, no local, sem qualquer
indisponibilidade forçada, mas é fortemente recomendado ler as
[notas de lançamento](https://www.debian.org/releases/stretch/releasenotes),
assim como o
[guia de instalação](https://www.debian.org/releases/stretch/installmanual)
para possíveis problemas, e para instruções detalhadas de como instalar e
atualizar. As notas de lançamento serão melhoradas e traduzidas para outros
idiomas nas semanas após o lançamento.

## Sobre o Debian

O Debian é um sistema operacional livre, desenvolvido por milhares de
voluntários ao redor do mundo que colaboram através da Internet. O pontos chave
do projeto Debian são a sua base de voluntários, a sua dedicação ao Contrato
Social do Debian e ao Software Livre, e o seu compromisso de fornecer o melhor
sistema operacional possível. Este novo lançamento é outro passo importante
nessa direção.

## Informações de contato

Para mais informações, por favor, visite as páginas web do Debian em
<https://www.debian.org> ou envie um e-mail (em inglês) para <press@debian.org>. 
Observação:

Texto original em: <https://www.debian.org/News/2017/20170617.pt.html>