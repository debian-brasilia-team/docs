---
title: Como foi o Debian Day 2017 no Brasil
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:58:01.176Z
tags: blog, geral
editor: markdown
dateCreated: 2017-09-07T18:04:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

O [Debian Day](https://wiki.debian.org/DebianDay) (em português "Dia do Debian", ou ainda "Dia D") um evento internacional realizado anualmente no dia 16 de
agosto, ou no sábado mais próximo, para comemorar o aniversário do
[Projeto Debian](https://www.debian.org). Sendo um dia totalmente dedicado a
divulgação e contribuições ao Debian.

Cada comunidade é livre para organizar a sua programação local, que pode ser um
evento com palestras e/ou oficinas sobre Debian, ou um encontro em um
bar/pizzaria/restaurante. Independente do tamanho do evento, o importante é
promover o encontro da comunidade de software livre da cidade para celebrar o
aniversário do maior projeto de software livre do mundo!

Historicamente o Brasil é o país com mais cidades organizando o Debian Day, e
este ano não foi diferente. As comunidades de 7 cidades brasileiras
organizaram o [Debian Day em 2017](https://wiki.debian.org/DebianDay/2017):

- [Curitiba - PR](https://wiki.debian.org/Brasil/Eventos/DebianDayCuritiba2017)
- [Francisco Beltrão - PR](http://eventos.fb.utfpr.edu.br/debianday)
- [Maceió - AL](http://www.oxehc.com.br)
- [Natal - RN](http://www.potilivre.org/noticias/613-debian-day-natal-2017)
- [Recife - PE](https://www.facebook.com/RoboticaLivre)
- [Salvador - BA](http://raulhc.cc/Agenda.2017-08-16-DebianDay)
- [São Paulo - SP](https://www.meetup.com/Grupo-de-Compartilhamento-do-Conhecimento-Santos-Dumont/events/242207831) 
Algumas destas cidades publicaram fotos dos seus eventos:

- Curitiiba: <https://www.flickr.com/photos/curitibalivre/sets/72157685044712731> - Maceió: <https://www.flickr.com/photos/debianbrasil/albums/72157716019609901>
- Natal: <http://bit.ly/2gLqnIw>
- Recife: <http://bit.ly/2gKJi6c>

Achou legal? Em 2018 você também pode organizar um Debian Day na sua cidade.


  ![Logo debianday](https://debianbrasil.org.br/blog/imagens/logo-debianday.png)

