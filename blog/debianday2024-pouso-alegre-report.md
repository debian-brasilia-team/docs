---
title: Debian Day 2024 in Pouso Alegre - Brazil
description: by Thiago Pezzo (Tico), Giovani Ferreira
published: true
date: 2025-03-01T17:39:17.026Z
tags: blog, english
editor: markdown
dateCreated: 2024-08-18T15:00:00.000Z
---

<!-- author: Thiago Pezzo (Tico), Giovani Ferreira -->

by [Thiago Pezzo](https://contributors.debian.org/contributor/tico@salsa/) and
[Giovani Ferreira](https://contributors.debian.org/contributor/giovani/)

Local celebrations of [Debian 2024 Day](https://wiki.debian.org/DebianDay/2024)
also happened on [Pouso Alegre, MG, Brazil] (https://www.openstreetmap.org/relation/315431). In this year we managed to organize two days of lectures!

On the 14th of August 2024, Wednesday morning, we were on the
[Federal Institute of Education, Science and Technology of the South of Minas Gerais] (https://portal.ifsuldeminas.edu.br/index.php), (IFSULDEMINAS), Pouso Alegre campus. We did an introductory presentation of the
[Project Debian](https://wwww.debian.org), operating system and community, for
the three years of the Technical Course in Informatics (professional high school). The event was closed to IFSULDEMINAS students and talked to 60 people.

On August 17th, 2024, a Saturday morning, we held the event open to the
community at the [University of the Sapucaí Valley](https://www.uniivas.edu.br/) (Univás), with institutional support of the Information Systems Course. We speak about the Debian Project with [Giovani Ferreira](https://contributors.debian.org/contributor/giovani/) (Debian Developer); about the [Debian pt_BR translation team](https://wiki.de.bi.org/Brazil/Translate/) with Thiago Pezzo; about everyday experiences using free software with Virginia
Cardoso; and on how to set up a development environment ready for production
using Debian and Docker with Marcos António dos Santos. After the lectures,
snacks, coffee and cake were served, while the participants talked, asked
questions and shared experiences.

We would like to thank all the people who have helped us:

- Michelle Nery (IFSULDEMINAS) and André Martins (UNIVÁS) for the aid in the local organization - Paulo Santana (Debian Brazil) by the general organization
- Virginia Cardoso, Giovani Ferreira, Marco António and Thiago Pezzo for the lectures - And a special thanks to all of you who participated in our celebratio

Some pictures from Pouso Alegre:

![Presentation at IFSULDEMINAS Pouso Alegre campus 1](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-ifsuldeminas1.jpg =400x) ![Presentation at IFSULDEMINAS Pouso Alegre campus 2](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-ifsuldeminas2.jpg =400x) ![Presentation at UNIVÁS Fátima campus 1](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-univas1.jpg =400x) ![Presentation at UNIVÁS Fátima campus 2](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-univas2.jpg =400x) ![Presentation at UNIVÁS Fátima campus 3](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-univas3.jpg =400x) ![Presentation at UNIVÁS Fátima campus 4](https://debianbrasil.org.br/blog/imagens/debianday-pouso-alegre-2024-univas4.jpg =400x) 