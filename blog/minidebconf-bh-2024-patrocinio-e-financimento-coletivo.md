---
title: MiniDebConf BH 2024 - patrocínio e financiamento coletivo
description: por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:40:55.402Z
tags: blog, geral
editor: markdown
dateCreated: 2024-01-21T11:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

![MiniDebConf BH 2024](https://debianbrasil.org.br/blog/imagens/logo-debconf-bh-600px.svg)

Já está rolando a
[inscrição de participante](https://bh.mini.debconf.org/evento/inscricao/) e a
[chamada de atividades](https://bh.mini.debconf.org/evento/chamada-de-atividades/) para a **MiniDebConf Belo Horizonte 2024**, que acontecerá de 27 a 30 de abril
no Campus Pampulha da UFMG.

Este ano estamos ofertando
[bolsas de alimentação, hospedagem e passagens](https://bh.mini.debconf.org/evento/bolsas/) para contribuidores(as) ativos(as) do Projeto Debian.

## Patrocínio:

Para a realização da MiniDebConf, estamos buscando
[patrocínio financeiro](https://bh.mini.debconf.org/patrocinio/seja-patrocinador) de empresas e entidades. Então se você trabalha em uma empresa/entidade (ou
conhece alguém que trabalha em uma) indique o nosso plano de patrocínio para
ela. Lá você verá os valores de cada cota e os seus benefícios.

## Financiamento coletivo:

Mas você também pode ajudar a realização da MiniDebConf por meio do nosso
[financiamento coletivo](https://bh.mini.debconf.org/doacoes/)!

Faça uma **doação de qualquer valor** e tenha o seu nome publicado no site do
evento como apoiador(a) da MiniDebConf Belo Horizonte 2024.

Mesmo que você não pretenda vir a Belo Horizonte para participar do evento, você pode doar e assim contribuir para o mais importante evento do
[Projeto Debian](https://www.debian.org/) no Brasil.

## Contato

Qualquer dúvida, mande um email para <contato@debianbrasil.org.br>

## Organização

[![Debian Brasil](https://bh.mini.debconf.org/media/pages_files/logo-debian-brasil-150-100.svg)](https://debianbrasil.org.br) [![Debian ](https://bh.mini.debconf.org/media/pages_files/logo-debian-150-100.svg)](https://debian.org) [![Debian MG](https://bh.mini.debconf.org/media/pages_files/logo-debian-mg-150-100.svg)](https://debian-minas-gerais.gitlab.io/site/) [![DCC](https://bh.mini.debconf.org/media/pages_files/logo-DCC-370-80.svg)](https://dcc.ufmg.br) 