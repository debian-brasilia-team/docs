---
title: Participação da comunidade Debian na Latinoware 2016
description: por Giovani Ferreira
published: true
date: 2025-03-01T17:41:42.353Z
tags: blog, geral
editor: markdown
dateCreated: 2016-11-03T19:17:00.000Z
---

<!-- author: Giovani Ferreira -->

A [Comunidade Debian Brasil](http://debianbrasil.org.br) realizou
mais uma MiniDebConf durante a 13ª edição da
[Latinoware](http://www.latinoware.org), que aconteceu de 19 a 21 de outubro de
2016 em Foz do Iguaçu. Durante os dias 19 e 20 de outubro foram realizadas 7
palestras, 1 sessão com 4 lightning talks e 1 painel.

[Estas atividades](https://wiki.debian.org/Brasil/Eventos/MiniDebConfLatinoware2016) trataram dos mais variados temas dentro do
[Projeto Debian](http://www.debian.org) que foram desde questões técnicas sobre
ferramentas utilizadas no projeto até a apresentação de como funciona toda
comunidade, chamando cada vez mais pessoas a contribuir.

Nos últimos anos os membros do projeto tem feito um esforço especial para
atrair mais pessoas e, de modo especial, a partir de agora iniciou-se um
movimento para a inclusão de mulheres e este foi o tema de uma das Lightning
Talks. A comunidade Debian Brasil quer, como forma de aumentar a diversidade,
fazer um convite especial à todas as mulheres a participarem dentro das mais
variadas [formas de colaboração](https://wiki.debian.org/MulheresBrasil).

Durante todo o evento tivemos um estande na área de exposição,  onde o público
podia sanar suas dúvidas sobre o projeto e entender melhor a organização da
comunidade no Brasil. O estande também serviu como ponto de encontro dos membros da comunidade. Houve distribuição de brindes e comercialização de alguns itens
(camisetas, canecas e adesivos) para auxiliar a organização de projetos da
comunidade.

Aproveitamos para deixar um agradecimento a organização da Latinoware pelo
espaço que nos foi cedido dentro do evento, agradecemos também aos membros da
comunidade que tiveram a disponibilidade na realização das palestras e
agradecemos ao público presente nas atividades realizadas.


Abaixo esao disponíveis os arquivos usados nas apresentações:

Bastidores Debian: entenda como a distribuição funciona

- João Eriberto Mota Filho
- [Apresentação em pdf](http://www.eriberto.pro.br/palestras/bastidores_debian.pdf) 
Conhecendo o Debian Bug Tracking System (BTS)

- Giovani Augusto Ferreira
- [Apresentação em pdf](/apresentacoes-latinoware-2016/giovani-conhecendo-o-debian-bug-tracking-system.pdf) 
Conheça o Debian Forensics Team

- Giovani Augusto Ferreira
- [Apresentação em pdf](/apresentacoes-latinoware-2016/giovani-conheca-o-debian-forensics-team.pdf) 
Debian para quem quer ser grande

- Gilmar dos Reis
- [Apresentação em pdf](/apresentacoes-latinoware-2016/gilmar-debian-para-quem-quer-ser-grande.pdf) 
Debian - um universo em construção

- Giovani Augusto Ferreira
- [Apresentação em pdf](/apresentacoes-latinoware-2016/giovani-debian-um-universo-em-construcao.pdf) 
Empacotamento de software no Debian

- João Eriberto Mota Filho
- [Apresentação em pdf](http://www.eriberto.pro.br/palestras/empacotamento_debian.pdf) 
Entenda a Debian Policy

- Thadeu Cascardo.

Entrando para o Time de Tradução do Debian

- Daniel Lenharo
- [Apresentação em pdf](/apresentacoes-latinoware-2016/daniel-entrando-para-o-time-de-traducao-do-debian.pdf) 
Instalando um GNU Debian 100% livre

- Alessandro Moura
- [Apresentação em pdf](/apresentacoes-latinoware-2016/alessandro-instalando-um-gnu-debian-100-livre.pdf) 
Mulheres do meu Brasil, cadê vocês???

- João Eriberto Mota Filho
- [Apresentação em pdf](http://eriberto.pro.br/palestras/mulheres_brasil.pdf)

Novidades do APT (Advance Package Tool)

- Raphael Mota.
- [Apresentação em pdf](/apresentacoes-latinoware-2016/raphael-novidades-do-apt.pdf) 
Organizando eventos no Debian

- Daniel Lenharo
- Giovani Ferreira
- [Apresentação em pdf](/apresentacoes-latinoware-2016/giovani-daniel-organizando-eventos-no-debian.pdf) 
Ultimate debian database (udd) e debian.net: Um show de informações e
curiosidades

- João Eriberto Mota Filho
- [Apresentação em pdf](http://eriberto.pro.br/palestras/udd_debiannet.pdf)

## Fotos:

<https://www.flickr.com/photos/debianbrasil/albums/72157716022510362>