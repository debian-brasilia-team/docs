---
title: Novo líder Debian procura mais inovação para o projeto
description: por Paulo Francisco Slomp
published: true
date: 2025-03-01T17:41:26.125Z
tags: blog, geral
editor: markdown
dateCreated: 2013-07-22T16:59:00.000Z
---

<!-- author: Paulo Francisco Slomp -->

Postado por TutorFree às 09:30 13 de julho de 2013 , enviado por Slomp

O novo líder do projeto Debian GNU / Linux, Lucas Nussbaum, pretende aumentar
a quantidade de inovação que acontece no projeto em si, e não apenas em seus
derivados. Lucas (foto acima) , um professor assistente do curso de ciência da
computação pela Universidade de Lorena, foi eleito como líder (em abril),
derrotando dois outros candidatos. Ele está envolvido com o projeto Debian
desde 2005.

"Geralmente, eu tenho a impressão de que o Debian tenha sido classificada como
uma base muito sólida sobre a qual foram construas várias outras distribuições", disse em uma entrevista. "Muitas coisas interessantes acontecem no ecossistema
Debian, mas muitas vezes fora do próprio projeto, por meio de derivativos, mas
não há nenhuma razão para que a inovação não deva acontecer diretamente no
Debian".

Lucas afirmou que o projeto irá fornecer versões estáveis a cada ~24 meses.
Continuará fornecendo versões de testes que é usado e tido como muito boas e
confiáveis. Claro, ele pode não ser adequado para o seu servidor de missão
crítica, mas é simplesmente perfeito para tê-lo em seu laptop. Usando as
versões de testes do Debian também é uma forma de contribuir para o Software
Livre, como testes de software e reportar bugs, isso melhora a qualidade do
software livre em geral (e da próxima versão estável do Debian,
especificamente)".

Artigo original e a entrevista completa você confere no itwire
<http://www.itwire.com/business-it-news/open-source/60564-new-debian-leader-seeks-more-innovation-within-project>. 
Fonte:
<http://tutorfreebr.blogspot.com.br/2013/07/novo-lider-debian-procura-mais-inovacao.html>