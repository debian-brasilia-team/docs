---
title: Curitiba sediará a DebConf19
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:58:17.315Z
tags: blog, geral
editor: markdown
dateCreated: 2018-01-28T18:18:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

É com grande alegria que informamos que na última quinta-feira (25/01/18) foi
confirmado que **Curitiba sediará a DebConf19 - Conferência Mundial dos(as) Desenvolvedores(as) do Projeto Debian, em 2019**. 
O grupo de Curitiba apresentou o seu
[BID](https://wiki.debconf.org/wiki/DebConf19/Bids/Curitiba) durante a
[reuinião via IRC](http://meetbot.debian.net/debconf-team/2018/debconf-team.2018-01-25-14.33.html) e recebeu bastante elogios. Curitiba já havia concorrido ano passado para sediar a DebConf18 mas perdeu para a cidade de Hsinchu (Taiwan) . Este ano além de
Curitiba, o pessoal da cidade de Bratislava apresentou o seu BID mas apenas para aprender como funciona o processo de avaliação porque eles pretendem concorrer
pra valer ano que vem para a DebConf20, já este não foi possível eles
finalizarem a candidatura.

A data ainda será confirmada, mas a previsão é que aconteça em julho no campus
central da
[UTFPR - Universidade Tecnológica Federal do Paraná](http://www.utfpr.edu.br/curitiba/o-campus/pasta2). 
Durante o evento receberemos contribuidores do Projeto Debian, principalmente
Desenvolvedores oficiais, de vários países.

Esta será a segunda vez que o Brasil sedia uma edição da DebConf. A quinta
edição ([DebConf4](https://debconf4.debconf.org/)) aconteceu de 26 de maio a 2
de junho de 2004 em Porto Alegre.

Veja o
[vídeo](https://gensho.ftp.acc.umu.se/pub/debian-meetings/2017/debconf17/debconf-19-in-your-city.vp9.webm) da apresentação do grupo de Curitiba realizada no dia 12 de agosto durante a
[DebConf18](https://debconf17.debconf.org) em Montreal. Abaixo a foto com Samuel (esquerda) e Valéssio (direta).

![Samuel valessio dc18](https://debianbrasil.org.br/blog/imagens/samuel-valessio-dc18.jpg  =400x)

Foto de Aigars Mahinovs

Ontem (27/01) fizemos um encontro para começar a discutir a organização.
Esperamos no futuro contar com a ajuda de mais pessoas, mesmo que a distância,
para que possamos fazer um grande evento! Abaixo, da esquerda para direita:
Ângelo, Paulo, Cleber, Daniel e Terceiro.

![Reuniao jupter 2018 01 27](https://debianbrasil.org.br/blog/imagens/reuniao-jupter-2018-01-27.jpg  =400x) 
Se você quiser conhecer o local onde acontecerá a DebConf19 venha para a
[MiniDebConf Curitiba 2018](https://minidebconf.curitiba.br/2018) que acontecerá de 11 a 14 de abril :-)