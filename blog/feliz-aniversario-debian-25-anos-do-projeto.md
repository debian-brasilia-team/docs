---
title: Feliz aniversário Debian! 25 anos do Projeto
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:39:39.978Z
tags: blog, geral
editor: markdown
dateCreated: 2018-08-16T17:05:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Hoje (16/08/2018) o [Projeto Debian](https://www.debian.org/) completa 25 anos
de existência.

Feliz aniversário Debian!

E obrigado a todos(as) os(as) contribuidores(as) que fazem deste o maior projeto de Software Livre do mundo.

<https://bits.debian.org/2018/08/debian-is-25-pt-BR.html>

![Debian25years](https://debianbrasil.org.br/blog/imagens/debian25years =400x)


