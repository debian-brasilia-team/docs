---
title: BITS da equipe debian-l10n-portuguese - Nov/2018
description: por Daniel Lenharo de Souza
published: true
date: 2025-03-01T17:57:24.723Z
tags: blog, geral
editor: markdown
dateCreated: 2018-12-02T22:42:00.000Z
---

<!-- author: Daniel Lenharo de Souza -->

Mensalmente emitimos um relatório da
[equipe debian-l10n-portuguese](https://wiki.debian.org/Brasil/Traduzir) do
Projeto [Debian](https://www.debian.org) e este é o quarto deles. O objetivo é
que as atividades da equipe sejam conhecidas e possamos dar os devidos créditos
pelo trabalho produzido por nossos contribuidores. Acreditamos que isso poderá
estimular a participação de novos tradutores e revisores.

## Visão geral de Novembro de 2018

-   07 Páginas traduzidas/atualizadas no site oficial.
-   04 Traduções PO.
-   01 Traduções de páginas wiki

## Pessoas com atividades registradas durante novembro de 2018

-   Adriana Costa
-   Adriano Rafael Gomes
-   Daniel Lenharo de Souza
-   Leandro Ramos
-   Leonardo S. S. da Rocha
-   Paulo Henrique de Lima Santana
-   Qobi Ben Nun
-   Ricardo Fantin da Costa

Se você fez alguma colaboração de tradução que não está listada acima, nos
avise na lista (debian-l10n-portuguese@lists.debian.org) para que seu trabalho
seja devidamente registrado e reconhecido!

## Itens traduzidos

-   wml://devel/website/index.wml
-   wml://www.debian.org/support.wml
-   wml://www.debian.org/events/admin.wml
-   wml://www.debian.org/events/booth.wml
-   wml://www.debian.org/events/checklist.wml
-   wml://www.debian.org/events/keysigning.wml
-   wml://www.debian.org/events/requirements.wml
-   po-debconf://mxallowd/pt\_BR.po
-   po-debconf://sash/pt\_BR.po
-   po-debconf://qcumber/pt\_BR.po
-   po-debconf://isdnutils/pt\_BR.po
-   <https://wiki.debian.org/pt\_BR/VNCviewer>

A equipe de tradução para português do Brasil agradece a todos que colaboram
para que o Debian se torne mais universal!

Venha [ajudar](https://wiki.debian.org/Brasil/Traduzir) você também. Toda ajuda
é bem-vinda e estamos precisando da sua.