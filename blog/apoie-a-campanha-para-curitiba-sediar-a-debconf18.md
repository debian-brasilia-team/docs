---
title: Apoie a campanha para Curitiba sediar a DebConf18
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:57:00.014Z
tags: blog, geral
editor: markdown
dateCreated: 2016-12-06T19:14:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Todos os anos acontece em algum cidade do mundo a
[DebConf - Conferência dos Desenvolvedores do Projeto Debian](https://debconf.org/). A escolha da cidade que sediará a DebConf é realizada um ano e meio antes dela
acontecer, e é decidida por meio de uma votação entre os Desenvolvedores Debian
que participam do [DebConf Global Team](https://wiki.debian.org/Teams/DebConfGlobalTeam). 
O Brasil já sediou uma DebConf. Foi em 2004 quando a [DebConf4](https://debconf4.debconf.org/) aconteceu em Porto Alegre, alguns dias antes do FISL daquele ano. Neste vídeo
produzido pela Infomedia é possível ver um pouco do que aconteceu:

<https://www.youtube.com/watch?v=QJwsw4kn2d8>

Agora **Curitiba está participando da disputa para sediar a DebConf18,** que
será a 19ª edição do evento e acontecerá em 2018. Com o apoio da comunidade
brasileira de desenvolvedores, usuários e entusiastas do Debian, queremos trazer novamente os Desenvolvedores Debian de todo o mundo para o nosso país.

A ideia de candidatar Curitiba para sediar uma DebConf veio apos a realiação de
[vários eventos durantes os anos de 2015 e 2016](https://wiki.debian.org/DebianEvents/br/), mas principalmente após realizarmos a
[MiniDebConf Curitiba 2016](http://br2016.mini.debconf.org/).

Para apoiar a candidatura de Curitiba, compartilhe os banners nas redes sociais. Você também pode trocar a capa do perfil/página/grupo no Facebook e no Twitter.

Em todas as publicações, não esqueça de usar as hashtags:
**\#Debian \#DebConf18 \#Curitiba**

Outra ideia legal é criar um subdomínio "debian" para o seu site, e usar o
arquivo [index.html](http://softwarelivre.org/debianbrasil/campanha/index.html)
como esse. Veja alguns exemplos:

-   <http://debian.softwarelivre.org>
-   <http://debian.curitibalivre.org.br>
-   <http://debian.mulheres.eti.br>
-   <http://debian.flisol.org.br>
-   <http://debian.slcampusparty.com.br>
-   <http://debian.enec.org.br>

O projeto (chamado de BID) de Curitiba para sediar a DebConf18 está em
construção e pode ser visto em:

-   <https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba>

A outra cidade concorrente é Taipei (Taiwan) e a escolha da cidade que sediará
a DebConf18 deverá acontecer no final de janeiro de 2017.

**Banner 754px x 200px **

![](https://debianbrasil.org.br/blog/imagens/banner-754-200.png =400x)

**Banner 754px x 753px**

![](https://debianbrasil.org.br/blog/imagens/banner-754-754.png =400x)

**Banner 851px x 315px - capa de perfil/página/grupo no Facebook**

![](https://debianbrasil.org.br/blog/imagens/cabecalho-facebook-851-315.png =400x)

**Banner 1500px x 500px - capa do twitter**

![](https://debianbrasil.org.br/blog/imagens/cabecalho-twitter-1500-500.png =400x)

**Arquivos svg:**

- [754px x 200px](/campanha/banner-754-200.svg)
- [754px x 754px](/campanha/banner-754-754.svg)
- [851px x 315px](/campanha/cabecalho-facebook-851-315.svg)
- [1500px x 500px](/campanha/cabecalho-twitter-1500-500.svg)


Se tiver alguma sugestão sobre a campanha ou sobre a candidatura de Curitiba,
deixe um comentário abaixo ou envie um email para: <contato@curitibalivre.org.br>