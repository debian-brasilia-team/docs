---
title: MiniDebConf Curitiba 2017 - um breve relato
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:41:14.441Z
tags: blog, geral
editor: markdown
dateCreated: 2017-03-30T19:15:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

![Foto banner minidebconf2017](https://debianbrasil.org.br/blog/imagens/minidebconf-2017-pulpito.jpg =400x) 
### Com esta segunda edição da MiniDebConf Curitiba, o evento se consolida como o principal encontro da comunidade brasileiras de usuários e desenvolvedores Debian. 
De 17 a 19 de março de 2017 foi realizada a segunda edição da
[MiniDebConf Curitiba 2017](http://br2017.mini.debconf.org/) no Campus central
da [UTFPR - Universidade Tecnológica Federal do Paraná](http://www.utfpr.edu.br/), em Curitiba - PR.

No dia 17 (sexta-feira) o evento aconteceu das 14:00h às 19:00h no
miniauditório, iniciando com a abertura oficial da MiniDebConf Curitiba 2017
seguida de 03 palestras. Nos dias 18 e 19 (sábado e domingo) as atividades
aconteceram das 10:00h às 19:00h paralelamente em ambientes diferentes: no
miniauditório, em uma sala ao lado da secretaria, e em dois laboratórios.

### Números da edição 2017

Durante os três dias de evento aconteceram diversas atividades para todos os
níveis de usuários e colaboradores do projeto Debian. A
[programação oficial](http://br2017.mini.debconf.org/programacao.shtml) foi
composta de:

-   16 palestras de todos os níveis;
-   06 oficinas para atividades do tipo mão na massa;
-   06 lightning talks sobre temas diversos;
-   Install fest para instalar Debian nos notebook dos participantes;
-   BSP (Bug Squashing Party - Festa de Caça à Bugs) que colaborou na resolução
de alguns bugs críticos e consequentemente foi uma preciosa contribuição da
comunidade brasileira para o lançamento da versão 9 - codinome Stretch, que
deve acontecer em breve.

Os números finais da MiniDebConf Curitiba 2017 são os seguintes:

-   Total de pessoas inscritas: 342
-   Total de pessoas presentes: 143

Dos 143 participantes, 12 eram contribuidores oficiais brasileiros sendo 7 DDs
(Debian Developers) e 5 (Debian Maintainers). A organização foi realizada por 9
pessoas que começaram a trabalhar ainda no final de 2016 e 22 voluntários que
ajudaram durante o evento.

A diferença entre o número de pessoas inscritas e o número de pessoas presentes
provavelmente se explica pelo fato de não haver cobrança de inscrição, então se
a pessoa desistir de ir ao evento ela não terá prejuízo financeiro.

A edição 2017 da MiniDebconf Curitiba mostra o resultado dos constantes esforços realizados ao longo dos últimos anos para atrair mais colaboradores para a
comunidade Debian no Brasil. Esse resultado pode ser visto nos números
comparados a [edição de 2016](http://br2016.mini.debconf.org/): houve um
aumento de 68% no número de participantes; aumento em 50% do número de palestras na grade; maior diversidade de temas e elevação do nível técnico das atividades
e; entre as edições de 2016 e 2017,
[4 pessoas se tornaram Debian Developer e 3 Debian Maintainer](/blog/brasileiros-mantenedores-e-desenvolvedores-debian-a-partir-de-julho-de-2015/). Você pode ler o relato da
[MiniDebConf 2016](/blog/mini-debconf-curitiba-2016-foi-um-sucesso/).

![Minidebconf2017 banners](https://debianbrasil.org.br/blog/imagens/minidebconf2017-banners.jpg =400x)

### Participação feminina

Tivemos a presença de 32 mulheres entre participantes, voluntárias e
organizadoras, mas infelizmente não tivemos nenhuma palestrante mulher. Apesar
do esforço da organização para parantir palestrantes mulheres que pudessem falar sobre temas relacionados ao Debian, isso não foi possível. Foi discutida a
possibilidade de trazer pela menos uma Debian Developer estrangeira da Europa,
mas os recursos financeiros não foram suficientes.

A organização reconhece esta falha e reforça a necessidade da
[campanha iniciada ano passado](https://wiki.debian.org/MulheresBrasil) para
promover a maior participação de mulheres contribuindo para o Projeto Debian e
palestrando sobre temas relacionados. O Brasil tem duas mulheres Debian
Developers mas atualmente elas moram fora do país. O objetivo nos próximos anos
é aumentar este número.

![Minidebconf2017 mulheres](https://debianbrasil.org.br/blog/imagens/minidebconf2017-mulheres.jpg =400x)

### Campanha de financiamento coletivo

Uma novidade importante marcou esta edição da MiniDebConf: elaboramos uma
[campanha de financiamento coletivo](http://br2017.mini.debconf.org/doacao.shtml) (crowdfunding) e conseguimos arrecadar o dinheiro para custear as despesas diversas como
materiais gráficos (crachás, cartazes, banners de lona), cordões de crachá,
coffee-breaks, diárias do pessoal que trabalhou no final de semana no auditório
e nos laboratórios da Universidade, etc. O crowdfunding foi uma experência
inédita, até então não tínhamos relato de tal iniciativa e isso mostra mais uma
vez a força e empenho da comunidade. A meta inicial era de R\$ 2.000,00 e foram
arrecadados R\$ 2.800,00 com a colaboração de 52 doadores do Brasil e do
exterior.

Cada participante recebeu: crachá, cordão de crachá com a logo do Debian, cubo
de papel para montar com dicas de como contribuir com o Debian.

[Prestação de contas.](https://debianbrasil.org.br/blog/imagens/prestacao-contas-minidebconf-2017.png =400x) 
![Minidebconf2017 doadores](https://debianbrasil.org.br/blog/imagens/minidebconf2017-doadores.jpg =400x)

### Venda de produtos

Não podemos deixar de comentar da tradicional
[lojinha](http://loja.curitibalivre.org.br/), que se faz presente em todos os
eventos, com diversos produtos como camisetas, chaveiros, bottons, canecas e
adesivos. Os valores arrecados com a venda dos diversos produtos é totalmente
revertida para custear a realização dos eventos Debian no Brasil e também outros eventos organizados pela
[Comunidade Curitiba Livre](http://curitibalivre.org.br/).

![Minidebconf2017 lojinha](https://debianbrasil.org.br/blog/imagens/minidebconf2017-lojinha.jpg =400x)

### Bug Squashing Party - Festa de Caça à Bugs (BSP)

Durante o evento realizamos uma
[Bug Squashing Party (BSP)](https://wiki.debian.org/BSP/2017/03/br/Curitiba),
onde visamos resolver bugs críticos que impedissem o lançamento da próxima
versão estável do Debian (Stretch). Apesar de ter acontecido paralelamente a
diversas atividades dentro da programação do evento, conseguimos nos dedicar
uma parte do tempo e enviar patches para solucionar
[3 bugs críticos](https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org;tag=bsp-2017-03-br-curitiba). Além dos 3 bugs fechados, houve a descoberta de um bug grave que afetava updates Jessie -> Stretch, este bug foi reportado por um participante do evento e este
teve ajuda de um DM, que não estava participando do BSP, mas que o auxiliou na
abertura do bugreport e na resolução do mesmo
[#858200](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=858200).

Consideramos um bom resultado, já que tivemos 3 pessoas trabalhando nessa
atividade no decorrer dos dias, sendo essa uma contribuição valiosa da
comunidade Debian Brasil em um curto espaço de tempo.

![Minidebconf2017 bsp](https://debianbrasil.org.br/blog/imagens/minidebconf2017-bsp.jpg =400x)

### Fotos e vídeos

Fizemos um esforço para transmitir ao vivo as atividades que aconteceram no
miniauditório e gravamos para disponibilizar os arquivos no repositório de
vídeos do Debian. Você pode assistir as palestras no link abaixo:

[youtube](https://www.youtube.com/playlist?list=PLU90bw3OpxLoKKXpGVpGzEiITDDnHzgAu) ou
[peertube](https://peertube.debian.social/videos/watch/playlist/cf8f5f6e-f4b0-4ab9-9645-942b5cb2fde1) 
Fotógrafos voluntários ajudaram a registrar todo o evento e as fotos estão no
link:

<http://deb.li/2017br>

### Uso de redes sociais livres

Desde o início da organização da MiniDebConf Curitiba 2017 usamos as redes
sociais livres para divulgar o evento. Além dessa divulgação prévia, tínhamos
como meta fazer uma cobertura durante o evento publicando noticias e fotos nas
redes sociais livres para ajudar a gerar conteúdo e levar para mais pessoas
para elas.

Com a ajuda de voluntários, conseguimos atingir parcialmente esse objetivo
porque infelizmente a rede diaspora.softwarelivre.org ficou offline três dias
antes do início da MiniDebConf, e a rede identi.ca ficou fora do ar no segundo
dia. Veja as pulblicações que fizemos e aproveite para se inscrever nas redes:

-   GNU Social: <https://quitter.se/debianbrasil>
-   Pump.io: <https://identi.ca/debianbrasil>
-   Diaspora: <https://diaspora.softwarelivre.org/u/curitibalivre>
-   Hubzilla: <https://hub.vilarejo.pro.br/channel/debianbrasil>

### Evento Parceiro

Durante a MiniDebConf aconteceu um evento convidado chamado
["Inspire-se! Mulheres na Computação - Experiências e Aprendizados"](https://www.sympla.com.br/inspire-se-mulheres-na-computacao---experiencias-e-aprendizados__123063) que foi organizado pelo grupo de estudos
[Emílias - Armação em Bits](http://emilias.dainf.ct.utfpr.edu.br/) da UTFPR e a
Adriana Cássia da Costa que participou da organizaçao
da MiniDebConf. Essa atividade aconteceu no sábado (18/03) no período da tarde
com uma painel que debateu sobre como ser desenvolvedora de software, uma
palestra sobre machine learning outra sobre iniciativar para mulheres na
tecnologia.

O maior do público foi composto por mulheres que participam de alguma iniciativa que incentiva a participação de mulheres na tecnologia e também tivemos pessoas
que tem interesse em trabalhar com tecnologia e foram ao evento para saber como
fazer isso.

Essa foi a primeira vez que tivemos eventos parceiros acontecendo durante a
MiniDebConf e o resultado foi muito positivo. Conseguimos debater vários
assuntos como o mercado de trabalho de TI, como se tornar um bom profissional
na área, a inclusão de pessoas de diferentes áreas na tecnologia, a questão
ética e social que estão envolvidas na tecnologia.

Veja as fotos aqui: <https://www.flickr.com/photos/curitibalivre/albums/72157679965837780> 
### Agradecimentos

Agradecemos a todos que estiveram presentes e fizeram da MiniDebConf Curitiba
2017 um sucesso!

Agradecemos aos palestrantes que nos presentearam com palestras de alto nível,
aos participantes que a todo momento expuseram os seus pontos de vista e
enriqueceram as discussões, e a todos os voluntários e voluntárias que não
mediram esforços para a organização do evento.

Agradecemos aos professores e funcionários da UTFPR - Universidade Tecnológica
Federal do Paraná, que tão bem nos receberam e cederam seu espaço para a
realização da MiniDebConf.

Por fim, agradecemos as empresas [4Linux](https://www.4linux.com.br/) e
[Locaweb](https://www.locaweb.com.br/) que acreditaram em nosso potencial e
adquiriram cada uma delas a cota de patrocínio de maior valor.

**Reforçamos o convite para que mais pessoas venham colaborar com a comunidade Debian Brasil.** 
![Foto oficial minidebconf2017](https://debianbrasil.org.br/blog/imagens/foto-oficial-minidebconf2017.jpg)

![Minidebconf2017 patroc org](https://debianbrasil.org.br/blog/imagens/minidebconf2017-patroc-org.png)