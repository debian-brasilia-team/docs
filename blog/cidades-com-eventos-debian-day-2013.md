---
title: Cidades com eventos Debian Day 2013
description: por André Felipe Machado
published: true
date: 2025-03-01T17:57:52.101Z
tags: blog, geral
editor: markdown
dateCreated: 2013-08-07T00:41:00.000Z
---

<!-- author: André Felipe Machado -->

![](https://debianbrasil.org.br/blog/imagens/debianday.png)

### O que é

É um [evento internacional](https://wiki.debian.org/DebianDay) realizado
anualmente no dia 16 de agosto ou no final de semana mais próximo para comemorar o aniversário do Debian.

Sendo um dia totalmente dedicado a divulgação e contribuições ao projeto.

Tendo como nome do evento **"DebianDay"**, em português **"Dia Debian"**, ou
ainda **"Dia D"**.

### Como participar?

Nesta página, coordenamos esforços de grupos de usuários e comunidades do Brasil para celebrar o **DebianDay Brasil 2013**.

Você pode criar uma página para o evento de sua cidade, utilizando na
coordenação e divulgação programática do evento. Se preferir você pode cadastrar um link direto para um blog ou site externo do DebianDay de sua cidade ou grupo. 
Publique [nesta página wiki](https://wiki.debian.org/DebianDay/2013) as
informações sobre eventos e comemorações Debian Day 2013 em sua cidade.

Se você esta interessado em ter ou organizar o DebianDay em sua cidade e tem
mais dúvidas, por favor entre em contato através do tópico de
[fórum deste ano](http://softwarelivre.org/debian-rs/forum-de-discussoes/o-que-faremos-no-debian-day-2013). 
### Divulgando

Você pode utilizar nosso mini-banner para divulgar esta página e incentivar
mais cidades ou grupos participarem

![](https://debianbrasil.org.br/blog/imagens/dday.png)
