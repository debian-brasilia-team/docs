---
title: Como foram as atividades Debian na #CPBR11 em São Paulo
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T19:11:31.492Z
tags: blog, geral
editor: markdown
dateCreated: 2018-02-05T12:36:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A comunidade Debian Brasil esteve presente na
[Campus Party Brasil 2018 - CPBR11](http://brasil.campus-party.org/), que
aconteceu de 30 de janeiro a 04 de fevereiro no Anhembi em São Paulo.

![Cpbr11 2018 001](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-001.jpg  =400x)

## Atividades realizadas

**1 - Oficina para assinatura de chaves OpenPGP**

Realizada no dia 31 de janeiro das 18h00min às 20h00min

Instrutor:

- Giovani Ferreira (DD)

<https://campuse.ro/events/campus-party-brasil-2018/workshop/oficina-para-assinatura-de-chaves-openpgp-cpbr11-comunidades-softwarelivre> 
![Cpbr11 2018 037](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-037.jpg  =400x)

**2 - Oficina: Servidor web em Debian: HTML, PHP e MySQL na prática**

Realizada no dia 1 de fevereiro das 10h30min às 13h00min

Instrutor:

- Wellton Costa

<https://campuse.ro/events/campus-party-brasil-2018/workshop/servidor-web-em-debian-html-php-e-mysql-na-pratica-cpbr11-comunidades-softwarelivre> 
![Cpbr11 2018 059](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-059.jpg  =400x)

**3 - Oficina: Ensinando e praticando o "transplante" de uma instalação Debian GNU/Linux** 
Realizada no dia 2 de fevereiro das 15h00min às 17h00min

Instrutor:

- Paulo Roberto Alves de Oliveira (Kretcheu) (DM)

<https://campuse.ro/events/campus-party-brasil-2018/workshop/ensinando-e-praticando-o-transplante-de-uma-instalacao-debian-gnulinux-cpbr11-comunidades-softwarelivre> 
**4 - Oficina: Debian no desktop, instalando e conhecendo as opções**

Realizada no dia 2 de fevereiro das 17h45min às 19h45min

Instrutor:

- Daniel Lenharo (DD)

<https://campuse.ro/events/campus-party-brasil-2018/workshop/debian-no-desktop-instalando-e-conhecendo-as-opcoes-cpbr11-comunidades-softwarelivre> 
![Cpbr11 2018 063](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-063.jpg  =400x)

**5 - Painel: Debian - ouvindo experiências e contribuindo para o projeto**

Realizado no dia 03 de fevereiro da 00h00min às 1h00min.

Palestrantes:

- Daniel Lenharo (DD)
- Paulo Henrique de Lima Santana (DM)
- Antonio C. C. Marques

[Vídeo](https://www.youtube.com/watch?v=AXiiyuCexao)

<https://campuse.ro/events/campus-party-brasil-2018/talk/debian-ouvindo-experiencias-e-contribuindo-para-o-projeto-cpbr11> 
![Cpbr11 2018 079](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-079.jpg  =400x)

Foto da esquerda para direita: Paulo, Antonio e Daniel

**6 - Workshop: Contribuindo para o projeto Debian**

Realizado no dia 03 de fevereiro das 10h30min às 13h00min.

Palestrantes:

- Daniel Lenharo (DD)
- Lucas Kanashiro (DD)
- Paulo Henrique de Lima Santana (DM)

<https://campuse.ro/events/campus-party-brasil-2018/talk/workshop-contribuindo-para-o-projeto-debian> 
![Cpbr11 2018 086](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-086.jpg  =400x)

Foto da esquerda para direita: Paulo, Lucas e Daniel

Após uma apresentação inicial realizada pelo Paulo sobre
[como contribuir](http://softwarelivre.org/arquivos-de-apresentacoes/o-projeto-debian-quer-voce.pdf) para o Projeto Debian, o pessoal foi separado em dois grupos para aprender e/ou
tirar dúvidas sobre empacotamento com o Lucas e sobre tradução com o Daniel.

![Cpbr11 2018 099](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-099.jpg  =400x)

Foto Lucas ensinando empacotamento

![Cpbr11 2018 089](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-089.jpg  =400x)

Foto Daniel ensinando tradução

![Cpbr11 2018 012](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-012.jpg  =400x)

Foto da esquerda pra direita: Rafael, Kretcheu, Giovani, Daniel e Paulo

## Outras fotos

![Cpbr11 2018 139](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-139.jpg  =400x)

Foto da esquerda pra direita: Paulo, Antonio e Daniel

![Cpbr11 2018 017](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-017.jpg  =400x)

Foto Antonio Marques - contribuidor Debian e Maker

![Cpbr11 2018 140](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-140.jpg  =400x)

Giovani Ferreira

![Cpbr11 2018 142](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-142.jpg  =400x)

Foto adesivos à venda

![Cpbr11 2018 143](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-143.jpg  =400x)

Foto camisas à venda

![Cpbr11 2018 144](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-144.jpg  =400x)

Foto camisas à venda

![Cpbr11 2018 145](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-145.jpg  =400x)

Foto  tweet da OSI

![Cpbr11 2018 146](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-146.jpg  =400x)

Foto tweet do Bruce Perens

![Cpbr11 2018 110](https://debianbrasil.org.br/blog/imagens/cpbr11-2018-110.jpg  =400x)

**Veja mais fotos no link abaixo:**

<https://www.flickr.com/photos/slcampusparty/albums/72157669221197639>