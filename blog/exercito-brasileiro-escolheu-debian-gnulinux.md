---
title: Exército Brasileiro escolheu Debian GNU/Linux
description: por André Felipe Machado
published: true
date: 2025-03-01T17:39:35.475Z
tags: blog, geral
editor: markdown
dateCreated: 2013-07-22T22:03:00.000Z
---

<!-- author: André Felipe Machado -->

Em tempos do escândalo de vazamento e
[bisbilhotagem de informações internacional](http://softwarelivre.org/debian-rs/blog/windows-tem-backdoor-para-nsa-desde-1999-enquanto-debian-e-livre), alicerçada em lei dos EUA, vale a pena estudar os motivos porque o Exército Brasileiro escolheu Debian GNU/Linux.

Faz todo o sentido usar software livre quando você, sua empresa ou instituição
possui informações de algum valor de segurança, importância estratégica, ou
diferencial competitivo.

Leia sobre a [escolha](http://eriberto.pro.br/artigos/debian_no_exercito.pdf)
do Exército Brasileiro.
