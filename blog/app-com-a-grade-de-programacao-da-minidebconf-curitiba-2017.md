---
title: App com a grade de programação da MiniDebConf Curitiba 2017
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:57:02.268Z
tags: blog, geral
editor: markdown
dateCreated: 2017-03-06T13:35:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Está disponível na Play Store o app com informações sobre a
**MiniDebConf Curitiba** que acontecerá de 17 a 19 de março no Campus Central
da UTFPR - Universidade Tecnológica Federal do Paraná.

Você poderá consultar a grade completa de atividades com a programação das
palestras, dos painéis, das lightning talks, das oficinas e dos eventos sociais. Também é possível consultar os currículos dos palestrantes, a localização da
UTFPR, o mapa do Campus.

Além disso, estão disponíveis informações gerais sobre o evento, lista dos
patrocinadores, política anti-assédio, como realizar a sua inscrição, lista de
organizadores, e formas de contato com a organização.

<https://play.google.com/store/apps/details?id=com.minidebconfcuritiba>

O app está disponível apenas para android.

Para quem não quiser instalar pelo loja do google, o arquivo apk está disponível no link abaixo:

<http://debian.softwarelivre.org/app>