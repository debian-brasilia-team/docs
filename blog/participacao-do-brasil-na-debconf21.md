---
title: Participação do Brasil na DebConf21
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:41:46.869Z
tags: blog, geral
editor: markdown
dateCreated: 2021-09-02T17:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A equipe de publicidade do Debian publicou uma notícia no site com o título
[termina a DebConf21 on-line](https://www.debian.org/News/2021/20210828)
relatando de maneira geral como foi o evento. Como já era de se esperar,
a [DebConf21](https://debconf21.debconf.org) que aconteceu online de 24 a 28 de
agosto de 2021 foi um sucesso!

Na edição da DebConf deste ano foi aberta uma chamada para trilhas de atividades por idioma, e nós do Debian Brasil nos inscrevemos para receber propostas de
atividades em português.

Recebemos 7 propostas de atividades e foi possível agendar todas na
[grade de programação](https://debconf21.debconf.org/schedule/),
divididas em três dias durante a noite, ideal para quem mora no Brasil por ser
fora do nosso horário comercial.

As atividades foram:

* [Abertura da trilha português](https://saimei.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-150-abertura-da-trilha-portugues.webm) - Paulo Henrique de Lima Santana e Daniel Lenharo. * [BoF Times locais Brasil](https://chuangtzu.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-179-bof-times-locais-brasil.webm) - Daniel Lenharo, Paulo Santana, Lucas Kanashiro, Sérgio Durigan, Paulo Kretcheu, Felipe Maia, Sérgio Cipriano. * [Construindo máquinas virtuais com KVM, QEMU e LibVirt](https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-160-construindo-maquinas-virtuais-com-kvm-qemu-e-libvirt.webm) - Fernão Vellozo. * [Hacker ou Maker: Uma analise a partir do contrato social do sistema operacional universal](https://chuangtzu.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-158-hacker-ou-maker-uma-analise-a-partir-do-contrato-social-do-sistema-operacional-universal.webm) - Antonio Marques. * [Meus passos utilizando Linux](https://saimei.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-159-meus-passos-utilizando-linux.webm) - Tiago Zaniquelli. * [O Profissional da Educação e o Debian: pelo uso de softwares livres nas escolas](https://saimei.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-157-o-profissional-da-educacao-e-o-debian-pelo-uso-de-softwares-livres-nas-escolas.webm) - Ramon Mulin. * [Trabalho das comunidades brasileiras na tradução de softwares livres](https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-151-trabalho-das-comunidades-brasileiras-na-traducao-de-softwares-livres.webm) - Thiago Pezzo, Carlos Melara, Daniel Lenharo, Paulo Santana, Ricardo Fonseca. 
Sobre essa última atividade, você pode ler aqui no nosso site um
[resumo](https://debianbrasil.org.br/blog/como-foi-a-participacao-da-equipe-de-localizacao-na-debconf21) do que aconteceu.

Além destas atividades em português, tivemos a participação dos brasileiros
Antonio Terceiro, Daniel Lenharo, Fernao Vellozo, Lucas Kanashiro, Nick Vidal,
Paulo Santana, Sérgio Durigan Júnior em outras atividades em inglês.

Os brasileiros Carlos Melara, Daniel Lenharo e Paulo Santana ajudaram durante
a transmissão das atividades desempenhando os papeis de diretores ou de
apresentadores (talk meister).

![Voluntários](https://debianbrasil.org.br/blog/imagens/debconf21-voluntarios.png)

Infelizmente não tivemos a participação de mulheres brasileiras entre os
palestrantes e voluntários, o que não ajuda o aumento da diversidade do Debian
no Brasil :-(

Este ano tivemos também uma empresa nacional entre os patrocinadores.
A [Globo](https://www.globo.com/), que já havia patrocinando a DebConf19 em
Curitiba, foi patrocinadora Silver na DebConf21, o que nos deixou muito felizes
por ter uma empresa brasileira apoiando o evento.

![Globo](https://debianbrasil.org.br/blog/imagens/logo-globo-100px.png)

No [encerramento da DebConf21](https://chuangtzu.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-180-closing-ceremony-cheese-and-wine.webm) foram apresentados alguns dados estatísticos que mostraram os(as) brasileiros(as) entre os primeiros lugares.

Quantidade de pessoas inscritas (attendees):

![Número de pessoas inscritas](https://debianbrasil.org.br/blog/imagens/debconf21-inscritos-1.png =400x)

Gráfico das pessoas inscritas por país:

![Gráfico de pessoas inscritas](https://debianbrasil.org.br/blog/imagens/debconf21-inscritos-2.png =600x)

Atividades realizadas:

![Eventos](https://debianbrasil.org.br/blog/imagens/debconf21-palestrantes-1.png =400x)

Gráfico dos palestrantes (speakers) por país:

![Gráfico palestrantes](https://debianbrasil.org.br/blog/imagens/debconf21-palestrantes-2.png =600x)

Dados sobre a transmissão:

![Transmissão](https://debianbrasil.org.br/blog/imagens/debconf21-transmissao-1.png =400x)

Gráfico das pessoas que assistiram por país:

![Gráfico público](https://debianbrasil.org.br/blog/imagens/debconf21-transmissao-2.png =600x)

Agradecemos a todas as pessoas envolvidas na realização da DebConf21 e aquelas
que prestigiaram o evento assistindo as atividades.

Os(As) participantes foram convidadas a enviarem uma foto para compor a
foto oficial da DebConf21.

![Foto oficial](https://debianbrasil.org.br/blog/imagens/debconf21_group_small.jpg)

Esperamos que em 2022 possamos nos encontrar pessoalmente na
[DebConf22 em Kosovo](https://chuangtzu.ftp.acc.umu.se/pub/debian-meetings/2021/DebConf21/debconf21-130-debconf22-in-kosovo.webm). 

