---
title: Como foi a participação da comunidade Debian na #CPBR10
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T19:10:42.062Z
tags: blog, geral
editor: markdown
dateCreated: 2017-02-10T17:37:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

Durante a [Campus Party Brasil 2017 - CPBR10](http://brasil.campus-party.org/),
que aconteceu de 31 de janeiro a 04 de fevereiro de 2017 no Anhembi em São
Paulo, a comunidade Debian realizou várias atividades.

No dia 01/02 (quarta-feira) a [Karen Sandler,](https://nm.debian.org/person/karen) que era uma das convidadas internacionais destaque no evento, fez uma palestra
no palco principal com o título
"[Cyborgs Unidos!](https://sfconservancy.org/news/2017/feb/09/karen-campus-party-brasil/)". A Karen é Debian Developer e Diretora Executiva da
[Software Freedom Conservancy](https://sfconservancy.org).

![Karen cpbr10](https://debianbrasil.org.br/blog/imagens/karen-cpbr10.jpg =400x)

No dia 02/02 (quinta-feira) o
[Samuel Henrique](https://contributors.debian.org/contributor/samueloph)
ministrou o workshop "[Primeiros passos Debian
(e derivados)](http://campuse.ro/events/campus-party-brasil-2017/workshop/primeiros-passos-debian-e-derivados)". O Samuel é atualmente
[Debian Maintainer](https://nm.debian.org/person/samueloph).

![Samuel cpbr10](https://debianbrasil.org.br/blog/imagens/samuel-cpbr10.jpg =400x)

No dia 03/02 (sexta-feira) foi a vez do
[Daniel Lenharo](https://contributors.debian.org/contributor/lenharo)
apresentar a palesta
"[Debian - o sistema universal](http://campuse.ro/events/campus-party-brasil-2017/talk/debian-o-sistema-universal)". O Daniel está atualmente passando pelo processo de provas para se tornar
[Debian Developer](https://nm.debian.org/person/lenharo).

![Daniel cpbr10](https://debianbrasil.org.br/blog/imagens/daniel-cpbr10.jpg =400x)

A última atividade foi um painel realizado no dia 03/02 (sexta-feira) com o
título
"[Debian - Ouvindo experiências e contribuindo para o projeto](http://campuse.ro/events/campus-party-brasil-2017/talk/painel-debian-no-brasil)", onde participam o Sanuel Henrique, o Daniel Lenharo e o
[Paulo Henrique de Lima Santana](https://contributors.debian.org/contributor/phls-guest%40alioth/). O Paulo é atualmente [Debian Maintainer](https://nm.debian.org/person/phls).

![Daniel samuel paulo](https://debianbrasil.org.br/blog/imagens/damiel-samuel-paulo.jpg =400x)

Usamos um conhecido meme para explicar como qualquer pessoa pode ajudar o Debian :-) 
![Como ajudar o debian](https://debianbrasil.org.br/blog/imagens/como-ajudar-o-debian.png =400x)

Além das atividades nos palcos, a comunidade aproveitou para vender alguns
materiais como camisetas e adesivos. O dinheiro arrecadado com as vendas será
revertido na organização de eventos de software livre organizados pela
[Comunidade Curitiba Livre](http://curitibalivre.org.br/).

[Wellton Costa](http://campuse.ro/events/vire-um-curador-na-cpbr10-votos/talk/seja-dono-de-verdade-seu-libreoffice-online-e-sua-propria-nuvem-com-nextcloud), [Thiago Mendonça](http://campuse.ro/events/campus-party-brasil-2017/workshop/sincronizacao-e-backup-de-arquivos-com-software-livre/), Samuel Henrique e Paulo Kretcheu instalaram Debian nos notebooks do pessoal que
foi até a bancada pedir ajuda para instalar. Todos os anos nós realizamos um
install fest  nas bancadas para todos os interessadores em instalar GNU/Linux
na Campus Party Brasil.

[Bancadas cpbr10](https://debianbrasil.org.br/blog/imagens/bancadas-cpbr10.jpg =400x)

Houve ainda a divulgação da [MiniDebConf Curitiba 2017](http://br2017.mini.debconf.org/) que acontecerá de 17 a 19 de março no campus central da UTFPR.

![Adesivos cpbr10](https://debianbrasil.org.br/blog/imagens/adesivos-cpbr10.jpg =400x)

![Camisetas cpbr10](https://debianbrasil.org.br/blog/imagens/camisetas-cpbr10.jpg =400x)

Como Curitiba está
[concorrendo para sediar a DebConf18](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba), fizemos algumas camisetas para promover uma ação de apoio a nossa candidatura.
[Brasil loves Debian](http://debian.softwarelivre.org/). Durante os dias da
Campus nós mandamos algumas fotos para o canal do IRC \#debconf-team.

Veja algumas fotos:

![Samuel daniel barbara paulo cpbr10](https://debianbrasil.org.br/blog/imagens/samuel-daniel-barbara-paulo-cpbr10.jpg =400x) 
![Samuel paulo wellton cpbr10](https://debianbrasil.org.br/blog/imagens/samuel-paulo-wellton-cpbr10.jpg =400x) 
![Thiago ana cpbr10](https://debianbrasil.org.br/blog/imagens/thiago-ana-cpbr10.jpg =400x)

O Jon Maddog gravou uma mensagem de apoio a nossa candidatura:

Você pode ver todas as fotos no álbum:

<https://www.flickr.com/photos/slcampusparty/albums/72157676339619643>