---
title: Links da live Debian Brasil de 27/04/2021
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:25.311Z
tags: blog, geral
editor: markdown
dateCreated: 2021-05-01T10:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A seguir estão os links citados na live do
[canal Debian Brasil no YouTube](https://www.youtube.com/debianbrasiloficial)
do dia 27 de abril de 2021.

[Vídeo](https://www.youtube.com/watch?v=fhYHmMc9Zuw) para assistir.

Participantes:

* [Daniel Lenharo](https://www.sombra.eti.br/)
* [Paulo Santana](https://phls.com.br)

Bloco de notícias

* [DebConf21](https://debconf21.debconf.org/): será online de 22 a 29 de
agosto de 2021.
    * Provável trilha em português
* Canais de notícias que voltaram a ficar ativos:
    * telegram: [DebianBrasilNoticias](https://t.me/DebianBrasilNoticias)
    * lista de discussão: [debian-news-portuguese](https://lists.debian.org/debian-news-portuguese/) * [Resultado da eleição do DPL](https://bits.debian.org/2021/04/2021dpl-election-pt-BR.html) * [Resultado do sprint do time de tradução](https://debianbrasil.org.br/blog/resultado-do-sprint-ddtp-ddtss-da-equipe-l10n-pt-br/) * [Chamada](https://lists.debian.org/debian-l10n-portuguese/2021/04/msg00137.html) para o próximo sprint do time de tradução que acontecerá de 24/04 a 02/05
focado no debian-handbook.

Conheça e coloabore para o [screenshots.debian.net](https://screenshots.debian.net) 
* <https://lists.debian.org/debian-news-portuguese/2021/msg00004.html>

Conheça e coloabore para o [debtags.debian.org](https://debtags.debian.org)

* <https://lists.debian.org/debian-devel-announce/2021/04/msg00005.html>