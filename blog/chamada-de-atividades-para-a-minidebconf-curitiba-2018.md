---
title: Chamada de atividades para a MiniDebConf Curitiba 2018
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:57:38.799Z
tags: blog, geral
editor: markdown
dateCreated: 2017-12-16T20:44:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

## Sobre a MiniDebConf Curitiba 2018

A [MiniDebConf](https://minidebconf.curitiba.br/2018/) é o encontro da comunidade Debian no Brasil. A edição de 2018 vai acontecer no Campus Central da UTFPR -
Universidade Tecnológica Federal do Paraná, em Curitiba, entre 11 e 14 de abril
de 2018.

Durante os dias 11 e 12 vai acontecer a MiniDebcamp, um momento onde vamos nos
concentrar em fazer contribuições práticas com o projeto, não havendo palestras
ou outras atividades do gênero. Nos dias 13 e 14, acontece a MiniDebConf
propriamente dita, com palestras, debates, oficinas, com as atividades práticas
continuando em paralelo.

Para mais informações:

<https://minidebconf.curitiba.br/2018>

## Tópicos

Em princípio, qualquer tópico relacionado a software livre está dentro do escopo do evento. No entanto, será dada prioridade a tópicos relacionados ao projeto
Debian, e dentre estes, será dada prioridade a tópicos relacionados a
contribuição com o projeto.

## Diversidade

A MiniDebConf é um evento comprometido com a diversidade. Gostaríamos muito de
receber propostas de atividades de pessoas que fazem parte de grupos com pouca
representação na comunidade de software livre e que gostariam de falar sobre
algo relacionado ao Debian. Se você faz parte de um desses grupos, nos envie sua proposta. Se você conhece alguém que faz parte de um desses grupos, incentive
que essa pessoa nos envie uma proposta de atividade.

Se você gostaria de palestrar sobre Debian mas não tem certeza sobre o que
poderia falar, entre em contato que podemos te ajudar.

## Tipos de atividade

Você pode enviar 4 tipos de atividades:

-   Lightning talk (5 minutos)
-   Painel
-   Palestra
-   Oficina (3 horas)

Para os paineis e palestras você pode escolher o tempo máximo de duração:

-   30 minutos
-   1 hora
-   1 hora e 30 minutos
-   2 horas

## Datas

**Limite para submissão: 31 de janeiro de 2018.**

Notificação de resultado da seleção aos proponentes : até o dia 11 de fevereiro
de 2018.

## Submissão

Para enviar sua proposta de atividade acesse o formulário (indisponível).