---
title: Como foi o Minievento Debian no FTSL 2017 em Curitiba
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:58:03.813Z
tags: blog, geral
editor: markdown
dateCreated: 2017-10-02T19:28:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

De 27 a 29 de setembro de 2017 aconteceu a 9a edição
[Fórum de Tecnologia em Software Livre - FTSL](http://ftsl.org.br), no Campus
central da
[Universidade Tecnológica Federal do Paraná - UTFPR](http://www.utfpr.edu.br/curitiba/o-campus/pasta2), mesmo local onde aconteceu a
[MiniDebConf Curitiba](http://br2017.mini.debconf.org/) em março deste ano. E a
comunidade Debian participou realizando algumas palestras no terceiro dia do
evento.

Alguns meses atrás a organização do FTSL procurou membros das comunidades de
Software Livre em Curitiba e propôs que eles organizassem pequenos eventos
dentro do FTSL. Cada comunidade teria uma sala disponível para organizar suas
atividades em um dos três dias do evento (quarta, quinta ou sexta).

Algumas dessas comunidades que realizaram suas atividades foram:
[PHP](https://phppr.org/), [Python](https://grupypr.github.io/) e
[Debian](/blog/debian-brasil-estara-no-ftsl-2017-em-curitiba/).

A Comunidade Debian realizou 6 palestras ao longo do terceiro dia:


### 1 - Por que sou usuário do GNU/Linux/DEBIAN?

- Palestante: Antonio C C Marques

![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-antonio.jpg =400x)


### 2 - Como se tornar um membro oficial do Debian (DD ou DM)

- Arquivo da apresentação: [pdf](http://www.eriberto.pro.br/palestras/debian-dd.pdf) - Vídeo da palestra: [youtube](https://www.youtube.com/watch?v=UGuv-PZYr3w) e [peertube](https://peertube.debian.social/videos/watch/f41aefde-f01e-4f71-be37-2854f63d3cb1) - Palestrante: [João Eriberto Mota Filho](https://contributors.debian.org/contributor/eriberto@debian.org/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-eriberto.jpg =400x)


### 3 - Debian - O sistema universal

- Arquivo da apresentação: [pdf](http://softwarelivre.org/arquivos-de-apresentacoes/debian-o-sistema-universal-daniel-lenharo-ftsl-2017.pdf) - Vídeo da palestra: [youtube](https://www.youtube.com/watch?v=McUhtsBhu6E&feature=youtu.be) e [peertube](https://peertube.debian.social/videos/watch/164d1535-7128-40ca-b02f-93313d21d643) - Palestante: [Daniel Lenharo de Souza](https://contributors.debian.org/contributor/lenharo@debian.org/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-daniel-1.jpg =400x)


### 4 - O Projeto Debian quer você!

- Arquivo da apresentação: [pdf](http://softwarelivre.org/arquivos-de-apresentacoes/o-projeto-debian-quer-voce-paulo-santana-ftsl-2017.pdf) e [odp](http://softwarelivre.org/arquivos-de-apresentacoes/o-projeto-debian-quer-voce-paulo-santana-ftsl-2017.odp) - Palestrante: [Paulo Henrique de Lima Santana](https://contributors.debian.org/contributor/phls-guest@alioth/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-paulo.jpg =400x)


### 5 - Conhecendo o trabalho da equipe de tradução do Debian

- Arquivo da apresentação: [pdf](http://softwarelivre.org/arquivos-de-apresentacoes/conhecendo-o-trabalho-da-equipe-de-traducao-do-debian-daniel-lenharo-ftsl-2017.pdf) - Palestrante: [Daniel Lenharo de Souza](https://contributors.debian.org/contributor/lenharo@debian.org/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-daniel-2.jpg =400x)


### 6 - Debian 101

- Arquivo da apresentação: pdf
- Palestrante: [Samuel Henrique](https://contributors.debian.org/contributor/samueloph-guest@alioth/) 
![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-samuel.jpg =400x)

Aproveitamos o FTSL para vender vários produtos produzidos pela
[Comunidade Curitiba Livre](http://curitibalivre.org.br/) para os
participantes. Lembrando que o lucro revertido para a organização de futuros
eventos de Software Livre. Você pode ver nas fotos abaixo os produtos:


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-1.jpg =400x)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-2.jpg =400x)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-3.jpg =400x)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-4.jpg =400x)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-produtos-5.jpg =400x)


Entre os produtos novos que fizemos está a camisa "Debian Women" para
incentivar a participação de mais mulheres no Projeto Debian. Para saber mais
sobre o projeto Debian Women acesse
[https://www.debian.org/women](https://www.debian.org/women/index.pt.html) e a
wiki brasileira <https://wiki.debian.org/MulheresBrasil>


A Adriana da Costa garantiu a camisa dela :-)


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-adriana.jpg =400x)


Para comprar os produtos online, acesse o site:
<http://loja.curitibalivre.org.br>

Juntamos o pessoal para tirar uma foto:


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-debian-todos.jpg =400x)

Foto da esquerda pra direita: Eriberto, Paulo, Adriana, Daniel, Antonioe Samuel


Agradecemos imensamente aos organizadores do FTSL 2017 pelo empenho na
organização do evento e pela disponibilidade do espaço para realizamos nossas
atividades. A organização está de parabéns pelo sucesso do FTSL 2017.


![](https://debianbrasil.org.br/blog/imagens/ftsl-2017-alisson-christian.jpg =400x)

Alisson Coelho e Christian Mendes - coordenadores da organização do FTSL 2017

 Todas as fotos podem ser vistas aqui:

<https://www.flickr.com/photos/curitibalivre/albums/72157661046698288>
