---
title: MiniDebConf Curitiba 2016 - site e inscrições
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:40:46.096Z
tags: blog, geral
editor: markdown
dateCreated: 2016-01-08T13:27:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A comunidade brasileira de usuários e desenvolvedores Debian convida a todos
para participarem da **MiniDebConf Curitiba 2016** que acontecerá nos dias
05 e 06 de março sede de [Aldeia Coworking](http://www.aldeiaco.com.br) em
Curitiba - Paraná.

A MiniDebConf Curitiba 2016 é um evento aberto a todos, independente do seu
nível de conhecimento sobre Debian. O mais importante será reunir a comunidade
para celebrar o maior projeto de Software Livre no mundo, por isso queremos
receber desde usuários inexperientes que estão iniciando o seu contato com o
Debian até Desenvolvedores oficiais do projeto.

A programação da MiniDebConf Curitiba 2016 será composta de palestras de nível
básico e intermediário para aqueles participantes que estão tendo o primeiro
contato com o Debian ou querem conhecer mais sobre determinados assuntos, e
workshops/oficinas de nível intermediário e avançado para usuários do Debian
que querem colocar a mão-na-massa durante o evento.

A inscrição na MiniDebConf Curitiba 2016 é totalmente gratuita e pode ser feita
no formulário disponível no site do evento. É importante realizar a inscrição
prévia para que possamos dimensionar o tamanho do evento de acordo com a
quantidade de participantes.

Site da MiniDebConf Curitiba 2016:

<http://br2016.mini.debconf.org>

**Organização:**

[Debian Brasil](https://debianbrasil.org.br)

[Comunidade Curitiba Livre](http://curitibalivre.org.br/)

[Aldeia Coworking](http://aldeiaco.com.br/)
