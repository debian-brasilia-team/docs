---
title: Reunião realizada em Curitiba para organizar a MiniDebConf 2018 e o BID 2019
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:42:23.601Z
tags: blog, geral
editor: markdown
dateCreated: 2017-12-13T19:15:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

No sábado passado (09/12/2017) alguns participantes da comunidade Debian em
Curitiba estiveram reunidos na [Universidade Positivo](http://www.up.edu.br/)
para discutir a organização da
[MiniDebConf Curitiba 2018](https://minidebconf.curitiba.br/2018) e atualizar o
[BID de Curitiba](https://wiki.debconf.org/wiki/DebConf19/Bids/Curitiba) para a
DebConf19.

A MiniDebConf Curitiba 2018 acontecerá de 11 a 14 de abril no campus central da
UTFPR - Universidade Tecnológica Federal do Paraná. A inscrição é gratuita e
estamos com uma campanha de financiamento coletivo para ajudar a custear o
evento.

Mais infomações: <https://minidebconf.curitiba.br/2018>

Estamos candidatando Curitiba pelo segundo ano consecutivo para sediar uma
edição da
[DebConf - Conferência dos Desenvolvedores do Projeto Debian](https://debconf.org/). Para isso, é necessário preparar um documento chamado "BID" onde estão os
detalhes da candidatura para sediar a DebConf19 que acontecerá em 2019.

Mais informações: <https://wiki.debconf.org/wiki/DebConf19/Bids/Curitiba>

Estiveram presentes a reunião:

-   Alisson Coelho
-   Antonio C. C. Marques
-   Antonio Terceiro
-   Cleber Ianes
-   Daniel Lenharo de Souza
-   Paulo Henrique de Lima Santana (phls)
-   Samuel Henrique


Contamos ainda com a ajuda da profa. Juliana Tibães e do Prof. Marcelo Szostak
que gentilmente reservaram a sala para a nossa reunião.

![Reuniao minidc bid 2017 12 09 01](https://debianbrasil.org.br/blog/imagens/reuniao-minidc-bid-2017-12-09-01.jpg =400x) 
![Reuniao minidc bid 2017 12 09 08](https://debianbrasil.org.br/blog/imagens/reuniao-minidc-bid-2017-12-09-08.jpg =400x) 