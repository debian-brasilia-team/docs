---
title: Textos sobre o falecimento do Ian Murdock
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:42:28.505Z
tags: blog, geral
editor: markdown
dateCreated: 2015-12-30T21:49:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

## Veja abaixo alguns textos sobre o falecimento do Ian Murdock, criador do Debian: 

**Debian mourns the passing of Ian Murdock**

- <https://bits.debian.org/2015/12/mourning-ian-murdock.html>

**In Memoriam: Ian Murdock**

- <http://blog.docker.com/2015/12/ian-murdock>

**In Memoriam: Ian Murdock (em português)**

- <http://softwarelivre.org/portal/noticias/in-memoriam-ian-murdock>

**Criador do Debian, Ian Murdock faleceu hoje aos 42 anos**

- <http://www.diolinux.com.br/2015/12/ian-murdock-faleceu-death.html?m=1>

**Morre aos 42 anos o fundador do Debian GNU/Linux!**

- [https://sempreupdate.org/gnulinux/noticias/2015/morre-aos-42-anos-o-fundador-do-debian-gnulinux](https://sempreupdate.org/gnulinux/noticias/2015/morre-aos-42-anos-o-fundador-do-debian-gnulinux/) 
**Aos 42, morre Ian Murdock, criador do Debian**

- <http://tobias.ws/blog/aos-42-morre-ian-murdock-criador-do-debian>

**Ha muerto Ian Murdock, fundador del proyecto Debian**

- <http://hipertextual.com/2015/12/ha-muerto-ian-murdock>

**The latest Tweets from Iam Murdock**

- <https://archive.is/OPlI7>
- <http://pastebin.com/yk8bgru5>

**Reddit**

- <https://www.reddit.com/r/Bad_Cop_No_Donut/comments/3ynv0g/debian_linux_founder_ian_murdock_abused_by_cops> - <https://www.reddit.com/r/linux/comments/3ytbmi/in_memoriam_ian_murdock>
- <https://news.ycombinator.com/item?id=10813524>

**Debian founder and Docker employee Ian Murdock has died at 42**

- <http://venturebeat.com/2015/12/30/debian-founder-and-docker-employee-ian-murdock-has-died-at-42> 
**Fundador de Debian y el empleado Docker Ian Murdock ha muerto a los 42**

- <http://www.linuxpreview.org/gnulinux/fundador-de-debian-y-el-empleado-docker-ian-murdock-ha-muerto-a-los-42> 
**R.I.P Ian Murdock, Founder of Debian Linux, Dead at 42**

- [http://thehackernews.com/2015/12/debian-ian-murdock-died.html](http://thehackernews.com/2015/12/debian-ian-murdock-died.html%C3%82) 
**A Requiem for Ian Murdock (por Bradley M. Kuhn)**

- <http://ebb.org/bkuhn/blog/2015/12/30/ian-murdock.html>

**Debian Linux founder Ian Murdock dead at 42**

- <http://www.theregister.co.uk/2015/12/30/ian_murdock_debian_founder>

**Prominent Programmer Dies In Apparent Suicide After Violent Encounter With San Francisco Police** 
- <http://sanfrancisco.cbslocal.com/2015/12/31/prominent-programmer-dies-in-apparent-suicide-after-violent-encounter-with-san-francisco-police> 
**New release under development; suggestions requested**

- <https://groups.google.com/forum/m/#!msg/comp.os.linux.development/Md3Modzg5TU/xty88y5OLaMJ> 
![Ian-4](https://debianbrasil.org.br/blog/imagens/ian-4.png)

![Ian-5](https://debianbrasil.org.br/blog/imagens/ian-5.jpg)

![Ian-2](https://debianbrasil.org.br/blog/imagens/ian-2.png)

![Ian-3](https://debianbrasil.org.br/blog/imagens/ian-3.jpg)