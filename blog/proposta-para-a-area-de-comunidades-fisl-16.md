---
title: Proposta para a Área de Comunidades - FISL 16
description: por Giovani Ferreira
published: true
date: 2025-03-01T17:42:00.596Z
tags: blog, geral
editor: markdown
dateCreated: 2015-06-04T05:13:00.000Z
---

<!-- author: Giovani Ferreira -->

**Nome da comunidade/projeto/entidade/grupo;**

Comunidade Debian Brasil

**Estimativa do número pessoas movimentando as atividades no espaço do grupo
na Área das Comunidades; **

A ser definido

**Conteúdos/informações que demonstram contribuições do grupo em questão
(continuas para o desenvolvimento do projeto, de tecnologias livres e de código
aberto, para o qual o grupo foi criado)**

A Comunidade Debian Brasil é um projeto formado por usuários, desenvolvedores,
tradutores e documentadores voluntários, com o objetivo central de tornar o
Projeto Debian mais próximo dos usuários de nosso país, o sistema Debian
localizado para o português brasileiro.

**Proposta de atividades/ações no espaço da Área das Comunidade:**

- Apresentação dos projetos e times do Debian Brasil
- Motivar a participação na Comunidade Debian Brasil
- Incentivar a promoção da Comunidade Debian Brasil em eventos regionais
- Oficinas: empacotamento / tradução
- Install Fest
- Festa de assinatura de chaves
- Sorteio de brindes

**Como contribuirão com o install fest, ou seja, que projetos de software
livre vinculados à sua comunidade eles podem ajudar a instalar;**

Debian 8

**Contribuição para atividades que ajudem a movimentar o FISL, ou seja,
integração com os visitantes;**

A ser definido

**Lightning talks (18 minutos) que o grupo poderá promover na Área das Comunidades;** Como colaborar com o Projeto Debian e a comunidade Debian Brasil

**Deixar claro como convidarão as pessoas.**

- Site oficial Página da comunidade no softwarelivre.org
- Listas de discussão
- Canais IRC

**Links para as páginas e listas principais do projeto em que a comunidade/grupo faz parte, bem com a(s)**
**lista(s) do próprio grupo;**

- <https://debianbrasil.org.br>
- <http://wiki.debianbrasil.org/>
- <debian-br-geral@lists.alioth.debian.org>
- <debian-user-portuguese@lists.debian.org>
- <http://twitter.com/debianbrasil>
- <http://www.facebook.com/debianbrasil>

**Informar se enviou uma proposta de Encontro Comunitário no FISL (via a
chamada de trabalhos);**

A ser definido
**Indicar de 2 a 4 coordenadores do grupo na Área das Comunidades (nome e
e-mail de cada um).**

A ser definido
**Informar outras atividades: hackathon, workshop, install-party, festa de
lançamento, aniversário da comunidade, **
**mesa redonda, URC etc. **

- Oficinas: empacotamento / tradução
- Festa de assinatura de chaves