---
title: DebConf19 tem uma logo
description: por Paulo Henrique de Lima Santana
published: true
date: 2025-03-01T17:58:24.062Z
tags: blog, geral
editor: markdown
dateCreated: 2018-06-06T14:40:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana -->

A organização da
[DebConf19 - Conferência Mundial de Desenvolvedores(as) do Projeto Debian](https://www.debconf.org), que acontecerá em Curitiba em julho de 2019 está orgulhosa em anúnciar a logo
vencedora do concurso que recebeu propostas para a escolha da imagem que
identificará o evento.

Recebemos
[11 propostas](https://wiki.debian.org/DebConf/19/Artwork/LogoProposals)
de logos, algumas fazem alusão a Curitiba e outras ao Brasil. Depois foi feita
uma votação pública para qualquer pessoa votar nas suas três logos preferidas.
Foram computados os votos e o
[resultado final](https://salsa.debian.org/debconf-team/public/data/dc19/-/blob/master/artwork/debconf19-brand/proposals/resultado-concurso-logo-debconf19.pdf) indicou que a [logo vencedora](https://wiki.debian.org/DebConf/19/Artwork)
foi a elaborada pelo brasileiro Ramon Mulin que combina a palavra DebConf19 com
um [tamanduá-bandeira](https://pt.wikipedia.org/wiki/Tamandu%C3%A1-bandeira).

O tamanduá-bandeira é uma das quatro maiores espécies de tamanduás encontradas
na América Central e na América do Sul, e é uma espécie em perigo de extinção.
É facilmente reconhecido pelo longo nariz e pela língua estendida,
características que facilitam se alimentar de formigas e cupins, e o animal é
um dos símbolos do Brasil.

Agradecemos a cada pessoa que disponibilizou seu tempo para elaborar e enviar
sua proposta, e as pessoas que gentilmente participaram do processo de votação.
Essas iniciatiavas fazem com que a organização da DebConf19 se sinta confiante
que a comunidade irá apoiar e ajudar na realização de um grande evento no Brasil! 
![](https://debianbrasil.org.br/blog/imagens/dc19-logo-horizontal.png =400x)


