---
title: Planeta Debian Brasil no Twitter e TweetMyPlanet
description: por Debian Brasil
published: true
date: 2025-03-01T17:41:51.470Z
tags: blog, geral
editor: markdown
dateCreated: 2012-08-24T20:09:00.000Z
---

<!-- author: Debian Brasil -->

Há alguns dias, vi que existe uma conta de
[twitter](https://twitter.com/planetdebian) (@planetdebian) que é
responsável por divulgar os posts que aparecem no
[Planet Debian](http://planet.debian.org).

Achei a idéia bem interessante e fui atrás da versão basileira desta conta, uma
que divulgue os posts do
[Planeta Debian Brasil](http://planeta.debianbrasil.org), mas não encontrei
nenhum.

[![](https://pbs.twimg.com/profile_images/2532043737/ihml82jiq1sk4pbg2d9y_400x400.png "Debian Brasil")](http://twitter.com/planetadebianbr) 

Então, tomei a liberdade de criar um:
[@planetadebianbr](http://twitter.com/planetadebianbr), e para automatizar
o tweet de novos posts no Planeta criei um script que lê o RSS Feed do planet e
twitta os posts que ainda não foram twittados. Chamei ele de
[TweetMyPlanet](http://gitorious.org/earruda/tweetmyplanet),
e ele está sob a licença GPLv3.

Fonte:
[http://earruda.eti.br/blog/2012/08/planeta-debian-brasil-no-twitter-e-tweetmyplanet/](http://earruda.eti.br/blog/2012/08/planeta-debian-brasil-no-twitter-e-tweetmyplanet/) 