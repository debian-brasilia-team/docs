---
title: Debian Day 30 anos em Maceió
description: por Paulo Henrique de Lima Santana (phls)
published: true
date: 2025-03-01T17:38:56.233Z
tags: blog, geral
editor: markdown
dateCreated: 2023-09-11T05:00:00.000Z
---

<!-- author: Paulo Henrique de Lima Santana (phls) -->

O Debian Day em Maceió 2023 foi realizado no auditório do Senai em Maceió com
apoio e realização do [Oxe Hacker Club](http://oxehacker.club/).

Se inscreveram cerca de 90 pessoas, e 40 estiveram presentes no sábado para
participarem do evento que contou com as 6 palestras a seguir:

- Debian Package - Daniel Pimentel
- Attacking Linux EDRs for Fun and Profit - Tiago Peixoto
- Docker: Introdução ao mundo dos containers - Baltazar
- Hardening, Debian e CIS Benchmarks - Moises
- Carreira e Software Livre em Cyber Security - Edo
- O Software Livre já pode pagar minhas contas? - Gilberto Martins

O Debian Day teve ainda um install fest e desconferência (papo aleatório,
comes e bebes).

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-1.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-2.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-3.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-4.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-5.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-6.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-7.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-8.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-9.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-10.jpg =400x)

![Debian Day Maceió 2023 1](https://debianbrasil.org.br/blog/imagens/debianday-maceio-2023-11.jpg =400x)

