---
title: BITS da equipe debian-l10n-portuguese - Out/2018
description: por Daniel Lenharo de Souza
published: true
date: 2025-03-01T17:57:27.125Z
tags: blog, geral
editor: markdown
dateCreated: 2018-11-02T18:43:00.000Z
---

<!-- author: Daniel Lenharo de Souza -->

Mensalmente emitimos um relatório da
[equipe debian-l10n-portuguese](https://wiki.debian.org/Brasil/Traduzir) do
Projeto [Debian](https://www.debian.org) e este é o terceiro deles. O objetivo
é que as atividades da equipe sejam conhecidas e possamos dar os devidos
créditos pelo trabalho produzido por nossos contribuidores. Acreditamos que isso poderá estimular a participação de novos tradutores e revisores.

## Visão geral de Outubro de 2018

-   04 Traduções de páginas wiki

## Pessoas com atividades registradas durante setembro de 2018

-   Daniel Lenharo de Souza
-   Fred Maranhão
-   Francisco Neto
-   Lia Ayumi Takiguchi
-   Ricardo Fantin da Costa
-   Paulo Henrique de Lima Santana
-   Thiago Nunes (Qobi Ben Nun)

Se você fez alguma colaboração de tradução que não está listada acima, nos avise na lista (debian-l10n-portuguese@lists.debian.org) para que seu trabalho seja
devidamente registrado e reconhecido!

## Itens traduzidos

-   <https://wiki.debian.org/pt\_BR/RemoteDesktop>
-   <https://wiki.debian.org/pt\_BR/DebianNPDatabases>
-   <https://wiki.debian.org/pt\_BR/PostfixAndSASL>
-   <https://wiki.debian.org/pt\_BR/TimeZoneChanges>

A equipe de tradução para português do Brasil agradece a todos que colaboram
para que o Debian se torne mais universal!

Venha [ajudar](https://wiki.debian.org/Brasil/Traduzir) você também. Toda ajuda
é bem-vinda e estamos precisando da sua.