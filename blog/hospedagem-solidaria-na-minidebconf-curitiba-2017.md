---
title: Hospedagem Solidária na MiniDebConf Curitiba 2017
description: por Daniel Lenharo de Souza
published: true
date: 2025-03-01T17:39:55.902Z
tags: blog, geral
editor: markdown
dateCreated: 2017-01-11T11:35:00.000Z
---

<!-- author: Daniel Lenharo de Souza -->

A Comunidade Debian, quer dar uma ajuda para quem precisa de um cantinho para
ficar na [MiniDebConf Curitiba 2017](http://br2017.mini.debconf.org/),
nos dias 17, 18 e 19 de março.

Assim, criamos 2 tópicos no fórum para que você possa oferecer ou pedir um
quarto (cama, sofá, etc.) durante a MiniDebConf.

Não há coisa melhor que aproveitar um evento próximo à pessoas que compartilham
dos mesmo valores!!!

Obs: O fórum foi desativado.
