---
title: sendmail
description: 
published: true
date: 2025-03-01T17:43:21.390Z
tags: 
editor: markdown
dateCreated: 2023-06-19T17:46:24.546Z
---

`msmtp` é um cliente SMTP que pode ser usado para enviar e-mails de MUAs (mailagentes de usuário) como Mutt ou Emacs. Ele encaminha e-mails para um servidor SMTP(por exemplo, em um provedor de correio gratuito), que cuida do envio finalentrega. Usando perfis, pode ser facilmente configurado para usardiferentes servidores SMTP com diferentes configurações, o que tornaideal para clientes móveis.

<br/>

## Instalação

```bash
apt install msmtp
```

<br/>

## Configuração
Você precisa colocar suas configurações em `~/.msmtprc`.

Um exemplo é fornecido em `/usr/share/doc/msmtp/examples/msmtprc-user.example`.

<br/>

#### Gmail

Para enviar e-mail através de uma conta do Gmail:

```bash
# Defina valores padrão para todas as contas a seguir.
defaults
port 587
tls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile -

account gmail
host smtp.gmail.com
from <user>@gmail.com
auth on
user <user>@gmail.com
passwordeval gpg --no-tty --quiet --decrypt ~/.msmtp-gmail.gpg

# Defina uma conta padrão
account default: gmail
```

<br/>

#### Riseup

Para enviar e-mail através de uma conta do Riseup:

```bash
# Defina valores padrão para todas as contas a seguir.
defaults
port 587
tls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile -

account riseup
host mail.riseup.net
from <user>@riseup.net
auth on
user <user>@riseup.net
passwordeval gpg --no-tty --quiet --decrypt ~/.msmtp-gmail.gpg

# Defina uma conta padrão
account default: riseup
```

As credenciais podem ser armazenadas em um arquivo de senha criptografado por OpenPGP, que pode ser criado assim:

```bash
gpg --encrypt --output=.msmtp-gmail.gpg --recipient=<user>@gmail.com - <<END
Esta é a minha senha secreta
END
```

Certifique-se de não esquecer o traço próximo ao final, o que faz com que o gpguse "entrada padrão" para ler a senha de sua chave OpenPGP privada.Depois de lançar este comando, ele aguarda sua senha.Digite sua senha, pressione `Enter` e finalize com `control-d`. Então, gpgcriptografa sua senha, armazena o resultado no arquivo e termina.

Para testá-lo, execute:

```bash
msmtp endereço@email.com <<END
Olá,
Esta é a minha mensagem de teste.
END
```

O `gpg-agent` solicitará sua senha da chave OpenPGP ao enviar um e-mail.

Então você deve encontrar seu e-mail enviado na caixa de entrada do destinatário logo depois.

#### msmtp-mta
Quando uma interface MTA padrão é desejada, mas um servidor completo comoPostfix ou Exim não é desejado, então o pacote `msmtp-mta` pode ser uma solução:

Instale o pacote msmtp-mta .

```bash
apt install msmtp-mta
```

O pacote fornece um link simbólico `/usr/sbin/sendmail` para msmtp que outro software pode usar para enviar e-mail. O pacote `msmtp-mta` também fornece um daemon msmtpd que escuta em 127.0.0.1:25 . Isso é desabilitado por padrão. Tenha em mente que o daemon não tem como saber qual usuário está se conectando a ele, então ele tem que usar o .msmtprc do usuário . O `/etc/msmtprc` em todo o sistema ainda será aplicado.

A configuração do MTA pode ser testada usando o programa de correio (fornecido por um pacote como `mailutils` ou `bsd-mailx` ):

```bash
apt install mailutils
# ou
apt install bsd-mailx
```

```bash
mail -s "teste" endereço@email.com <<END
Isto é um teste
END
```
Se o acima não funcionar, tente adicionar esta linha abaixo em `/etc/mail.rc`:

```bash
set mta=/usr/bin/msmtp
```
