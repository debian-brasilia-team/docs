---
title: dh-make
description: 
published: true
date: 2025-03-01T17:43:12.250Z
tags: 
editor: markdown
dateCreated: 2025-02-27T20:50:26.454Z
---

Como criar um modelo inicial para empacotamento no Debian usando `dh_make` para a criação de pacotes.

## Instalação das dependências

```bash
apt install build-essential dh-make
```

Entendendo o nome do pacote e a versão

Vamos supor que temos uma fonte original com o nome do arquivo `hello-world-golang-0.1.0.tar.gz`. Usaremos `hello-world-golang` como o nome do pacote e `0.1.0` como a versão upstream.

É importante lembrar o seguinte sobre nomes de pacotes:

- Consiste apenas em letras minúsculas `(a-z)`, dígitos `(0-9)`, sinais de mais `(+)` e menos `(-)`, e pontos `(.)`
- Deve ter pelo menos dois `(2)` caracteres de comprimento
- Deve começar com um caractere alfanumérico
- Recomendado ser menor que `30` caracteres, se possível.

e versões upstream:

- Deve começar com um dígito `(0-9)`
- Deve consistir apenas em caracteres alfanuméricos `(0-9A-Za-z)`, sinais de mais `(+)`, tis `(~)` e pontos `(.)`
- Recomendado ser menor que `8` caracteres, se possível.


> Se uma versão original não usar um esquema de versão reconhecido pela cadeia de ferramentas deb (ou seja, `0.1.0`), remova os valores inválidos da versão upstream. Esses valores removidos podem ser registrados no arquivo `debian/changelog`. Se você precisar criar uma string de versão, use o formato `AAAAMMDD` (ou seja, `20150713`).

## Configurando o dh_make

Crie as variáveis de ambiente do shell `$DEBEMAIL` e `$DEBFULLNAME`. Elas serão usadas para gerar valores nas entradas dos arquivos `control` e `changelog`.


```bash
$ cat >>~/.bashrc <<EOF
DEBEMAIL="pessoa@exemplo.com"
DEBFULLNAME="Pessoa Teste"
export DEBEMAIL DEBFULLNAME
EOF
$ . ~/.bashrc
```

## Configurando a estrutura de diretórios



1) Crie o diretório e depois mude para o novo diretório:

```bash
mkdir hello-world-golang; cd hello-world-golang/
```

2) Baixe e descomprima o codigo fonte original:

```
wget https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang/-/archive/0.1.0/hello-world-golang-0.1.0.tar.gz
tar -xvzf hello-world-golang-0.1.0.tar.gz
```

3) Mude para o diretório onde foi descomprimido:

```bash
cd hello-world-golang-0.1.0/
```

## Crie o pacote inicial usando o arquivo fonte


Dentro do diretório fonte desempacotado (ou seja, `./hello-world-golang-0.1.0`), execute o seguinte:

```bash
dh_make -f ../hello-world-golang-0.1.0.tar.gz
```


O parametro `-f` define o caminho do arquivo como o arquivo fonte original (`hello-world-golang_0.1.0.orig.tar.gz`), o que pula a cópia da árvore do programa atual.


## Algumas ações do dh_make

Após chamar `dh_make -f <caminho do arquivo>`, será solicitado que você selecione o tipo de pacote que está criando.

```bash
Type of package: single binary, indep binary, multiple binary, library, kernel module, kernel patch?
[s/i/m/l/k/n] s

# PT-BR
Tipo de pacote: binário único, binário indep, binário múltiplo, biblioteca, módulo de kernel, patch de kernel?
[s/i/m/l/k/n] s
```

Para este exemplo, podemos assumir que `hello-world-golang-0.1.0` é um binário único.

Em seguida, se você configurou suas variáveis de ambiente do shell `$DEBEMAIL` e `$DEBFULLNAME`, você deverá ver a saída de um arquivo de controle com os campos relevantes já preenchidos. Caso contrário, você terá que editar o arquivo `control` manualmente.

```bash
cd debian
rm hello-world-golang-docs.docs \
    *.ex \
    README.* \
    upstream/metadata.ex
```

Atualize o arquivo `upstream/metadata`

```yaml
---
Bug-Database: https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang/issues
Bug-Submit: https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang/issues/new
Repository-Browse: https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang
Repository: https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang.git
```

Atualize o arquivo `changelog` para:

```yaml
hello-world-golang (0.1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #123456)

 -- Pessoa Teste <pessoa@exemplo.com>  Thu, 04 Apr 2024 22:41:02 +0100
```

Atualize o arquivo `control` para:

```yaml
Source: hello-world-golang
Section: golang
Priority: optional
Maintainer: Pessoa Teste <pessoa@exemplo.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any
Standards-Version: 4.6.2
Homepage: https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang
Vcs-Browser: https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang
Vcs-Git: https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang.git
XS-Go-Import-Path: salsa.debian.org/debian-brasilia-team/packages/hello-world-golang
Testsuite: autopkgtest-pkg-go

Package: hello-world-golang
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: hello world golang package
 Initial package for demonstrations purposes using golang, this program will
 show a message saying hello world when invoked.
```


Atualize o arquivo `copyright` para:


```yaml
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hello-world-golang
Source: https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang

Files: *
Copyright: 2024 Pessoa Teste <pessoa@exemplo.com>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```


Atualize o arquivo `rules` para:


```makefile
#!/usr/bin/make -f

%:
	dh $@ --buildsystem=golang --with=golang

override_dh_auto_install:
	dh_auto_install -- --no-source
```


salsa-ci.yml.ex para salsa-ci.yml com o seguinte conteudo.

```yaml
# For more information on what jobs are run see:
# https://salsa.debian.org/salsa-ci-team/pipeline
#
# To enable the jobs, go to your repository (at salsa.debian.org)
# and click over Settings > CI/CD > Expand (in General pipelines).
# In "CI/CD configuration file" write debian/salsa-ci.yml and click
# in "Save Changes". The CI tests will run after the next commit.
---
include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/recipes/debian.yml
```


Atualize o arquivo `watch` com o seguinte conteúdo.

```bash
version=4
opts="filenamemangle=s%(?:.*?)?v?(@ANY_VERSION@@ARCHIVE_EXT@)%@PACKAGE@-$1%" \
    https://salsa.debian.org/debian-brasilia-team/packages/hello-world-golang/-/tags \
    archive/v?@ANY_VERSION@/hello-world-golang-v?\d\S*@ARCHIVE_EXT@
```