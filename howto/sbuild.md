---
title: sbuild
description: Configuração do sbuild
published: true
date: 2025-03-01T17:43:19.185Z
tags: howto sbuild
editor: markdown
dateCreated: 2023-06-07T01:52:50.646Z
---

Vamos dar uma olhada em como efetuar a configuração inicial do `sbuild`.

---
- [O que é?](#o-que-é)
- [TL;DR (i.e., indo direto ao ponto)](#tldr-ie-indo-direto-ao-ponto)
	- [Dando permissão para seu usário](#dando-permiss%C3%A3o-para-seu-us%C3%A1rio)
	- [Testando a instalação](#testando-a-instala%C3%A7%C3%A3o)
- [Por baixo dos panos](#por-baixo-dos-panos)
	- [Instalando o pacote](#instalando-o-pacote)
  - [Criando o chroot](#criando-o-chroot)
- [Configurando o sbuild](#configurando-o-sbuild)
- [Configurando o restante do ambiente](#configurando-o-restante-do-ambiente)
	- [Variáveis de ambiente](#vari%C3%A1veis-de-ambiente)
	- [git-buildpackage](#git-buildpackage)
---


# O que é?

O `sbuild` é uma das ferramentas de construção de pacotes que o Debian possui.  Por ser o que o sistema oficial de *build* da distribuição utiliza, ele acaba sendo bastante recomendado e utilizado por desenvolvedores do projeto.

A ideia por trás do `sbuild` é simples: ele cria um *chroot* onde o pacote será construído, efetua a instalação de todas as dependências de construção (*Build-Depends*) dentro do *chroot*, e depois realiza o *build* propriamente dito.  Dentre as vantagens dessa abordagem, podemos citar:

- Melhor isolamento na hora de construir o pacote.

- Eliminação da necessidade de instalar as *Build-Depends* no seu computador.

- Maior proximidade com o ambiente de construção que o pacote encontrará na infra-estrutura oficial.

Por baixo dos panos, o `sbuild` utiliza uma outra ferramenta muito útil chamada `schroot`.

# TL;DR (i.e., indo direto ao ponto)

> Recomendamos que você invista algum tempo aprendendo o que de fato acontece "por baixo dos panos" na hora de configurar o `sbuild` e o *chroot*.  Isso vai ajudar bastante quando você tiver algum problema com a ferramenta no futuro.

Se você quiser criar o *chroot* rapidamente, pode seguir os seguintes passos:

``` shell
sudo apt install sbuild-debian-developer-setup
sudo sbuild-debian-developer-setup
```

Isso vai criar um *chroot* da distribuição `unstable`.

## Dando permissão para seu usário

Seu usário precisa ser adicionado ao grupo `sbuild` para que ele possa utilizar o *chroot*.

``` shell
sudo sbuild-adduser $USER
```

O comando acima aplicará o efeito somente após reiniciar a máquina, para testar o sbuild sem reiniciar pode usar o seguinte comando temporário:

``` shell
newgrp sbuild
```


## Testando a instalação

Para testar a instalação vamos usar o pacote [hello](tracker.debian.org/hello).


``` shell
sbuild --dist=unstable hello
```

Se quiser aprender de fato como a criação do *chroot* funciona, leia a seção [Por baixo dos panos](#por-baixo-dos-panos). Caso contrário, pule para a seção [Configurando o `sbuild`](#configurando-o-sbuild).

# Por baixo dos panos

## Instalando o pacote

Vamos instalar o pacote `sbuild` e algumas ferramentas que serão úteis na hora de configurarmos o *chroot*.

``` shell
apt install sbuild debootstrap apt-cacher-ng
```

- O `sbuild` é a ferramenta de construção de pacotes propriamente dita.

- O `debootstrap` baixa e descompacta a imagem de um sistema Debian completo em algum diretório.  É essa imagem que vamos usar como base na hora de criar o *chroot*.

- O `apt-cacher-ng` serve como um *proxy* entre sua máquina e o *archive* do Debian.  Ele faz *cache* dos pacotes que são baixados, e, se você precisar baixar algum pacote que já esteja no *cache*, não é necessário baixá-lo novamente.  Muito útil quando a gente constrói algo com o `sbuild`.

## Criando o *chroot*

Se nós estivéssemos lidando diretamente com *chroots*, nós poderíamos invocar a ferramenta `debootstrap` "na mão" pra criar o *chroot* pra gente.  Isso funcionaria sem problemas, mas ainda precisaríamos fazer ajustes manuais pra configurar o *chroot* pra funcionar com o `schroot`.

O `sbuild` vem com um *helper* que já faz todo esse trabalho pra gente: o `sbuild-createchroot`.

Vamos executar o comando do seguinte modo (como `root`):

``` shell
sbuild-createchroot --include=eatmydata \
  --command-prefix=eatmydata \
  --alias=sid unstable \
  /srv/chroot/unstable-amd64-sbuild \
  http://localhost:3142/deb.debian.org/debian
```

Esse comando já vai fazer o *setup* do *chroot* (que vai ficar em `/srv/chroot/unstable-amd64-sbuild/`), e também criará a configuração necessária para o `schroot` (em `/etc/schroot/chroot.d/`).

# Configurando o sbuild

Agora que seu usuário já pode realizar *builds* com o `sbuild`, você precisa configurar a ferramenta de acordo.

Os próximos comandos assumem que você esteja *logado* como seu usuário, e **não** como `root`.

Primeiro, vamos copiar (para a `$HOME`) o arquivo que contém exemplos de várias configurações aceitas pelo `sbuild`:

``` shell
cp /usr/share/doc/sbuild/examples/example.sbuildrc ~/.sbuildrc
```

Sugiro dar uma olhada geral no arquivo.  Ele possui comentários bastante explicativos.

Vamos adicionar as seguintes diretivas no arquivo:

``` perl
# Faz o build de pacotes arch-independent.
$build_arch_all = 1;

# Por padrão realiza o build para unstable
$distribution = 'unstable';

# Produz um source package (.dsc) além do pacote binário.
$build_source = 1;

# Uploads para o Debian precisam ser, na grande maioria das vezes, source-only.
$source_only_changes = 1;

# Não executa o "clean" target fora do chroot.
$clean_source = 0;

# Sempre executa o lintian no final dos builds.
$run_lintian = 1;
$lintian_opts = ['--info', '--display-info'];
```

> Se quiser ativar checagens adicionais (e.g. tags pedantic `P:` e experimental `E:`), coloque:
> $lintian_opts = [ '--info', '--display-info', '--pedantic', '--display-experimental' ];
{.is-info}

# Configurando o restante do ambiente

## Variáveis de ambiente

No final do seu arquivo `~/.bashrc`, defina as seguintes variáveis de ambiente:

``` shell
export DEBFULLNAME="Seu Nome"
export DEBEMAIL="seu@email.com"
```

# git-buildpackage

O `git-buildpackage` é uma ferramenta criada para auxiliar o uso do `git` no empacotamento.

```bash
sudo apt install git-buildpackage
```

Para usar `sbuild` junto com o `git-buildpackage`, coloque a seguinte diretiva no arquivo `~/.gbp.conf`:

``` python
[DEFAULT]
builder = sbuild
pristine-tar = True
```

Caso queira, é possível adicionar a opção de assinar tags junto com sua chave GPG.

> Apenas faça isso caso você já saiba o que é uma chave GPG e tenha uma configurada no seu sistema, se esse não for o caso, não é necessário adicionar o que está abaixo
> 
{.is-danger}


```
sign-tags = True
keyid = <gpg-fingerprint>
```

# FAQ

## Como abrir uma shell ao final da construção do pacote

Para isso pode ser adicionado a opção `--finished-build-commands='%s'` junto ao comando sbuild:

```
sbuild --finished-build-commands='%s'
```

## Problemas ao rodar sbuild-debian-developer-setup no Ubuntu

Mensagens como a seguir:

```
W: Failure trying to run: chroot "/srv/chroot/unstable-amd64-sbuild" /bin/true 
```

1. Delete o chroot criado `rm -rf /srv/chroot/unstable-amd64-sbuild`
2. Siga o tutorial a partir da sessão `Por baixo dos panos` apresentada nessa página
3. Na hora de executar o comand `sbuild-createchroot` inclue a opção `--merged-usr`.


```
sbuild-createchroot --include=eatmydata \
  --command-prefix=eatmydata \
  --alias=sid \
  --merged-usr \
  unstable \
  /srv/chroot/unstable-amd64-sbuild \
  http://localhost:3142/deb.debian.org/debian
```

## Como resolver o erro usr-is-merged

Se você se deparou com a seguinte mensagem durante uma atualização do chroot ou durante uma construção com o sbuild, não tema!

```bash
******************************************************************************
*
* The usr-is-merged package cannot be installed because this system does
* not have a merged /usr.
*
* Please install the usrmerge package to convert this system to merged-/usr.
*
* For more information please read https://wiki.debian.org/UsrMerge.
*
******************************************************************************
```

Há duas possibilidades de solução, recriar o chroot ou fazer a migração manual para merged-usr. Bem, a primeira opção é suficientemente simples, mas a segunda já é um pouco mais complicada. Por isso, aqui está o passo-a-passo desta última para ajudá-la(o).

Será necessário iniciar uma sessão no chroot **source** e fazer a instalação do pacote `usrmerge` manualmente para migrar o chroot. Veja abaixo:

```bash
root@cezanne:~# schroot -c source:<nome_do_chroot>
(<nome_do_chroot>)root@cezanne:~# apt update
...
(<nome_do_chroot>)root@cezanne:~# apt download usrmerge
Get:1 http://127.0.0.1:3142/deb.debian.org/debian unstable/main amd64 usrmerge all 38 [13.0 kB]
Fetched 13.0 kB in 0s (466 kB/s)   
W: Download is performed unsandboxed as root as file '/root/usrmerge_38_all.deb' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
(<nome_do_chroot>)root@cezanne:~# apt install /root/usrmerge_38_all.deb
...
Preparing to unpack .../usr-is-merged_38_all.deb ...


******************************************************************************
*
* The usr-is-merged package cannot be installed because this system does
* not have a merged /usr.
*
* Please install the usrmerge package to convert this system to merged-/usr.
*
* For more information please read https://wiki.debian.org/UsrMerge.
*
******************************************************************************


dpkg: error processing archive /var/cache/apt/archives/usr-is-merged_38_all.deb (--unpack):
 new usr-is-merged package pre-installation script subprocess returned error exit status 1
Errors were encountered while processing:
 /var/cache/apt/archives/usr-is-merged_38_all.deb
E: Sub-process /usr/bin/dpkg returned an error code (1)
(<nome_do_chroot>)root@cezanne:~# rm /etc/unsupported-skip-usrmerge-conversion
(<nome_do_chroot>)root@cezanne:~# dpkg -i usrmerge_38_all.deb 
(Reading database ... 13023 files and directories currently installed.)
Preparing to unpack usrmerge_38_all.deb ...
Unpacking usrmerge (38) over (38) ...
Setting up usrmerge (38) ...
The system has been successfully converted.
(<nome_do_chroot>)root@cezanne:~# exit
exit
```

Pronto, seu chroot foi convertido com sucesso!


