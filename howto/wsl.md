---
title: Windows Subsystem for Linux (WSL)
description: 
published: true
date: 2025-03-01T17:43:25.807Z
tags: 
editor: markdown
dateCreated: 2023-09-23T13:22:34.117Z
---

## Pré-requisitos

Você deve estar executando o **Windows 10 versão 2004 e superior (Build 19041 e superior) ou o Windows 11**.

> Se você estiver em versões anteriores, consulte a [página de instalação manual](https://learn.microsoft.com/pt-br/windows/wsl/install-manual).{.is-warning}


Siga página abaixo para efetuar a instalação e configuração de seu ambiente.

- https://learn.microsoft.com/pt-br/windows/wsl/install

<br/>

## Instalando Debian

Após ter o WSL versão 2, basta executar os seguites comandos para instalar a distribuiçãoÇ

```bash
wsl --install --distribution Debian

# Listar distribuições instaladas e online
wsl --list --online

# Iniciar terminal com máquina Debian
wsl -d
```

<br/>

#### Outros commandos

```bash
# Desligar maquina virtual
wsl --shutdown Debian

# Atualizar WSL
wsl --update
```

<br/>

## Instalar o Terminal do Windows (opcional)

O uso do Terminal do Windows permite que você abra várias guias ou painéis de janela para exibir e alternar rapidamente entre várias distribuições do Linux ou outras linhas de comando **(PowerShell, Prompt de Comando, CLI do Azure etc.)**. Você pode personalizar totalmente o terminal com esquemas de cores exclusivos, estilos de fonte, tamanhos, imagens de tela de fundo e atalhos de teclado personalizados. [Saiba mais](https://learn.microsoft.com/pt-br/windows/terminal).

[Instalar o Terminal do Windows.](https://learn.microsoft.com/pt-br/windows/terminal/get-started)

![terminal-windows.png](/terminal-windows.png)

<br/>

## Visual Studio Code Extensões 

Abaixo uma lista de extensões que podem ser intaladas par conseguir ter uma melhor usabilidade do WSL com a IDE Visual Studio Code.

---

**Nome:** WSL
**Descrição:** Abra qualquer pasta no Windows Subsystem for Linux (WSL) e aproveite o conjunto completo de recursos do Visual Studio Code.
**Marketplace Link:** https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl

---
