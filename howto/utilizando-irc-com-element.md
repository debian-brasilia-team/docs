---
title: Utilizando IRC com Element
description: 
published: true
date: 2025-03-01T17:43:23.574Z
tags: 
editor: markdown
dateCreated: 2024-04-12T00:21:13.387Z
---

## O que é IRC

Internet Relay Chat (IRC) é um protocolo de comunicação utilizado na Internet. Ele é utilizado basicamente como bate-papo (chat) e troca de arquivos, permitindo a conversa em grupo ou privada. Para poder se comunicar via esse protocolo é necessário um cliente de IRC, e o mais interessante disso tudo é que no geral consomem poucos recursos computacionais e de rede.

Canais de IRC ainda são o principal meio de comunicação de diversas comunidades de software livre, como por exemplo o projeto Debian. Esse meio de comunicação têm perdido algum espaço, especialmente na comunidade brasileira, onde boa parte dos grupos vêm adotando o Telegram como o principal meio de comunicação. O argumento mais usado por defensores do Telegram é a facilidade de uso e a possibilidade de se parmanecer sempre logado e poder receber novas mensagens a qualquer momento. Todavia, novos clientes de IRC vêm surgindo com as facilidades e usabilidade similares ao Telegram, um deles é o Element (matrix), que diferente do Telegram é um projeto totalmente open-source e que iremos apresentar nas seções seguintes.

<br/>

## Instalação
Existe 3 versões do element

- Entre no [site](https://element.io/) Oficial do Elemente e crie uma conta
- Mobile Encontrado nas plataformas [App Store](https://apps.apple.com/app/id1631335820), [Google play](https://play.google.com/store/apps/details?id=io.element.android.x) e [Fdroid](https://f-droid.org/en/packages/io.element.android.x/).
- Desktop Disponível para para os sistemas [GNU/Linux](https://element.io/get-started), [Windows](https://packages.riot.im/desktop/install/win32/x64/Element%20Setup.exe) e [Mac OS](https://packages.riot.im/desktop/install/macos/Element.dmg)

<br/>

## Entrando nos canais

A primeira maneira de se entrar em uma sala é pela conversa OFTC IRC Bridge Status. Assim que finalizar a instalação com sucesso, basta entrar nessa conversa e enviar:

```
!join #sala-que-deseja-entrar
```

Para conhecer os canais do Debian basta entrar em https://wiki.debian.org/IRC. Usando como exemplo o canal do Debian Brasília, a mensagem ficaria da seguinte maneira:

```
!join debian-devel-br
!join debian-bsb
```
Existe também a possibilidade de entrar em salas pela interface gráfica, mas ela é bem simples então não vimos necessidade de demonstrar. Caso necessite executar mais ações pelo canal do OFTC IRC Bridge Status, basta utilizar o comando

```
!help
```
