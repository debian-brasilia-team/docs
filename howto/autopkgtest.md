---
title: autopkgtest
description: Configuração do autopkgtest
published: true
date: 2025-03-01T17:43:07.710Z
tags: autopkgtest, testes, test
editor: markdown
dateCreated: 2024-04-29T04:04:44.480Z
---

Vamos dar uma olhada em como efetuar a configuração inicial do `autopkgtest`.

---
- [O que é?]()
- [Executando com o sbuild]()
- [Executando com o QEMU]()
---


# O que é?

O `autopkgtest` é um software que permite executar os testes do pacote que você está rodando localmente.

Para os métodos escritos a seguir, vale ressaltar que é necessário estar no diretório do pacote.

# Executando com o schroot

Para rodar o autopkgtest com o sbuild, basta executar esse comando:

```shell
autopkgtest -B . -- schroot unstable-amd64-sbuild
```

# Executando com o QEMU

É uma boa prática rodar os testes locais do pacote que você está trabalhando para ter certeza que suas alterações estão funcionando corretamente, para isso, é preciso instalar as suas dependências com os comandos abaixo:

``` shell
sudo apt update
sudo apt install cmdtest
sudo apt install vmdb2
```

Após isso, é preciso configurar sua imagem Debian unstable, isso é feito facilmente com o qemu, uma das ferramentas inclusas no autopkgtest.

``` shell
sudo autopkgtest-build-qemu unstable autopkgtest-unstable.img
```

Espere ele criar a imagem e com ela pronta, seu ambiente está configurado e você pode rodar os testes locais do debian/tests com:

```shell
autopkgtest . -- qemu autopkgtest-unstable.img

```
Pronto, os testes foram executados com sucesso!

