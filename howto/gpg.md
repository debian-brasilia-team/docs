---
title: GPG
description: 
published: true
date: 2025-03-01T17:43:14.402Z
tags: 
editor: markdown
dateCreated: 2023-06-15T13:34:26.497Z
---

# Sumário


- [Sobre](/howto/gpg/sobre)
- [Verificando as chaves GPG existentes](/howto/gpg/verificando-as-chaves-GPG-existentes)
- [Gerando uma nova chave](/howto/gpg/gerando-uma-nova-chave)
- [Adicionando uma chave GPG a sua conta do Salsa](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa)
- [Assine seus commits do Git](/howto/gpg/assine-seus-commits-do-git)