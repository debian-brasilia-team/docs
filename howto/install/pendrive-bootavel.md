---
title: Pendrive Bootavel utilizando dd
description: 
published: true
date: 2025-03-01T17:30:13.372Z
tags: boot, pendrive, install, dd, iso
editor: markdown
dateCreated: 2024-04-27T16:09:48.309Z
---

Nesta página iremos cobrir como escrever/criar um Debian .iso em um dispositivo USB inicializável no Linux usando o comando `dd`.

<br>

## Passo 1 – Baixe a imagem .iso do Debian
Visite cdimage.debian.org e pegue a imagem iso do CD/DVD.


https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/

Ao final da pagina deve ter um arquivo no seguinte formato:

`debian-X.Y.Z-amd64-netinst.iso`

<br>

## Passo 2 – Encontre o nome do seu dispositivo USB no Linux

Insira seu pendrive e digite o seguinte comando `df` para ver se ele é montado automaticamente em um sistema desktop Debian ou qualquer outro sistema desktop Linux:

```
$ df

Filesystem            1K-blocks      Used Available Use% Mounted on
udev                   16366516         0  16366516   0% /dev
tmpfs                   3280840      1892   3278948   1% /run
/dev/sda1              95534500  24979716  65655664  28% /
tmpfs                  16404188    119600  16284588   1% /dev/shm
tmpfs                      5120        16      5104   1% /run/lock
/dev/sda6             354636252 147538976 189009496  44% /home
/dev/sdc1                495584    495584         0 100% /media/arthurbdiniz/kingston
```

Você também pode tentar o comando `lsblk` ou `dmesg` para listar seus dispositivos usb.

Você precisa desmontar o caminho onde está montado ou o dispositivo.


```bash
sudo umount /media/<resto-do-caminho>

# ou

sudo umount /dev/<nome-do-filesystem>
```

<br>

## Passo 3 – Verifique o arquivo de imagem de CD/DVD .iso do Debian

> DICA
Você pode encontrar a soma de verificação da imagem iso do DVD, como na própria página de download do https://cdimage.debian.org/debian-cd/current/amd64/iso-cd.
{.is-info}

Baixe os arquivos `SHA512SUMS` e `SHA512SUMS.sign`

Por exemplo, você deve obter a seguinte saída:

1. Pegue a chave debian que foi utilizada para assinar o arquivo SHA512SUMS

```bash
gpg --recv-key DA87E80D6294BE9B
```

2. Verificando a assinatura do arquivo SHA512SUMS

```bash
gpg --verify SHA512SUMS.sign

# Como resultado termos a seguinte mensagem
gpg: Good signature from "Debian CD signing key <debian-cd@lists.debian.org>"
```

> Isso significa que as cifras dentro do arquivo SHA512SUMS realmente vieram dos servidores do Debian e não de alguma outra fonte não confiável.


3. Verificando o arquivo .iso


- Pegue a hash referente a imagem ISO dentro do arquivo `SHA512SUMS`

- Use o comando `shasum` para fazer a checagem:

```bash
echo "<hash> *debian-X.Y.Z-amd64-netinst.iso" | shasum -a 512 --check
```

#### Exemplo de checagem

> Cada arquivo ISO gerado pelo Debian vai ter uma hash diferente.
{.is-info}


```bash
cat SHA512SUMS

33c08e56c83d13007e4a5511b9bf2c4926c4aa12fd5dd56d493c0653aecbab380988c5bf1671dbaea75c582827797d98c4a611f7fb2b131fbde2c677d5258ec9  debian-12.5.0-amd64-netinst.iso
7cd6ec7d359515117558e91650fce1b98af0082cb2149b3d94fb480f657f51b06934d7b450fe563eeef023ccf59472b8faa870ee50c369f4c4ceecea8da0c60f  debian-edu-12.5.0-amd64-netinst.iso
968067c0874b8cc865f7c0ff802b86ba09013a20f0318e39b38e0e37af5c557ccd5145ae7a2f8be01492a5298386aba437b545471450f9ae80fdaf6dac5e454d  debian-mac-12.5.0-amd64-netinst.iso


echo "33c08e56c83d13007e4a5511b9bf2c4926c4aa12fd5dd56d493c0653aecbab380988c5bf1671dbaea75c582827797d98c4a611f7fb2b131fbde2c677d5258ec9 *debian-12.5.0-amd64-netinst.iso" \
| shasum -a 512 --check

debian-12.5.0-amd64-netinst.iso: OK
```

Depois de verificado, você pode gravá-lo no dispositivo USB.

<br>

## Passo 4 – Crie um pendrive inicializável no Linux

> Tenha cuidado com os nomes do pendrive/disco USB. Nomes errados sempre resultam em perda de dados. Certifique-se de digitar o nome correto.
{.is-warning}

Digite o seguinte comando dd para criar uma imagem USB inicializável a partir de um arquivo .ISO

```bash
sudo dd if=debian-X.Y.Z-amd64-netinst.iso of=/dev/<NOME-DO-DISCO> bs=1M status=progress
```

O comando `dd` gravará os dados do processo em um pendrive e uma barra de progresso aparecerá na tela.

Após o processo completar remova o dispositivo.

#### Compreendendo as opções do comando dd

- **dd**: Inicie o comando dd para gravar a imagem ISO do DVD/CD.
- **if=/iso/debian.iso**: Caminho para o arquivo de entrada.
- **of=/dev/NOME-DO-DISCO**: Caminho para o disco/pendrive USB de destino.
- **bs=1M**: lê e grava até BYTES bytes por vez. Neste exemplo, 1 milhão de cada vez.
- **status=progress**: exibe a barra de progresso ao gravar a imagem no pendrive.

<br>

## Passo 5 - Instale o Debian usando a unidade USB recém-criada

Isso é tudo, pessoal! Agora você tem o Debian em um pendrive, inicializável e pronto para instalar em seu laptop, desktop ou sistema baseado em servidor.
