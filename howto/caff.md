---
title: caff
description: 
published: true
date: 2025-03-01T17:43:09.990Z
tags: 
editor: markdown
dateCreated: 2023-09-17T12:25:31.408Z
---

caff é sinônimo de "CA - fire and forget" (dispare e esqueça).

É um script Perl que ajuda você a automatizar todo o conjunto de etapas manuais que você precisa executar para cada chave que deseja assinar após uma festa de assinatura de chaves.

Esta ferramenta faz parte do pacote [signing-party](https://tracker.debian.org/pkg/signing-party) que será usado principalmente após uma [festa de assinatura de chaves](https://wiki.debian.org/Keysigning) .

<br/>

## Instalação

```
sudo apt-get install signing-party
```

<br/>

## Requisitos

> Geralmente é preciso ter um MTA (agente de transferência de email) configurado corretamente. Caso ainda não tenha configurado recomendamos fazer seguindo a página [sendmail](/howto/sendmail).
Isso geralmente significa que o comando `mail -s "test_email" user@mailprovider.com` deve funcionar.
{.is-warning}

<br/>

## Configuração


Após a instalação você deve configurar seu ambiente caff local, isso deve ser feito em  `$HOME/.caffrc`. Logo após a instalação não há nada lá, você pode criar o arquivo **.caffrc** simplesmente chamando `caff` na linha de comando.

```
# change the name
$CONFIG{'owner'} = 'Bill Gates';

# change the email address
$CONFIG{'email'} = 'bg@ms-dollar.com';

# your keyid
$CONFIG{'keyid'} = [ qw{1234567890ABCDEF} ];
```

<br/>

## Uso

O uso do caff para enviar e-mails assinados é bastante simples. Você deve chamar `caff [options]` com um ou mais **KeyID** e o caff prosseguirá com suas chaves.

```
$ gpg --receive-keys 01234567ABCDEFAB
$ caff 01234567ABCDEFAB
```

<br/>

Existem algumas perguntas que irão aparecer. Por favor, leia com atenção e responda-as. No final você chegará ao __prompt__ do gpg se não usar a opção `-m yes`.

Lá você deve digitar salvar para salvar as assinaturas.

Finalmente, caff enviará o e-mail de assinatura se responder à última pergunta com `y`.

```
gpg> save
[INFO] 11AE38DA1D42BF58 1 Max Example <max.example AT example DOT com> done.
[INFO] key BBB07FB2543E6CDDAD258F70B28DE9FDA9342BF08 done.
Mail signature for Max Example <max.example AT example DOT com> to 'max.example AT example DOT com'? [Y/n] y
```

Caso queria mais informações sobre o **caff** visite a página de manual para ler sobre as opções que você pode usar.

<br/>

## Recendo sua chave assinada

Quando compartilhar sua chave GPG publica com outras pessoas e elas assinarem utilizando o **caff**, você recebera um email com dois arquivos em anexo, `signedkey.msg` e `msg.asc`.

Para adicionar a assinatura em seu chaveiro baixe o arquivo `msg.asc` e rode o seguinte comandos:

```
$ gpg -d msg.asc | gpg --import
$ gpg --send-keys <sua-fingerprint>
```