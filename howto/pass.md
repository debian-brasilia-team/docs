---
title: pass
description: 
published: true
date: 2025-03-01T17:43:16.682Z
tags: 
editor: markdown
dateCreated: 2023-09-17T12:26:23.261Z
---

## Por que armazenar senhas em texto plano é uma péssima ideia

Armazenar senhas em formato de texto plano é altamente desaconselhado por várias razões de segurança por isso usar uma ferramenta como o `pass` (ou qualquer outro gerenciador de senhas seguro) é uma escolha mais segura.

<br/>

## O que é o pass e como usá-lo

O comando `pass` é uma ferramenta útil para gerenciar senhas no Linux. Ele permite que você armazene, gere e recupere senhas de forma segura. Aqui estão os passos básicos para usar o `pass`

<br/>

## Instalação

Primeiro, você precisa instalar o `pass` em seu sistema com o seguinte comando:

```
sudo apt-get install pass
```

<br/>

## Configuração

Antes de começar a usar o `pass`, você deve inicializar um repositório de senhas e associá-lo à sua chave GPG para garantir segurança. É recomendado utilizar seguinte comando para fazer isso:

```
pass init <sua-chave-gpg>
```

Se você ainda não possui uma chave GPG, você pode criar uma seguindo o tutorial sobre [como gerar uma chave GPG](/howto/gpg)

<br/>

## Armazenar senhas

Aqui está um exemplo passo a passo de como armazenar a senha de um e-mail fictício chamado meu@email.net:

```
pass insert <meu@email.net>
```

Isso irá criar uma entrada chamada meu@email.net no seu repositório de senhas. O `pass` irá solicitar que você insira a senha e a confirme.

<br/>

## Recuperar senhas
Se por algum momento você precisar recuperar a senha do e-mail, use o seguinte comando:

```
pass show <meu@email.net>
```

O `pass` irá decifrar a senha e mostrá-la na saída padrão.


> É importante lembrar que o exemplo mencionado anteriormente, o armazenamento de senhas de e-mails, é apenas um dos muitos casos possíveis. O comando `pass` é uma ótima opção se você deseja gerenciar senhas para diferentes tipos de serviços, abrangendo desde redes sociais até instituições bancárias e uma série de outros tipos de serviços.
{.is-warning}
