---
title: Adicionando uma chave GPG à sua conta do Salsa
description: 
published: true
date: 2025-03-01T17:29:57.644Z
tags: 
editor: markdown
dateCreated: 2023-06-11T18:55:32.615Z
---

## Sumário

- [Sobre](/howto/gpg/sobre)
- [Verificando as chaves GPG existentes](/howto/gpg/verificando-as-chaves-GPG-existentes)
- [Gerando uma nova chave](/howto/gpg/gerando-uma-nova-chave)
- [Adicionando uma chave GPG a sua conta do Salsa](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa)
- [Assine seus commits do Git](/howto/gpg/assine-seus-commits-do-git)

---

Para configurar sua conta no salsa.debian.org para usar sua nova (ou existente) chave GPG, você também precisará da chave de sua conta.

## Sobre a adição de chaves GPG à sua conta
Para assinar commits associados à sua conta no Salsa, você pode adicionar uma chave GPG pública à sua conta pessoal. Antes de adicionar uma chave, você deve [verificar as chaves existentes](/howto/gpg/verificando-as-chaves-GPG-existentes). Se você não encontrar nenhuma chave existente, poderá [gerar e copiar uma nova chave](/pt-br/howto/gpg/gerando-uma-nova-chave).

Você pode adicionar várias chaves públicas à sua conta no Salsa. Commits assinados por qualquer uma das chaves privadas correspondentes serão exibidos como verificados. Se você remover uma chave pública, quaisquer commits assinados pela chave privada correspondente não serão mais exibidos como verificados.

Para verificar o máximo possível de seus commits, você pode adicionar chaves expiradas e revogadas. Se a chave atender a todos os outros requisitos de verificação, as confirmações que foram assinadas anteriormente por qualquer uma das chaves privadas correspondentes serão exibidas como verificadas e indicarão que sua chave de assinatura expirou ou foi revogada.

Ao verificar uma assinatura, o Salsa extrai a assinatura e tenta analisar seu ID de chave. O ID da chave é então correspondido com as chaves adicionadas ao Salsa. Até que uma chave GPG correspondente seja adicionada ao Salsa, ele não poderá verificar suas assinaturas.

## Adicionando uma chave GPG
1. No canto superior direito de qualquer página, clique na foto do seu perfil e clique em `Preferências`.

![salsa-preferencias.png](/assets/images/salsa-preferencias.png)


2. Na barra lateral, clique em `Chaves GPG`.

3. No campo `Chave`, cole a chave GPG que você copiou quando gerou sua chave GPG .

4. Clique em Adicionar chave GPG.

## Revogar uma chave GPG

Se uma chave GPG for comprometida, revogue-a. A revogação de uma chave altera os commits futuros e passados:

- Commits anteriores assinados por esta chave são marcados como não verificados.
- Confirmações futuras assinadas por esta chave são marcadas como não verificadas.

Para revogar uma chave GPG:

1. No canto superior direito, selecione seu avatar.
2. Selecione Editar perfil .
3. Selecione Chaves GPG.
4. Selecione Revogar ao lado da chave GPG que você deseja excluir.

## Remover uma chave GPG

Quando você remove uma chave GPG da sua conta do Salsa (GitLab):

- Confirmações anteriores assinadas com esta chave permanecem verificadas.
- As confirmações futuras (incluindo quaisquer confirmações criadas, mas ainda não enviadas) que tentam usar essa chave não são verificadas.

Para remover uma chave GPG da sua conta:

1. No canto superior direito, selecione seu avatar.
2. Selecione Editar perfil .
3. Selecione `Chaves GPG`.
4. Selecione `Remover` ao lado da chave GPG que deseja excluir.

Se você precisar cancelar a verificação de commits futuros e passados, revogue a chave GPG associada.



