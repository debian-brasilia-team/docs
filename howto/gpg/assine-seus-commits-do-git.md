---
title: Assine seus commits do Git
description: 
published: true
date: 2025-03-01T17:30:00.185Z
tags: 
editor: markdown
dateCreated: 2023-06-19T17:23:43.473Z
---

## Sumário

- [Sobre](/howto/gpg/sobre)
- [Verificando as chaves GPG existentes](/howto/gpg/verificando-as-chaves-GPG-existentes)
- [Gerando uma nova chave](/howto/gpg/gerando-uma-nova-chave)
- [Adicionando uma chave GPG a sua conta do Salsa](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa)
- [Assine seus commits do Git](/howto/gpg/assine-seus-commits-do-git)

---

Depois de [adicionar sua chave pública à sua conta](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa) , você pode assinar commits individuais manualmente ou configurar o Git como padrão para commits assinados. Lembrando que é necessário ter o git instalado.

- Assine confirmações individuais do Git manualmente:
	1. Adicione -Ssinalizador a qualquer commit que você deseja assinar:
     ```bash
     git commit -S -m "My commit message"
     ```

	2. Digite a senha da sua chave GPG quando solicitado.
	3. Envie para o Salsa (GitLab) e verifique se seus commits foram verificados .

- Assine todos os commits do Git por padrão executando este comando:
```bash
git config --global commit.gpgsign true
```

## Definir chave de assinatura condicionalmente

Se você mantiver chaves de assinatura para fins separados, como trabalho e uso pessoal, use uma IncludeIfdeclaração em seu .gitconfigarquivo para definir com qual chave você assina os commits.

Pré-requisitos:
>  Requer Git versão 2.13 ou posterior.
{.is-info}


1. No mesmo diretório do `~/.gitconfig` arquivo principal, crie um segundo arquivo, como `.gitconfig-salsa`.
2. Em seu arquivo principal `~/.gitconfig`, adicione suas configurações do Git para trabalhar em projetos não Salsa (GitLab).
3. Anexe esta informação ao final do seu `~/.gitconfig` arquivo principal:

```bash
# O conteúdo deste arquivo está incluído apenas para URLs salsa.debian.org
[includeIf "hasconfig:remote.*.url:https://salsa.debian.org/**"]
  # Edite esta linha para apontar para seu arquivo de configuração alternativo
  path = ~/.gitconfig-salsa
```

4. Em seu `.gitconfig-salsa` arquivo alternativo, adicione as substituições de configuração a serem usadas ao confirmar em um repositório Salsa (GitLab). Todas as configurações de seu `~/.gitconfig` arquivo principal são retidas, a menos que você as substitua explicitamente. Neste exemplo,

```bash
# Alterne ~/.gitconfig-salsa file
# Esses valores são usados para repositórios correspondentes à string 'salsa.debian.org',
# e substituem seus valores correspondentes em ~/.gitconfig

[user]
  name = <Seu Nome>
  email = usuário@exemplo.com
  signingkey = <KEY ID>

[commit]
  gpgsign = true
```