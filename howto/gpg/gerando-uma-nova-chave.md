---
title: Gerando uma nova chave GPG
description: 
published: true
date: 2025-03-01T17:30:02.852Z
tags: 
editor: markdown
dateCreated: 2023-06-11T18:22:21.994Z
---

## Sumário

- [Sobre](/howto/gpg/sobre)
- [Verificando as chaves GPG existentes](/howto/gpg/verificando-as-chaves-GPG-existentes)
- [Gerando uma nova chave](/howto/gpg/gerando-uma-nova-chave)
- [Adicionando uma chave GPG a sua conta do Salsa](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa)
- [Assine seus commits do Git](/howto/gpg/assine-seus-commits-do-git)

---

1. Como existem várias versões do GPG, pode ser necessário consultar a página de manual relevante para encontrar o comando de geração de chave apropriado.
	```
	gpg --default-new-key-algo rsa4096 --gen-key
	```
2. Selecione o algoritmo que sua chave deve usar ou pressione `Enter` para selecionar a opção padrão, RSA and RSA.

3. Selecione o comprimento da chave, em bits. Recomendamos chaves de `4096 bits`.

4. Insira o período de tempo em que a chave deve ser válida. Pressione Enterpara especificar a seleção padrão, indicando que a chave não expira. A menos que você exija uma data de expiração, recomendamos aceitar esse padrão.

5. Verifique se suas seleções estão corretas.

6. Digite suas informações de ID de usuário.

	> Observação: quando solicitado a inserir seu endereço de e-mail, certifique-se de inserir o endereço de e-mail verificado para sua conta do Salsa.
{.is-info}


7. Digite uma senha segura, depois digite novamente para confirma-la.

8. Use o comando baixo para listar o formato longo das chaves GPG para as quais você possui uma chave pública e privada. Uma chave privada é necessária para assinar commits ou tags. No comando abaixo, no EMAIL susbtitua pelo seu e-mail cadastrado no Salsa, sem as <>.
	```
	gpg --list-secret-keys --keyid-format=long <EMAIL>
	```

	> Observação: algumas instalações do GPG no Linux podem exigir que você use `gpg2 --list-keys --keyid-format LONG` para visualizar uma lista de suas chaves existentes. Nesse caso, você também precisará configurar o Git para usar `gpg2` executando `git config --global gpg.program gpg2`.

9. Na lista de chaves GPG, copie o formato longo do ID da chave GPG que você gostaria de usar. Neste exemplo, o ID da chave GPG é `3AA5C34371567BD2`:
    ```
    gpg --list-secret-keys --keyid-format=long teste@example.com
    /home/teste/.gnupg/secring.gpg
    ------------------------------------
    sec   4096R/3AA5C34371567BD2 2016-03-10 [expires: 2025-06-11]
    uid                          Teste <teste@example.com>
    ssb   4096R/4BB6D45482678BE3 2013-06-11
    ```

10. Para mostrar a chave pública associada, execute este comando, substituindo `<ID>` pelo ID da chave GPG da etapa anterior:
    ```
    gpg --armor --export <ID>
    ```


11. Copie sua chave GPG, começando `-----BEGIN PGP PUBLIC KEY BLOCK-----` e terminando com `-----END PGP PUBLIC KEY BLOCK-----`.

12. [Adicione a chave GPG à sua conta do Salsa (GitLab)](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa).

## Enviar nova chave para o servidor de chaves

Para que outras pessoas consigam ter acesso a sua chave pública utilizamos uma rede de chaveiros que guardam chaves GPG. Para fazer upload de sua chave utilize o comando abaixo.

```bash
gpg --keyserver keyserver.ubuntu.com --send-key <ID>
```

Todos os chaveiros atualizam entre si mas esse processo pode demorar um pouco.

Depois de ter enviado sua chave pode tentar receber sua própria chave ou de outros por meio do comando. Único campo que necessário para receber uma chave é o ID

```bash
gpg --keyserver keyring.debian.org --recv-keys <ID>
```

Caso queira deixar configurado o keyserver em um aquivo de configuração salve dentro de ` ~/.gnupg/gpg.conf`

```
keyserver hkp://keyserver.ubuntu.com
# or
keyserver hkp://keyring.debian.org
```

Lembrando que o keyring do **Debian** permite apenas o **pull** de chaves, enquanto que o do **Ubuntu** permite **pull** e **push**.

