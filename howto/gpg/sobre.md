---
title: Sobre
description: 
published: true
date: 2025-03-01T17:30:08.222Z
tags: 
editor: markdown
dateCreated: 2023-06-11T18:18:01.588Z
---

## Sumário

- [Sobre](/howto/gpg/sobre)
- [Verificando as chaves GPG existentes](/howto/gpg/verificando-as-chaves-GPG-existentes)
- [Gerando uma nova chave](/howto/gpg/gerando-uma-nova-chave)
- [Adicionando uma chave GPG a sua conta do Salsa](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa)
- [Assine seus commits do Git](/howto/gpg/assine-seus-commits-do-git)

---

GPG significa `GNU Privacy Guard`. É uma implementação de código aberto do padrão de criptografia e descriptografia `Pretty Good Privacy (PGP)`. O `GPG` oferece privacidade `criptográfica` e `autenticação` para comunicação de dados. Permite aos usuários criptografar e assinar dados, garantindo confidencialidade e integridade.

O GPG utiliza um sistema de criptografia de chave pública em que cada usuário possui um par de chaves: uma `chave pública` e uma `chave privada`. A chave pública é compartilhada com outros e é usada para criptografar dados que somente o destinatário pretendido pode descriptografar com sua chave privada. A chave privada é mantida em segredo e é usada para descriptografar os dados criptografados e assinar mensagens para fornecer autenticação.

O `GPG` é amplamente utilizado para garantir a segurança de comunicações de `e-mail`, `criptografia de arquivos` e `assinaturas digitais`. Pode ser usado para verificar a autenticidade de arquivos baixados e criptografar informações sensíveis, garantindo que apenas partes autorizadas possam acessá-las. O GPG tornou-se uma ferramenta padrão para comunicação segura e está disponível para vários sistemas operacionais incluindo o Debian.

```
apt install gpg
```