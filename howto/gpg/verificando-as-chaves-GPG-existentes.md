---
title: Verificando as chaves GPG existentes
description: 
published: true
date: 2025-03-01T17:30:10.796Z
tags: 
editor: markdown
dateCreated: 2023-06-11T18:34:14.932Z
---

## Sumário

- [Sobre](/howto/gpg/sobre)
- [Verificando as chaves GPG existentes](/howto/gpg/verificando-as-chaves-GPG-existentes)
- [Gerando uma nova chave](/howto/gpg/gerando-uma-nova-chave)
- [Adicionando uma chave GPG a sua conta do Salsa](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa)
- [Assine seus commits do Git](/howto/gpg/assine-seus-commits-do-git)

---

Antes de gerar uma chave GPG, você pode verificar se possui alguma chave GPG existente.

1. Use o comando `gpg --list-secret-keys --keyid-format=long` para listar o formato longo das chaves GPG para as quais você possui uma chave `pública` e `privada`. Uma chave privada é necessária para assinar `commits` ou `tags`.
    ```
    gpg --list-secret-keys --keyid-format=long <EMAIL>
    ```
	> Observação: algumas instalações do GPG no Linux podem exigir que você use `gpg2 --list-keys --keyid-format LONG` para visualizar uma lista de suas chaves existentes. Nesse caso, você também precisará configurar o Git para usar `gpg2` executando `git config --global gpg.program gpg2`.
{.is-info}


2. Verifique a saída do comando para ver se você tem um par de chaves GPG.
	- Se não houver pares de chaves GPG ou se você não quiser usar nenhum que esteja disponível para assinar commits e tags, [gere uma nova chave GPG](/howto/gpg/gerando-uma-nova-chave).
	- Se houver um par de chaves GPG existente e você quiser usá-lo para assinar commits e tags, você pode exibir a chave pública usando o seguinte comando, substituindo o ID da chave GPG que você gostaria de usar. Neste exemplo, o ID da chave GPG é `3AA5C34371567BD2`:
      ```
      gpg --armor --export <ID>
      ```
	Você pode [adicionar sua chave GPG à sua conta do Salsa (GitLab)](/howto/gpg/adicionando-uma-chave-GPG-a-sua-conta-do-salsa).