<!---

Leia: https://wiki.debianbsb.org/pt-br/processo-de-contribuicao-com-pacotes

Remova abaixo o que não for necessário.

--->

**Link do Tracker**: https://tracker.debian.org/pkg/<nome_do_pacote>

**Link do Salsa**: https://salsa.debian.org/...

**Link da Merge Request (se houver)**: https://salsa.debian.org/...

**Bugs relacionados**:
- WNPP, se aplicável

**Link do lintian do pacote (se existir)**: https://udd.debian.org/lintian/?packages=<nome_do_pacote>
